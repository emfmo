/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.NonExistenceGroup;
import org.modelversioning.core.conditions.OptionGroup;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.ui.commons.OperationsUICommonsPlugin;

/**
 * {@link LabelProvider} for {@link OperationSpecification}s, {@link Template}s,
 * and {@link Condition}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationSpecificationLabelProvider extends LabelProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Image getImage(Object element) {
		if (element instanceof OperationSpecification) {
			return OperationsUICommonsPlugin
					.getImage(OperationsUICommonsPlugin.IMG_OPERATIONSPECIFICATION);
		} else if (element instanceof Template) {
			Template template = (Template) element;
			if (element instanceof RefinementTemplate) {
				return OperationsUICommonsPlugin
						.getImage(OperationsUICommonsPlugin.IMG_REFINEMENT_TEMPLATE);
			} else {
				if (!template.isExistence()) {
					return OperationsUICommonsPlugin
							.getImage(OperationsUICommonsPlugin.IMG_NEX_TEMPLATE);
				} else if (!template.isMandatory()) {
					return OperationsUICommonsPlugin
							.getImage(OperationsUICommonsPlugin.IMG_OPT_TEMPLATE);
				} else {
					return OperationsUICommonsPlugin
							.getImage(OperationsUICommonsPlugin.IMG_TEMPLATE);
				}
			}
		} else if (element instanceof OptionGroup) {
			return OperationsUICommonsPlugin
					.getImage(OperationsUICommonsPlugin.IMG_OPT_GROUP);
		} else if (element instanceof NonExistenceGroup) {
			return OperationsUICommonsPlugin
					.getImage(OperationsUICommonsPlugin.IMG_NEX_GROUP);
		} else {
			return super.getImage(element);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof OperationSpecification) {
			OperationSpecification operationSpecification = (OperationSpecification) element;
			return operationSpecification.getName();
		} else if (element instanceof Template) {
			Template template = (Template) element;
			if (element instanceof RefinementTemplate) {
				return template.getTitle()
						+ "->" //$NON-NLS-1$
						+ ((RefinementTemplate) template).getRefinedTemplate()
								.getTitle();
			}
			return template.getTitle();
		} else if (element instanceof OptionGroup) {
			OptionGroup optionGroup = (OptionGroup) element;
			return optionGroup.getName();
		} else if (element instanceof NonExistenceGroup) {
			NonExistenceGroup nonExistenceGroup = (NonExistenceGroup) element;
			return nonExistenceGroup.getName();
		} else {
			return super.getText(element);
		}
	}

}
