/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.provider;

import org.eclipse.jface.viewers.ViewerSorter;
import org.modelversioning.operations.OperationSpecification;

/**
 * {@link ViewerSorter} for {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationSpecificationViewerSorter extends ViewerSorter {

}
