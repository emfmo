/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.operations.OperationSpecification;

/**
 * Content provider for {@link OperationSpecification}s. The
 * {@link OperationSpecification}s to provide have to be set either using the
 * constructor or using the {@link #inputChanged(Viewer, Object, Object)}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationSpecificationContentProvider implements
		IStructuredContentProvider, ITreeContentProvider {

	/**
	 * Specified whether to provide {@link Template#isActive() inactive}
	 * templates.
	 */
	private boolean includeInActiveTemplates = false;

	/**
	 * Specified whether to provide {@link Template#isParameter() non-parameter}
	 * templates.
	 * 
	 * TODO Currently unused.
	 */
	@SuppressWarnings("unused")
	private boolean includeNonParameterTemplates = false;

	/**
	 * Specified whether to provide {@link Template#isExistence() non-existence}
	 * templates.
	 */
	private boolean includeNonExistenceTemplates = false;

	/**
	 * The {@link List} of {@link OperationSpecification}s to display.
	 */
	private Collection<OperationSpecification> operationSpecifications = new ArrayList<OperationSpecification>();

	/**
	 * Creates an empty provider with default configuration.
	 */
	public OperationSpecificationContentProvider() {
		super();
	}

	/**
	 * Constructor configuring providing options.
	 * 
	 * @param includeInActiveTemplates
	 *            whether to provide {@link Template#isActive() inactive}
	 *            templates.
	 * @param includeNonParameterTemplates
	 *            whether to provide {@link Template#isParameter()
	 *            non-parameter} templates.
	 * @param includeNonExistenceTemplates
	 *            whether to provide {@link Template#isExistence()
	 *            non-existence} templates.
	 */
	public OperationSpecificationContentProvider(
			boolean includeInActiveTemplates,
			boolean includeNonParameterTemplates,
			boolean includeNonExistenceTemplates) {
		super();
		this.includeInActiveTemplates = includeInActiveTemplates;
		this.includeNonParameterTemplates = includeNonParameterTemplates;
		this.includeNonExistenceTemplates = includeNonExistenceTemplates;
	}

	/**
	 * Constructor configuring providing options and specifying the
	 * {@link OperationSpecification OperationSpecifications} to provide.
	 * 
	 * @param includeInActiveTemplates
	 *            whether to provide {@link Template#isActive() inactive}
	 *            templates.
	 * @param includeNonParameterTemplates
	 *            whether to provide {@link Template#isParameter()
	 *            non-parameter} templates.
	 * @param includeNonExistenceTemplates
	 *            whether to provide {@link Template#isExistence()
	 *            non-existence} templates.
	 * @param operationSpecifications
	 *            to provide.
	 */
	public OperationSpecificationContentProvider(
			boolean includeInActiveTemplates,
			boolean includeNonParameterTemplates,
			boolean includeNonExistenceTemplates,
			Collection<OperationSpecification> operationSpecifications) {
		super();
		this.includeInActiveTemplates = includeInActiveTemplates;
		this.includeNonParameterTemplates = includeNonParameterTemplates;
		this.includeNonExistenceTemplates = includeNonExistenceTemplates;
		this.operationSpecifications = operationSpecifications;
	}

	/**
	 * Creates a new provider for the specified {@link OperationSpecification}s.
	 * 
	 * @param operationSpecifications
	 *            to provide.
	 */
	public OperationSpecificationContentProvider(
			Collection<OperationSpecification> operationSpecifications) {
		super();
		this.operationSpecifications.addAll(operationSpecifications);
	}

	/**
	 * Specified whether this provider should provide the specified
	 * <code>template</code>.
	 * 
	 * @param template
	 *            to check.
	 * @return <code>true</code> if it should provide, <code>false</code>
	 *         otherwise.
	 */
	private boolean shouldProvide(Template template) {
		// TODO regard {@link #includeNonParameterTemplates}
		if ((includeNonExistenceTemplates || !ConditionsUtil.isNonExistence(
				template, true))
				&& (includeInActiveTemplates || !ConditionsUtil.isInActive(
						template, true))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof OperationSpecification
				|| inputElement instanceof Template) {
			// if template or operation specification return children
			return getChildren(inputElement);
		} else if (inputElement instanceof EObject) {
			// if any eobject return contents
			return ((EObject) inputElement).eContents().toArray();
		} else {
			// else get all registered operations
			return operationSpecifications.toArray();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput instanceof Collection<?>) {
			this.operationSpecifications.clear();
			Collection<?> list = (Collection<?>) newInput;
			for (Object object : list) {
				if (object instanceof OperationSpecification) {
					this.operationSpecifications
							.add((OperationSpecification) object);
				}
			}

		}
		viewer.refresh();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof OperationSpecification) {
			OperationSpecification operationSpecification = (OperationSpecification) parentElement;
			Object[] array = new Object[1];
			array[0] = operationSpecification.getPreconditions()
					.getRootTemplate();
			return array;
		} else if (parentElement instanceof Template) {
			Template template = (Template) parentElement;
			List<Template> subTemplates = new ArrayList<Template>();
			for (Template subTemplate : template.getSubTemplates()) {
				if (shouldProvide(subTemplate)) {
					subTemplates.add(subTemplate);
				}
			}
			return subTemplates.toArray();
		}
		return new Object[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getParent(Object element) {
		if (element instanceof OperationSpecification) {
			return null;
		} else if (element instanceof Template) {
			Template template = (Template) element;
			if (template.getParentTemplate() != null) {
				return template.getParentTemplate();
			} else {
				for (OperationSpecification spec : operationSpecifications) {
					if (template.equals(spec.getPreconditions()
							.getRootTemplate())) {
						return spec;
					}
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof OperationSpecification) {
			OperationSpecification operationSpecification = (OperationSpecification) element;
			if (operationSpecification.getPreconditions().getRootTemplate() != null) {
				return true;
			}
		} else if (element instanceof Template) {
			Template template = (Template) element;
			return getChildren(template).length > 0;
		}
		return false;
	}

}
