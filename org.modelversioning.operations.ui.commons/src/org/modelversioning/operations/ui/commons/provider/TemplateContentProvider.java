/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Content provider for {@link Template}s. The {@link Template}s to provide have
 * to be set either using the constructor or using the
 * {@link #inputChanged(Viewer, Object, Object)} as a {@link Collection} of
 * {@link Template}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateContentProvider implements IStructuredContentProvider,
		ITreeContentProvider {

	/**
	 * Specified whether to provide {@link Template#isActive() inactive}
	 * templates.
	 */
	private boolean includeInActiveTemplates = false;

	/**
	 * Specified whether to provide {@link Template#isExistence() non-existence}
	 * templates.
	 */
	private boolean includeNonExistenceTemplates = false;

	/**
	 * the {@link Template}s to provide.
	 */
	private Collection<Template> templates = new ArrayList<Template>();

	/**
	 * Creates an empty provider.
	 */
	public TemplateContentProvider() {
		super();
	}

	/**
	 * Empty provider with specified providing options.
	 * 
	 * @param includeInActiveTemplates
	 *            whether to provide {@link Template#isActive() inactive}
	 *            templates.
	 * @param includeNonExistenceTemplates
	 *            whether to provide {@link Template#isExistence()
	 *            non-existence} templates.
	 */
	public TemplateContentProvider(boolean includeInActiveTemplates,
			boolean includeNonExistenceTemplates) {
		super();
		this.includeInActiveTemplates = includeInActiveTemplates;
		this.includeNonExistenceTemplates = includeNonExistenceTemplates;
	}

	/**
	 * Creates a provider for the specified {@link Template Templates}.
	 * 
	 * @param templates
	 *            templates to provide.
	 */
	public TemplateContentProvider(Collection<Template> templates) {
		super();
		setTemplates(templates);
	}

	/**
	 * Creates a provider for the specified {@link Template Templates} with
	 * specified providing options.
	 * 
	 * @param includeInActiveTemplates
	 *            whether to provide {@link Template#isActive() inactive}
	 *            templates.
	 * @param includeNonExistenceTemplates
	 *            whether to provide {@link Template#isExistence()
	 *            non-existence} templates.
	 * @param templates
	 *            templates to provide.
	 */
	public TemplateContentProvider(boolean includeInActiveTemplates,
			boolean includeNonExistenceTemplates, Collection<Template> templates) {
		super();
		this.includeInActiveTemplates = includeInActiveTemplates;
		this.includeNonExistenceTemplates = includeNonExistenceTemplates;
		setTemplates(templates);
	}

	/**
	 * Specified whether this provider should provide the specified
	 * <code>template</code>.
	 * 
	 * @param template
	 *            to check.
	 * @return <code>true</code> if it should provide, <code>false</code>
	 *         otherwise.
	 */
	private boolean shouldProvide(Template template) {
		if ((includeNonExistenceTemplates || !ConditionsUtil.isNonExistence(
				template, true))
				&& (includeInActiveTemplates || !ConditionsUtil.isInActive(
						template, true))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets the specified <code>templates</code>.
	 * 
	 * @param templates
	 *            templates to set.
	 */
	private void setTemplates(Collection<Template> templates) {
		this.templates.clear();
		for (Template template : templates) {
			if (shouldProvide(template)) {
				this.templates.add(template);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Template) {
			Template template = (Template) inputElement;
			List<Template> subTemplates = new ArrayList<Template>();
			for (Template subTemplate : template.getSubTemplates()) {
				if (shouldProvide(subTemplate)) {
					subTemplates.add(subTemplate);
				}
			}
			return subTemplates.toArray();
		} else {
			return this.templates.toArray();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput instanceof Collection<?>) {
			this.templates.clear();
			Collection<?> list = (Collection<?>) newInput;
			for (Object object : list) {
				if (object instanceof Template) {
					if (shouldProvide((Template) object)) {
						this.templates.add((Template) object);
					}
				}
			}

		} else if (newInput instanceof Template) {
			this.templates.clear();
			if (shouldProvide((Template) newInput)) {
				this.templates.add((Template) newInput);
			}
		}
		viewer.refresh();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof Template) {
			Template template = (Template) parentElement;
			List<Template> subTemplates = new ArrayList<Template>();
			for (Template subTemplate : template.getSubTemplates()) {
				if (shouldProvide(subTemplate)) {
					subTemplates.add(subTemplate);
				}
			}
			return subTemplates.toArray();
		} else if (parentElement instanceof Collection<?>) {
			return ((Collection<?>) parentElement).toArray();
		}
		return new Object[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getParent(Object element) {
		if (element instanceof Template) {
			Template template = (Template) element;
			if (template.getParentTemplate() != null) {
				return template.getParentTemplate();
			} else {
				return this.templates;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Template) {
			return getChildren(templates).length > 0;
		} else if (element instanceof Collection<?>) {
			return ((Collection<?>) element).size() > 0;
		}
		return false;
	}

}
