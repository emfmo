/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.operations.ui.commons.views;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.part.ViewPart;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.ui.commons.provider.OperationSpecificationLabelProvider;
import org.modelversioning.operations.ui.commons.provider.TemplateContentProvider;
import org.modelversioning.ui.commons.ITreeView;

/**
 * View showing the specified {@link Template} and its children.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class TemplateTreeView extends ViewPart implements ITreeView {

	/**
	 * Tree viewer of this view part.
	 */
	private TreeViewer viewer;
	/**
	 * The root object
	 */
	private Template rootTemplate;

	/**
	 * The constructor.
	 * 
	 * @param the
	 *            root {@link Template} to visualize.
	 */
	public TemplateTreeView(Template rootTemplate) {
		this.rootTemplate = rootTemplate;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.BORDER);
		viewer.setContentProvider(new TemplateContentProvider());
		viewer.setLabelProvider(new OperationSpecificationLabelProvider());
		Collection<Template> templates = new ArrayList<Template>();
		templates.add(rootTemplate);
		viewer.setInput(templates);
		viewer.expandAll();
	}

	/**
	 * Sets the root object to visualize.
	 * 
	 * @param eObject
	 *            object to visualize.
	 */
	public void setRootTemplate(Template rootTemplate) {
		this.rootTemplate = rootTemplate;
		viewer.setInput(rootTemplate.eContainer());
		viewer.refresh();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * Returns the tree contained by this view.
	 * 
	 * @return the tree.
	 */
	public Tree getTree() {
		return viewer.getTree();
	}

	/**
	 * Returns the currently selected {@link Template}s.
	 * 
	 * @return the currently selected {@link Template}s.
	 */
	public Template[] getSelectedTemplates() {
		TreeItem[] selection = getTree().getSelection();
		Template[] templates = new Template[selection.length];
		int i = 0;
		for (TreeItem treeItem : selection) {
			if (treeItem.getData() instanceof Template) {
				templates[i++] = (Template) treeItem.getData();
			}
		}
		return templates;
	}

	/**
	 * Sets the {@link Template} to select.
	 * 
	 * @param template
	 *            to select.
	 */
	public void setSelectedTemplate(Template template) {
		// set first to start
		getTree().setSelection(getTree().getItem(0));
		// search for tree item representing template
		for (TreeItem item : getTree().getItems()) {
			findToSelectedTemplate(template, item);
		}
	}

	/**
	 * Sets the selected template to the template containing the specified
	 * {@link Condition}.
	 * 
	 * @param condition
	 *            to set template.
	 */
	public void setSelectedTemplate(Condition condition) {
		setSelectedTemplate(condition.getTemplate());
	}

	private void findToSelectedTemplate(Template template, TreeItem item) {
		if (item.getData() == template) {
			getTree().setSelection(item);
		} else {
			for (TreeItem childItem : item.getItems()) {
				findToSelectedTemplate(template, childItem);
			}
		}
	}

	/**
	 * Redraws this component.
	 */
	public void layout() {
		viewer.getControl().redraw();
	}
}