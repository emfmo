/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionType;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Wizard for creating new {@link Condition Conditions}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NewConditionWizard extends Wizard {

	/**
	 * {@link ConditionsModel} to which a new condition has to be added.
	 */
	private ConditionsModel conditionsModel;

	/**
	 * The selection provided from outside.
	 */
	private IStructuredSelection selection = null;

	private TemplateSelectionPage templateSelectionPage;
	private static final String TEMPLATE_SELECTION_PAGE_NAME = "templateSelectionPage";

	private Condition condition;

	/**
	 * Constructing a new wizard for the specified <code>conditionsModel</code>.
	 * 
	 * @param conditionsModel
	 *            to which a new condition has to be added.
	 */
	public NewConditionWizard(ConditionsModel conditionsModel) {
		super();
		this.conditionsModel = conditionsModel;
		setWizardProperties();
	}

	/**
	 * Sets the wizard properties.
	 */
	private void setWizardProperties() {
		super.setWindowTitle("New Condition");
		setHelpAvailable(false);
	}

	/**
	 * Sets the user's selection.
	 * 
	 * @param selection
	 *            selection to set.
	 */
	public void setSelection(IStructuredSelection selection) {
		this.selection = selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Adds the pages necessary to collect all information to create a
	 * condition.
	 */
	@Override
	public void addPages() {
		templateSelectionPage = new TemplateSelectionPage(
				TEMPLATE_SELECTION_PAGE_NAME, conditionsModel, selection);
		templateSelectionPage.setTitle("Select Template");
		templateSelectionPage
				.setDescription("Select the template to which the "
						+ "condition has to be added.");
		addPage(templateSelectionPage);
	}

	/**
	 * Returns the currently selected template.
	 * 
	 * @return the currently selected template.
	 */
	protected Template getSelectedTemplate() {
		return templateSelectionPage.getSelectedTemplate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canFinish() {
		return this.templateSelectionPage.isPageComplete();
	}

	/**
	 * Returns the created condition.
	 * 
	 * @return the created condition.
	 */
	public Condition getCreatedCondition() {
		return condition;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performFinish() {
		condition = ConditionsFactory.eINSTANCE.createCustomCondition();
		condition.setActive(true);
		ConditionsUtil.setExpression(condition, "<TODO>"); //$NON-NLS-1$
		condition.setState(State.USER_DEFINED);
		condition.setType(ConditionType.LINGUISTIC);
		if (condition != null) {
			getSelectedTemplate().getSpecifications().add(condition);
			return true;
		} else {
			return false;
		}
	}

}
