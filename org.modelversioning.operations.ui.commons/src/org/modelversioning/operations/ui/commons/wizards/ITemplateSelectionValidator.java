/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.wizards;

import org.modelversioning.core.conditions.Template;

/**
 * Implementations of this interface return whether a selected {@link Template}
 * is valid for a specific selection.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ITemplateSelectionValidator {

	/**
	 * Returns whether the currently selected {@link Template} in
	 * <code>template</code> is a valid selection.
	 * 
	 * @param template
	 *            to validate.
	 * @return <code>true</code> if valid, otherwise <code>false</code>.
	 */
	public boolean isValidSelection(Template template);

	/**
	 * Returns a message for the currently selected {@link Template}. E.g., why
	 * the selected template is an invalid selection.
	 * 
	 * @param template
	 *            to get message for.
	 * @return the message.
	 */
	public String getMessage(Template template);

}
