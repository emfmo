/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.commons.wizards;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.ui.commons.views.TemplateTreeView;

/**
 * Wizard Page to select a single template.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateSelectionPage extends WizardPage {

	private ConditionsModel conditionsModel;
	private IStructuredSelection preSelection;
	private TemplateTreeView templateTree;
	private ITemplateSelectionValidator selectionValidator = null;

	/**
	 * Creates this wizard page.
	 * 
	 * @param pageName
	 *            page name.
	 * @param conditionsModel
	 *            conditions model from which the template may be selected.
	 * @param preSelection
	 *            pre selection (might be <code>null</code>).
	 */
	public TemplateSelectionPage(String pageName,
			ITemplateSelectionValidator selectionValidator,
			ConditionsModel conditionsModel, IStructuredSelection preSelection) {
		super(pageName);
		this.selectionValidator = selectionValidator;
		this.conditionsModel = conditionsModel;
		this.preSelection = preSelection;
	}

	/**
	 * Creates this wizard page.
	 * 
	 * @param pageName
	 *            page name.
	 * @param conditionsModel
	 *            conditions model from which the template may be selected.
	 * @param preSelection
	 *            pre selection (might be <code>null</code>).
	 */
	public TemplateSelectionPage(String pageName,
			ConditionsModel conditionsModel, IStructuredSelection preSelection) {
		super(pageName);
		this.conditionsModel = conditionsModel;
		this.preSelection = preSelection;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createControl(Composite parent) {
		// create overall container
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(createOverallLayout());
		setControl(container);
		templateTree = new TemplateTreeView(conditionsModel.getRootTemplate());
		templateTree.createPartControl(container);
		// preselect template
		if (preSelection != null && !preSelection.isEmpty()) {
			if (preSelection.getFirstElement() instanceof Template) {
				templateTree.setSelectedTemplate((Template) preSelection
						.getFirstElement());
			} else if (preSelection.getFirstElement() instanceof Condition) {
				templateTree.setSelectedTemplate((Condition) preSelection
						.getFirstElement());
			}
		}
		// link template selection validator
		templateTree.getTree().addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (selectionValidator != null
						&& e.item.getData() instanceof Template) {
					boolean isValid = selectionValidator
							.isValidSelection((Template) e.item.getData());
					setPageComplete(isValid);
					if (isValid) {
						setErrorMessage(null);
						setMessage(selectionValidator
								.getMessage((Template) e.item.getData()));
					} else {
						setErrorMessage(selectionValidator
								.getMessage((Template) e.item.getData()));
					}
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				if (selectionValidator != null
						&& e.item.getData() instanceof Template) {
					boolean isValid = selectionValidator
							.isValidSelection((Template) e.item.getData());
					setPageComplete(isValid);
					if (isValid) {
						setErrorMessage(null);
						setMessage(selectionValidator
								.getMessage((Template) e.item.getData()));
					} else {
						setErrorMessage(selectionValidator
								.getMessage((Template) e.item.getData()));
					}
				}
			}
		});
	}

	/**
	 * Returns the currently selected {@link Template}. If no {@link Template}
	 * is selected this method returns <code>null</code>.
	 * 
	 * @return the selected {@link Template} or <code>null</code>.
	 */
	public Template getSelectedTemplate() {
		if (templateTree.getSelectedTemplates().length > 0) {
			return templateTree.getSelectedTemplates()[0];
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPageComplete() {
		return true;
	}

	/**
	 * Creates the overall layout of this wizard page.
	 * 
	 * @return the overall layout.
	 */
	private Layout createOverallLayout() {
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.marginHeight = 5;
		fillLayout.marginWidth = 5;
		fillLayout.spacing = 1;
		return fillLayout;
	}

}
