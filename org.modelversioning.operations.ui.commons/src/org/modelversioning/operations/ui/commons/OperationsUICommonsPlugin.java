package org.modelversioning.operations.ui.commons;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class OperationsUICommonsPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.modelversioning.operations.ui.commons";

	// The shared instance
	private static OperationsUICommonsPlugin plugin;

	/**
	 * The icons path.
	 */
	private static final String iconPath = "icons/"; //$NON-NLS-1$

	/** The image key for operation specifications. */
	public static final String IMG_OPERATIONSPECIFICATION = "operationSpecification"; //$NON-NLS-1$
	/** The image key non-existence group. */
	public static final String IMG_NEX_GROUP = "nex_group"; //$NON-NLS-1$
	/** The image key non-existence template. */
	public static final String IMG_NEX_TEMPLATE = "nex_template"; //$NON-NLS-1$
	/** The image key option group. */
	public static final String IMG_OPT_GROUP = "opt_group"; //$NON-NLS-1$
	/** The image key optional template. */
	public static final String IMG_OPT_TEMPLATE = "opt_template"; //$NON-NLS-1$
	/** The image key refinement template. */
	public static final String IMG_REFINEMENT_TEMPLATE = "refined_template"; //$NON-NLS-1$
	/** The image key template. */
	public static final String IMG_TEMPLATE = "template"; //$NON-NLS-1$

	/**
	 * The constructor
	 */
	public OperationsUICommonsPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static OperationsUICommonsPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the SWT Shell of the active workbench window or <code>null</code>
	 * if no workbench window is active.
	 * 
	 * @return the SWT Shell of the active workbench window, or
	 *         <code>null</code> if no workbench window is active
	 */
	public static Shell getShell() {
		IWorkbenchWindow window = getActiveWorkbenchWindow();
		if (window == null)
			return null;
		return window.getShell();
	}

	/**
	 * Returns the active workbench window.
	 * 
	 * @return the active workbench window.
	 */
	public static IWorkbenchWindow getActiveWorkbenchWindow() {
		IWorkbench workbench = getActiveWorkbench();
		if (workbench == null)
			return null;
		return workbench.getActiveWorkbenchWindow();
	}

	/**
	 * Returns the active workbench.
	 * 
	 * @return the active workbench.
	 */
	public static IWorkbench getActiveWorkbench() {
		OperationsUICommonsPlugin plugin = getDefault();
		if (plugin == null)
			return null;
		return plugin.getWorkbench();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, IMG_OPERATIONSPECIFICATION,
				"operationSpecification_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_NEX_GROUP, "nex_group.gif"); //$NON-NLS-1$
		registerImage(registry, IMG_NEX_TEMPLATE, "nex_template.gif"); //$NON-NLS-1$
		registerImage(registry, IMG_OPT_GROUP, "opt_group.gif"); //$NON-NLS-1$
		registerImage(registry, IMG_OPT_TEMPLATE, "opt_template.gif"); //$NON-NLS-1$
		registerImage(registry, IMG_REFINEMENT_TEMPLATE, "refinement_template.gif"); //$NON-NLS-1$
		registerImage(registry, IMG_TEMPLATE, "template.gif"); //$NON-NLS-1$
	}

	/**
	 * Registers the specified fileName under the specified key to the specified
	 * registry.
	 * 
	 * @param registry
	 *            registry to register image.
	 * @param key
	 *            key of image.
	 * @param fileName
	 *            file name.
	 */
	private void registerImage(ImageRegistry registry, String key,
			String fileName) {
		try {
			IPath path = new Path(iconPath + fileName);
			URL url = FileLocator.find(getBundle(), path, null);
			if (url != null) {
				ImageDescriptor desc = ImageDescriptor.createFromURL(url);
				registry.put(key, desc);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Returns the image for the specified image name.
	 * 
	 * @param img
	 *            image name.
	 * @return the image.
	 */
	public static Image getImage(String key) {
		return getDefault().getImageRegistry().get(key);
	}

}
