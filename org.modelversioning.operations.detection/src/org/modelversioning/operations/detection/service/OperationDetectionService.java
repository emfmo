/**
 * <copyright>
 *
 * Copyright (c) 2011 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.service;

import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.detection.IOperationDetectionEngine;
import org.modelversioning.operations.detection.OperationDetectionPlugin;
import org.modelversioning.operations.detection.impl.OperationDetectionEngineImpl;
import org.modelversioning.operations.detection.impl.OperationOccurrenceWeaver;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.repository.ModelOperationRepositoryPlugin;

/**
 * This class offers services to find {@link OperationOccurrence occurrences} of
 * {@link OperationSpecification OperationSpecifications} in a
 * {@link ComparisonResourceSnapshot}.
 * 
 * <p>
 * By default, this service uses the {@link ModelOperationRepository} provided
 * in {@link ModelOperationRepositoryPlugin#getOperationRepository()}. If you
 * wish to use another one, set it using
 * {@link #setModelOperationRepository(ModelOperationRepository)}.
 * </p>
 * 
 * <p>
 * Also, by default, the {@link OperationDetectionEngineImpl} will be used for
 * detection occurrences. Another one may be
 * {@link #setConditionEvaluationEngine(IConditionEvaluationEngine) set} if
 * needed.
 * </p>
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationDetectionService {

	/** The model operation repositry to use. */
	private ModelOperationRepository modelOperationRepository = null;

	/** The condition evaluation engine to use. */
	private IConditionEvaluationEngine conditionEvaluationEngine = null;

	/** The operation detection engine to use. */
	private IOperationDetectionEngine operationDetectionEngine = null;

	/**
	 * Finds {@link OperationOccurrence occurrences} of
	 * {@link OperationSpecification OperationSpecifications} in the specified
	 * <code>comparisonResourceSnapshot</code> and, in case of occurrences,
	 * annotates them in the <code>comparisonResourceSnapshot</code>.
	 * 
	 * @param comparisonResourceSnapshot
	 *            to find and annotate {@link OperationOccurrence occurrences}.
	 */
	public void findAndAddOperationOccurrences(
			ComparisonResourceSnapshot comparisonResourceSnapshot) {
		try {
			IOperationDetectionEngine detectionEngine = getOperationDetectionEngine();
			detectionEngine
					.setOperationsRepository(getModelOperationRepository());
			OperationOccurrenceWeaver weaver = new OperationOccurrenceWeaver();
			Set<OperationOccurrence> occurrences = detectionEngine
					.findOccurrences(comparisonResourceSnapshot);
			for (OperationOccurrence occurrence : occurrences) {
				weaver.weaveOperationOccurrence(occurrence,
						comparisonResourceSnapshot);
			}
		} catch (UnsupportedConditionLanguage e) {
			IStatus status = new Status(IStatus.ERROR,
					OperationDetectionPlugin.PLUGIN_ID,
					"Unsupported condition language in "
							+ "registered operation specification.", e);
			OperationDetectionPlugin.getDefault().getLog().log(status);
		}
	}

	/**
	 * @return the currently used {@link ModelOperationRepository}.
	 */
	public ModelOperationRepository getModelOperationRepository() {
		if (modelOperationRepository == null) {
			modelOperationRepository = ModelOperationRepositoryPlugin
					.getDefault().getOperationRepository();
		}
		return modelOperationRepository;
	}

	/**
	 * @param modelOperationRepository
	 *            the {@link ModelOperationRepository} to set
	 */
	public void setModelOperationRepository(
			ModelOperationRepository modelOperationRepository) {
		this.modelOperationRepository = modelOperationRepository;
	}

	/**
	 * @return the currently used {@link IConditionEvaluationEngine}.
	 */
	public IConditionEvaluationEngine getConditionEvaluationEngine() {
		if (conditionEvaluationEngine == null) {
			conditionEvaluationEngine = new ConditionsEvaluationEngineImpl();
		}
		return conditionEvaluationEngine;
	}

	/**
	 * @param conditionEvaluationEngine
	 *            the {@link IConditionEvaluationEngine} to set
	 */
	public void setConditionEvaluationEngine(
			IConditionEvaluationEngine conditionEvaluationEngine) {
		this.conditionEvaluationEngine = conditionEvaluationEngine;
	}

	/**
	 * @return the currently used {@link IOperationDetectionEngine}
	 */
	public IOperationDetectionEngine getOperationDetectionEngine() {
		if (operationDetectionEngine == null) {
			operationDetectionEngine = new OperationDetectionEngineImpl(
					getConditionEvaluationEngine());
		}
		return operationDetectionEngine;
	}

	/**
	 * @param operationDetectionEngine
	 *            the {@link IOperationDetectionEngine} to set
	 */
	public void setOperationDetectionEngine(
			IOperationDetectionEngine operationDetectionEngine) {
		this.operationDetectionEngine = operationDetectionEngine;
	}

}
