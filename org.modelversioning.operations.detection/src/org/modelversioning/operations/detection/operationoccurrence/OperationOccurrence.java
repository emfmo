/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.operations.detection.operationoccurrence;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.AbstractDiffExtension;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;

import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;

import org.modelversioning.operations.OperationSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Occurrence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getPreConditionBinding <em>Pre Condition Binding</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperationId <em>Applied Operation Id</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getTitle <em>Title</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperationName <em>Applied Operation Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperation <em>Applied Operation</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getPostConditionBinding <em>Post Condition Binding</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getHiddenChanges <em>Hidden Changes</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getOrderHint <em>Order Hint</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence()
 * @model
 * @generated
 */
public interface OperationOccurrence extends DiffElement, AbstractDiffExtension {
	/**
	 * Returns the value of the '<em><b>Pre Condition Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Condition Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Condition Binding</em>' containment reference.
	 * @see #setPreConditionBinding(TemplateBindingCollection)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_PreConditionBinding()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TemplateBindingCollection getPreConditionBinding();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getPreConditionBinding <em>Pre Condition Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Condition Binding</em>' containment reference.
	 * @see #getPreConditionBinding()
	 * @generated
	 */
	void setPreConditionBinding(TemplateBindingCollection value);

	/**
	 * Returns the value of the '<em><b>Applied Operation Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied Operation Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Operation Id</em>' attribute.
	 * @see #setAppliedOperationId(String)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_AppliedOperationId()
	 * @model required="true"
	 * @generated
	 */
	String getAppliedOperationId();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperationId <em>Applied Operation Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Operation Id</em>' attribute.
	 * @see #getAppliedOperationId()
	 * @generated
	 */
	void setAppliedOperationId(String value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Applied Operation Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied Operation Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Operation Name</em>' attribute.
	 * @see #setAppliedOperationName(String)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_AppliedOperationName()
	 * @model required="true"
	 * @generated
	 */
	String getAppliedOperationName();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperationName <em>Applied Operation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Operation Name</em>' attribute.
	 * @see #getAppliedOperationName()
	 * @generated
	 */
	void setAppliedOperationName(String value);

	/**
	 * Returns the value of the '<em><b>Applied Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Operation</em>' reference.
	 * @see #setAppliedOperation(OperationSpecification)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_AppliedOperation()
	 * @model transient="true"
	 * @generated
	 */
	OperationSpecification getAppliedOperation();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getAppliedOperation <em>Applied Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Operation</em>' reference.
	 * @see #getAppliedOperation()
	 * @generated
	 */
	void setAppliedOperation(OperationSpecification value);

	/**
	 * Returns the value of the '<em><b>Post Condition Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post Condition Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post Condition Binding</em>' containment reference.
	 * @see #setPostConditionBinding(TemplateBindingCollection)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_PostConditionBinding()
	 * @model containment="true" required="true"
	 * @generated
	 */
	TemplateBindingCollection getPostConditionBinding();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getPostConditionBinding <em>Post Condition Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post Condition Binding</em>' containment reference.
	 * @see #getPostConditionBinding()
	 * @generated
	 */
	void setPostConditionBinding(TemplateBindingCollection value);

	/**
	 * Returns the value of the '<em><b>Hidden Changes</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.compare.diff.metamodel.DiffElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hidden Changes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hidden Changes</em>' reference list.
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_HiddenChanges()
	 * @model
	 * @generated
	 */
	EList<DiffElement> getHiddenChanges();

	/**
	 * Returns the value of the '<em><b>Order Hint</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order Hint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order Hint</em>' attribute.
	 * @see #setOrderHint(int)
	 * @see org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage#getOperationOccurrence_OrderHint()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getOrderHint();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence#getOrderHint <em>Order Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order Hint</em>' attribute.
	 * @see #getOrderHint()
	 * @generated
	 */
	void setOrderHint(int value);

} // OperationOccurrence
