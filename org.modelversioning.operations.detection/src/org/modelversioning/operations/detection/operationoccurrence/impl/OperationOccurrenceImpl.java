/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.operations.detection.operationoccurrence.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.merge.IMerger;
import org.eclipse.emf.compare.diff.metamodel.AbstractDiffExtension;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.diff.metamodel.DiffPackage;
import org.eclipse.emf.compare.diff.metamodel.impl.DiffElementImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;

import org.modelversioning.operations.OperationSpecification;

import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.detection.operationoccurrence.OperationoccurrencePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Occurrence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getHideElements <em>Hide Elements</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#isIsCollapsed <em>Is Collapsed</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getPreConditionBinding <em>Pre Condition Binding</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getAppliedOperationId <em>Applied Operation Id</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getAppliedOperationName <em>Applied Operation Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getAppliedOperation <em>Applied Operation</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getPostConditionBinding <em>Post Condition Binding</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getHiddenChanges <em>Hidden Changes</em>}</li>
 *   <li>{@link org.modelversioning.operations.detection.operationoccurrence.impl.OperationOccurrenceImpl#getOrderHint <em>Order Hint</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OperationOccurrenceImpl extends DiffElementImpl implements OperationOccurrence {
	/**
	 * The cached value of the '{@link #getHideElements() <em>Hide Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHideElements()
	 * @generated
	 * @ordered
	 */
	protected EList<DiffElement> hideElements;

	/**
	 * The default value of the '{@link #isIsCollapsed() <em>Is Collapsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCollapsed()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_COLLAPSED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsCollapsed() <em>Is Collapsed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsCollapsed()
	 * @generated
	 * @ordered
	 */
	protected boolean isCollapsed = IS_COLLAPSED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPreConditionBinding() <em>Pre Condition Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreConditionBinding()
	 * @generated
	 * @ordered
	 */
	protected TemplateBindingCollection preConditionBinding;

	/**
	 * The default value of the '{@link #getAppliedOperationId() <em>Applied Operation Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedOperationId()
	 * @generated
	 * @ordered
	 */
	protected static final String APPLIED_OPERATION_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAppliedOperationId() <em>Applied Operation Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedOperationId()
	 * @generated
	 * @ordered
	 */
	protected String appliedOperationId = APPLIED_OPERATION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAppliedOperationName() <em>Applied Operation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedOperationName()
	 * @generated
	 * @ordered
	 */
	protected static final String APPLIED_OPERATION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAppliedOperationName() <em>Applied Operation Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedOperationName()
	 * @generated
	 * @ordered
	 */
	protected String appliedOperationName = APPLIED_OPERATION_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAppliedOperation() <em>Applied Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppliedOperation()
	 * @generated
	 * @ordered
	 */
	protected OperationSpecification appliedOperation;

	/**
	 * The cached value of the '{@link #getPostConditionBinding() <em>Post Condition Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostConditionBinding()
	 * @generated
	 * @ordered
	 */
	protected TemplateBindingCollection postConditionBinding;

	/**
	 * The cached value of the '{@link #getHiddenChanges() <em>Hidden Changes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHiddenChanges()
	 * @generated
	 * @ordered
	 */
	protected EList<DiffElement> hiddenChanges;

	/**
	 * The default value of the '{@link #getOrderHint() <em>Order Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderHint()
	 * @generated
	 * @ordered
	 */
	protected static final int ORDER_HINT_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getOrderHint() <em>Order Hint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrderHint()
	 * @generated
	 * @ordered
	 */
	protected int orderHint = ORDER_HINT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationOccurrenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationoccurrencePackage.Literals.OPERATION_OCCURRENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiffElement> getHideElements() {
		if (hideElements == null) {
			hideElements = new EObjectWithInverseResolvingEList.ManyInverse<DiffElement>(DiffElement.class, this, OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS, DiffPackage.DIFF_ELEMENT__IS_HIDDEN_BY);
		}
		return hideElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsCollapsed() {
		return isCollapsed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsCollapsed(boolean newIsCollapsed) {
		boolean oldIsCollapsed = isCollapsed;
		isCollapsed = newIsCollapsed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED, oldIsCollapsed, isCollapsed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBindingCollection getPreConditionBinding() {
		return preConditionBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreConditionBinding(TemplateBindingCollection newPreConditionBinding, NotificationChain msgs) {
		TemplateBindingCollection oldPreConditionBinding = preConditionBinding;
		preConditionBinding = newPreConditionBinding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING, oldPreConditionBinding, newPreConditionBinding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreConditionBinding(TemplateBindingCollection newPreConditionBinding) {
		if (newPreConditionBinding != preConditionBinding) {
			NotificationChain msgs = null;
			if (preConditionBinding != null)
				msgs = ((InternalEObject)preConditionBinding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING, null, msgs);
			if (newPreConditionBinding != null)
				msgs = ((InternalEObject)newPreConditionBinding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING, null, msgs);
			msgs = basicSetPreConditionBinding(newPreConditionBinding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING, newPreConditionBinding, newPreConditionBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAppliedOperationId() {
		return appliedOperationId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedOperationId(String newAppliedOperationId) {
		String oldAppliedOperationId = appliedOperationId;
		appliedOperationId = newAppliedOperationId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_ID, oldAppliedOperationId, appliedOperationId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAppliedOperationName() {
		return appliedOperationName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedOperationName(String newAppliedOperationName) {
		String oldAppliedOperationName = appliedOperationName;
		appliedOperationName = newAppliedOperationName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME, oldAppliedOperationName, appliedOperationName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationSpecification getAppliedOperation() {
		if (appliedOperation != null && appliedOperation.eIsProxy()) {
			InternalEObject oldAppliedOperation = (InternalEObject)appliedOperation;
			appliedOperation = (OperationSpecification)eResolveProxy(oldAppliedOperation);
			if (appliedOperation != oldAppliedOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION, oldAppliedOperation, appliedOperation));
			}
		}
		return appliedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationSpecification basicGetAppliedOperation() {
		return appliedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedOperation(OperationSpecification newAppliedOperation) {
		OperationSpecification oldAppliedOperation = appliedOperation;
		appliedOperation = newAppliedOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION, oldAppliedOperation, appliedOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBindingCollection getPostConditionBinding() {
		return postConditionBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostConditionBinding(TemplateBindingCollection newPostConditionBinding, NotificationChain msgs) {
		TemplateBindingCollection oldPostConditionBinding = postConditionBinding;
		postConditionBinding = newPostConditionBinding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING, oldPostConditionBinding, newPostConditionBinding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostConditionBinding(TemplateBindingCollection newPostConditionBinding) {
		if (newPostConditionBinding != postConditionBinding) {
			NotificationChain msgs = null;
			if (postConditionBinding != null)
				msgs = ((InternalEObject)postConditionBinding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING, null, msgs);
			if (newPostConditionBinding != null)
				msgs = ((InternalEObject)newPostConditionBinding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING, null, msgs);
			msgs = basicSetPostConditionBinding(newPostConditionBinding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING, newPostConditionBinding, newPostConditionBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiffElement> getHiddenChanges() {
		if (hiddenChanges == null) {
			hiddenChanges = new EObjectResolvingEList<DiffElement>(DiffElement.class, this, OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDDEN_CHANGES);
		}
		return hiddenChanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOrderHint() {
		return orderHint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrderHint(int newOrderHint) {
		int oldOrderHint = orderHint;
		orderHint = newOrderHint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationoccurrencePackage.OPERATION_OCCURRENCE__ORDER_HINT, oldOrderHint, orderHint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void visit(DiffModel diffModel) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getImage() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IMerger provideMerger() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHideElements()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				return ((InternalEList<?>)getHideElements()).basicRemove(otherEnd, msgs);
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING:
				return basicSetPreConditionBinding(null, msgs);
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING:
				return basicSetPostConditionBinding(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				return getHideElements();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED:
				return isIsCollapsed();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING:
				return getPreConditionBinding();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_ID:
				return getAppliedOperationId();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__TITLE:
				return getTitle();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME:
				return getAppliedOperationName();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION:
				if (resolve) return getAppliedOperation();
				return basicGetAppliedOperation();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING:
				return getPostConditionBinding();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDDEN_CHANGES:
				return getHiddenChanges();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__ORDER_HINT:
				return getOrderHint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				getHideElements().clear();
				getHideElements().addAll((Collection<? extends DiffElement>)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED:
				setIsCollapsed((Boolean)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING:
				setPreConditionBinding((TemplateBindingCollection)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_ID:
				setAppliedOperationId((String)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__TITLE:
				setTitle((String)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME:
				setAppliedOperationName((String)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION:
				setAppliedOperation((OperationSpecification)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING:
				setPostConditionBinding((TemplateBindingCollection)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDDEN_CHANGES:
				getHiddenChanges().clear();
				getHiddenChanges().addAll((Collection<? extends DiffElement>)newValue);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__ORDER_HINT:
				setOrderHint((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				getHideElements().clear();
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED:
				setIsCollapsed(IS_COLLAPSED_EDEFAULT);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING:
				setPreConditionBinding((TemplateBindingCollection)null);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_ID:
				setAppliedOperationId(APPLIED_OPERATION_ID_EDEFAULT);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME:
				setAppliedOperationName(APPLIED_OPERATION_NAME_EDEFAULT);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION:
				setAppliedOperation((OperationSpecification)null);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING:
				setPostConditionBinding((TemplateBindingCollection)null);
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDDEN_CHANGES:
				getHiddenChanges().clear();
				return;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__ORDER_HINT:
				setOrderHint(ORDER_HINT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS:
				return hideElements != null && !hideElements.isEmpty();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED:
				return isCollapsed != IS_COLLAPSED_EDEFAULT;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__PRE_CONDITION_BINDING:
				return preConditionBinding != null;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_ID:
				return APPLIED_OPERATION_ID_EDEFAULT == null ? appliedOperationId != null : !APPLIED_OPERATION_ID_EDEFAULT.equals(appliedOperationId);
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION_NAME:
				return APPLIED_OPERATION_NAME_EDEFAULT == null ? appliedOperationName != null : !APPLIED_OPERATION_NAME_EDEFAULT.equals(appliedOperationName);
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__APPLIED_OPERATION:
				return appliedOperation != null;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__POST_CONDITION_BINDING:
				return postConditionBinding != null;
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDDEN_CHANGES:
				return hiddenChanges != null && !hiddenChanges.isEmpty();
			case OperationoccurrencePackage.OPERATION_OCCURRENCE__ORDER_HINT:
				return orderHint != ORDER_HINT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractDiffExtension.class) {
			switch (derivedFeatureID) {
				case OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS: return DiffPackage.ABSTRACT_DIFF_EXTENSION__HIDE_ELEMENTS;
				case OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED: return DiffPackage.ABSTRACT_DIFF_EXTENSION__IS_COLLAPSED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractDiffExtension.class) {
			switch (baseFeatureID) {
				case DiffPackage.ABSTRACT_DIFF_EXTENSION__HIDE_ELEMENTS: return OperationoccurrencePackage.OPERATION_OCCURRENCE__HIDE_ELEMENTS;
				case DiffPackage.ABSTRACT_DIFF_EXTENSION__IS_COLLAPSED: return OperationoccurrencePackage.OPERATION_OCCURRENCE__IS_COLLAPSED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isCollapsed: ");
		result.append(isCollapsed);
		result.append(", appliedOperationId: ");
		result.append(appliedOperationId);
		result.append(", title: ");
		result.append(title);
		result.append(", appliedOperationName: ");
		result.append(appliedOperationName);
		result.append(", orderHint: ");
		result.append(orderHint);
		result.append(')');
		return result.toString();
	}

} //OperationOccurrenceImpl
