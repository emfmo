/**
 * <copyright>
 *
 * Copyright (c) 2011 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;

/**
 * Weaves {@link OperationOccurrence operation occurrences} into a
 * {@link DiffModel}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationOccurrenceWeaver {

	/**
	 * Weaves the specified <code>occurrence</code> into the specified
	 * <code>comparisonSnapshot</code>.
	 * 
	 * @param occurrence
	 *            to weave in.
	 * @param comparisonSnapshot
	 *            into which the <code>occurrence</code> shall be weaved.
	 */
	public void weaveOperationOccurrence(OperationOccurrence occurrence,
			ComparisonResourceSnapshot comparisonSnapshot) {
		// find out most top change in all atomic changes
		EList<DiffElement> changes = occurrence.getHiddenChanges();
		DiffElement topLevelChange = null;
		int minParentCount = 0;
		for (DiffElement change : changes) {
			if (topLevelChange == null) {
				topLevelChange = change;
				minParentCount = org.modelversioning.core.util.EcoreUtil
						.createParentList(change).size();
			} else {
				int currentParentCount = org.modelversioning.core.util.EcoreUtil
						.createParentList(change).size();
				if (currentParentCount < minParentCount) {
					topLevelChange = change;
					minParentCount = currentParentCount;
				}
			}
		}

		// weave into diff model at the same parent
		if (topLevelChange != null) {
			EObject container = topLevelChange.eContainer();
			if (container instanceof DiffElement) {
				DiffElement containerDiff = (DiffElement) container;
				containerDiff.getSubDiffElements().add(occurrence);
			} else {
				addToRoot(occurrence, comparisonSnapshot);
			}
		} else {
			addToRoot(occurrence, comparisonSnapshot);
		}
	}

	private void addToRoot(OperationOccurrence occurrence,
			ComparisonResourceSnapshot comparisonSnapshot) {
		comparisonSnapshot.getDiff().getDifferences().add(occurrence);
	}

}
