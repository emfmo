/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import java.util.Comparator;

import org.modelversioning.core.diff.DiffSignature;
import org.modelversioning.operations.OperationSpecification;

/**
 * Compares {@link DiffSignature}s according to their diff sizes.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffSignatureSizeComparator implements
		Comparator<DiffSignature> {

	/**
	 * {@inheritDoc}
	 * 
	 * Compares two {@link OperationSpecification} according to its diff element
	 * size.
	 */
	@Override
	public int compare(DiffSignature o1, DiffSignature o2) {
		return (o1.getSignatureElements().size() - o2.getSignatureElements()
				.size())
				* -1;
	}

}
