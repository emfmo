/**
 * <copyright>
 *
 * Copyright (c) 2011 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.diff.propagation.IPropagationMappingProvider;
import org.modelversioning.core.diff.propagation.impl.DiffPropagationEngine;
import org.modelversioning.core.diff.service.DiffService;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;
import org.modelversioning.core.match.util.MatchUtil;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;

/**
 * Extends the {@link OperationDetectionEngineImpl} by iteratively keep
 * searching for {@link OperationOccurrence operation occurrences} based on
 * derived intermediate model states.
 * 
 * FIXME found {@link OperationOccurrence occurrences} do not link to the
 * correct diff elements (since potentially not existing due to intermediate
 * diff model).
 * 
 * FIXME we currently modify the specified resources!!!
 * 
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class IterativeOperationDetectionEngineImpl extends
		OperationDetectionEngineImpl implements IPropagationMappingProvider {

	private static final int MAX_ITERATION_DEPTH = 5;

	/**
	 * The resource set to use for creating intermediate resources.
	 */
	private ResourceSet resourceSet;
	/**
	 * The match service to use for matching intermediate models to final
	 * models.
	 */
	private MatchService matchService;
	/**
	 * The diff service to use for diffing intermediate models with final
	 * models.
	 */
	private DiffService diffService;

	/** The diff propagation engine to use for applying the found operations. */
	private DiffPropagationEngine diffPropagationEngine = new DiffPropagationEngine();

	/** The copier used for creating the intermediate models. */
	// private Copier copier;

	/** The intermediate origin resource. */
	private Resource originResource;

	/** The intermediate final resource. */
	private Resource finalVersionResource;

	private MatchModel matchModel;

	/**
	 * Returns the resource set to use for creating the intermediate resources.
	 * 
	 * @return the resource set to use.
	 */
	public ResourceSet getResourceSet() {
		return resourceSet;
	}

	/**
	 * Sets the resource set to use for creating the intermediate resources.
	 * 
	 * @param resourceSet
	 *            the resource set to use.
	 */
	public void setResourceSet(ResourceSet resourceSet) {
		this.resourceSet = resourceSet;
	}

	/**
	 * Returns the {@link MatchService} to use.
	 * 
	 * @return the {@link MatchService} to use.
	 */
	public MatchService getMatchService() {
		return matchService;
	}

	/**
	 * Sets the {@link MatchService} to use.
	 * 
	 * @param matchService
	 *            the {@link MatchService} to use.
	 */
	public void setMatchService(MatchService matchService) {
		this.matchService = matchService;
	}

	/**
	 * Returns the {@link DiffService} to use.
	 * 
	 * @return the {@link DiffService} to use.
	 */
	public DiffService getDiffService() {
		return diffService;
	}

	/**
	 * Sets the {@link DiffService} to use.
	 * 
	 * @param matchService
	 *            the {@link DiffService} to use.
	 */
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	/**
	 * Sets the origin resource.
	 * 
	 * @param originResource
	 *            to set.
	 */
	public void setOriginResource(Resource originResource) {
		this.originResource = originResource;
	}

	/**
	 * Sets the revised resource.
	 * 
	 * @param revisedResource
	 *            to set.
	 */
	public void setRevisedResource(Resource revisedResource) {
		this.finalVersionResource = revisedResource;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Additionally, this invokes the iterative detection process.
	 */
	@Override
	public Set<OperationOccurrence> findOccurrences(
			ComparisonResourceSnapshot comparisonSnapshot)
			throws UnsupportedConditionLanguage {

		diffPropagationEngine.setAddContainerOnly(true);

		matchModel = comparisonSnapshot.getMatch();

		// start recursively finding operation occurrences.
		Set<OperationOccurrence> foundOccurrences = new HashSet<OperationOccurrence>();
		foundOccurrences.addAll(doFindNextOccurrences(
				new HashSet<OperationOccurrence>(), comparisonSnapshot, 1));
		return foundOccurrences;
	}

	/**
	 * Finds operation occurrences by computing the intermediate states based on
	 * the specified <code>lastOccurrences</code>.
	 * 
	 * This method recursivley calls itself as long as further occurrences are
	 * found or until {@link #MAX_ITERATION_DEPTH} is reached.
	 * 
	 * @param lastOccurrences
	 *            to respect for computing the intermediate states.
	 * @param comparisonSnapshot
	 *            the last comparison resource snapshot to retrive the model
	 *            versions.
	 * @param iterationDepth
	 *            the current recursion depth.
	 * @return found occurrences.
	 */
	private Set<OperationOccurrence> doFindNextOccurrences(
			Set<OperationOccurrence> lastOccurrences,
			ComparisonResourceSnapshot comparisonSnapshot, int iterationDepth) {

		// execute changes contained by operation
		for (OperationOccurrence occurrence : lastOccurrences) {
			try {
				diffPropagationEngine.propagate(new HashSet<DiffElement>(
						occurrence.getHiddenChanges()), comparisonSnapshot
						.getMatch(), this, new NullProgressMonitor());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			// create new comparison resource snapshot between changed
			// intermediate version and final version.
			ComparisonResourceSnapshot intermediateCS = null;
			if (iterationDepth > 1) {
				intermediateCS = diff(getOriginResource(comparisonSnapshot),
						getRevisedResource(comparisonSnapshot));
			} else {
				intermediateCS = comparisonSnapshot;
			}

			// find occurrences
			Set<OperationOccurrence> foundOccurrences = super
					.findOccurrences(intermediateCS);
			for (OperationOccurrence oo : foundOccurrences) {
				oo.setOrderHint(iterationDepth);
			}

			if (foundOccurrences.size() > 0
					&& iterationDepth < MAX_ITERATION_DEPTH) {
				// recurse again
				foundOccurrences.addAll(doFindNextOccurrences(
						new HashSet<OperationOccurrence>(foundOccurrences),
						intermediateCS, iterationDepth + 1));
			}

			return foundOccurrences;

		} catch (MatchException e) {
			e.printStackTrace();
		} catch (UnsupportedConditionLanguage e) {
			e.printStackTrace();
		}

		return Collections.emptySet();
	}

	/**
	 * Returns the origin resource from the specified
	 * <code>comparisonSnapshot</code>.
	 * 
	 * @param comparisonSnapshot
	 *            to get origin resource from.
	 * @return the origin resource.
	 */
	private Resource getOriginResource(
			ComparisonResourceSnapshot comparisonSnapshot) {
		// return
		// comparisonSnapshot.getMatch().getLeftRoots().get(0).eResource();
		return originResource;
	}

	/**
	 * Returns the revised resource from the specified
	 * <code>comparisonSnapshot</code>.
	 * 
	 * @param comparisonSnapshot
	 *            to get revised resource from.
	 * @return the revised resource.
	 */
	private Resource getRevisedResource(
			ComparisonResourceSnapshot comparisonSnapshot) {
		// return
		// comparisonSnapshot.getMatch().getRightRoots().get(0).eResource();
		return finalVersionResource;
	}

	/**
	 * Computes a {@link ComparisonResourceSnapshot} for the specified
	 * resources.
	 * 
	 * @param originResource
	 *            the origin resource.
	 * @param revisedResource
	 *            the revised resource.
	 * @return the computed {@link ComparisonResourceSnapshot}.
	 * @throws MatchException
	 *             if matching fails.
	 */
	private ComparisonResourceSnapshot diff(Resource originResource,
			Resource revisedResource) throws MatchException {
		MatchModel matchModel = matchService.generateMatchModel(originResource,
				revisedResource);
		ComparisonResourceSnapshot intermediateCS = diffService
				.generateComparisonResourceSnapshot(matchModel);
		return intermediateCS;
	}

	/*
	 * Creates a copy of the specified <code>origin</code> resource preserving
	 * all IDs.
	 * 
	 * @param origin
	 *            the resource to create copy of.
	 * @return the created copy.
	 */
	// private Resource createResourceCopy(Resource origin) {
	// Resource resource = resourceSet.createResource(URI
	// .createFileURI("intermediate_resource_"
	// + EcoreUtil.generateUUID()));
	// Collection<EObject> modelRoots = copier.copyAll(origin.getContents());
	// resource.getContents().addAll(modelRoots);
	// for (Entry<EObject, EObject> entry : copier.entrySet()) {
	// UUIDUtil.copyUUID(entry.getKey(), entry.getValue());
	// }
	// copier.copyReferences();
	// return resource;
	// }

	@Override
	public Collection<EObject> getCounterpartObjects(EObject eObject) {
		if (eObject == null) {
			return Collections.emptySet();
		}
		Set<EObject> counterparts = new HashSet<EObject>();
		EObject counterpart = eObject;
		if (!isInOriginModel(eObject)) {
			counterpart = MatchUtil.getMatchingObject(eObject, matchModel);
		}
		counterparts.add(counterpart);
		return counterparts;
	}

	@Override
	public EObject getCounterpartObject(EObject eObject,
			DiffElement diffElement, EObject context) {
		if (eObject == null) {
			return null;
		}
		EObject counterpart = eObject;
		if (!isInOriginModel(eObject)) {
			counterpart = MatchUtil.getMatchingObject(eObject, matchModel);
		}
		return counterpart;
	}

	@Override
	public boolean isInOriginModel(EObject object) {
		if (object == null) {
			return false;
		}
		if (finalVersionResource == object.eResource()) {
			return false;
		} else {
			return true;
		}
	}

}
