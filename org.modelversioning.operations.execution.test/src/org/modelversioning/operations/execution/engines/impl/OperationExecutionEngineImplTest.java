/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.util.UUIDUtil;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.engines.IOperationBindingGenerator;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationExecutionEngineImplTest extends TestCase {
	
	private String singletonOperation = "models/ecore/convert_to_singleton/convert_to_singleton.operation";
	private String singletonValid1 = "models/ecore/convert_to_singleton/valid1.ecore";
	private String singletonInvalid1 = "models/ecore/convert_to_singleton/invalid1.ecore";
	private String encapsulateOperation = "models/ecore/encapsulate_field/encapsulate_field.operation";
	
	private String extractSuperClassOperation = "models/ecore/extract_super_class/extract_super_class.operation";
	private String extractSuperClassValid = "models/ecore/extract_super_class/valid.ecore";

	private ResourceSet resourceSet;
	private Resource singletonOperationResource;
	private Resource singletonValid1Resource;
	private Resource singletonInvalid1Resource;
	private Resource encapsulateOperationResource;
	private Resource extractSuperClassOperationResource;
	private Resource extractSuperClassValidResource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		// load resources
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());

		// load operations
		URI singletonOperationURI = URI.createURI(singletonOperation, true);
		singletonOperationResource = resourceSet.getResource(
				singletonOperationURI, true);
		URI encapsulateOperationURI = URI.createURI(encapsulateOperation, true);
		encapsulateOperationResource = resourceSet.getResource(
				encapsulateOperationURI, true);
		URI extractSuperClassURI = URI.createURI(extractSuperClassOperation,
				true);
		extractSuperClassOperationResource = resourceSet.getResource(
				extractSuperClassURI, true);

		// load samples
		URI singletonValid1URI = URI.createURI(singletonValid1, true);
		singletonValid1Resource = resourceSet.getResource(singletonValid1URI,
				true);
		URI singletonInvalid1URI = URI.createURI(singletonInvalid1, true);
		singletonInvalid1Resource = resourceSet.getResource(
				singletonInvalid1URI, true);
		URI extractSuperClassValidURI = URI.createURI(extractSuperClassValid,
				true);
		extractSuperClassValidResource = resourceSet.getResource(
				extractSuperClassValidURI, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		singletonOperationResource.unload();
		singletonInvalid1Resource.unload();
		singletonValid1Resource.unload();
		encapsulateOperationResource.unload();
		extractSuperClassOperationResource.unload();
		extractSuperClassValidResource.unload();
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.operations.execution.engines.impl.OperationExecutionEngineImpl#execute(org.modelversioning.operations.execution.IOperationBinding)}
	 * .
	 * @throws UnsupportedConditionLanguage 
	 * @throws IOException 
	 */
	public void testExecute() throws UnsupportedConditionLanguage, IOException {
		// create new generator
		IOperationBindingGenerator bindingGenerator = new OperationBindingGeneratorImpl();
		IOperationBinding validOperationBinding = null;
		
		// create new execution engine
		OperationExecutionEngineImpl execEngine = new OperationExecutionEngineImpl();

		// generate valid singleton binding with root
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) singletonOperationResource
						.getContents().get(0), singletonValid1Resource
						.getContents().get(0));
		
		// execute and test result
//		EObject eObject = execEngine.execute(validOperationBinding);
//		assertEquals(4, eObject.eContents().get(0).eContents().size());
//		boolean containsB = false;
//		boolean containsGetInstance = false;
//		for (EObject currentChild : eObject.eContents().get(0).eContents()) {
//			if (currentChild instanceof ENamedElement) {
//				ENamedElement namedElement = (ENamedElement) currentChild;
//				if ("B".equals(namedElement.getName())) {
//					containsB = true;
//				}
//				if ("getInstance".equals(namedElement.getName())) {
//					containsGetInstance = true;
//				}
//			}
//		}
//		assertFalse(containsB);
//		assertTrue(containsGetInstance);
		
		
		// generate valid extract superclass with two methods operation bindings
		ITemplateBinding prebinding = new TemplateBindingImpl();
		Template operationTemplate = ((OperationSpecification) extractSuperClassOperationResource
				.getContents().get(0)).getPreconditions().getRootTemplate()
				.getSubTemplates().get(0).getSubTemplates().get(0);
		prebinding.add(operationTemplate,
				extractSuperClassValidResource.getContents().get(0).eContents()
						.get(0).eContents().get(0));
		prebinding.add(operationTemplate,
				extractSuperClassValidResource.getContents().get(0).eContents()
						.get(0).eContents().get(2));
		
		// TODO testweise super-klasse binden
		Template superclassTemplate = ((OperationSpecification) extractSuperClassOperationResource
				.getContents().get(0)).getPreconditions().getRootTemplate()
				.getSubTemplates().get(1);
		prebinding.add(superclassTemplate,
				extractSuperClassValidResource.getContents().get(0).eContents()
						.get(2));
		
		
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) extractSuperClassOperationResource
						.getContents().get(0), prebinding);
		// set user input
		validOperationBinding.getUserInputValues().put(
				validOperationBinding.getMissingUserInputs().iterator().next(),
				"Vehicle");
		
		// execute extract super class
		EObject eObject = execEngine.execute(validOperationBinding);
		System.out.println(eObject);
		
		Resource resource = resourceSet.createResource(URI.createURI("models/ecore/extract_super_class/changed.ecore", true));
		UUIDUtil.removeUUIDs(eObject);
		resource.getContents().add(eObject);
		resource.save(null);
		// TODO test result
	}

}
