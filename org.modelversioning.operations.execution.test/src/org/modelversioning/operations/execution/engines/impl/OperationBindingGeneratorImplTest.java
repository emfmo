package org.modelversioning.operations.execution.engines.impl;

import java.util.Map;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.BindingException;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.engines.IOperationBindingGenerator;

public class OperationBindingGeneratorImplTest extends TestCase {

	private String singletonOperation = "models/ecore/convert_to_singleton/convert_to_singleton.operation";
	private String singletonValid1 = "models/ecore/convert_to_singleton/valid1.ecore";
	private String singletonInvalid1 = "models/ecore/convert_to_singleton/invalid1.ecore";
	private String encapsulateOperation = "models/ecore/encapsulate_field/encapsulate_field.operation";

	private String extractSuperClassOperation = "models/ecore/extract_super_class/extract_super_class.operation";
	private String extractSuperClassValid = "models/ecore/extract_super_class/valid.ecore";

	private ResourceSet resourceSet;
	private Resource singletonOperationResource;
	private Resource singletonValid1Resource;
	private Resource singletonInvalid1Resource;
	private Resource encapsulateOperationResource;
	private Resource extractSuperClassOperationResource;
	private Resource extractSuperClassValidResource;

	protected void setUp() throws Exception {
		super.setUp();

		// load resources
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());

		// load operations
		URI singletonOperationURI = URI.createURI(singletonOperation, true);
		singletonOperationResource = resourceSet.getResource(
				singletonOperationURI, true);
		URI encapsulateOperationURI = URI.createURI(encapsulateOperation, true);
		encapsulateOperationResource = resourceSet.getResource(
				encapsulateOperationURI, true);
		URI extractSuperClassURI = URI.createURI(extractSuperClassOperation,
				true);
		extractSuperClassOperationResource = resourceSet.getResource(
				extractSuperClassURI, true);

		// load samples
		URI singletonValid1URI = URI.createURI(singletonValid1, true);
		singletonValid1Resource = resourceSet.getResource(singletonValid1URI,
				true);
		URI singletonInvalid1URI = URI.createURI(singletonInvalid1, true);
		singletonInvalid1Resource = resourceSet.getResource(
				singletonInvalid1URI, true);
		URI extractSuperClassValidURI = URI.createURI(extractSuperClassValid,
				true);
		extractSuperClassValidResource = resourceSet.getResource(
				extractSuperClassValidURI, true);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		singletonOperationResource.unload();
		singletonInvalid1Resource.unload();
		singletonValid1Resource.unload();
		encapsulateOperationResource.unload();
		extractSuperClassOperationResource.unload();
		extractSuperClassValidResource.unload();
	}

	public void testGenerateOperationBindingOperationSpecificationEObject()
			throws UnsupportedConditionLanguage, BindingException {
		// create new generator
		IOperationBindingGenerator bindingGenerator = new OperationBindingGeneratorImpl();
		IOperationBinding validOperationBinding = null;

		// generate valid singleton binding with root
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) singletonOperationResource
						.getContents().get(0), singletonValid1Resource
						.getContents().get(0));

		assertTrue(validOperationBinding.getTemplateBinding().validate().isOK());

		assertEquals(validOperationBinding.getOperationSpecification(),
				(OperationSpecification) singletonOperationResource
						.getContents().get(0));
		assertEquals(validOperationBinding.getWorkingModelRoot(),
				singletonValid1Resource.getContents().get(0));

		assertTrue(validOperationBinding.isUnique());
	}

	public void testGenerateOperationBindingOperationSpecificationMapOfTemplateListOfEObject()
			throws UnsupportedConditionLanguage, BindingException {
		// create new generator
		IOperationBindingGenerator bindingGenerator = new OperationBindingGeneratorImpl();
		IOperationBinding validOperationBinding = null;

		// generate valid singleton binding with pre-bound class
		ITemplateBinding prebinding = new TemplateBindingImpl();
		// get class template and class
		Template classTemplate = ((OperationSpecification) singletonOperationResource
				.getContents().get(0)).getPreconditions().getRootTemplate()
				.getSubTemplates().get(0);
		EObject clazz = singletonValid1Resource.getContents().get(0)
				.eContents().get(0);
		prebinding.add(classTemplate, clazz);
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) singletonOperationResource
						.getContents().get(0), prebinding);

		assertEquals(validOperationBinding.getOperationSpecification(),
				(OperationSpecification) singletonOperationResource
						.getContents().get(0));
		assertEquals(validOperationBinding.getWorkingModelRoot(),
				singletonValid1Resource.getContents().get(0));

		assertTrue(validOperationBinding.getTemplateBinding().validate().isOK());
		assertTrue(validOperationBinding.isUnique());

		// generate invalid singleton binding with pre-bound class
		clazz = singletonInvalid1Resource.getContents().get(0).eContents().get(
				0);
		prebinding.add(classTemplate, clazz);
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) singletonOperationResource
						.getContents().get(0), prebinding);

		assertEquals(validOperationBinding.getOperationSpecification(),
				(OperationSpecification) singletonOperationResource
						.getContents().get(0));
		assertNull(validOperationBinding.getWorkingModelRoot());

		assertFalse(validOperationBinding.getTemplateBinding().validate()
				.isOK());
		assertTrue(validOperationBinding.isUnique());

		// generate valid extract superclass with two methods
		prebinding.clear();
		Template operationTemplate = ((OperationSpecification) extractSuperClassOperationResource
				.getContents().get(0)).getPreconditions().getRootTemplate()
				.getSubTemplates().get(0).getSubTemplates().get(0);
		prebinding.add(operationTemplate, extractSuperClassValidResource
				.getContents().get(0).eContents().get(0).eContents().get(0));
		prebinding.add(operationTemplate, extractSuperClassValidResource
				.getContents().get(0).eContents().get(0).eContents().get(2));
		validOperationBinding = bindingGenerator.generateOperationBinding(
				(OperationSpecification) extractSuperClassOperationResource
						.getContents().get(0), prebinding);

		assertTrue(validOperationBinding.isUnique());
		assertEquals(1, validOperationBinding.getMissingUserInputs().size());
		// set user input
		validOperationBinding.getUserInputValues().put(
				validOperationBinding.getMissingUserInputs().iterator().next(),
				"Vehicle");
		assertEquals(0, validOperationBinding.getMissingUserInputs().size());
	}

	/**
	 * Prints an Operation Binding to enable better debugging.
	 * 
	 * @param ob
	 *            Operation Binding to print.
	 * @param title
	 *            Title to help recognize the operaiton binding in console.
	 */
	protected void printOperationBinding(IOperationBinding ob, String title) {
		System.out.println("************** " + title + "("
				+ ob.getOperationSpecification().getName()
				+ ") *****************");
		System.out.println("Validity: " + ob.validate());
		System.out.println("Unique: " + ob.isUnique());
		System.out.println("==== User Inputs ====");
		System.out.println("== With Values ==");
		Map<UserInput, Object> userInputValues = ob.getUserInputValues();
		for (UserInput input : userInputValues.keySet()) {
			System.out.println(input + ":" + userInputValues.get(input));
		}
		System.out.println("== Missing ==");
		for (UserInput input : ob.getMissingUserInputs()) {
			System.out.println(input);
		}
		System.out.println("==== Bindings ====");
		for (ITemplateBinding map : ob.getTemplateBinding()
				.getAllPossibleBindings()) {
			System.out.println("== Binding ==");
			System.out.println("***********");
			Template rootTemplate = ob.getTemplateBinding().getRootTemplate();
			System.out.println(rootTemplate.getName() + ": ");
			for (EObject boundObject : map.getBoundObjects(rootTemplate)) {
				System.out.println("   " + boundObject);
			}
			TreeIterator<EObject> eAllContents = rootTemplate.eAllContents();
			while (eAllContents.hasNext()) {
				EObject nextChild = eAllContents.next();
				if (nextChild instanceof Template) {
					Template template = (Template) nextChild;
					System.out.println(template.getName() + ": ");
					if (map.getBoundObjects(template) != null) {
						for (EObject boundObject : map
								.getBoundObjects(template)) {
							System.out.println("   " + boundObject);
						}
					} else {
						System.out.println("   null");
					}
				}
			}
		}
	}

}
