/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.operations.repository;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.osgi.service.datalocation.Location;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.repository.impl.OperationRepositoryImpl;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class ModelOperationRepositoryPlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.modelversioning.operations.repository"; //$NON-NLS-1$

	private static final String OPERATION_SPECIFICATION_DIR = "/operation-specification"; //$NON-NLS-1$

	private static final String OPERATION_EXTENSION = ".operation"; //$NON-NLS-1$

	private static final String CANNOT_LOAD_OPERATIONS = "Error loading operation specifications into repository";

	private static final String CANNOT_SAVE_OPERATIONS = "Error persisting operation specifications in repository.";

	private final ResourceSet resourceSet = new ResourceSetImpl();

	/**
	 * The operation repository.
	 */
	private final OperationRepositoryImpl operationRepository = new OperationRepositoryImpl();

	// The shared instance
	private static ModelOperationRepositoryPlugin plugin;

	/**
	 * The constructor
	 */
	public ModelOperationRepositoryPlugin() {
	}

	/**
	 * {@inheritDoc}
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		loadOperationSpecifications();
	}

	/**
	 * {@inheritDoc}
	 */
	public void stop(BundleContext context) throws Exception {
		persistOperationSpecifications();
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the file path for persisting {@link OperationSpecification}s.
	 * 
	 * @return file path for persisting {@link OperationSpecification}s.
	 */
	private File getOperationSpecificationFilePath() {
		Location location = Platform.getConfigurationLocation();
		if (location != null) {
			URL configURL = location.getURL();
			if (configURL != null && configURL.getProtocol().startsWith("file")) { //$NON-NLS-1$
				return new File(configURL.getFile(), PLUGIN_ID
						+ OPERATION_SPECIFICATION_DIR);
			}
		}
		return getStateLocation().append(OPERATION_SPECIFICATION_DIR).toFile();
	}

	/**
	 * Loads the persisted {@link OperationSpecification}s into the repository.
	 */
	private void loadOperationSpecifications() {
		try {
			for (File file : getOperationSpecificationFilePath().getParentFile()
					.listFiles()) {
				Resource resource = resourceSet.getResource(URI
						.createFileURI(file.getAbsolutePath()), true);
				if (resource != null && resource.getContents() != null
						&& resource.getContents().size() > 0) {
					for (EObject eObject : resource.getContents()) {
						if (eObject instanceof OperationSpecification) {
							getOperationRepository().register(
									(OperationSpecification) eObject);
						}
					}
				}
			}
		} catch (Exception e) {
			getLog().log(
					new Status(IStatus.ERROR, PLUGIN_ID,
							CANNOT_LOAD_OPERATIONS, e));
		}
	}

	/**
	 * Persists currently registered {@link OperationSpecification}s.
	 */
	private void persistOperationSpecifications() {
		try {
			// delete all existing files
			if (getOperationSpecificationFilePath().getParentFile().listFiles() != null) {
				for (File file : getOperationSpecificationFilePath()
						.getParentFile().listFiles()) {
					file.delete();
				}
			}

			// save all operation specifications
			int i = 1;
			for (OperationSpecification spec : getOperationRepository()
					.getRegisteredOperationSpecifications()) {
				Resource resource = resourceSet.createResource(URI
						.createFileURI(getOperationSpecificationFilePath()
								.getPath()
								+ i + OPERATION_EXTENSION));
				try {
					resource.getContents().add(spec);
					resource.save(null);
				} catch (IOException e) {
					this.getLog().log(
							new Status(IStatus.ERROR, PLUGIN_ID,
									CANNOT_SAVE_OPERATIONS, e));
				}
				i++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static ModelOperationRepositoryPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the {@link ModelOperationRepository}.
	 * 
	 * @return the {@link ModelOperationRepository}.
	 */
	public ModelOperationRepository getOperationRepository() {
		return this.operationRepository;
	}

}
