/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.repository;

import java.util.Collection;

import org.modelversioning.operations.OperationSpecification;

/**
 * Repository which allows to register and unregister
 * {@link OperationSpecification}s. Registered {@link OperationSpecification}s
 * may be listed, searched and retrieved.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ModelOperationRepository {

	/**
	 * Registers the specified <code>operationSpecification</code> in the
	 * repository.
	 * 
	 * @param operationSpecification
	 *            {@link OperationSpecification} to register.
	 */
	public void register(OperationSpecification operationSpecification);

	/**
	 * Unregisters the specified <code>operationSpecification</code>. This will
	 * remove the <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            {@link OperationSpecification} to unregister.
	 */
	public void unregister(OperationSpecification operationSpecification);

	/**
	 * Returns a {@link Collection} of all languages for which an
	 * {@link OperationSpecification} is registered.
	 * 
	 * @return registered languages.
	 */
	public Collection<String> getRegisteredLanguages();

	/**
	 * Returns a not changable {@link Collection} of all registered
	 * {@link OperationSpecification}s.
	 * 
	 * @return all registered {@link OperationSpecification}s
	 */
	public Collection<OperationSpecification> getRegisteredOperationSpecifications();

	/**
	 * Returns a not changable {@link Collection} of all registered
	 * {@link OperationSpecification}s for the specified <code>extension</code>.
	 * 
	 * @param extension
	 *            extension used to filter registered
	 *            {@link OperationSpecification}.
	 * @return all registered {@link OperationSpecification}s for the specified
	 *         <code>extension</code>
	 */
	public Collection<OperationSpecification> getRegisteredOperationSpecifications(
			String extension);

	/**
	 * Adds a the specified <code>listener</code>.
	 * 
	 * @param listener
	 *            the {@link IModelOperationRepositoryListener} to add.
	 */
	public void addModelOperationListener(
			IModelOperationRepositoryListener listener);

	/**
	 * Removes the specified <code>listener</code>.
	 * 
	 * @param listener
	 *            the {@link IModelOperationRepositoryListener} to remove.
	 */
	public void removeModelOperationListener(
			IModelOperationRepositoryListener listener);

}
