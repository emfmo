/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.repository;

import org.modelversioning.operations.OperationSpecification;

/**
 * Interface which allows implementing classes to get notified when the
 * {@link ModelOperationRepository} changes its contained
 * {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IModelOperationRepositoryListener {

	/**
	 * Notifies the register event of the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            registered {@link OperationSpecification}.
	 */
	public void registerAction(OperationSpecification operationSpecification);

	/**
	 * Notifies the unregister event of the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            unregistered {@link OperationSpecification}.
	 */
	public void unregisterAction(OperationSpecification operationSpecification);

}
