/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.repository.IModelOperationRepositoryListener;
import org.modelversioning.operations.repository.ModelOperationRepository;

/**
 * {@inheritDoc}
 * 
 * Implements the {@link ModelOperationRepository} interface using a
 * {@link HashMap}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationRepositoryImpl implements ModelOperationRepository {

	/**
	 * The map saving all operation specifications.
	 */
	private Map<String, Set<OperationSpecification>> operationSpecifications = new HashMap<String, Set<OperationSpecification>>();

	/**
	 * The listeners.
	 */
	private Collection<IModelOperationRepositoryListener> listeners = new ArrayList<IModelOperationRepositoryListener>();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<OperationSpecification> getRegisteredOperationSpecifications() {
		Collection<OperationSpecification> all = new HashSet<OperationSpecification>();
		for (String ext : operationSpecifications.keySet()) {
			all.addAll(operationSpecifications.get(ext));
		}
		return Collections.unmodifiableCollection(all);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<OperationSpecification> getRegisteredOperationSpecifications(
			String extension) {
		if (operationSpecifications.get(extension) == null) {
			return Collections.emptySet();
		}
		return Collections.unmodifiableCollection(operationSpecifications
				.get(extension));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void register(OperationSpecification operationSpecification) {
		String ext = operationSpecification.getModelingLanguage();
		if (operationSpecifications.get(ext) == null) {
			this.operationSpecifications.put(ext,
					new HashSet<OperationSpecification>());
		}
		this.operationSpecifications.get(ext).add(operationSpecification);
		fireRegisterEvent(operationSpecification);
	}

	/**
	 * Notifies all listeners.
	 * 
	 * @param operationSpecification
	 *            added {@link OperationSpecification}.
	 */
	private void fireRegisterEvent(OperationSpecification operationSpecification) {
		for (IModelOperationRepositoryListener listener : this.listeners) {
			listener.registerAction(operationSpecification);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unregister(OperationSpecification operationSpecification) {
		String ext = operationSpecification.getModelingLanguage();
		if (operationSpecifications.get(ext) != null) {
			this.operationSpecifications.get(ext)
					.remove(operationSpecification);
		}
		fireUnregisterEvent(operationSpecification);
	}

	/**
	 * Notifies all listeners.
	 * 
	 * @param operationSpecification
	 *            removed {@link OperationSpecification}.
	 */
	private void fireUnregisterEvent(
			OperationSpecification operationSpecification) {
		for (IModelOperationRepositoryListener listener : this.listeners) {
			listener.unregisterAction(operationSpecification);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addModelOperationListener(
			IModelOperationRepositoryListener listener) {
		this.listeners.add(listener);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeModelOperationListener(
			IModelOperationRepositoryListener listener) {
		this.listeners.remove(listener);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<String> getRegisteredLanguages() {
		Set<String> keySet = operationSpecifications.keySet();
		keySet.remove("");
		return keySet;
	}

}
