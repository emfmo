/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.core.test.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.util.UUIDUtil;

public class UUIDResourceFactoryImplTest extends TestCase {

	public void testCreateResourceURI() throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		
		// register our uuid resource factory
		resourceSet.getResourceFactoryRegistry()
			.getExtensionToFactoryMap()
			.put("ecore", new org.modelversioning.core.impl.UUIDResourceFactoryImpl());
		URI fileURI = URI.createFileURI("models/origin.ecore");
		Resource origin = resourceSet.getResource(fileURI, true);
		
		// change a value
		((EPackage)origin.getContents().get(0)).setName("changed");
		
		// save resource back again
		origin.save(null);
		
		// reload resource
		origin = resourceSet.getResource(fileURI, true);
		
		// check if previous change was saved
		assertEquals(((EPackage)origin.getContents().get(0)).getName(), "changed");
		
		// check for ids
		String uuid = UUIDUtil.getUUID(origin.getContents().get(0));
		assertEquals(UUIDUtil.getObject(origin, uuid), origin.getContents().get(0));
		
		origin.unload();
	}

}
