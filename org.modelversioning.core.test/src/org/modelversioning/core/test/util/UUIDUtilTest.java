/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.test.util;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.util.UUIDUtil;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UUIDUtilTest extends TestCase {

	/**
	 * Test method for
	 * {@link org.modelversioning.core.util.UUIDUtil#getUUID(org.eclipse.emf.ecore.EObject)}
	 * .
	 * @throws IOException 
	 */
	public void testGetUUID() throws IOException {
		// Normally load model without serialized IDs
		ResourceSet resourceSet = new ResourceSetImpl();

		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new EcoreResourceFactoryImpl());

		URI fileURI = URI.createFileURI("models/origin_without_ids.ecore");
		Resource origin_without_ids = resourceSet.getResource(fileURI, true);
		
		// UUIDs should be null
		TreeIterator<EObject> iter1 = origin_without_ids.getAllContents();
		while(iter1.hasNext()) {
			assertNull(UUIDUtil.getUUID(iter1.next()));
		}
		
		// reload same file with UUID Resource factory
		ResourceSet resourceSetUUID = new ResourceSetImpl();
		resourceSetUUID.getResourceFactoryRegistry().getExtensionToFactoryMap()
		.put("ecore", new UUIDResourceFactoryImpl());
		
		Resource origin_without_ids_UUID = resourceSetUUID.getResource(fileURI, true);
		
		// then there should be UUIDs
		iter1 = origin_without_ids_UUID.getAllContents();
		while(iter1.hasNext()) {
			EObject eObject = iter1.next();
			if (eObject instanceof EModelElement) {
				assertNotNull(UUIDUtil.getUUID(eObject));
			}
		}
		
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.core.util.UUIDUtil#getObject(org.eclipse.emf.ecore.resource.Resource, java.lang.String)}
	 * .
	 */
	public void testGetObject() {
		ResourceSet resourceSetUUID = new ResourceSetImpl();
		resourceSetUUID.getResourceFactoryRegistry().getExtensionToFactoryMap()
		.put("ecore", new UUIDResourceFactoryImpl());
		
		URI fileURI = URI.createFileURI("models/origin_without_ids.ecore");
		Resource origin_without_ids_UUID = resourceSetUUID.getResource(fileURI, true);
		
		// then there should be UUIDs
		TreeIterator<EObject> iter1 = origin_without_ids_UUID.getAllContents();
		while(iter1.hasNext()) {
			EObject eObject = iter1.next();
			assertEquals(UUIDUtil.getObject(origin_without_ids_UUID, UUIDUtil.getUUID(eObject)), eObject);
		}
	}

}
