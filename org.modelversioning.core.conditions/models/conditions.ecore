<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0"
    xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="conditions"
    nsURI="http://modelversioning.org/core/conditions/metamodel/1.0" nsPrefix="conditions">
  <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
    <details key="documentation" value="This model represents symbols and their conditions as well as their relation ship to each other."/>
  </eAnnotations>
  <eClassifiers xsi:type="ecore:EClass" name="Template">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="A Template is a role of a model element in a concrete situation specified by a name and some conditions. A template may include sub templates (e.g. child elements like containments).&#xA;&#xA;For example, a template is the constructor set to private within a &quot;Convert to Singleton&quot; operation. Or any public attribute that is set to private and for which a getter and setter method is generated within the &quot;Generate Getter and Setter&quot; operation."/>
    </eAnnotations>
    <eOperations name="isMandatory" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eOperations name="isExistence" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" lowerBound="1" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="The name for technical representation."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="subTemplates" upperBound="-1"
        eType="#//Template" containment="true" eOpposite="#//Template/parentTemplate"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="specifications" upperBound="-1"
        eType="#//Condition" containment="true" eOpposite="#//Condition/template">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specifies a Template using Conditions."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="state" lowerBound="1" eType="#//State"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="title" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"
        defaultValueLiteral="">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="The human readable title."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="representative" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="A reference to the representing object in a model."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="model" lowerBound="1" eType="#//Model">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specifies the model type this symbol belongs to."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parentTemplate" eType="#//Template"
        eOpposite="#//Template/subTemplates">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="The parent symbol."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parentsFeature" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EStructuralFeature"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="containingModel" eType="#//ConditionsModel"
        eOpposite="#//ConditionsModel/rootTemplate"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="active" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean" defaultValueLiteral="true"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="parameter" lowerBound="1"
        eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"
        defaultValueLiteral="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specified whether this template is a parameter."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="optionGroups" upperBound="-1"
        eType="#//OptionGroup" eOpposite="#//OptionGroup/templates"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="nonExistenceGroups" upperBound="-1"
        eType="#//NonExistenceGroup" eOpposite="#//NonExistenceGroup/templates"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Condition" abstract="true">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="A Condition can be &lt;code>true&lt;/code> or &lt;code>false&lt;/code> for a specific EObject to evaluate. It specifies a template."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="type" lowerBound="1" eType="#//ConditionType">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specifies the type of this condition. E.g. the condition is ontological or linguistic."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="state" lowerBound="1" eType="#//State"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="active" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean" defaultValueLiteral="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="template" lowerBound="1"
        eType="#//Template" changeable="false" unsettable="true" eOpposite="#//Template/specifications"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="involvesTemplate" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"
        transient="true" defaultValueLiteral="true">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specifies whether this condition involves (e.g. references or uses) a {@link Template}."/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="ConditionType">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Specifies the type of a &lt;code>Condition&lt;/code>. E.g. a condition like &lt;code>name&lt;/code> is linguistic."/>
    </eAnnotations>
    <eLiterals name="ONTOLOGICAL"/>
    <eLiterals name="LINGUISTIC" value="1" literal="LINGUISTIC"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="State">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Specifies the state of a &lt;code>Symbol&lt;/code> or &lt;code>Condition&lt;/code>. E.g. the symbol is generated or user defined."/>
    </eAnnotations>
    <eLiterals name="GENERATED"/>
    <eLiterals name="EDITED" value="1"/>
    <eLiterals name="USER_DEFINED" value="2"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="FeatureCondition" eSuperTypes="#//Condition">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="A condition for a specific &lt;code>EStructuralFeature&lt;/code>."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="feature" lowerBound="1"
        eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EStructuralFeature"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="expression" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Defines one or more conditions represented by the &lt;code>expressions&lt;/code> list."/>
      </eAnnotations>
    </eStructuralFeatures>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="AttributeCorrespondenceCondition" eSuperTypes="#//FeatureCondition">
    <eStructuralFeatures xsi:type="ecore:EReference" name="correspondingTemplates"
        upperBound="-1" eType="#//Template"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="correspondingFeatures"
        upperBound="-1" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EStructuralFeature"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="CustomCondition" eSuperTypes="#//Condition">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="A custom (user defined) condition."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="expression" lowerBound="1"
        eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="Model">
    <eLiterals name="ORIGIN"/>
    <eLiterals name="WORKING" value="1"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ConditionsModel">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Container for a model of conditions."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="rootTemplate" lowerBound="1"
        eType="#//Template" containment="true" eOpposite="#//Template/containingModel"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="createdAt" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EDate"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="modelName" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="language" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"
        defaultValueLiteral="OCL">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Specifies the language in which conditions are represented."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="optionGroups" upperBound="-1"
        eType="#//OptionGroup" containment="true"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="nonExistenceGroups" upperBound="-1"
        eType="#//NonExistenceGroup" containment="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="EvaluationResult">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="The result of a condition evaluation."/>
    </eAnnotations>
    <eOperations name="isMultiResult" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eOperations name="isOK" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EBoolean"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="message" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="A human readable message."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="status" eType="#//EvaluationStatus">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Status of this evaluation result."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="exception" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EJavaObject">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Only set if there occured an error."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="evaluator" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="The id of the implementation that executed the evaluation."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="subResults" upperBound="-1"
        eType="#//EvaluationResult" containment="true" eOpposite="#//EvaluationResult/parentResult">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="The child results of this result."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parentResult" eType="#//EvaluationResult"
        eOpposite="#//EvaluationResult/subResults"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="failedCondition" eType="#//Condition">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="Indicates the conditions that failed. Only set, if there are conditions that failed."/>
      </eAnnotations>
    </eStructuralFeatures>
    <eStructuralFeatures xsi:type="ecore:EReference" name="failedCandidate" eType="ecore:EClass ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EObject"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="EvaluationStatus">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Specifies the status of an evaluation."/>
    </eAnnotations>
    <eLiterals name="SATISFIED" literal="SATISFIED">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="{@link Symbol} and its sub symbols or {@link Condition}s are valid and satisfied."/>
      </eAnnotations>
    </eLiterals>
    <eLiterals name="UNSATISFIED" value="1">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="{@link Symbol} and its sub symbols or {@link Condition}s are valid and &lt;em>not&lt;/em> satisfied."/>
      </eAnnotations>
    </eLiterals>
    <eLiterals name="ERROR" value="2">
      <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
        <details key="documentation" value="{@link Symbol} and its sub symbols or {@link Condition}s are invalid."/>
      </eAnnotations>
    </eLiterals>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="RefinementTemplate" eSuperTypes="#//Template">
    <eStructuralFeatures xsi:type="ecore:EReference" name="refinedTemplate" eType="#//Template"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="OptionGroup">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" eType="ecore:EDataType http://www.eclipse.org/emf/2002/Ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="templates" upperBound="-1"
        eType="#//Template" eOpposite="#//Template/optionGroups"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="replace" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EBoolean"
        defaultValueLiteral="true"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="NonExistenceGroup">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" eType="ecore:EDataType ../../org.eclipse.emf.ecore/model/Ecore.ecore#//EString"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="templates" upperBound="-1"
        eType="#//Template" eOpposite="#//Template/nonExistenceGroups"/>
  </eClassifiers>
</ecore:EPackage>
