/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The result of a condition evaluation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getMessage <em>Message</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getStatus <em>Status</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getException <em>Exception</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getEvaluator <em>Evaluator</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getSubResults <em>Sub Results</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getParentResult <em>Parent Result</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCondition <em>Failed Condition</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCandidate <em>Failed Candidate</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult()
 * @model
 * @generated
 */
public interface EvaluationResult extends EObject {
	/**
	 * Returns the value of the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A human readable message.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Message</em>' attribute.
	 * @see #setMessage(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_Message()
	 * @model
	 * @generated
	 */
	String getMessage();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getMessage <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' attribute.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(String value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.core.conditions.EvaluationStatus}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Status of this evaluation result.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see org.modelversioning.core.conditions.EvaluationStatus
	 * @see #setStatus(EvaluationStatus)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_Status()
	 * @model
	 * @generated
	 */
	EvaluationStatus getStatus();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see org.modelversioning.core.conditions.EvaluationStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(EvaluationStatus value);

	/**
	 * Returns the value of the '<em><b>Exception</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Only set if there occured an error.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Exception</em>' attribute.
	 * @see #setException(Object)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_Exception()
	 * @model
	 * @generated NOT
	 */
	Throwable getException();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getException <em>Exception</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception</em>' attribute.
	 * @see #getException()
	 * @generated NOT
	 */
	void setException(Throwable value);

	/**
	 * Returns the value of the '<em><b>Evaluator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The id of the implementation that executed the evaluation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Evaluator</em>' attribute.
	 * @see #setEvaluator(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_Evaluator()
	 * @model
	 * @generated
	 */
	String getEvaluator();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getEvaluator <em>Evaluator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluator</em>' attribute.
	 * @see #getEvaluator()
	 * @generated
	 */
	void setEvaluator(String value);

	/**
	 * Returns the value of the '<em><b>Sub Results</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.EvaluationResult}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.EvaluationResult#getParentResult <em>Parent Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The child results of this result.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sub Results</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_SubResults()
	 * @see org.modelversioning.core.conditions.EvaluationResult#getParentResult
	 * @model opposite="parentResult" containment="true"
	 * @generated
	 */
	EList<EvaluationResult> getSubResults();

	/**
	 * Returns the value of the '<em><b>Parent Result</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.EvaluationResult#getSubResults <em>Sub Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Result</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Result</em>' container reference.
	 * @see #setParentResult(EvaluationResult)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_ParentResult()
	 * @see org.modelversioning.core.conditions.EvaluationResult#getSubResults
	 * @model opposite="subResults" transient="false"
	 * @generated
	 */
	EvaluationResult getParentResult();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getParentResult <em>Parent Result</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Result</em>' container reference.
	 * @see #getParentResult()
	 * @generated
	 */
	void setParentResult(EvaluationResult value);

	/**
	 * Returns the value of the '<em><b>Failed Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Indicates the conditions that failed. Only set, if there are conditions that failed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Failed Condition</em>' reference.
	 * @see #setFailedCondition(Condition)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_FailedCondition()
	 * @model
	 * @generated
	 */
	Condition getFailedCondition();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCondition <em>Failed Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Failed Condition</em>' reference.
	 * @see #getFailedCondition()
	 * @generated
	 */
	void setFailedCondition(Condition value);

	/**
	 * Returns the value of the '<em><b>Failed Candidate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Failed Candidate</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Failed Candidate</em>' reference.
	 * @see #setFailedCandidate(EObject)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getEvaluationResult_FailedCandidate()
	 * @model
	 * @generated
	 */
	EObject getFailedCandidate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCandidate <em>Failed Candidate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Failed Candidate</em>' reference.
	 * @see #getFailedCandidate()
	 * @generated
	 */
	void setFailedCandidate(EObject value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isMultiResult();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isOK();

} // EvaluationResult
