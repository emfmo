/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This model represents symbols and their conditions as well as their relation ship to each other.
 * <!-- end-model-doc -->
 * @see org.modelversioning.core.conditions.ConditionsFactory
 * @model kind="package"
 * @generated
 */
public interface ConditionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "conditions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://modelversioning.org/core/conditions/metamodel/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "conditions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConditionsPackage eINSTANCE = org.modelversioning.core.conditions.impl.ConditionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.TemplateImpl <em>Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.TemplateImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getTemplate()
	 * @generated
	 */
	int TEMPLATE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Sub Templates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__SUB_TEMPLATES = 1;

	/**
	 * The feature id for the '<em><b>Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__SPECIFICATIONS = 2;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__STATE = 3;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__TITLE = 4;

	/**
	 * The feature id for the '<em><b>Representative</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__REPRESENTATIVE = 5;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__MODEL = 6;

	/**
	 * The feature id for the '<em><b>Parent Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__PARENT_TEMPLATE = 7;

	/**
	 * The feature id for the '<em><b>Parents Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__PARENTS_FEATURE = 8;

	/**
	 * The feature id for the '<em><b>Containing Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__CONTAINING_MODEL = 9;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__ACTIVE = 10;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__PARAMETER = 11;

	/**
	 * The feature id for the '<em><b>Option Groups</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__OPTION_GROUPS = 12;

	/**
	 * The feature id for the '<em><b>Non Existence Groups</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE__NON_EXISTENCE_GROUPS = 13;

	/**
	 * The number of structural features of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPLATE_FEATURE_COUNT = 14;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.ConditionImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__TYPE = 0;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__STATE = 1;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__ACTIVE = 2;

	/**
	 * The feature id for the '<em><b>Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__TEMPLATE = 3;

	/**
	 * The feature id for the '<em><b>Involves Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__INVOLVES_TEMPLATE = 4;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.FeatureConditionImpl <em>Feature Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.FeatureConditionImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getFeatureCondition()
	 * @generated
	 */
	int FEATURE_CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__TYPE = CONDITION__TYPE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__STATE = CONDITION__STATE;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__ACTIVE = CONDITION__ACTIVE;

	/**
	 * The feature id for the '<em><b>Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__TEMPLATE = CONDITION__TEMPLATE;

	/**
	 * The feature id for the '<em><b>Involves Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__INVOLVES_TEMPLATE = CONDITION__INVOLVES_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__FEATURE = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION__EXPRESSION = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl <em>Attribute Correspondence Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getAttributeCorrespondenceCondition()
	 * @generated
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__TYPE = FEATURE_CONDITION__TYPE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__STATE = FEATURE_CONDITION__STATE;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__ACTIVE = FEATURE_CONDITION__ACTIVE;

	/**
	 * The feature id for the '<em><b>Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__TEMPLATE = FEATURE_CONDITION__TEMPLATE;

	/**
	 * The feature id for the '<em><b>Involves Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__INVOLVES_TEMPLATE = FEATURE_CONDITION__INVOLVES_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__FEATURE = FEATURE_CONDITION__FEATURE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__EXPRESSION = FEATURE_CONDITION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Corresponding Templates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES = FEATURE_CONDITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Corresponding Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES = FEATURE_CONDITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Correspondence Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CORRESPONDENCE_CONDITION_FEATURE_COUNT = FEATURE_CONDITION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.CustomConditionImpl <em>Custom Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.CustomConditionImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getCustomCondition()
	 * @generated
	 */
	int CUSTOM_CONDITION = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__TYPE = CONDITION__TYPE;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__STATE = CONDITION__STATE;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__ACTIVE = CONDITION__ACTIVE;

	/**
	 * The feature id for the '<em><b>Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__TEMPLATE = CONDITION__TEMPLATE;

	/**
	 * The feature id for the '<em><b>Involves Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__INVOLVES_TEMPLATE = CONDITION__INVOLVES_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION__EXPRESSION = CONDITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Custom Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_CONDITION_FEATURE_COUNT = CONDITION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.ConditionsModelImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getConditionsModel()
	 * @generated
	 */
	int CONDITIONS_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Root Template</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__ROOT_TEMPLATE = 0;

	/**
	 * The feature id for the '<em><b>Created At</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__CREATED_AT = 1;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__MODEL_NAME = 2;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__LANGUAGE = 3;

	/**
	 * The feature id for the '<em><b>Option Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__OPTION_GROUPS = 4;

	/**
	 * The feature id for the '<em><b>Non Existence Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL__NON_EXISTENCE_GROUPS = 5;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_MODEL_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.EvaluationResultImpl <em>Evaluation Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.EvaluationResultImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getEvaluationResult()
	 * @generated
	 */
	int EVALUATION_RESULT = 6;

	/**
	 * The feature id for the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__MESSAGE = 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__STATUS = 1;

	/**
	 * The feature id for the '<em><b>Exception</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__EXCEPTION = 2;

	/**
	 * The feature id for the '<em><b>Evaluator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__EVALUATOR = 3;

	/**
	 * The feature id for the '<em><b>Sub Results</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__SUB_RESULTS = 4;

	/**
	 * The feature id for the '<em><b>Parent Result</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__PARENT_RESULT = 5;

	/**
	 * The feature id for the '<em><b>Failed Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__FAILED_CONDITION = 6;

	/**
	 * The feature id for the '<em><b>Failed Candidate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT__FAILED_CANDIDATE = 7;

	/**
	 * The number of structural features of the '<em>Evaluation Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATION_RESULT_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.RefinementTemplateImpl <em>Refinement Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.RefinementTemplateImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getRefinementTemplate()
	 * @generated
	 */
	int REFINEMENT_TEMPLATE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__NAME = TEMPLATE__NAME;

	/**
	 * The feature id for the '<em><b>Sub Templates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__SUB_TEMPLATES = TEMPLATE__SUB_TEMPLATES;

	/**
	 * The feature id for the '<em><b>Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__SPECIFICATIONS = TEMPLATE__SPECIFICATIONS;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__STATE = TEMPLATE__STATE;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__TITLE = TEMPLATE__TITLE;

	/**
	 * The feature id for the '<em><b>Representative</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__REPRESENTATIVE = TEMPLATE__REPRESENTATIVE;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__MODEL = TEMPLATE__MODEL;

	/**
	 * The feature id for the '<em><b>Parent Template</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__PARENT_TEMPLATE = TEMPLATE__PARENT_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Parents Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__PARENTS_FEATURE = TEMPLATE__PARENTS_FEATURE;

	/**
	 * The feature id for the '<em><b>Containing Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__CONTAINING_MODEL = TEMPLATE__CONTAINING_MODEL;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__ACTIVE = TEMPLATE__ACTIVE;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__PARAMETER = TEMPLATE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Option Groups</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__OPTION_GROUPS = TEMPLATE__OPTION_GROUPS;

	/**
	 * The feature id for the '<em><b>Non Existence Groups</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__NON_EXISTENCE_GROUPS = TEMPLATE__NON_EXISTENCE_GROUPS;

	/**
	 * The feature id for the '<em><b>Refined Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE__REFINED_TEMPLATE = TEMPLATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Refinement Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFINEMENT_TEMPLATE_FEATURE_COUNT = TEMPLATE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.OptionGroupImpl <em>Option Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.OptionGroupImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getOptionGroup()
	 * @generated
	 */
	int OPTION_GROUP = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_GROUP__NAME = 0;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_GROUP__TEMPLATES = 1;

	/**
	 * The feature id for the '<em><b>Replace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_GROUP__REPLACE = 2;

	/**
	 * The number of structural features of the '<em>Option Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTION_GROUP_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.impl.NonExistenceGroupImpl <em>Non Existence Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.impl.NonExistenceGroupImpl
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getNonExistenceGroup()
	 * @generated
	 */
	int NON_EXISTENCE_GROUP = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_EXISTENCE_GROUP__NAME = 0;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_EXISTENCE_GROUP__TEMPLATES = 1;

	/**
	 * The number of structural features of the '<em>Non Existence Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_EXISTENCE_GROUP_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.ConditionType <em>Condition Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.ConditionType
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getConditionType()
	 * @generated
	 */
	int CONDITION_TYPE = 10;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.State <em>State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.State
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getState()
	 * @generated
	 */
	int STATE = 11;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.Model <em>Model</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.Model
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 12;


	/**
	 * The meta object id for the '{@link org.modelversioning.core.conditions.EvaluationStatus <em>Evaluation Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.conditions.EvaluationStatus
	 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getEvaluationStatus()
	 * @generated
	 */
	int EVALUATION_STATUS = 13;


	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.Template <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template</em>'.
	 * @see org.modelversioning.core.conditions.Template
	 * @generated
	 */
	EClass getTemplate();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.core.conditions.Template#getName()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.Template#getSubTemplates <em>Sub Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Templates</em>'.
	 * @see org.modelversioning.core.conditions.Template#getSubTemplates()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_SubTemplates();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.Template#getSpecifications <em>Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Specifications</em>'.
	 * @see org.modelversioning.core.conditions.Template#getSpecifications()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_Specifications();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see org.modelversioning.core.conditions.Template#getState()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_State();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.modelversioning.core.conditions.Template#getTitle()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Title();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.Template#getRepresentative <em>Representative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Representative</em>'.
	 * @see org.modelversioning.core.conditions.Template#getRepresentative()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_Representative();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see org.modelversioning.core.conditions.Template#getModel()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Model();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.core.conditions.Template#getParentTemplate <em>Parent Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Template</em>'.
	 * @see org.modelversioning.core.conditions.Template#getParentTemplate()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_ParentTemplate();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.Template#getParentsFeature <em>Parents Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parents Feature</em>'.
	 * @see org.modelversioning.core.conditions.Template#getParentsFeature()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_ParentsFeature();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.core.conditions.Template#getContainingModel <em>Containing Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Model</em>'.
	 * @see org.modelversioning.core.conditions.Template#getContainingModel()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_ContainingModel();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see org.modelversioning.core.conditions.Template#isActive()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Active();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Template#isParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parameter</em>'.
	 * @see org.modelversioning.core.conditions.Template#isParameter()
	 * @see #getTemplate()
	 * @generated
	 */
	EAttribute getTemplate_Parameter();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.Template#getOptionGroups <em>Option Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Option Groups</em>'.
	 * @see org.modelversioning.core.conditions.Template#getOptionGroups()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_OptionGroups();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.Template#getNonExistenceGroups <em>Non Existence Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Non Existence Groups</em>'.
	 * @see org.modelversioning.core.conditions.Template#getNonExistenceGroups()
	 * @see #getTemplate()
	 * @generated
	 */
	EReference getTemplate_NonExistenceGroups();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see org.modelversioning.core.conditions.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Condition#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.modelversioning.core.conditions.Condition#getType()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Condition#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see org.modelversioning.core.conditions.Condition#getState()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_State();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Condition#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see org.modelversioning.core.conditions.Condition#isActive()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Active();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.core.conditions.Condition#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Template</em>'.
	 * @see org.modelversioning.core.conditions.Condition#getTemplate()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_Template();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.Condition#isInvolvesTemplate <em>Involves Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Involves Template</em>'.
	 * @see org.modelversioning.core.conditions.Condition#isInvolvesTemplate()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_InvolvesTemplate();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.FeatureCondition <em>Feature Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Condition</em>'.
	 * @see org.modelversioning.core.conditions.FeatureCondition
	 * @generated
	 */
	EClass getFeatureCondition();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.FeatureCondition#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.modelversioning.core.conditions.FeatureCondition#getFeature()
	 * @see #getFeatureCondition()
	 * @generated
	 */
	EReference getFeatureCondition_Feature();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.FeatureCondition#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.modelversioning.core.conditions.FeatureCondition#getExpression()
	 * @see #getFeatureCondition()
	 * @generated
	 */
	EAttribute getFeatureCondition_Expression();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition <em>Attribute Correspondence Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Correspondence Condition</em>'.
	 * @see org.modelversioning.core.conditions.AttributeCorrespondenceCondition
	 * @generated
	 */
	EClass getAttributeCorrespondenceCondition();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingTemplates <em>Corresponding Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Corresponding Templates</em>'.
	 * @see org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingTemplates()
	 * @see #getAttributeCorrespondenceCondition()
	 * @generated
	 */
	EReference getAttributeCorrespondenceCondition_CorrespondingTemplates();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingFeatures <em>Corresponding Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Corresponding Features</em>'.
	 * @see org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingFeatures()
	 * @see #getAttributeCorrespondenceCondition()
	 * @generated
	 */
	EReference getAttributeCorrespondenceCondition_CorrespondingFeatures();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.CustomCondition <em>Custom Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Condition</em>'.
	 * @see org.modelversioning.core.conditions.CustomCondition
	 * @generated
	 */
	EClass getCustomCondition();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.CustomCondition#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.modelversioning.core.conditions.CustomCondition#getExpression()
	 * @see #getCustomCondition()
	 * @generated
	 */
	EAttribute getCustomCondition_Expression();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.ConditionsModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel
	 * @generated
	 */
	EClass getConditionsModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.modelversioning.core.conditions.ConditionsModel#getRootTemplate <em>Root Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Template</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getRootTemplate()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EReference getConditionsModel_RootTemplate();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.ConditionsModel#getCreatedAt <em>Created At</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Created At</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getCreatedAt()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EAttribute getConditionsModel_CreatedAt();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.ConditionsModel#getModelName <em>Model Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Name</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getModelName()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EAttribute getConditionsModel_ModelName();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.ConditionsModel#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getLanguage()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EAttribute getConditionsModel_Language();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.ConditionsModel#getOptionGroups <em>Option Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Option Groups</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getOptionGroups()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EReference getConditionsModel_OptionGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.ConditionsModel#getNonExistenceGroups <em>Non Existence Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Non Existence Groups</em>'.
	 * @see org.modelversioning.core.conditions.ConditionsModel#getNonExistenceGroups()
	 * @see #getConditionsModel()
	 * @generated
	 */
	EReference getConditionsModel_NonExistenceGroups();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.EvaluationResult <em>Evaluation Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluation Result</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult
	 * @generated
	 */
	EClass getEvaluationResult();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.EvaluationResult#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getMessage()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EAttribute getEvaluationResult_Message();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.EvaluationResult#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getStatus()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EAttribute getEvaluationResult_Status();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.EvaluationResult#getException <em>Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exception</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getException()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EAttribute getEvaluationResult_Exception();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.EvaluationResult#getEvaluator <em>Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluator</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getEvaluator()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EAttribute getEvaluationResult_Evaluator();

	/**
	 * Returns the meta object for the containment reference list '{@link org.modelversioning.core.conditions.EvaluationResult#getSubResults <em>Sub Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Results</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getSubResults()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EReference getEvaluationResult_SubResults();

	/**
	 * Returns the meta object for the container reference '{@link org.modelversioning.core.conditions.EvaluationResult#getParentResult <em>Parent Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Result</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getParentResult()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EReference getEvaluationResult_ParentResult();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCondition <em>Failed Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Failed Condition</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getFailedCondition()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EReference getEvaluationResult_FailedCondition();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.EvaluationResult#getFailedCandidate <em>Failed Candidate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Failed Candidate</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationResult#getFailedCandidate()
	 * @see #getEvaluationResult()
	 * @generated
	 */
	EReference getEvaluationResult_FailedCandidate();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.RefinementTemplate <em>Refinement Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Refinement Template</em>'.
	 * @see org.modelversioning.core.conditions.RefinementTemplate
	 * @generated
	 */
	EClass getRefinementTemplate();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.conditions.RefinementTemplate#getRefinedTemplate <em>Refined Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refined Template</em>'.
	 * @see org.modelversioning.core.conditions.RefinementTemplate#getRefinedTemplate()
	 * @see #getRefinementTemplate()
	 * @generated
	 */
	EReference getRefinementTemplate_RefinedTemplate();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.OptionGroup <em>Option Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Option Group</em>'.
	 * @see org.modelversioning.core.conditions.OptionGroup
	 * @generated
	 */
	EClass getOptionGroup();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.OptionGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.core.conditions.OptionGroup#getName()
	 * @see #getOptionGroup()
	 * @generated
	 */
	EAttribute getOptionGroup_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.OptionGroup#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Templates</em>'.
	 * @see org.modelversioning.core.conditions.OptionGroup#getTemplates()
	 * @see #getOptionGroup()
	 * @generated
	 */
	EReference getOptionGroup_Templates();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.OptionGroup#isReplace <em>Replace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Replace</em>'.
	 * @see org.modelversioning.core.conditions.OptionGroup#isReplace()
	 * @see #getOptionGroup()
	 * @generated
	 */
	EAttribute getOptionGroup_Replace();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.conditions.NonExistenceGroup <em>Non Existence Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Existence Group</em>'.
	 * @see org.modelversioning.core.conditions.NonExistenceGroup
	 * @generated
	 */
	EClass getNonExistenceGroup();

	/**
	 * Returns the meta object for the attribute '{@link org.modelversioning.core.conditions.NonExistenceGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.modelversioning.core.conditions.NonExistenceGroup#getName()
	 * @see #getNonExistenceGroup()
	 * @generated
	 */
	EAttribute getNonExistenceGroup_Name();

	/**
	 * Returns the meta object for the reference list '{@link org.modelversioning.core.conditions.NonExistenceGroup#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Templates</em>'.
	 * @see org.modelversioning.core.conditions.NonExistenceGroup#getTemplates()
	 * @see #getNonExistenceGroup()
	 * @generated
	 */
	EReference getNonExistenceGroup_Templates();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.core.conditions.ConditionType <em>Condition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Condition Type</em>'.
	 * @see org.modelversioning.core.conditions.ConditionType
	 * @generated
	 */
	EEnum getConditionType();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.core.conditions.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State</em>'.
	 * @see org.modelversioning.core.conditions.State
	 * @generated
	 */
	EEnum getState();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.core.conditions.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Model</em>'.
	 * @see org.modelversioning.core.conditions.Model
	 * @generated
	 */
	EEnum getModel();

	/**
	 * Returns the meta object for enum '{@link org.modelversioning.core.conditions.EvaluationStatus <em>Evaluation Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluation Status</em>'.
	 * @see org.modelversioning.core.conditions.EvaluationStatus
	 * @generated
	 */
	EEnum getEvaluationStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConditionsFactory getConditionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.TemplateImpl <em>Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.TemplateImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getTemplate()
		 * @generated
		 */
		EClass TEMPLATE = eINSTANCE.getTemplate();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__NAME = eINSTANCE.getTemplate_Name();

		/**
		 * The meta object literal for the '<em><b>Sub Templates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__SUB_TEMPLATES = eINSTANCE.getTemplate_SubTemplates();

		/**
		 * The meta object literal for the '<em><b>Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__SPECIFICATIONS = eINSTANCE.getTemplate_Specifications();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__STATE = eINSTANCE.getTemplate_State();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__TITLE = eINSTANCE.getTemplate_Title();

		/**
		 * The meta object literal for the '<em><b>Representative</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__REPRESENTATIVE = eINSTANCE.getTemplate_Representative();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__MODEL = eINSTANCE.getTemplate_Model();

		/**
		 * The meta object literal for the '<em><b>Parent Template</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__PARENT_TEMPLATE = eINSTANCE.getTemplate_ParentTemplate();

		/**
		 * The meta object literal for the '<em><b>Parents Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__PARENTS_FEATURE = eINSTANCE.getTemplate_ParentsFeature();

		/**
		 * The meta object literal for the '<em><b>Containing Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__CONTAINING_MODEL = eINSTANCE.getTemplate_ContainingModel();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__ACTIVE = eINSTANCE.getTemplate_Active();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPLATE__PARAMETER = eINSTANCE.getTemplate_Parameter();

		/**
		 * The meta object literal for the '<em><b>Option Groups</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__OPTION_GROUPS = eINSTANCE.getTemplate_OptionGroups();

		/**
		 * The meta object literal for the '<em><b>Non Existence Groups</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPLATE__NON_EXISTENCE_GROUPS = eINSTANCE.getTemplate_NonExistenceGroups();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.ConditionImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__TYPE = eINSTANCE.getCondition_Type();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__STATE = eINSTANCE.getCondition_State();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__ACTIVE = eINSTANCE.getCondition_Active();

		/**
		 * The meta object literal for the '<em><b>Template</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION__TEMPLATE = eINSTANCE.getCondition_Template();

		/**
		 * The meta object literal for the '<em><b>Involves Template</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__INVOLVES_TEMPLATE = eINSTANCE.getCondition_InvolvesTemplate();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.FeatureConditionImpl <em>Feature Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.FeatureConditionImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getFeatureCondition()
		 * @generated
		 */
		EClass FEATURE_CONDITION = eINSTANCE.getFeatureCondition();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_CONDITION__FEATURE = eINSTANCE.getFeatureCondition_Feature();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_CONDITION__EXPRESSION = eINSTANCE.getFeatureCondition_Expression();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl <em>Attribute Correspondence Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.AttributeCorrespondenceConditionImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getAttributeCorrespondenceCondition()
		 * @generated
		 */
		EClass ATTRIBUTE_CORRESPONDENCE_CONDITION = eINSTANCE.getAttributeCorrespondenceCondition();

		/**
		 * The meta object literal for the '<em><b>Corresponding Templates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_TEMPLATES = eINSTANCE.getAttributeCorrespondenceCondition_CorrespondingTemplates();

		/**
		 * The meta object literal for the '<em><b>Corresponding Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CORRESPONDENCE_CONDITION__CORRESPONDING_FEATURES = eINSTANCE.getAttributeCorrespondenceCondition_CorrespondingFeatures();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.CustomConditionImpl <em>Custom Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.CustomConditionImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getCustomCondition()
		 * @generated
		 */
		EClass CUSTOM_CONDITION = eINSTANCE.getCustomCondition();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUSTOM_CONDITION__EXPRESSION = eINSTANCE.getCustomCondition_Expression();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.ConditionsModelImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getConditionsModel()
		 * @generated
		 */
		EClass CONDITIONS_MODEL = eINSTANCE.getConditionsModel();

		/**
		 * The meta object literal for the '<em><b>Root Template</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONS_MODEL__ROOT_TEMPLATE = eINSTANCE.getConditionsModel_RootTemplate();

		/**
		 * The meta object literal for the '<em><b>Created At</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITIONS_MODEL__CREATED_AT = eINSTANCE.getConditionsModel_CreatedAt();

		/**
		 * The meta object literal for the '<em><b>Model Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITIONS_MODEL__MODEL_NAME = eINSTANCE.getConditionsModel_ModelName();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITIONS_MODEL__LANGUAGE = eINSTANCE.getConditionsModel_Language();

		/**
		 * The meta object literal for the '<em><b>Option Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONS_MODEL__OPTION_GROUPS = eINSTANCE.getConditionsModel_OptionGroups();

		/**
		 * The meta object literal for the '<em><b>Non Existence Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITIONS_MODEL__NON_EXISTENCE_GROUPS = eINSTANCE.getConditionsModel_NonExistenceGroups();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.EvaluationResultImpl <em>Evaluation Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.EvaluationResultImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getEvaluationResult()
		 * @generated
		 */
		EClass EVALUATION_RESULT = eINSTANCE.getEvaluationResult();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_RESULT__MESSAGE = eINSTANCE.getEvaluationResult_Message();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_RESULT__STATUS = eINSTANCE.getEvaluationResult_Status();

		/**
		 * The meta object literal for the '<em><b>Exception</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_RESULT__EXCEPTION = eINSTANCE.getEvaluationResult_Exception();

		/**
		 * The meta object literal for the '<em><b>Evaluator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATION_RESULT__EVALUATOR = eINSTANCE.getEvaluationResult_Evaluator();

		/**
		 * The meta object literal for the '<em><b>Sub Results</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_RESULT__SUB_RESULTS = eINSTANCE.getEvaluationResult_SubResults();

		/**
		 * The meta object literal for the '<em><b>Parent Result</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_RESULT__PARENT_RESULT = eINSTANCE.getEvaluationResult_ParentResult();

		/**
		 * The meta object literal for the '<em><b>Failed Condition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_RESULT__FAILED_CONDITION = eINSTANCE.getEvaluationResult_FailedCondition();

		/**
		 * The meta object literal for the '<em><b>Failed Candidate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATION_RESULT__FAILED_CANDIDATE = eINSTANCE.getEvaluationResult_FailedCandidate();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.RefinementTemplateImpl <em>Refinement Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.RefinementTemplateImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getRefinementTemplate()
		 * @generated
		 */
		EClass REFINEMENT_TEMPLATE = eINSTANCE.getRefinementTemplate();

		/**
		 * The meta object literal for the '<em><b>Refined Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFINEMENT_TEMPLATE__REFINED_TEMPLATE = eINSTANCE.getRefinementTemplate_RefinedTemplate();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.OptionGroupImpl <em>Option Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.OptionGroupImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getOptionGroup()
		 * @generated
		 */
		EClass OPTION_GROUP = eINSTANCE.getOptionGroup();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_GROUP__NAME = eINSTANCE.getOptionGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTION_GROUP__TEMPLATES = eINSTANCE.getOptionGroup_Templates();

		/**
		 * The meta object literal for the '<em><b>Replace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTION_GROUP__REPLACE = eINSTANCE.getOptionGroup_Replace();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.impl.NonExistenceGroupImpl <em>Non Existence Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.impl.NonExistenceGroupImpl
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getNonExistenceGroup()
		 * @generated
		 */
		EClass NON_EXISTENCE_GROUP = eINSTANCE.getNonExistenceGroup();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_EXISTENCE_GROUP__NAME = eINSTANCE.getNonExistenceGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_EXISTENCE_GROUP__TEMPLATES = eINSTANCE.getNonExistenceGroup_Templates();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.ConditionType <em>Condition Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.ConditionType
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getConditionType()
		 * @generated
		 */
		EEnum CONDITION_TYPE = eINSTANCE.getConditionType();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.State <em>State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.State
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getState()
		 * @generated
		 */
		EEnum STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.Model <em>Model</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.Model
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getModel()
		 * @generated
		 */
		EEnum MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.conditions.EvaluationStatus <em>Evaluation Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.conditions.EvaluationStatus
		 * @see org.modelversioning.core.conditions.impl.ConditionsPackageImpl#getEvaluationStatus()
		 * @generated
		 */
		EEnum EVALUATION_STATUS = eINSTANCE.getEvaluationStatus();

	}

} //ConditionsPackage
