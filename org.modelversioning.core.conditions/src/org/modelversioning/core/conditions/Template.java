/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Template is a role of a model element in a concrete situation specified by a name and some conditions. A template may include sub templates (e.g. child elements like containments).
 * 
 * For example, a template is the constructor set to private within a "Convert to Singleton" operation. Or any public attribute that is set to private and for which a getter and setter method is generated within the "Generate Getter and Setter" operation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.Template#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getSubTemplates <em>Sub Templates</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getSpecifications <em>Specifications</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getState <em>State</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getTitle <em>Title</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getRepresentative <em>Representative</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getModel <em>Model</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getParentTemplate <em>Parent Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getParentsFeature <em>Parents Feature</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getContainingModel <em>Containing Model</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#isActive <em>Active</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#isParameter <em>Parameter</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getOptionGroups <em>Option Groups</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Template#getNonExistenceGroups <em>Non Existence Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate()
 * @model
 * @generated
 */
public interface Template extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The name for technical representation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Sub Templates</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.Template}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Template#getParentTemplate <em>Parent Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Templates</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Templates</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_SubTemplates()
	 * @see org.modelversioning.core.conditions.Template#getParentTemplate
	 * @model opposite="parentTemplate" containment="true"
	 * @generated
	 */
	EList<Template> getSubTemplates();

	/**
	 * Returns the value of the '<em><b>Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.Condition}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Condition#getTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies a Template using Conditions.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Specifications</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Specifications()
	 * @see org.modelversioning.core.conditions.Condition#getTemplate
	 * @model opposite="template" containment="true"
	 * @generated
	 */
	EList<Condition> getSpecifications();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.core.conditions.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.core.conditions.State
	 * @see #setState(State)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_State()
	 * @model required="true"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.core.conditions.State
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The human readable title.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Title()
	 * @model default=""
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Representative</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A reference to the representing object in a model.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Representative</em>' reference.
	 * @see #setRepresentative(EObject)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Representative()
	 * @model required="true"
	 * @generated
	 */
	EObject getRepresentative();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getRepresentative <em>Representative</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Representative</em>' reference.
	 * @see #getRepresentative()
	 * @generated
	 */
	void setRepresentative(EObject value);

	/**
	 * Returns the value of the '<em><b>Model</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.core.conditions.Model}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies the model type this symbol belongs to.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Model</em>' attribute.
	 * @see org.modelversioning.core.conditions.Model
	 * @see #setModel(Model)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Model()
	 * @model required="true"
	 * @generated
	 */
	Model getModel();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getModel <em>Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model</em>' attribute.
	 * @see org.modelversioning.core.conditions.Model
	 * @see #getModel()
	 * @generated
	 */
	void setModel(Model value);

	/**
	 * Returns the value of the '<em><b>Parent Template</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Template#getSubTemplates <em>Sub Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parent symbol.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent Template</em>' container reference.
	 * @see #setParentTemplate(Template)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_ParentTemplate()
	 * @see org.modelversioning.core.conditions.Template#getSubTemplates
	 * @model opposite="subTemplates" transient="false"
	 * @generated
	 */
	Template getParentTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getParentTemplate <em>Parent Template</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Template</em>' container reference.
	 * @see #getParentTemplate()
	 * @generated
	 */
	void setParentTemplate(Template value);

	/**
	 * Returns the value of the '<em><b>Parents Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parents Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parents Feature</em>' reference.
	 * @see #setParentsFeature(EStructuralFeature)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_ParentsFeature()
	 * @model
	 * @generated
	 */
	EStructuralFeature getParentsFeature();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getParentsFeature <em>Parents Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parents Feature</em>' reference.
	 * @see #getParentsFeature()
	 * @generated
	 */
	void setParentsFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Containing Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.ConditionsModel#getRootTemplate <em>Root Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Model</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Model</em>' container reference.
	 * @see #setContainingModel(ConditionsModel)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_ContainingModel()
	 * @see org.modelversioning.core.conditions.ConditionsModel#getRootTemplate
	 * @model opposite="rootTemplate" transient="false"
	 * @generated
	 */
	ConditionsModel getContainingModel();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#getContainingModel <em>Containing Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing Model</em>' container reference.
	 * @see #getContainingModel()
	 * @generated
	 */
	void setContainingModel(ConditionsModel value);

	/**
	 * Returns the value of the '<em><b>Active</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active</em>' attribute.
	 * @see #setActive(boolean)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Active()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isActive();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active</em>' attribute.
	 * @see #isActive()
	 * @generated
	 */
	void setActive(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isExistence();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specified whether this template is a parameter.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameter</em>' attribute.
	 * @see #setParameter(boolean)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_Parameter()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isParameter();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Template#isParameter <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' attribute.
	 * @see #isParameter()
	 * @generated
	 */
	void setParameter(boolean value);

	/**
	 * Returns the value of the '<em><b>Option Groups</b></em>' reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.OptionGroup}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.OptionGroup#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Option Groups</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Option Groups</em>' reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_OptionGroups()
	 * @see org.modelversioning.core.conditions.OptionGroup#getTemplates
	 * @model opposite="templates"
	 * @generated
	 */
	EList<OptionGroup> getOptionGroups();

	/**
	 * Returns the value of the '<em><b>Non Existence Groups</b></em>' reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.NonExistenceGroup}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.NonExistenceGroup#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Non Existence Groups</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Existence Groups</em>' reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getTemplate_NonExistenceGroups()
	 * @see org.modelversioning.core.conditions.NonExistenceGroup#getTemplates
	 * @model opposite="templates"
	 * @generated
	 */
	EList<NonExistenceGroup> getNonExistenceGroups();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isMandatory();

} // Template
