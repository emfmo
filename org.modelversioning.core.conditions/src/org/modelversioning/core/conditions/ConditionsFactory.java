/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.modelversioning.core.conditions.ConditionsPackage
 * @generated
 */
public interface ConditionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConditionsFactory eINSTANCE = org.modelversioning.core.conditions.impl.ConditionsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Template</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Template</em>'.
	 * @generated
	 */
	Template createTemplate();

	/**
	 * Returns a new object of class '<em>Feature Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Condition</em>'.
	 * @generated
	 */
	FeatureCondition createFeatureCondition();

	/**
	 * Returns a new object of class '<em>Attribute Correspondence Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Correspondence Condition</em>'.
	 * @generated
	 */
	AttributeCorrespondenceCondition createAttributeCorrespondenceCondition();

	/**
	 * Returns a new object of class '<em>Custom Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Condition</em>'.
	 * @generated
	 */
	CustomCondition createCustomCondition();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	ConditionsModel createConditionsModel();

	/**
	 * Returns a new object of class '<em>Evaluation Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Evaluation Result</em>'.
	 * @generated
	 */
	EvaluationResult createEvaluationResult();

	/**
	 * Returns a new object of class '<em>Refinement Template</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Refinement Template</em>'.
	 * @generated
	 */
	RefinementTemplate createRefinementTemplate();

	/**
	 * Returns a new object of class '<em>Option Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Option Group</em>'.
	 * @generated
	 */
	OptionGroup createOptionGroup();

	/**
	 * Returns a new object of class '<em>Non Existence Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Existence Group</em>'.
	 * @generated
	 */
	NonExistenceGroup createNonExistenceGroup();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ConditionsPackage getConditionsPackage();

} //ConditionsFactory
