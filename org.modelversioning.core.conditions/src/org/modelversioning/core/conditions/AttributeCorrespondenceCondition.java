/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Correspondence Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingTemplates <em>Corresponding Templates</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition#getCorrespondingFeatures <em>Corresponding Features</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getAttributeCorrespondenceCondition()
 * @model
 * @generated
 */
public interface AttributeCorrespondenceCondition extends FeatureCondition {
	/**
	 * Returns the value of the '<em><b>Corresponding Templates</b></em>' reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.Template}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Templates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Templates</em>' reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getAttributeCorrespondenceCondition_CorrespondingTemplates()
	 * @model
	 * @generated
	 */
	EList<Template> getCorrespondingTemplates();

	/**
	 * Returns the value of the '<em><b>Corresponding Features</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EStructuralFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Features</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Features</em>' reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getAttributeCorrespondenceCondition_CorrespondingFeatures()
	 * @model
	 * @generated
	 */
	EList<EStructuralFeature> getCorrespondingFeatures();

} // AttributeCorrespondenceCondition
