/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.service;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.engines.impl.ConditionsGenerationEngineImpl;

/**
 * This class offers a convenient access to the {@link ConditionsModel}
 * generation facilities of this package.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsGenerationService {
	
	/**
	 * Generates an instance of the {@link ConditionsModel} for the specified
	 * <code>eObject</code>.
	 * 
	 * @param eObject
	 *            the {@link EObject} to create the model for.
	 * @return the created {@link ConditionsModel}.
	 */
	public ConditionsModel generateConditionsModel(EObject eObject) {
		return new ConditionsGenerationEngineImpl().generateConditionsModel(eObject);
	}

}
