/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.modelversioning.core.conditions.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.modelversioning.core.conditions.ConditionsPackage
 * @generated
 */
public class ConditionsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ConditionsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ConditionsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionsSwitch<Adapter> modelSwitch =
		new ConditionsSwitch<Adapter>() {
			@Override
			public Adapter caseTemplate(Template object) {
				return createTemplateAdapter();
			}
			@Override
			public Adapter caseCondition(Condition object) {
				return createConditionAdapter();
			}
			@Override
			public Adapter caseFeatureCondition(FeatureCondition object) {
				return createFeatureConditionAdapter();
			}
			@Override
			public Adapter caseAttributeCorrespondenceCondition(AttributeCorrespondenceCondition object) {
				return createAttributeCorrespondenceConditionAdapter();
			}
			@Override
			public Adapter caseCustomCondition(CustomCondition object) {
				return createCustomConditionAdapter();
			}
			@Override
			public Adapter caseConditionsModel(ConditionsModel object) {
				return createConditionsModelAdapter();
			}
			@Override
			public Adapter caseEvaluationResult(EvaluationResult object) {
				return createEvaluationResultAdapter();
			}
			@Override
			public Adapter caseRefinementTemplate(RefinementTemplate object) {
				return createRefinementTemplateAdapter();
			}
			@Override
			public Adapter caseOptionGroup(OptionGroup object) {
				return createOptionGroupAdapter();
			}
			@Override
			public Adapter caseNonExistenceGroup(NonExistenceGroup object) {
				return createNonExistenceGroupAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.Template <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.Template
	 * @generated
	 */
	public Adapter createTemplateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.Condition
	 * @generated
	 */
	public Adapter createConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.FeatureCondition <em>Feature Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.FeatureCondition
	 * @generated
	 */
	public Adapter createFeatureConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.AttributeCorrespondenceCondition <em>Attribute Correspondence Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.AttributeCorrespondenceCondition
	 * @generated
	 */
	public Adapter createAttributeCorrespondenceConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.CustomCondition <em>Custom Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.CustomCondition
	 * @generated
	 */
	public Adapter createCustomConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.ConditionsModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.ConditionsModel
	 * @generated
	 */
	public Adapter createConditionsModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.EvaluationResult <em>Evaluation Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.EvaluationResult
	 * @generated
	 */
	public Adapter createEvaluationResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.RefinementTemplate <em>Refinement Template</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.RefinementTemplate
	 * @generated
	 */
	public Adapter createRefinementTemplateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.OptionGroup <em>Option Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.OptionGroup
	 * @generated
	 */
	public Adapter createOptionGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.modelversioning.core.conditions.NonExistenceGroup <em>Non Existence Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.modelversioning.core.conditions.NonExistenceGroup
	 * @generated
	 */
	public Adapter createNonExistenceGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ConditionsAdapterFactory
