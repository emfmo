/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.TemplateMaskLiterals;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.impl.OCLLiterals;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingsImpl;

/**
 * Utilities for {@link ConditionsModel}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsUtil {

	/**
	 * Single Template pattern.
	 */
	public final static Pattern singleTemplateNamePattern = Pattern
			.compile(TemplateMaskLiterals.SINGLE_TEMPLATE_FIND_REGEX);

	/**
	 * Single Prefixed Template pattern.
	 */
	public final static Pattern singlePrefixedTemplateNamePattern = Pattern
			.compile(TemplateMaskLiterals.SINGLE_PREFIXED_TEMPLATE_FIND_REGEX);

	/**
	 * Single Unclosed Template pattern.
	 */
	public final static Pattern singleUnclosedTemplateNamePattern = Pattern
			.compile(TemplateMaskLiterals.SINGLE_TEMPLATE_UNCLOSED_FIND_REGES);

	/**
	 * Returns a set of active conditions of the specified <code>template</code>
	 * .
	 * 
	 * @param template
	 *            template to get active conditions from.
	 * @return active conditions.
	 */
	public static Set<Condition> getActiveConditions(Template template) {
		Set<Condition> conditions = new HashSet<Condition>();
		for (Condition condition : template.getSpecifications()) {
			if (condition.isActive()) {
				conditions.add(condition);
			}
		}
		return conditions;
	}

	/**
	 * Activates all specified <code>conditions</code>.
	 * 
	 * @param conditions
	 *            to activate.
	 */
	public static void setActive(List<FeatureCondition> conditions) {
		for (Condition condition : conditions) {
			condition.setActive(true);
		}
	}

	/**
	 * Inactivates all specified <code>conditions</code>.
	 * 
	 * @param conditions
	 *            to inactivate.
	 */
	public static void setInActive(List<FeatureCondition> conditions) {
		for (Condition condition : conditions) {
			condition.setActive(false);
		}
	}

	/**
	 * Returns a set of active and template involving conditions of the
	 * specified <code>template</code>.
	 * 
	 * @param template
	 *            template to get active conditions from.
	 * @return active and template involving conditions.
	 */
	public static Set<Condition> getActiveTemplateInvolvingConditions(
			Template template) {
		Set<Condition> activeConditions = getActiveConditions(template);
		Set<Condition> activeInvolvingConditions = new HashSet<Condition>();
		for (Condition condition : activeConditions) {
			if (condition.isInvolvesTemplate()) {
				activeInvolvingConditions.add(condition);
			}
		}
		return activeInvolvingConditions;
	}

	/**
	 * Creates a list of parents of the specified <code>template</code> from
	 * <code>template</code>'s parent to root template. If <code>template</code>
	 * is root itself, the returned list is empty.
	 * 
	 * @param template
	 *            template to start going upwards the containment hierarchy.
	 * @return a list of parent templates from <code>template</code>'s parent to
	 *         root template.
	 */
	public static List<Template> createParentList(Template template) {
		List<Template> parentsList = new ArrayList<Template>();
		Template parentTemplate = template;
		while ((parentTemplate = parentTemplate.getParentTemplate()) != null) {
			parentsList.add(parentTemplate);
		}
		return parentsList;
	}

	/**
	 * Returns the root template of the specified <code>template</code>.
	 * 
	 * @param template
	 *            {@link Template} to get root of.
	 * @return the root {@link Template}.
	 */
	public static Template getRootTemplate(final Template template) {
		Template rootTemplate = template;
		while (rootTemplate.getParentTemplate() != null) {
			rootTemplate = rootTemplate.getParentTemplate();
		}
		return rootTemplate;
	}

	/**
	 * Returns the {@link ConditionsModel} containing the specified
	 * <code>template</code>.
	 * 
	 * @param template
	 *            to get containing {@link ConditionsModel} of.
	 * @return the containing {@link ConditionsModel} or <code>null</code> if
	 *         there is none.
	 */
	public static ConditionsModel getContainingConditionsModel(Template template) {
		Template rootTemplate = getRootTemplate(template);
		if (rootTemplate.eContainer() instanceof ConditionsModel) {
			return (ConditionsModel) rootTemplate.eContainer();
		}
		return null;
	}

	/**
	 * Finds and returns the least common parent of both templates
	 * <code>template1</code> and <code>template2</code>. If no common parent is
	 * found this method returns <code>null</code>.
	 * 
	 * @param template1
	 *            first base template.
	 * @param template2
	 *            second base template.
	 * @return least common parent or <code>null</code> if there is none.
	 */
	public static Template findLeastCommonParentTemplate(Template template1,
			Template template2) {

		List<Template> parents1 = createParentList(template1);
		List<Template> parents2 = createParentList(template2);

		// check if one of the objects is root
		if (template1.getParentTemplate() == null
				&& template2.getParentTemplate() == null
				&& template1.equals(template2)) {
			return template1;
		} else if (template1.getParentTemplate() == null
				|| parents2.contains(template1)) {
			return template1;
		} else if (template2.getParentTemplate() == null
				|| parents1.contains(template2)) {
			return template2;
		}

		for (Template currentParent : parents1) {
			if (parents2.contains(currentParent)) {
				return currentParent;
			}
		}

		return null;
	}

	/**
	 * Returns a {@link EList} of all {@link Template}s contained by the
	 * specified {@link ConditionsModel}s.
	 * 
	 * @param conditionsModel
	 *            to get all {@link Template}s from.
	 * @return all {@link Template}s contained by <code>conditionsModel</code>.
	 */
	public static EList<Template> getAllTemplates(
			ConditionsModel conditionsModel) {
		EList<Template> list = new BasicEList<Template>();
		list.add(conditionsModel.getRootTemplate());
		list.addAll(getAllTemplates(conditionsModel.getRootTemplate()));
		return list;
	}

	/**
	 * Returns a {@link EList} of all (child and child's child ...)
	 * {@link Template}s contained by the specified {@link Template}s.
	 * 
	 * @param template
	 *            to get all {@link Template}s from.
	 * @return all {@link Template}s contained by <code>template</code>.
	 */
	public static Collection<? extends Template> getAllTemplates(
			Template template) {
		EList<Template> list = new BasicEList<Template>();
		if (!template.getSubTemplates().isEmpty()) {
			Iterator<Template> iterator = template.getSubTemplates().iterator();
			while (iterator.hasNext()) {
				Template next = iterator.next();
				list.add(next);
				list.addAll(getAllTemplates(next));
			}
		}
		return list;
	}

	/**
	 * Specifies whether the specified <code>template</code> is
	 * {@link Template#isMandatory() optional}.
	 * 
	 * @param template
	 *            to check.
	 * @param includeImplicit
	 *            specifies whether also check for optional parents.
	 * @return <code>true</code> if optional, <code>false</code> if mandatory.
	 */
	public static boolean isOptional(Template template, boolean includeImplicit) {
		if (!template.isMandatory()) {
			return true;
		}
		if (includeImplicit) {
			Template parentTemplate = template;
			while ((parentTemplate = parentTemplate.getParentTemplate()) != null) {
				if (!parentTemplate.isMandatory()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Specifies whether the specified <code>template</code> is
	 * {@link Template#isExistence() non-existence}.
	 * 
	 * @param template
	 *            to check.
	 * @param includeImplicit
	 *            specifies whether also check for non-existence parents.
	 * @return <code>true</code> if non-existence, <code>false</code> if
	 *         existence.
	 */
	public static boolean isNonExistence(Template template,
			boolean includeImplicit) {
		if (!template.isExistence()) {
			return true;
		}
		if (includeImplicit) {
			Template parentTemplate = template;
			while ((parentTemplate = parentTemplate.getParentTemplate()) != null) {
				if (!parentTemplate.isExistence()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Specifies whether the specified <code>template</code> is
	 * {@link Template#isActive() inactive}.
	 * 
	 * @param template
	 *            to check.
	 * @param includeImplicit
	 *            specifies whether also check for inactive parents.
	 * @return <code>true</code> if inactive, <code>false</code> if active.
	 */
	public static boolean isInActive(Template template, boolean includeImplicit) {
		if (!template.isActive()) {
			return true;
		}
		if (includeImplicit) {
			Template parentTemplate = template;
			while ((parentTemplate = parentTemplate.getParentTemplate()) != null) {
				if (!parentTemplate.isActive()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Returns the expression of the specified <code>condition</code>.
	 * 
	 * @param condition
	 *            to get expression from.
	 * @return the expression or <code>null</code> if it cannot be retrieved.
	 */
	public static String getExpression(Condition condition) {
		if (condition instanceof FeatureCondition) {
			return ((FeatureCondition) condition).getExpression();
		} else if (condition instanceof CustomCondition) {
			return ((CustomCondition) condition).getExpression();
		}
		return null;
	}

	/**
	 * Sets the expression of the specified <code>condition</code>.
	 * 
	 * @param condition
	 *            to set expression to.
	 */
	public static void setExpression(Condition condition, String expression) {
		if (condition instanceof FeatureCondition) {
			((FeatureCondition) condition).setExpression(expression);
		} else if (condition instanceof CustomCondition) {
			((CustomCondition) condition).setExpression(expression);
		}
	}

	/**
	 * Specified whether two conditions are semantically equal.
	 * 
	 * @param condition1
	 *            first condition to compare.
	 * @param condition2
	 *            second condition to compare.
	 * @return <code>true</code> if semantically equal, <code>false</code>
	 *         otherwise.
	 */
	public static boolean isEqual(Condition condition1, Condition condition2) {
		if (!condition1.eClass().equals(condition2.eClass())) {
			return false;
		}
		if (!ConditionsUtil.getExpression(condition1).equals(
				ConditionsUtil.getExpression(condition2))) {
			return false;
		}
		if (condition1 instanceof FeatureCondition
				&& condition2 instanceof FeatureCondition) {
			FeatureCondition featureCondition1 = (FeatureCondition) condition1;
			FeatureCondition featureCondition2 = (FeatureCondition) condition2;
			if (!featureCondition1.getFeature().equals(
					featureCondition2.getFeature())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Specifies whether <code>template</code> contains a {@link Condition}
	 * which is semantically equal to <code>condition</code>.
	 * 
	 * @param template
	 *            to check.
	 * @param condition
	 *            to check.
	 * @return <code>true</code> if there is a condition, <code>false</code> if
	 *         not.
	 */
	public static boolean containsEqualCondition(Template template,
			Condition condition) {
		for (Condition toCompareTo : template.getSpecifications()) {
			if (isEqual(toCompareTo, condition)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the {@link Template} contained by the specified
	 * <code>conditionsModel</code> which represents <code>element</code>.
	 * 
	 * @param element
	 *            representative of {@link Template} to find.
	 * @param conditionsModel
	 *            to search {@link Template} in.
	 * @return the found {@link Template} or <code>null</code> if no
	 *         {@link Template} could be found.
	 */
	public static Template getTemplateByRepresentative(EObject element,
			ConditionsModel conditionsModel) {
		if (element == null) {
			return null;
		}
		for (Template template : getAllTemplates(conditionsModel)) {
			if (template.getRepresentative().equals(element)) {
				return template;
			}
		}
		return null;
	}

	/**
	 * Returns a {@link Map} of representative objects to templates.
	 * 
	 * @param conditionsModel
	 *            to create map from.
	 * @return map of representatives to their templates.
	 */
	public static Map<EObject, Template> getRepresentativeToTemplateMap(
			ConditionsModel conditionsModel) {
		Map<EObject, Template> map = new HashMap<EObject, Template>();

		for (Template template : getAllTemplates(conditionsModel)) {
			map.put(template.getRepresentative(), template);
		}

		return map;
	}

	/**
	 * Returns a {@link Map} of templates to representative objects.
	 * 
	 * @param conditionsModel
	 *            to create map from.
	 * @return map of templates to their representatives.
	 */
	public static Map<Template, EObject> getTemplateToRepresentativeMap(
			ConditionsModel conditionsModel) {
		Map<Template, EObject> map = new HashMap<Template, EObject>();

		for (Template template : getAllTemplates(conditionsModel)) {
			map.put(template, template.getRepresentative());
		}

		return map;
	}

	/**
	 * Returns a {@link Set} of {@link Template}s that are not bound in the
	 * specified <code>templateBinding</code> but that are contained by the
	 * specified <code>conditionsModel</code>.
	 * 
	 * @param templateBinding
	 *            to check for missing templates.
	 * @param conditionsModel
	 *            containing all available templates.
	 * @return unbound templates.
	 */
	public static Set<Template> getUnboundTemplates(
			ITemplateBinding templateBinding, ConditionsModel conditionsModel) {
		Set<Template> missingTemplates = new HashSet<Template>();
		Set<Template> boundTemplates = templateBinding.getTemplates();
		for (Template template : getAllTemplates(conditionsModel)) {
			if (!boundTemplates.contains(template)) {
				missingTemplates.add(template);
			}
		}
		return missingTemplates;
	}

	/**
	 * Adds active conditions of the specified template in <code>element</code>
	 * to the specified <code>list</code>.
	 * 
	 * @param list
	 *            to add active conditions to.
	 * @param element
	 *            template.
	 */
	private static void addActiveConditionsToList(List<Object> list,
			EObject element) {
		Template template;
		if (element instanceof Template) {
			template = (Template) element;
			if (template.isActive()) {
				list.add(template);
			}
			for (Condition c : template.getSpecifications()) {
				if (c.isActive()) {
					list.add(c);
				}
			}
			for (Template subTemplate : template.getSubTemplates()) {
				addActiveConditionsToList(list, subTemplate);
			}
		}
	}

	/**
	 * Returns a list of all active conditions contained by the
	 * {@link ConditionsModel} <code>cm</code>.
	 * 
	 * @param cm
	 *            conditions model.
	 * @return an array of all active conditions.
	 */
	public static Object[] getActiveConditionArray(ConditionsModel cm) {
		List<Object> list = new ArrayList<Object>();
		addActiveConditionsToList(list, cm.getRootTemplate());
		return list.toArray();
	}

	/**
	 * Returns the {@link Template} with the specified <code>templateName</code>
	 * contained by the specified <code>conditionsModel</code> or
	 * <code>null</code> if no template with the specified
	 * <code>templateName</code> can be found.
	 * 
	 * @param templateName
	 *            to search for.
	 * @param conditionsModel
	 *            to search in.
	 * @return template with <code>templateName</code> or <code>null</code>.
	 */
	public static Template getTemplateByName(String templateName,
			ConditionsModel conditionsModel) {
		for (Template template : getAllTemplates(conditionsModel)) {
			if (templateName.equals(template.getName())) {
				return template;
			}
		}
		return null;
	}

	/**
	 * Extracts all referenced feature names for the specified
	 * <code>template</code> in specified <code>prefix</code> in
	 * <code>expression</code>.
	 * 
	 * @param prefix
	 *            prefix of <code>template</code>.
	 * @param template
	 *            template to look out for.
	 * @param expression
	 *            to extract from.
	 * @return referenced feature names.
	 */
	public static Set<String> getReferencedFeatureNames(String prefix,
			Template template, String expression) {
		String regex = "(#\\{(" + prefix + TemplateMaskLiterals.PREFIX_SEP //$NON-NLS-1$
				+ template.getName() + ")\\}.[a-zA-Z0-9]*)"; //$NON-NLS-1$
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expression);

		Set<String> set = new HashSet<String>();
		while (matcher.find()) {
			String match = matcher.group();
			String featureName = match
					.substring(match.indexOf(OCLLiterals.DOT) + 1);
			if (featureName != null && featureName.length() > 0) {
				set.add(featureName);
			}
		}
		return set;
	}

	/**
	 * Extracts all feature names for the specified <code>templateName</code> in
	 * <code>expression</code>.
	 * 
	 * @param templateName
	 *            templateName to look out for.
	 * @param expression
	 *            to extract from.
	 * @return referenced feature names.
	 */
	public static Set<String> getReferencedFeatureNames(String templateName,
			String expression) {
		String regex = "(#\\{(" + templateName + ")\\}.[a-zA-Z0-9]*)"; //$NON-NLS-1$
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(expression);

		Set<String> set = new HashSet<String>();
		while (matcher.find()) {
			String match = matcher.group();
			String featureName = match
					.substring(match.indexOf(OCLLiterals.DOT) + 1);
			if (featureName != null && featureName.length() > 0) {
				set.add(featureName);
			}
		}
		return set;
	}

	/**
	 * Extracts all feature names for the specified <code>template</code> in
	 * <code>expression</code>.
	 * 
	 * @param template
	 *            template to look out for.
	 * @param expression
	 *            to extract from.
	 * @return referenced feature names.
	 */
	public static Set<String> getReferencedFeatureNames(Template template,
			String expression) {
		return getReferencedFeatureNames(template.getName(), expression);
	}

	/**
	 * Extracts all referenced (masked) template names from the specified
	 * <code>expression</code>. This method returns a {@link Map} of prefix to
	 * referenced {@link Template} names. For unprefixed templates, the key of
	 * the {@link Map} is an empty {@link String}.
	 * 
	 * @param expression
	 *            to get referenced templates from.
	 * @return a {@link Map} of prefix to referenced {@link Template} names.
	 */
	public static Map<String, Set<String>> getReferencedTemplateNames(
			String expression) {
		Map<String, Set<String>> map = getReferencedPrefixedTemplateNames(expression);
		Set<String> unprefixedTemplateNames = getReferencedUnprefixedTemplateNames(expression);
		if (unprefixedTemplateNames.size() > 0) {
			map.put("", unprefixedTemplateNames); //$NON-NLS-1$
		}
		return map;
	}

	/**
	 * Extracts all referenced (masked) and prefixed template names from the
	 * specified <code>expression</code>. This method returns a {@link Map} of
	 * prefix to referenced {@link Template} names.
	 * 
	 * @param expression
	 *            to get referenced templates from.
	 * @return a {@link Map} of prefix to referenced {@link Template} names.
	 */
	public static Map<String, Set<String>> getReferencedPrefixedTemplateNames(
			String expression) {
		Map<String, Set<String>> extractedTemplateNames = new HashMap<String, Set<String>>();
		Matcher templateMatcher = ConditionsUtil.singlePrefixedTemplateNamePattern
				.matcher(expression);

		while (templateMatcher.find()) {
			String match = templateMatcher.group();
			String prefix = match.substring(2,
					match.indexOf(TemplateMaskLiterals.PREFIX_SEP));
			String templateName = match.substring(
					match.indexOf(TemplateMaskLiterals.PREFIX_SEP) + 1,
					match.length() - 1);
			if (templateName != null && templateName.length() > 0) {
				if (extractedTemplateNames.get(prefix) == null) {
					extractedTemplateNames.put(prefix, new HashSet<String>());
				}
				extractedTemplateNames.get(prefix).add(templateName);
			}
		}
		return extractedTemplateNames;
	}

	/**
	 * Extracts all referenced (masked) and unprefixed template names from the
	 * specified <code>expression</code>. This method returns a set of
	 * referenced {@link Template} names.
	 * 
	 * @param expression
	 *            to get referenced templates from.
	 * @return a Set of referenced {@link Template} names.
	 */
	public static Set<String> getReferencedUnprefixedTemplateNames(
			String expression) {
		Set<String> extractedTemplateNames = new HashSet<String>();

		Matcher templateMatcher = ConditionsUtil.singleTemplateNamePattern
				.matcher(expression);

		while (templateMatcher.find()) {
			String match = templateMatcher.group();
			String templateName = match.substring(2, match.length() - 1);
			if (templateName != null && templateName.length() > 0) {
				extractedTemplateNames.add(templateName);
			}
		}

		return extractedTemplateNames;
	}

	/**
	 * Returns the last matching unclosed referenced template name (prefixed or
	 * not). If no match, this method returns <code>null</code>.
	 * 
	 * @param expression
	 *            to match.
	 * @return last template name or <code>null</code>.
	 */
	public static String getUnclosedTemplateNames(String expression) {
		Matcher templateMatcher = ConditionsUtil.singleUnclosedTemplateNamePattern
				.matcher(expression);

		while (templateMatcher.find()) {
			String match = templateMatcher.group();
			return match;
		}
		return null;
	}

	/**
	 * Removes all masks used for template masking. E.g. for a {@link Condition}
	 * with the expression &quot;= #{xxx}.name&quot;, the returning
	 * {@link String} will be &quot;= xxx.name&quot;
	 * 
	 * @param condition
	 *            to get unmasked expression for.
	 * @return the unmasked expression of <code>condition</code>.
	 */
	public static String getUnmaskedConditionExpression(Condition condition) {
		String expression = ConditionsUtil.getExpression(condition);
		String maskStart = TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
				.replace("#", "\\#"); //$NON-NLS-1$ //$NON-NLS-2$
		maskStart = maskStart.replace("{", "\\{");
		String maskEnd = TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END;
		expression = expression.replaceAll(maskStart, ""); //$NON-NLS-1$
		expression = expression.replace(maskEnd, ""); //$NON-NLS-1$
		return expression;
	}

	/**
	 * Creates a {@link ITemplateBinding} for the specified <code>map</code>.
	 * 
	 * @param map
	 *            to create {@link ITemplateBinding} for.
	 * @return the created {@link ITemplateBinding}.
	 */
	public static ITemplateBinding createTemplateBinding(
			Map<Template, EObject> map) {
		ITemplateBinding binding = new TemplateBindingImpl();
		for (Entry<Template, EObject> entry : map.entrySet()) {
			binding.add(entry.getKey(), entry.getValue());
		}
		return binding;
	}

	/**
	 * Creates a {@link ITemplateBindings} for the specified single binding
	 * <code>map</code>.
	 * 
	 * @param map
	 *            to create {@link ITemplateBinding} for.
	 * @return the created {@link ITemplateBindings}.
	 */
	public static ITemplateBindings createTemplateBindings(
			Map<Template, EObject> map) {
		ITemplateBinding binding = createTemplateBinding(map);
		TemplateBindingsImpl bindings = new TemplateBindingsImpl(
				getRootTemplate(binding.getTemplates().iterator().next()));
		bindings.getAllPossibleBindings().add(binding);
		return bindings;
	}

	/**
	 * Merges the specified {@link ITemplateBindings} to a new
	 * {@link ITemplateBinding}.
	 * 
	 * @param bindings
	 *            to merge.
	 * @return the merged {@link ITemplateBinding}.
	 */
	public static ITemplateBinding mergeTemplateBindings(
			ITemplateBindings bindings) {
		ITemplateBinding binding = new TemplateBindingImpl();
		for (Template template : bindings.getTemplates()) {
			binding.add(template, bindings.getBoundObjects(template));
		}
		return binding;
	}

	/**
	 * Debug prints the specified <code>conditionsModel</code> to the print
	 * stream <code>out</code>.
	 * 
	 * @param conditionsModel
	 *            to print.
	 * @param out
	 *            to print to.
	 */
	public static void debugPring(ConditionsModel conditionsModel,
			PrintStream out) {
		out.println("====");
		EList<Template> templates = getAllTemplates(conditionsModel);
		for (Template template : templates) {
			out.println(template.getTitle() + ": ");
			for (Condition condition : template.getSpecifications()) {
				if (condition instanceof FeatureCondition) {
					System.out
							.println("      "
									+ ((FeatureCondition) condition)
											.getFeature().getName() + " "
									+ getExpression(condition));
				} else {
					System.out.println(getExpression(condition));
				}
			}
		}
		out.println("====");
	}

	/**
	 * Debug prints the specified template <code>binding</code> to the print
	 * stream <code>out</code>.
	 * 
	 * @param binding
	 *            to print.
	 * @param out
	 *            to print to.
	 */
	public static void debugPring(ITemplateBinding binding, PrintStream out) {
		out.println("====");
		for (Template template : binding.getTemplates()) {
			out.println(template.getTitle() + ": ");
			if (binding.getBoundObjects(template) != null) {
				for (EObject boundObject : binding.getBoundObjects(template)) {
					out.println("   " + boundObject);
				}
			} else {
				out.println("   null");
			}
		}
		out.println("====");
	}

	/**
	 * Debug prints the specified template <code>binding</code> to the print
	 * stream <code>out</code>.
	 * 
	 * @param binding
	 *            to print.
	 * @param out
	 *            to print to.
	 */
	public static void debugPring(ITemplateBindings binding, PrintStream out) {
		out.println("====");
		for (Template template : binding.getTemplates()) {
			out.println(template.getTitle() + ": ");
			if (binding.getBoundObjects(template) != null) {
				for (EObject boundObject : binding.getBoundObjects(template)) {
					out.println("   " + boundObject);
				}
			} else {
				out.println("   null");
			}
		}
		out.println("====");
	}

}
