/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.templatebindings.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.modelversioning.core.conditions.templatebindings.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TemplatebindingsFactoryImpl extends EFactoryImpl implements TemplatebindingsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TemplatebindingsFactory init() {
		try {
			TemplatebindingsFactory theTemplatebindingsFactory = (TemplatebindingsFactory)EPackage.Registry.INSTANCE.getEFactory("http://modelversioning.org/core/templateBindings/metamodel/1.0"); 
			if (theTemplatebindingsFactory != null) {
				return theTemplatebindingsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TemplatebindingsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplatebindingsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TemplatebindingsPackage.TEMPLATE_BINDING_COLLECTION: return createTemplateBindingCollection();
			case TemplatebindingsPackage.TEMPLATE_BINDING: return createTemplateBinding();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBindingCollection createTemplateBindingCollection() {
		TemplateBindingCollectionImpl templateBindingCollection = new TemplateBindingCollectionImpl();
		return templateBindingCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBinding createTemplateBinding() {
		TemplateBindingImpl templateBinding = new TemplateBindingImpl();
		return templateBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplatebindingsPackage getTemplatebindingsPackage() {
		return (TemplatebindingsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TemplatebindingsPackage getPackage() {
		return TemplatebindingsPackage.eINSTANCE;
	}

} //TemplatebindingsFactoryImpl
