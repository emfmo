/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.templatebindings.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import org.modelversioning.core.conditions.Template;

import org.modelversioning.core.conditions.templatebindings.TemplateBinding;
import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;
import org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl#getEObjects <em>EObjects</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.impl.TemplateBindingImpl#getTemplateName <em>Template Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TemplateBindingImpl extends EObjectImpl implements TemplateBinding {
	/**
	 * The cached value of the '{@link #getEObjects() <em>EObjects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> eObjects;

	/**
	 * The cached value of the '{@link #getTemplate() <em>Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplate()
	 * @generated
	 * @ordered
	 */
	protected Template template;

	/**
	 * The default value of the '{@link #getTemplateName() <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateName()
	 * @generated
	 * @ordered
	 */
	protected static final String TEMPLATE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTemplateName() <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemplateName()
	 * @generated
	 * @ordered
	 */
	protected String templateName = TEMPLATE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TemplatebindingsPackage.Literals.TEMPLATE_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemplateBindingCollection getCollection() {
		if (eContainerFeatureID() != TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION) return null;
		return (TemplateBindingCollection)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollection(TemplateBindingCollection newCollection, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCollection, TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollection(TemplateBindingCollection newCollection) {
		if (newCollection != eInternalContainer() || (eContainerFeatureID() != TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION && newCollection != null)) {
			if (EcoreUtil.isAncestor(this, newCollection))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCollection != null)
				msgs = ((InternalEObject)newCollection).eInverseAdd(this, TemplatebindingsPackage.TEMPLATE_BINDING_COLLECTION__BINDINGS, TemplateBindingCollection.class, msgs);
			msgs = basicSetCollection(newCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION, newCollection, newCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getEObjects() {
		if (eObjects == null) {
			eObjects = new EObjectResolvingEList<EObject>(EObject.class, this, TemplatebindingsPackage.TEMPLATE_BINDING__EOBJECTS);
		}
		return eObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getTemplate() {
		if (template != null && template.eIsProxy()) {
			InternalEObject oldTemplate = (InternalEObject)template;
			template = (Template)eResolveProxy(oldTemplate);
			if (template != oldTemplate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE, oldTemplate, template));
			}
		}
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template basicGetTemplate() {
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplate(Template newTemplate) {
		Template oldTemplate = template;
		template = newTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE, oldTemplate, template));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemplateName(String newTemplateName) {
		String oldTemplateName = templateName;
		templateName = newTemplateName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE_NAME, oldTemplateName, templateName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCollection((TemplateBindingCollection)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				return basicSetCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				return eInternalContainer().eInverseRemove(this, TemplatebindingsPackage.TEMPLATE_BINDING_COLLECTION__BINDINGS, TemplateBindingCollection.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				return getCollection();
			case TemplatebindingsPackage.TEMPLATE_BINDING__EOBJECTS:
				return getEObjects();
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE:
				if (resolve) return getTemplate();
				return basicGetTemplate();
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE_NAME:
				return getTemplateName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				setCollection((TemplateBindingCollection)newValue);
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__EOBJECTS:
				getEObjects().clear();
				getEObjects().addAll((Collection<? extends EObject>)newValue);
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE:
				setTemplate((Template)newValue);
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE_NAME:
				setTemplateName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				setCollection((TemplateBindingCollection)null);
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__EOBJECTS:
				getEObjects().clear();
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE:
				setTemplate((Template)null);
				return;
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE_NAME:
				setTemplateName(TEMPLATE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TemplatebindingsPackage.TEMPLATE_BINDING__COLLECTION:
				return getCollection() != null;
			case TemplatebindingsPackage.TEMPLATE_BINDING__EOBJECTS:
				return eObjects != null && !eObjects.isEmpty();
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE:
				return template != null;
			case TemplatebindingsPackage.TEMPLATE_BINDING__TEMPLATE_NAME:
				return TEMPLATE_NAME_EDEFAULT == null ? templateName != null : !TEMPLATE_NAME_EDEFAULT.equals(templateName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (templateName: ");
		result.append(templateName);
		result.append(')');
		return result.toString();
	}

} //TemplateBindingImpl
