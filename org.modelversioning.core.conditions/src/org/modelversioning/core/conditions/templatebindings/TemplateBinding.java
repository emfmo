/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.templatebindings;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getEObjects <em>EObjects</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplate <em>Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplateName <em>Template Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBinding()
 * @model
 * @generated
 */
public interface TemplateBinding extends EObject {
	/**
	 * Returns the value of the '<em><b>Collection</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' container reference.
	 * @see #setCollection(TemplateBindingCollection)
	 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBinding_Collection()
	 * @see org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection#getBindings
	 * @model opposite="bindings" transient="false"
	 * @generated
	 */
	TemplateBindingCollection getCollection();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getCollection <em>Collection</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection</em>' container reference.
	 * @see #getCollection()
	 * @generated
	 */
	void setCollection(TemplateBindingCollection value);

	/**
	 * Returns the value of the '<em><b>EObjects</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EObjects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EObjects</em>' reference list.
	 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBinding_EObjects()
	 * @model
	 * @generated
	 */
	EList<EObject> getEObjects();

	/**
	 * Returns the value of the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template</em>' reference.
	 * @see #setTemplate(Template)
	 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBinding_Template()
	 * @model transient="true"
	 * @generated
	 */
	Template getTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplate <em>Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template</em>' reference.
	 * @see #getTemplate()
	 * @generated
	 */
	void setTemplate(Template value);

	/**
	 * Returns the value of the '<em><b>Template Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template Name</em>' attribute.
	 * @see #setTemplateName(String)
	 * @see org.modelversioning.core.conditions.templatebindings.TemplatebindingsPackage#getTemplateBinding_TemplateName()
	 * @model required="true"
	 * @generated
	 */
	String getTemplateName();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.templatebindings.TemplateBinding#getTemplateName <em>Template Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template Name</em>' attribute.
	 * @see #getTemplateName()
	 * @generated
	 */
	void setTemplateName(String value);

} // TemplateBinding
