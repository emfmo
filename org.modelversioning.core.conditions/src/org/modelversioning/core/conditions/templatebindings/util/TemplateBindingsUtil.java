/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.templatebindings.util;

import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingsImpl;
import org.modelversioning.core.conditions.templatebindings.TemplateBinding;
import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;
import org.modelversioning.core.conditions.templatebindings.TemplatebindingsFactory;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Provides utility facilities for {@link TemplateBinding}s and
 * {@link TemplateBindingCollection}s.
 * 
 * Among others, it provides the facility to convert {@link ITemplateBindings}
 * and {@link ITemplateBinding}s to {@link TemplateBindingCollection}s or
 * {@link TemplateBinding} and back again.
 * 
 * The reason for the need of this converting facility is mainly historic since
 * in the first step template bindings were only needed as normal interface and
 * class. Later it got clear, that these bindings are needed in terms of ecore
 * models. In future releases this conversion might get obsolete, as soon as
 * only the ecore based implementations of these bindings are used throughout
 * the whole system.
 * 
 * The implementations {@link TemplateBindingImpl} and
 * {@link TemplateBindingsImpl} are used.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateBindingsUtil {

	/**
	 * The used factory to create template bindings.
	 */
	private static TemplatebindingsFactory tbFactory = TemplatebindingsFactory.eINSTANCE;

	/**
	 * Converts a {@link TemplateBindingCollection} into
	 * {@link ITemplateBindings}.
	 * 
	 * @param tbCollection
	 *            to convert.
	 * @return created {@link ITemplateBindings}.
	 */
	public static ITemplateBindings convert(
			TemplateBindingCollection tbCollection) {
		ITemplateBindings templateBindings = new TemplateBindingsImpl(
				getRootTemplate(tbCollection));
		TemplateBindingImpl templateBindingImpl = new TemplateBindingImpl();
		for (TemplateBinding binding : tbCollection.getBindings()) {
			templateBindingImpl.add(binding.getTemplate(),
					binding.getEObjects());
		}
		templateBindings.getAllPossibleBindings().add(templateBindingImpl);
		return templateBindings;
	}

	/**
	 * Returns the root template of the specified <code>tbCollection</code>.
	 * 
	 * @param tbCollection
	 *            to get root template for.
	 * @return the root template or <code>null</code> if there is not binding.
	 */
	public static Template getRootTemplate(
			TemplateBindingCollection tbCollection) {
		if (tbCollection.getBindings().size() > 0) {
			Template firstTemplate = tbCollection.getBindings().get(0)
					.getTemplate();
			return ConditionsUtil.getRootTemplate(firstTemplate);
		}
		return null;
	}

	/**
	 * Converts a {@link ITemplateBindings} into
	 * {@link TemplateBindingCollection}.
	 * 
	 * @param templateBindings
	 *            to convert.
	 * @return created {@link TemplateBindingCollection}.
	 */
	public static TemplateBindingCollection convert(
			ITemplateBindings templateBindings) {
		TemplateBindingCollection collection = tbFactory
				.createTemplateBindingCollection();

		for (Template template : templateBindings.getTemplates()) {
			TemplateBinding templateBinding = tbFactory.createTemplateBinding();
			templateBinding.setTemplateName(template.getName());
			templateBinding.setTemplate(template);
			templateBinding.getEObjects().addAll(
					templateBindings.getBoundObjects(template));
			collection.getBindings().add(templateBinding);

		}
		return collection;
	}

}
