/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.Model;
import org.modelversioning.core.conditions.NonExistenceGroup;
import org.modelversioning.core.conditions.OptionGroup;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getSubTemplates <em>Sub Templates</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getSpecifications <em>Specifications</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getState <em>State</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getRepresentative <em>Representative</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getModel <em>Model</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getParentTemplate <em>Parent Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getParentsFeature <em>Parents Feature</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getContainingModel <em>Containing Model</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#isActive <em>Active</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#isParameter <em>Parameter</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getOptionGroups <em>Option Groups</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.TemplateImpl#getNonExistenceGroups <em>Non Existence Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TemplateImpl extends EObjectImpl implements Template {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubTemplates() <em>Sub Templates</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubTemplates()
	 * @generated
	 * @ordered
	 */
	protected EList<Template> subTemplates;

	/**
	 * The cached value of the '{@link #getSpecifications() <em>Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition> specifications;

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final State STATE_EDEFAULT = State.GENERATED;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state = STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRepresentative() <em>Representative</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentative()
	 * @generated
	 * @ordered
	 */
	protected EObject representative;

	/**
	 * The default value of the '{@link #getModel() <em>Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel()
	 * @generated
	 * @ordered
	 */
	protected static final Model MODEL_EDEFAULT = Model.ORIGIN;

	/**
	 * The cached value of the '{@link #getModel() <em>Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel()
	 * @generated
	 * @ordered
	 */
	protected Model model = MODEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParentsFeature() <em>Parents Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentsFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature parentsFeature;

	/**
	 * The default value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACTIVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected boolean active = ACTIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #isParameter() <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParameter()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARAMETER_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isParameter() <em>Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParameter()
	 * @generated
	 * @ordered
	 */
	protected boolean parameter = PARAMETER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOptionGroups() <em>Option Groups</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptionGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<OptionGroup> optionGroups;

	/**
	 * The cached value of the '{@link #getNonExistenceGroups() <em>Non Existence Groups</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNonExistenceGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<NonExistenceGroup> nonExistenceGroups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemplateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.Literals.TEMPLATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Template> getSubTemplates() {
		if (subTemplates == null) {
			subTemplates = new EObjectContainmentWithInverseEList<Template>(Template.class, this, ConditionsPackage.TEMPLATE__SUB_TEMPLATES, ConditionsPackage.TEMPLATE__PARENT_TEMPLATE);
		}
		return subTemplates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Condition> getSpecifications() {
		if (specifications == null) {
			specifications = new EObjectContainmentWithInverseEList<Condition>(Condition.class, this, ConditionsPackage.TEMPLATE__SPECIFICATIONS, ConditionsPackage.CONDITION__TEMPLATE);
		}
		return specifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getRepresentative() {
		if (representative != null && representative.eIsProxy()) {
			InternalEObject oldRepresentative = (InternalEObject)representative;
			representative = eResolveProxy(oldRepresentative);
			if (representative != oldRepresentative) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.TEMPLATE__REPRESENTATIVE, oldRepresentative, representative));
			}
		}
		return representative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetRepresentative() {
		return representative;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentative(EObject newRepresentative) {
		EObject oldRepresentative = representative;
		representative = newRepresentative;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__REPRESENTATIVE, oldRepresentative, representative));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel(Model newModel) {
		Model oldModel = model;
		model = newModel == null ? MODEL_EDEFAULT : newModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__MODEL, oldModel, model));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getParentTemplate() {
		if (eContainerFeatureID() != ConditionsPackage.TEMPLATE__PARENT_TEMPLATE) return null;
		return (Template)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentTemplate(Template newParentTemplate, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentTemplate, ConditionsPackage.TEMPLATE__PARENT_TEMPLATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentTemplate(Template newParentTemplate) {
		if (newParentTemplate != eInternalContainer() || (eContainerFeatureID() != ConditionsPackage.TEMPLATE__PARENT_TEMPLATE && newParentTemplate != null)) {
			if (EcoreUtil.isAncestor(this, newParentTemplate))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentTemplate != null)
				msgs = ((InternalEObject)newParentTemplate).eInverseAdd(this, ConditionsPackage.TEMPLATE__SUB_TEMPLATES, Template.class, msgs);
			msgs = basicSetParentTemplate(newParentTemplate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__PARENT_TEMPLATE, newParentTemplate, newParentTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getParentsFeature() {
		if (parentsFeature != null && parentsFeature.eIsProxy()) {
			InternalEObject oldParentsFeature = (InternalEObject)parentsFeature;
			parentsFeature = (EStructuralFeature)eResolveProxy(oldParentsFeature);
			if (parentsFeature != oldParentsFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.TEMPLATE__PARENTS_FEATURE, oldParentsFeature, parentsFeature));
			}
		}
		return parentsFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetParentsFeature() {
		return parentsFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentsFeature(EStructuralFeature newParentsFeature) {
		EStructuralFeature oldParentsFeature = parentsFeature;
		parentsFeature = newParentsFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__PARENTS_FEATURE, oldParentsFeature, parentsFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsModel getContainingModel() {
		if (eContainerFeatureID() != ConditionsPackage.TEMPLATE__CONTAINING_MODEL) return null;
		return (ConditionsModel)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingModel(ConditionsModel newContainingModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainingModel, ConditionsPackage.TEMPLATE__CONTAINING_MODEL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingModel(ConditionsModel newContainingModel) {
		if (newContainingModel != eInternalContainer() || (eContainerFeatureID() != ConditionsPackage.TEMPLATE__CONTAINING_MODEL && newContainingModel != null)) {
			if (EcoreUtil.isAncestor(this, newContainingModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingModel != null)
				msgs = ((InternalEObject)newContainingModel).eInverseAdd(this, ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE, ConditionsModel.class, msgs);
			msgs = basicSetContainingModel(newContainingModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__CONTAINING_MODEL, newContainingModel, newContainingModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActive(boolean newActive) {
		boolean oldActive = active;
		active = newActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__ACTIVE, oldActive, active));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isMandatory() {
		return (this.optionGroups == null || this.optionGroups.isEmpty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isExistence() {
		return (this.nonExistenceGroups == null || this.nonExistenceGroups.isEmpty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(boolean newParameter) {
		boolean oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TEMPLATE__PARAMETER, oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptionGroup> getOptionGroups() {
		if (optionGroups == null) {
			optionGroups = new EObjectWithInverseResolvingEList.ManyInverse<OptionGroup>(OptionGroup.class, this, ConditionsPackage.TEMPLATE__OPTION_GROUPS, ConditionsPackage.OPTION_GROUP__TEMPLATES);
		}
		return optionGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NonExistenceGroup> getNonExistenceGroups() {
		if (nonExistenceGroups == null) {
			nonExistenceGroups = new EObjectWithInverseResolvingEList.ManyInverse<NonExistenceGroup>(NonExistenceGroup.class, this, ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS, ConditionsPackage.NON_EXISTENCE_GROUP__TEMPLATES);
		}
		return nonExistenceGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubTemplates()).basicAdd(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSpecifications()).basicAdd(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentTemplate((Template)otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainingModel((ConditionsModel)otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOptionGroups()).basicAdd(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getNonExistenceGroups()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				return ((InternalEList<?>)getSubTemplates()).basicRemove(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				return ((InternalEList<?>)getSpecifications()).basicRemove(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				return basicSetParentTemplate(null, msgs);
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				return basicSetContainingModel(null, msgs);
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				return ((InternalEList<?>)getOptionGroups()).basicRemove(otherEnd, msgs);
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				return ((InternalEList<?>)getNonExistenceGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				return eInternalContainer().eInverseRemove(this, ConditionsPackage.TEMPLATE__SUB_TEMPLATES, Template.class, msgs);
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				return eInternalContainer().eInverseRemove(this, ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE, ConditionsModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__NAME:
				return getName();
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				return getSubTemplates();
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				return getSpecifications();
			case ConditionsPackage.TEMPLATE__STATE:
				return getState();
			case ConditionsPackage.TEMPLATE__TITLE:
				return getTitle();
			case ConditionsPackage.TEMPLATE__REPRESENTATIVE:
				if (resolve) return getRepresentative();
				return basicGetRepresentative();
			case ConditionsPackage.TEMPLATE__MODEL:
				return getModel();
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				return getParentTemplate();
			case ConditionsPackage.TEMPLATE__PARENTS_FEATURE:
				if (resolve) return getParentsFeature();
				return basicGetParentsFeature();
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				return getContainingModel();
			case ConditionsPackage.TEMPLATE__ACTIVE:
				return isActive();
			case ConditionsPackage.TEMPLATE__PARAMETER:
				return isParameter();
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				return getOptionGroups();
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				return getNonExistenceGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__NAME:
				setName((String)newValue);
				return;
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				getSubTemplates().clear();
				getSubTemplates().addAll((Collection<? extends Template>)newValue);
				return;
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				getSpecifications().clear();
				getSpecifications().addAll((Collection<? extends Condition>)newValue);
				return;
			case ConditionsPackage.TEMPLATE__STATE:
				setState((State)newValue);
				return;
			case ConditionsPackage.TEMPLATE__TITLE:
				setTitle((String)newValue);
				return;
			case ConditionsPackage.TEMPLATE__REPRESENTATIVE:
				setRepresentative((EObject)newValue);
				return;
			case ConditionsPackage.TEMPLATE__MODEL:
				setModel((Model)newValue);
				return;
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				setParentTemplate((Template)newValue);
				return;
			case ConditionsPackage.TEMPLATE__PARENTS_FEATURE:
				setParentsFeature((EStructuralFeature)newValue);
				return;
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				setContainingModel((ConditionsModel)newValue);
				return;
			case ConditionsPackage.TEMPLATE__ACTIVE:
				setActive((Boolean)newValue);
				return;
			case ConditionsPackage.TEMPLATE__PARAMETER:
				setParameter((Boolean)newValue);
				return;
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				getOptionGroups().clear();
				getOptionGroups().addAll((Collection<? extends OptionGroup>)newValue);
				return;
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				getNonExistenceGroups().clear();
				getNonExistenceGroups().addAll((Collection<? extends NonExistenceGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				getSubTemplates().clear();
				return;
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				getSpecifications().clear();
				return;
			case ConditionsPackage.TEMPLATE__STATE:
				setState(STATE_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__REPRESENTATIVE:
				setRepresentative((EObject)null);
				return;
			case ConditionsPackage.TEMPLATE__MODEL:
				setModel(MODEL_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				setParentTemplate((Template)null);
				return;
			case ConditionsPackage.TEMPLATE__PARENTS_FEATURE:
				setParentsFeature((EStructuralFeature)null);
				return;
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				setContainingModel((ConditionsModel)null);
				return;
			case ConditionsPackage.TEMPLATE__ACTIVE:
				setActive(ACTIVE_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__PARAMETER:
				setParameter(PARAMETER_EDEFAULT);
				return;
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				getOptionGroups().clear();
				return;
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				getNonExistenceGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TEMPLATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ConditionsPackage.TEMPLATE__SUB_TEMPLATES:
				return subTemplates != null && !subTemplates.isEmpty();
			case ConditionsPackage.TEMPLATE__SPECIFICATIONS:
				return specifications != null && !specifications.isEmpty();
			case ConditionsPackage.TEMPLATE__STATE:
				return state != STATE_EDEFAULT;
			case ConditionsPackage.TEMPLATE__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case ConditionsPackage.TEMPLATE__REPRESENTATIVE:
				return representative != null;
			case ConditionsPackage.TEMPLATE__MODEL:
				return model != MODEL_EDEFAULT;
			case ConditionsPackage.TEMPLATE__PARENT_TEMPLATE:
				return getParentTemplate() != null;
			case ConditionsPackage.TEMPLATE__PARENTS_FEATURE:
				return parentsFeature != null;
			case ConditionsPackage.TEMPLATE__CONTAINING_MODEL:
				return getContainingModel() != null;
			case ConditionsPackage.TEMPLATE__ACTIVE:
				return active != ACTIVE_EDEFAULT;
			case ConditionsPackage.TEMPLATE__PARAMETER:
				return parameter != PARAMETER_EDEFAULT;
			case ConditionsPackage.TEMPLATE__OPTION_GROUPS:
				return optionGroups != null && !optionGroups.isEmpty();
			case ConditionsPackage.TEMPLATE__NON_EXISTENCE_GROUPS:
				return nonExistenceGroups != null && !nonExistenceGroups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", state: ");
		result.append(state);
		result.append(", title: ");
		result.append(title);
		result.append(", model: ");
		result.append(model);
		result.append(", active: ");
		result.append(active);
		result.append(", parameter: ");
		result.append(parameter);
		result.append(')');
		return result.toString();
	}

} //TemplateImpl
