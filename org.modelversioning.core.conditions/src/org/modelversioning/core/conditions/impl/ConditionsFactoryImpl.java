/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.modelversioning.core.conditions.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConditionsFactoryImpl extends EFactoryImpl implements ConditionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ConditionsFactory init() {
		try {
			ConditionsFactory theConditionsFactory = (ConditionsFactory)EPackage.Registry.INSTANCE.getEFactory("http://modelversioning.org/core/conditions/metamodel/1.0"); 
			if (theConditionsFactory != null) {
				return theConditionsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ConditionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ConditionsPackage.TEMPLATE: return createTemplate();
			case ConditionsPackage.FEATURE_CONDITION: return createFeatureCondition();
			case ConditionsPackage.ATTRIBUTE_CORRESPONDENCE_CONDITION: return createAttributeCorrespondenceCondition();
			case ConditionsPackage.CUSTOM_CONDITION: return createCustomCondition();
			case ConditionsPackage.CONDITIONS_MODEL: return createConditionsModel();
			case ConditionsPackage.EVALUATION_RESULT: return createEvaluationResult();
			case ConditionsPackage.REFINEMENT_TEMPLATE: return createRefinementTemplate();
			case ConditionsPackage.OPTION_GROUP: return createOptionGroup();
			case ConditionsPackage.NON_EXISTENCE_GROUP: return createNonExistenceGroup();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ConditionsPackage.CONDITION_TYPE:
				return createConditionTypeFromString(eDataType, initialValue);
			case ConditionsPackage.STATE:
				return createStateFromString(eDataType, initialValue);
			case ConditionsPackage.MODEL:
				return createModelFromString(eDataType, initialValue);
			case ConditionsPackage.EVALUATION_STATUS:
				return createEvaluationStatusFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ConditionsPackage.CONDITION_TYPE:
				return convertConditionTypeToString(eDataType, instanceValue);
			case ConditionsPackage.STATE:
				return convertStateToString(eDataType, instanceValue);
			case ConditionsPackage.MODEL:
				return convertModelToString(eDataType, instanceValue);
			case ConditionsPackage.EVALUATION_STATUS:
				return convertEvaluationStatusToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template createTemplate() {
		TemplateImpl template = new TemplateImpl();
		return template;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureCondition createFeatureCondition() {
		FeatureConditionImpl featureCondition = new FeatureConditionImpl();
		return featureCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeCorrespondenceCondition createAttributeCorrespondenceCondition() {
		AttributeCorrespondenceConditionImpl attributeCorrespondenceCondition = new AttributeCorrespondenceConditionImpl();
		return attributeCorrespondenceCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomCondition createCustomCondition() {
		CustomConditionImpl customCondition = new CustomConditionImpl();
		return customCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsModel createConditionsModel() {
		ConditionsModelImpl conditionsModel = new ConditionsModelImpl();
		return conditionsModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationResult createEvaluationResult() {
		EvaluationResultImpl evaluationResult = new EvaluationResultImpl();
		return evaluationResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RefinementTemplate createRefinementTemplate() {
		RefinementTemplateImpl refinementTemplate = new RefinementTemplateImpl();
		return refinementTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptionGroup createOptionGroup() {
		OptionGroupImpl optionGroup = new OptionGroupImpl();
		return optionGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonExistenceGroup createNonExistenceGroup() {
		NonExistenceGroupImpl nonExistenceGroup = new NonExistenceGroupImpl();
		return nonExistenceGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionType createConditionTypeFromString(EDataType eDataType, String initialValue) {
		ConditionType result = ConditionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertConditionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State createStateFromString(EDataType eDataType, String initialValue) {
		State result = State.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model createModelFromString(EDataType eDataType, String initialValue) {
		Model result = Model.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertModelToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationStatus createEvaluationStatusFromString(EDataType eDataType, String initialValue) {
		EvaluationStatus result = EvaluationStatus.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEvaluationStatusToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsPackage getConditionsPackage() {
		return (ConditionsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ConditionsPackage getPackage() {
		return ConditionsPackage.eINSTANCE;
	}

} //ConditionsFactoryImpl
