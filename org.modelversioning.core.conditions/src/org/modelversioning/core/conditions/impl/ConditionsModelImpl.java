/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.NonExistenceGroup;
import org.modelversioning.core.conditions.OptionGroup;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getRootTemplate <em>Root Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getCreatedAt <em>Created At</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getModelName <em>Model Name</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getOptionGroups <em>Option Groups</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionsModelImpl#getNonExistenceGroups <em>Non Existence Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConditionsModelImpl extends EObjectImpl implements ConditionsModel {
	/**
	 * The cached value of the '{@link #getRootTemplate() <em>Root Template</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootTemplate()
	 * @generated
	 * @ordered
	 */
	protected Template rootTemplate;

	/**
	 * The default value of the '{@link #getCreatedAt() <em>Created At</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreatedAt()
	 * @generated
	 * @ordered
	 */
	protected static final Date CREATED_AT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCreatedAt() <em>Created At</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreatedAt()
	 * @generated
	 * @ordered
	 */
	protected Date createdAt = CREATED_AT_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected String modelName = MODEL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String LANGUAGE_EDEFAULT = "OCL";

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected String language = LANGUAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOptionGroups() <em>Option Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptionGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<OptionGroup> optionGroups;

	/**
	 * The cached value of the '{@link #getNonExistenceGroups() <em>Non Existence Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNonExistenceGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<NonExistenceGroup> nonExistenceGroups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionsModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.Literals.CONDITIONS_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getRootTemplate() {
		return rootTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootTemplate(Template newRootTemplate, NotificationChain msgs) {
		Template oldRootTemplate = rootTemplate;
		rootTemplate = newRootTemplate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE, oldRootTemplate, newRootTemplate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootTemplate(Template newRootTemplate) {
		if (newRootTemplate != rootTemplate) {
			NotificationChain msgs = null;
			if (rootTemplate != null)
				msgs = ((InternalEObject)rootTemplate).eInverseRemove(this, ConditionsPackage.TEMPLATE__CONTAINING_MODEL, Template.class, msgs);
			if (newRootTemplate != null)
				msgs = ((InternalEObject)newRootTemplate).eInverseAdd(this, ConditionsPackage.TEMPLATE__CONTAINING_MODEL, Template.class, msgs);
			msgs = basicSetRootTemplate(newRootTemplate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE, newRootTemplate, newRootTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreatedAt(Date newCreatedAt) {
		Date oldCreatedAt = createdAt;
		createdAt = newCreatedAt;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_MODEL__CREATED_AT, oldCreatedAt, createdAt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelName(String newModelName) {
		String oldModelName = modelName;
		modelName = newModelName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_MODEL__MODEL_NAME, oldModelName, modelName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLanguage(String newLanguage) {
		String oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_MODEL__LANGUAGE, oldLanguage, language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptionGroup> getOptionGroups() {
		if (optionGroups == null) {
			optionGroups = new EObjectContainmentEList<OptionGroup>(OptionGroup.class, this, ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS);
		}
		return optionGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NonExistenceGroup> getNonExistenceGroups() {
		if (nonExistenceGroups == null) {
			nonExistenceGroups = new EObjectContainmentEList<NonExistenceGroup>(NonExistenceGroup.class, this, ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS);
		}
		return nonExistenceGroups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				if (rootTemplate != null)
					msgs = ((InternalEObject)rootTemplate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE, null, msgs);
				return basicSetRootTemplate((Template)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				return basicSetRootTemplate(null, msgs);
			case ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS:
				return ((InternalEList<?>)getOptionGroups()).basicRemove(otherEnd, msgs);
			case ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS:
				return ((InternalEList<?>)getNonExistenceGroups()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				return getRootTemplate();
			case ConditionsPackage.CONDITIONS_MODEL__CREATED_AT:
				return getCreatedAt();
			case ConditionsPackage.CONDITIONS_MODEL__MODEL_NAME:
				return getModelName();
			case ConditionsPackage.CONDITIONS_MODEL__LANGUAGE:
				return getLanguage();
			case ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS:
				return getOptionGroups();
			case ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS:
				return getNonExistenceGroups();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				setRootTemplate((Template)newValue);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__CREATED_AT:
				setCreatedAt((Date)newValue);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__MODEL_NAME:
				setModelName((String)newValue);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__LANGUAGE:
				setLanguage((String)newValue);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS:
				getOptionGroups().clear();
				getOptionGroups().addAll((Collection<? extends OptionGroup>)newValue);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS:
				getNonExistenceGroups().clear();
				getNonExistenceGroups().addAll((Collection<? extends NonExistenceGroup>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				setRootTemplate((Template)null);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__CREATED_AT:
				setCreatedAt(CREATED_AT_EDEFAULT);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__MODEL_NAME:
				setModelName(MODEL_NAME_EDEFAULT);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__LANGUAGE:
				setLanguage(LANGUAGE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS:
				getOptionGroups().clear();
				return;
			case ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS:
				getNonExistenceGroups().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_MODEL__ROOT_TEMPLATE:
				return rootTemplate != null;
			case ConditionsPackage.CONDITIONS_MODEL__CREATED_AT:
				return CREATED_AT_EDEFAULT == null ? createdAt != null : !CREATED_AT_EDEFAULT.equals(createdAt);
			case ConditionsPackage.CONDITIONS_MODEL__MODEL_NAME:
				return MODEL_NAME_EDEFAULT == null ? modelName != null : !MODEL_NAME_EDEFAULT.equals(modelName);
			case ConditionsPackage.CONDITIONS_MODEL__LANGUAGE:
				return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
			case ConditionsPackage.CONDITIONS_MODEL__OPTION_GROUPS:
				return optionGroups != null && !optionGroups.isEmpty();
			case ConditionsPackage.CONDITIONS_MODEL__NON_EXISTENCE_GROUPS:
				return nonExistenceGroups != null && !nonExistenceGroups.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (createdAt: ");
		result.append(createdAt);
		result.append(", modelName: ");
		result.append(modelName);
		result.append(", language: ");
		result.append(language);
		result.append(')');
		return result.toString();
	}

} //ConditionsModelImpl
