/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionType;
import org.modelversioning.core.conditions.ConditionsPackage;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionImpl#getState <em>State</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionImpl#isActive <em>Active</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionImpl#getTemplate <em>Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.impl.ConditionImpl#isInvolvesTemplate <em>Involves Template</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ConditionImpl extends EObjectImpl implements Condition {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final ConditionType TYPE_EDEFAULT = ConditionType.ONTOLOGICAL;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected ConditionType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final State STATE_EDEFAULT = State.GENERATED;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state = STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACTIVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected boolean active = ACTIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #isInvolvesTemplate() <em>Involves Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInvolvesTemplate()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INVOLVES_TEMPLATE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isInvolvesTemplate() <em>Involves Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInvolvesTemplate()
	 * @generated
	 * @ordered
	 */
	protected boolean involvesTemplate = INVOLVES_TEMPLATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.Literals.CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(ConditionType newType) {
		ConditionType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActive(boolean newActive) {
		boolean oldActive = active;
		active = newActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION__ACTIVE, oldActive, active));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Template getTemplate() {
		if (eContainerFeatureID() != ConditionsPackage.CONDITION__TEMPLATE) return null;
		return (Template)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInvolvesTemplate() {
		return involvesTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInvolvesTemplate(boolean newInvolvesTemplate) {
		boolean oldInvolvesTemplate = involvesTemplate;
		involvesTemplate = newInvolvesTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION__INVOLVES_TEMPLATE, oldInvolvesTemplate, involvesTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TEMPLATE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return eBasicSetContainer(otherEnd, ConditionsPackage.CONDITION__TEMPLATE, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TEMPLATE:
				return eBasicSetContainer(null, ConditionsPackage.CONDITION__TEMPLATE, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ConditionsPackage.CONDITION__TEMPLATE:
				return eInternalContainer().eInverseRemove(this, ConditionsPackage.TEMPLATE__SPECIFICATIONS, Template.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TYPE:
				return getType();
			case ConditionsPackage.CONDITION__STATE:
				return getState();
			case ConditionsPackage.CONDITION__ACTIVE:
				return isActive();
			case ConditionsPackage.CONDITION__TEMPLATE:
				return getTemplate();
			case ConditionsPackage.CONDITION__INVOLVES_TEMPLATE:
				return isInvolvesTemplate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TYPE:
				setType((ConditionType)newValue);
				return;
			case ConditionsPackage.CONDITION__STATE:
				setState((State)newValue);
				return;
			case ConditionsPackage.CONDITION__ACTIVE:
				setActive((Boolean)newValue);
				return;
			case ConditionsPackage.CONDITION__INVOLVES_TEMPLATE:
				setInvolvesTemplate((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION__STATE:
				setState(STATE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION__ACTIVE:
				setActive(ACTIVE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION__INVOLVES_TEMPLATE:
				setInvolvesTemplate(INVOLVES_TEMPLATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITION__TYPE:
				return type != TYPE_EDEFAULT;
			case ConditionsPackage.CONDITION__STATE:
				return state != STATE_EDEFAULT;
			case ConditionsPackage.CONDITION__ACTIVE:
				return active != ACTIVE_EDEFAULT;
			case ConditionsPackage.CONDITION__TEMPLATE:
				return getTemplate() != null;
			case ConditionsPackage.CONDITION__INVOLVES_TEMPLATE:
				return involvesTemplate != INVOLVES_TEMPLATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", state: ");
		result.append(state);
		result.append(", active: ");
		result.append(active);
		result.append(", involvesTemplate: ");
		result.append(involvesTemplate);
		result.append(')');
		return result.toString();
	}

} //ConditionImpl
