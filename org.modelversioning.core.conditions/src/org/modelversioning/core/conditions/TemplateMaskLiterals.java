/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions;

/**
 * Contains constants for masking templates in condition expressions.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateMaskLiterals {

	/**
	 * Used to start template mask.
	 */
	public static final String SINGLE_TEMPLATE_MASK_START = "#{";

	/**
	 * Used to end template mask
	 */
	public static final String SINGLE_TEMPLATE_MASK_END = "}";

	/**
	 * Separator for prefixes for template masks.
	 */
	public static final String PREFIX_SEP = ":";

	/**
	 * Pattern for finding masked templates in a {@link String}.
	 */
	public static final String SINGLE_TEMPLATE_FIND_REGEX = "(#\\{([a-zA-Z_0-9]*)\\})";

	/**
	 * Pattern for finding prefixed, masked templates in a {@link String}.
	 */
	public static final String SINGLE_PREFIXED_TEMPLATE_FIND_REGEX = "(#\\{([a-zA-Z_0-9]*"
			+ PREFIX_SEP + "[a-zA-Z_0-9]*)\\})";

	/**
	 * Pattern for finding unclosed masked (prefixed or not) templates in a
	 * {@link String}.
	 */
	public static final String SINGLE_TEMPLATE_UNCLOSED_FIND_REGES = "#\\{?([a-zA-Z_0-9]*:?[a-zA-Z_0-9]*)$";

}
