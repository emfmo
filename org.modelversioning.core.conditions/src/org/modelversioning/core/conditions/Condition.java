/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A Condition can be <code>true</code> or <code>false</code> for a specific EObject to evaluate. It specifies a template.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.Condition#getType <em>Type</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Condition#getState <em>State</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Condition#isActive <em>Active</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Condition#getTemplate <em>Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.Condition#isInvolvesTemplate <em>Involves Template</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition()
 * @model abstract="true"
 * @generated
 */
public interface Condition extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.core.conditions.ConditionType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies the type of this condition. E.g. the condition is ontological or linguistic.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see org.modelversioning.core.conditions.ConditionType
	 * @see #setType(ConditionType)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition_Type()
	 * @model required="true"
	 * @generated
	 */
	ConditionType getType();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Condition#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see org.modelversioning.core.conditions.ConditionType
	 * @see #getType()
	 * @generated
	 */
	void setType(ConditionType value);

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.core.conditions.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.core.conditions.State
	 * @see #setState(State)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition_State()
	 * @model required="true"
	 * @generated
	 */
	State getState();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Condition#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.core.conditions.State
	 * @see #getState()
	 * @generated
	 */
	void setState(State value);

	/**
	 * Returns the value of the '<em><b>Active</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active</em>' attribute.
	 * @see #setActive(boolean)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition_Active()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isActive();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Condition#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active</em>' attribute.
	 * @see #isActive()
	 * @generated
	 */
	void setActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Template</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Template#getSpecifications <em>Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template</em>' container reference.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition_Template()
	 * @see org.modelversioning.core.conditions.Template#getSpecifications
	 * @model opposite="specifications" unsettable="true" required="true" transient="false" changeable="false"
	 * @generated
	 */
	Template getTemplate();

	/**
	 * Returns the value of the '<em><b>Involves Template</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies whether this condition involves (e.g. references or uses) a {@link Symbol}.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Involves Template</em>' attribute.
	 * @see #setInvolvesTemplate(boolean)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getCondition_InvolvesTemplate()
	 * @model default="true"
	 * @generated
	 */
	boolean isInvolvesTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.Condition#isInvolvesTemplate <em>Involves Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Involves Template</em>' attribute.
	 * @see #isInvolvesTemplate()
	 * @generated
	 */
	void setInvolvesTemplate(boolean value);

} // Condition
