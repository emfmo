/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Option Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.OptionGroup#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.OptionGroup#getTemplates <em>Templates</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.OptionGroup#isReplace <em>Replace</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getOptionGroup()
 * @model
 * @generated
 */
public interface OptionGroup extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getOptionGroup_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.OptionGroup#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Templates</b></em>' reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.Template}.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Template#getOptionGroups <em>Option Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Templates</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Templates</em>' reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getOptionGroup_Templates()
	 * @see org.modelversioning.core.conditions.Template#getOptionGroups
	 * @model opposite="optionGroups"
	 * @generated
	 */
	EList<Template> getTemplates();

	/**
	 * Returns the value of the '<em><b>Replace</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Replace</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Replace</em>' attribute.
	 * @see #setReplace(boolean)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getOptionGroup_Replace()
	 * @model default="true"
	 * @generated
	 */
	boolean isReplace();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.OptionGroup#isReplace <em>Replace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Replace</em>' attribute.
	 * @see #isReplace()
	 * @generated
	 */
	void setReplace(boolean value);

} // OptionGroup
