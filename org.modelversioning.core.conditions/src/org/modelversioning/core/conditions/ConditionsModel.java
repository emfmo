/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * $Id$
 */
package org.modelversioning.core.conditions;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Container for a model of conditions.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getRootTemplate <em>Root Template</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getCreatedAt <em>Created At</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getModelName <em>Model Name</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getOptionGroups <em>Option Groups</em>}</li>
 *   <li>{@link org.modelversioning.core.conditions.ConditionsModel#getNonExistenceGroups <em>Non Existence Groups</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel()
 * @model
 * @generated
 */
public interface ConditionsModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Root Template</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.core.conditions.Template#getContainingModel <em>Containing Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Template</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Template</em>' containment reference.
	 * @see #setRootTemplate(Template)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_RootTemplate()
	 * @see org.modelversioning.core.conditions.Template#getContainingModel
	 * @model opposite="containingModel" containment="true" required="true"
	 * @generated
	 */
	Template getRootTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.ConditionsModel#getRootTemplate <em>Root Template</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Template</em>' containment reference.
	 * @see #getRootTemplate()
	 * @generated
	 */
	void setRootTemplate(Template value);

	/**
	 * Returns the value of the '<em><b>Created At</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created At</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Created At</em>' attribute.
	 * @see #setCreatedAt(Date)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_CreatedAt()
	 * @model
	 * @generated
	 */
	Date getCreatedAt();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.ConditionsModel#getCreatedAt <em>Created At</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Created At</em>' attribute.
	 * @see #getCreatedAt()
	 * @generated
	 */
	void setCreatedAt(Date value);

	/**
	 * Returns the value of the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Name</em>' attribute.
	 * @see #setModelName(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_ModelName()
	 * @model
	 * @generated
	 */
	String getModelName();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.ConditionsModel#getModelName <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Name</em>' attribute.
	 * @see #getModelName()
	 * @generated
	 */
	void setModelName(String value);

	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * The default value is <code>"OCL"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies the language in which conditions are represented.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(String)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_Language()
	 * @model default="OCL"
	 * @generated
	 */
	String getLanguage();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.ConditionsModel#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Option Groups</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.OptionGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Option Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Option Groups</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_OptionGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<OptionGroup> getOptionGroups();

	/**
	 * Returns the value of the '<em><b>Non Existence Groups</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.core.conditions.NonExistenceGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Non Existence Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Existence Groups</em>' containment reference list.
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getConditionsModel_NonExistenceGroups()
	 * @model containment="true"
	 * @generated
	 */
	EList<NonExistenceGroup> getNonExistenceGroups();

} // ConditionsModel
