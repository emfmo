/**
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Refinement Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.core.conditions.RefinementTemplate#getRefinedTemplate <em>Refined Template</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.core.conditions.ConditionsPackage#getRefinementTemplate()
 * @model
 * @generated
 */
public interface RefinementTemplate extends Template {
	/**
	 * Returns the value of the '<em><b>Refined Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refined Template</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refined Template</em>' reference.
	 * @see #setRefinedTemplate(Template)
	 * @see org.modelversioning.core.conditions.ConditionsPackage#getRefinementTemplate_RefinedTemplate()
	 * @model
	 * @generated
	 */
	Template getRefinedTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.core.conditions.RefinementTemplate#getRefinedTemplate <em>Refined Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refined Template</em>' reference.
	 * @see #getRefinedTemplate()
	 * @generated
	 */
	void setRefinedTemplate(Template value);

} // RefinementTemplate
