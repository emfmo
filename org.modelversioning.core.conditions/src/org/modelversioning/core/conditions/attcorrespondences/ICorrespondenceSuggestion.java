/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.attcorrespondences;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.Template;

/**
 * A suggestion to replace a fixed value in a condition by reference to an
 * attribute's value of another template.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ICorrespondenceSuggestion extends
		Comparable<ICorrespondenceSuggestion> {

	/**
	 * Returns the referenced template(s).
	 * 
	 * @return the referenced templates.
	 */
	public List<Template> getReferencedTemplates();

	/**
	 * Returns the referenced feature(s).
	 * 
	 * @return the referenced feature(s).
	 */
	public List<EStructuralFeature> getReferencedFeatures();

	/**
	 * Returns the template for which the suggestion suggests a correspondence.
	 * 
	 * @return the template for which the suggestion suggests a correspondence.
	 */
	public Template getTemplate();

	/**
	 * Returns the condition containing the expression for which the suggestion
	 * suggests a correspondence.
	 * 
	 * @return the condition containing the expression for which the suggestion
	 *         suggests a correspondence.
	 */
	public Condition getCondition();

	/**
	 * Returns the expression with the replacement (by a template reference) for
	 * a fixed value.
	 * 
	 * @return the replaced expression.
	 */
	public String getReplacedExpression();

	/**
	 * Returns the importance of this suggestion.
	 * 
	 * @return the importance.
	 */
	public double getImportance();

	/**
	 * Returns the description clarifying this correspondence suggestion.
	 * 
	 * @return the description.
	 */
	public String getDescription();

	/**
	 * Applies this suggestion to the condition.
	 */
	public void apply();

}
