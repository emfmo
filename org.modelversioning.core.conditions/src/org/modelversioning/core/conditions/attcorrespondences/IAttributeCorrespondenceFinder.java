/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.attcorrespondences;

import java.util.List;

import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.Template;

/**
 * Finds value correspondences between attributes to replace fixed values in
 * conditions by references to other template's attributes.
 * 
 * To use a finder, the #in
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IAttributeCorrespondenceFinder {

	/**
	 * Initializes this finder for one condition model. That means, that
	 * suggestions can be created for references within this
	 * {@link ConditionsModel} itself.
	 * 
	 * @param conditionsModel
	 *            to find attribute correspondences in.
	 */
	public void initialize(ConditionsModel conditionsModel);

	/**
	 * Initializes this finder to generate {@link ICorrespondenceSuggestion} for
	 * correspondences in {@link Condition}s in <code>conditionsModel</code> to
	 * <code>referencedConditionsModel</code> using the specified
	 * <code>prefix</code>.
	 * 
	 * @param conditionsModel
	 *            to find attribute correspondences in.
	 * @param referencedConditionsModel
	 *            which will be referenced by the correspondence suggestions.
	 * @param prefix
	 *            prefix to use for {@link Template} references in
	 *            <code>referencedConditionsModel</code>.
	 */
	public void initialize(ConditionsModel conditionsModel,
			ConditionsModel referencedConditionsModel, String prefix);

	/**
	 * Returns all available suggestions for correspondences.
	 * 
	 * @return all available suggestions.
	 */
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions();

	/**
	 * Returns available suggestions for the specified <code>template</code>.
	 * 
	 * @param template
	 *            to get suggestions for.
	 * @return available suggestions for <code>template</code>.
	 */
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions(
			Template template);

	/**
	 * Returns available suggestions for the specified <code>condition</code>.
	 * 
	 * @param condition
	 *            to get suggestions for.
	 * @return available suggestions for <code>condition</code>.
	 */
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions(
			Condition condition);

}
