/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.attcorrespondences.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.modelversioning.core.conditions.AttributeCorrespondenceCondition;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.attcorrespondences.ICorrespondenceSuggestion;

/**
 * Implements {@link ICorrespondenceSuggestion}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class CorrespondenceSuggestionImpl implements ICorrespondenceSuggestion {

	private double importance = 0;

	private Condition condition = null;

	private List<Template> referencedTemplates = new ArrayList<Template>();

	private List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();

	private Template template = null;

	private String replacedExpression = null;

	private String description = null;

	private boolean applied = false;

	/**
	 * Creates a new, empty {@link CorrespondenceSuggestionImpl}.
	 */
	protected CorrespondenceSuggestionImpl() {
		// protected constructor.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void apply() {
		Assert.isTrue(condition != null,
				"Cannot apply suggestion to null condition");
		Assert.isTrue(condition instanceof FeatureCondition
				|| condition instanceof CustomCondition,
				"Unknown condition type");
		if (!applied) {
			EcoreUtil.remove(condition);
			template.getSpecifications().add(
					generateAttributeCorrespondenceCondition());
			applied = true;
		}
	}

	/**
	 * Generates the {@link AttributeCorrespondenceCondition} to replace the
	 * current condition with.
	 * 
	 * @return the generated {@link AttributeCorrespondenceCondition}.
	 */
	private AttributeCorrespondenceCondition generateAttributeCorrespondenceCondition() {
		AttributeCorrespondenceCondition attributeCorrespondenceCondition = ConditionsFactory.eINSTANCE
				.createAttributeCorrespondenceCondition();
		attributeCorrespondenceCondition.setActive(true);
		attributeCorrespondenceCondition.setInvolvesTemplate(true);
		attributeCorrespondenceCondition.setState(condition.getState());
		attributeCorrespondenceCondition.setType(condition.getType());
		attributeCorrespondenceCondition.setExpression(replacedExpression);
		if (condition instanceof FeatureCondition) {
			attributeCorrespondenceCondition
					.setFeature(((FeatureCondition) condition).getFeature());
		} else if (this.features.size() > 0) {
			attributeCorrespondenceCondition.setFeature(this.features.get(0));
		}
		attributeCorrespondenceCondition.getCorrespondingFeatures().addAll(
				this.getReferencedFeatures());
		attributeCorrespondenceCondition.getCorrespondingTemplates().addAll(
				this.getReferencedTemplates());
		return attributeCorrespondenceCondition;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Condition getCondition() {
		return condition;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getImportance() {
		return importance;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Template> getReferencedTemplates() {
		return referencedTemplates;
	}

	/**
	 * @return the features
	 */
	public List<EStructuralFeature> getReferencedFeatures() {
		return features;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getReplacedExpression() {
		return replacedExpression;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Template getTemplate() {
		return template;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * @param importance
	 *            the importance to set
	 */
	protected void setImportance(double importance) {
		this.importance = importance;
	}

	/**
	 * @param condition
	 *            the condition to set
	 */
	protected void setCondition(Condition condition) {
		this.condition = condition;
	}

	/**
	 * @param referencedTemplates
	 *            the referencedTemplates to set
	 */
	protected void setReferencedTemplates(List<Template> referencedTemplates) {
		this.referencedTemplates = referencedTemplates;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	protected void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * @param replacedExpression
	 *            the replacedExpression to set
	 */
	protected void setReplacedExpression(String replacedExpression) {
		this.replacedExpression = replacedExpression;
	}

	/**
	 * @param description
	 *            the descirption to set
	 */
	protected void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Implemented using the importance ({@link #getImportance()}).
	 */
	@Override
	public int compareTo(ICorrespondenceSuggestion o) {
		if (this.getImportance() == o.getImportance()) {
			return 0;
		} else if (this.getImportance() > o.getImportance()) {
			return 1;
		} else {
			return -1;
		}
	}

}
