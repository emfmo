/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.attcorrespondences.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.TemplateMaskLiterals;
import org.modelversioning.core.conditions.attcorrespondences.IAttributeCorrespondenceFinder;
import org.modelversioning.core.conditions.attcorrespondences.ICorrespondenceSuggestion;
import org.modelversioning.core.conditions.engines.impl.OCLLiterals;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Implements the {@link IAttributeCorrespondenceFinder} by a simple string
 * matching technique.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class AttributeCorrespondenceFinderImpl implements
		IAttributeCorrespondenceFinder {

	/**
	 * The suggestion list.
	 */
	private List<ICorrespondenceSuggestion> suggestions = new ArrayList<ICorrespondenceSuggestion>();

	/**
	 * String finder pattern.
	 */
	private static Pattern stringFinderPattern = Pattern
			.compile(OCLLiterals.APO + "[\\w]*" + OCLLiterals.APO); //$NON-NLS-1$

	private FindMode mode = FindMode.SINGLE_MODEL;

	private String prefix = null;

	private ConditionsModel conditionsModel = null;

	private ConditionsModel referencedConditionsModel = null;

	private Map<String, List<StringTemplateFeature>> stringToFeatureMap = new HashMap<String, List<StringTemplateFeature>>();

	private enum FindMode {
		SINGLE_MODEL, DUAL_MODEL
	}

	/**
	 * Returns whether this finder is in Single Mode (one
	 * {@link ConditionsModel}).
	 * 
	 * @return <code>true</code> if in Single Mode. Otherwise,
	 *         <code>false</code>.
	 */
	protected boolean isInSingleMode() {
		return this.mode.equals(FindMode.SINGLE_MODEL);
	}

	/**
	 * Returns whether this finder is in Dual Mode (two {@link ConditionsModel}
	 * s).
	 * 
	 * @return <code>true</code> if in Dual Mode. Otherwise, <code>false</code>.
	 */
	protected boolean isInDualMode() {
		return this.mode.equals(FindMode.DUAL_MODEL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions() {
		return Collections.unmodifiableList(suggestions);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions(
			Template template) {
		return Collections.unmodifiableList(filterByTemplate(suggestions,
				template));
	}

	/**
	 * Filters the specified {@link List} of {@link ICorrespondenceSuggestion}
	 * by {@link Template}.
	 * 
	 * @param suggestions
	 *            the list.
	 * @param template
	 *            the {@link Template} to filter by.
	 * @return the filtered list.
	 */
	private List<? extends ICorrespondenceSuggestion> filterByTemplate(
			List<ICorrespondenceSuggestion> suggestions, Template template) {
		List<ICorrespondenceSuggestion> list = new ArrayList<ICorrespondenceSuggestion>();
		for (ICorrespondenceSuggestion suggestion : suggestions) {
			if (template.equals(suggestion.getTemplate())) {
				list.add(suggestion);
			}
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ICorrespondenceSuggestion> getCorrespondenceSuggestions(
			Condition condition) {
		return Collections.unmodifiableList(filterByCondition(suggestions,
				condition));
	}

	/**
	 * Filters the specified {@link List} of {@link ICorrespondenceSuggestion}
	 * by {@link Condition}.
	 * 
	 * @param suggestions
	 *            the list.
	 * @param condition
	 *            the {@link Condition} to filter by.
	 * @return the filtered list.
	 */
	private List<? extends ICorrespondenceSuggestion> filterByCondition(
			List<ICorrespondenceSuggestion> suggestions, Condition condition) {
		List<ICorrespondenceSuggestion> list = new ArrayList<ICorrespondenceSuggestion>();
		for (ICorrespondenceSuggestion suggestion : suggestions) {
			if (condition.equals(suggestion.getCondition())) {
				list.add(suggestion);
			}
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(ConditionsModel conditionsModel) {
		// set values and the find mode
		this.conditionsModel = conditionsModel;
		this.referencedConditionsModel = null;
		this.prefix = null;
		this.mode = FindMode.SINGLE_MODEL;

		generateStringToFeatureMap(this.conditionsModel);
		generateSuggestions(this.conditionsModel);

		sortSuggestions();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize(ConditionsModel conditionsModel,
			ConditionsModel referencedConditionsModel, String prefix) {
		// set values and the find mode
		this.conditionsModel = conditionsModel;
		this.referencedConditionsModel = referencedConditionsModel;
		this.prefix = prefix;
		this.mode = FindMode.DUAL_MODEL;

		generateStringToFeatureMap(this.referencedConditionsModel);
		generateSuggestions(this.conditionsModel);

		sortSuggestions();
	}

	/**
	 * Generates the suggestions for the specified <code>conditionsModel</code>
	 * against the currently set {@link #stringToFeatureMap}.
	 * 
	 * @param conditionsModel
	 *            {@link ConditionsModel} to process.
	 */
	private void generateSuggestions(ConditionsModel conditionsModel) {
		generateSuggestions(conditionsModel.getRootTemplate());
	}

	/**
	 * Generates recursively the suggestions for the specified
	 * <code>template</code> and its sub templates against the currently set
	 * {@link #stringToFeatureMap}.
	 * 
	 * @param template
	 *            {@link Template} to process.
	 */
	private void generateSuggestions(Template template) {
		for (Condition condition : template.getSpecifications()) {
			// get expression of each condition
			String expression = ConditionsUtil.getExpression(condition);
			// find string occurrences
			Matcher matcher = stringFinderPattern.matcher(expression);
			while (matcher.find()) {
				// for each string
				String match = matcher.group();
				String string = match.substring(1, match.length() - 1);
				List<StringTemplateFeature> stringToFeatures = getStringToFeatures(string);
				// find StringTemplateFeature
				for (StringTemplateFeature stringTemplateFeature : stringToFeatures) {

					// create replaced expression
					String replacement = TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START;
					if (this.mode.equals(FindMode.DUAL_MODEL)) {
						replacement += prefix;
						replacement += TemplateMaskLiterals.PREFIX_SEP;
					}
					replacement += stringTemplateFeature.getTemplate()
							.getName();
					replacement += TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
							+ OCLLiterals.DOT
							+ stringTemplateFeature.getFeature().getName();

					String replacedExpression = expression.replace(match,
							replacement);

					// generate suggestion
					CorrespondenceSuggestionImpl suggestion = new CorrespondenceSuggestionImpl();
					suggestion.setTemplate(template);
					suggestion.setCondition(condition);
					suggestion.getReferencedTemplates().add(
							stringTemplateFeature.getTemplate());
					suggestion.getReferencedFeatures().add(
							stringTemplateFeature.getFeature());
					suggestion.setImportance(1d);
					suggestion.setDescription("Replace " + match
							+ " with a reference to the feature '"
							+ stringTemplateFeature.getFeature().getName()
							+ "' of the template '"
							+ stringTemplateFeature.getTemplate().getTitle()
							+ "'.");
					suggestion.setReplacedExpression(replacedExpression);
					suggestions.add(suggestion);
				}
			}
		}

		// process sub templates
		if (!template.getSubTemplates().isEmpty()) {
			for (Template subTemplate : template.getSubTemplates()) {
				generateSuggestions(subTemplate);
			}
		}
	}

	/**
	 * Generates the {@link #stringToFeatureMap}.
	 * 
	 * @param conditionsModel
	 *            to generate the map of.
	 */
	private void generateStringToFeatureMap(ConditionsModel conditionsModel) {
		stringToFeatureMap.clear();
		Template rootTemplate = conditionsModel.getRootTemplate();
		generateStringToFeatureMap(rootTemplate);
	}

	/**
	 * Recursively fills the {@link #stringToFeatureMap} with the specified
	 * <code>template</code> and its sub templates.
	 * 
	 * @param template
	 *            template to fill the map with.
	 */
	private void generateStringToFeatureMap(Template template) {
		EClass eClass = template.getRepresentative().eClass();
		for (EStructuralFeature feature : eClass.getEAllStructuralFeatures()) {
			// if feature type is string
			EClassifier eRawType = feature.getEGenericType().getERawType();

			if (eRawType instanceof EDataType
					&& eRawType.getName().equals("EString")) { //$NON-NLS-1$
				// get value of representative
				String value = (String) template.getRepresentative().eGet(
						feature);
				if (value != null) {
					// add value to feature mapping
					addStringToFeature(new StringTemplateFeature(value,
							feature, template));
				}
			}
		}
		// process sub templates if available
		if (!template.getSubTemplates().isEmpty()) {
			for (Template subTemplate : template.getSubTemplates()) {
				generateStringToFeatureMap(subTemplate);
			}
		}
	}

	/**
	 * Adds a {@link StringTemplateFeature} to the {@link #stringToFeatureMap}.
	 * 
	 * @param stringTemplateFeature
	 *            to add.
	 */
	private void addStringToFeature(StringTemplateFeature stringTemplateFeature) {
		if (this.stringToFeatureMap.get(stringTemplateFeature.getString()) == null) {
			this.stringToFeatureMap.put(stringTemplateFeature.getString(),
					new ArrayList<StringTemplateFeature>());
		}
		this.stringToFeatureMap.get(stringTemplateFeature.getString()).add(
				stringTemplateFeature);
	}

	/**
	 * Returns the {@link StringTemplateFeature}s for the specified
	 * <code>string</code>.
	 * 
	 * @param string
	 *            to get {@link StringTemplateFeature} for.
	 * @return the {@link StringTemplateFeature}s.
	 */
	private List<StringTemplateFeature> getStringToFeatures(String string) {
		if (this.stringToFeatureMap.get(string) == null) {
			return Collections.emptyList();
		} else {
			return this.stringToFeatureMap.get(string);
		}
	}

	/**
	 * Sorts the suggestion list.
	 */
	private void sortSuggestions() {
		Collections.sort(suggestions);
	}

}
