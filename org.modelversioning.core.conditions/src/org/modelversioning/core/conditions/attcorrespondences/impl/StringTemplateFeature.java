/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.attcorrespondences.impl;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.modelversioning.core.conditions.Template;

/**
 * A {@link EStructuralFeature} of a {@link Template}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class StringTemplateFeature {
	
	private String string;
	private EStructuralFeature feature;
	private Template template;
	
	/**
	 * Empty constructor.
	 */
	protected StringTemplateFeature() {
		super();
	}
	/**
	 * Creates a new {@link StringTemplateFeature} setting the string, the feature and the template.
	 * @param string string to set.
	 * @param feature feature of the template to set.
	 * @param template template to set.
	 */
	protected StringTemplateFeature(String string, EStructuralFeature feature, Template template) {
		super();
		this.string = string;
		this.feature = feature;
		this.template = template;
	}
	/**
	 * @return the feature
	 */
	protected EStructuralFeature getFeature() {
		return feature;
	}
	/**
	 * @param feature the feature to set
	 */
	protected void setFeature(EStructuralFeature feature) {
		this.feature = feature;
	}
	/**
	 * @return the template
	 */
	protected Template getTemplate() {
		return template;
	}
	/**
	 * @param template the template to set
	 */
	protected void setTemplate(Template template) {
		this.template = template;
	}
	/**
	 * @return the string
	 */
	protected String getString() {
		return string;
	}
	/**
	 * @param string the string to set
	 */
	protected void setString(String string) {
		this.string = string;
	}

}
