/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ParserException;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.EvaluationStatus;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.BindingException;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Class creating empty {@link ITemplateBinding}s and finding valid
 * {@link ITemplateBindings}.
 * 
 * This class does not inherently regards the containment structure. Only if no
 * prebinding and no candidates are specified for a certain template, the
 * selection of new candidates is based on the containment hierarchy.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateBindingCreator {

	/**
	 * Message if no valid reference binding could be found.
	 */
	private static final String NO_REFERENCE_BINDING = "No reference binding could be found";

	/**
	 * The conditions evaluator instance to use.
	 */
	private final ConditionsEvaluationEngineImpl evaluator;

	/** The detailed evaluation results. */
	private EList<EvaluationResult> evaluationResults = null;

	/**
	 * Constructor setting the {@link ConditionsEvaluationEngineImpl} to use.
	 * 
	 * @param evaluator
	 *            to use.
	 */
	public TemplateBindingCreator(final ConditionsEvaluationEngineImpl evaluator) {
		super();
		this.evaluator = evaluator;
	}

	/**
	 * Creates an empty {@link ITemplateBinding}.
	 * 
	 * @return created {@link ITemplateBinding}.
	 */
	public ITemplateBinding createEmptyTemplateBinding() {
		return new TemplateBindingImpl();
	}

	/**
	 * Finds valid {@link ITemplateBindings} for the specified pre-binding of
	 * <code>eObject</code> to <code>template</code>.
	 * 
	 * @param template
	 *            {@link Template} to pre-bind.
	 * @param eObject
	 *            {@link EObject} to pre-bind.
	 * @return found {@link ITemplateBindings}.
	 * @throws BindingException
	 *             if binding fails.
	 * @throws ParserException
	 *             if condition evaluation fails.
	 */
	public ITemplateBindings findTemplateBindings(final Template template,
			final EObject eObject) throws ParserException, BindingException {
		ITemplateBinding preBinding = createEmptyTemplateBinding();
		preBinding.add(template, eObject);
		return findTemplateBindings(preBinding);
	}

	/**
	 * Finds valid {@link ITemplateBindings} for the specified
	 * <code>preBinding</code>.
	 * 
	 * @param preBinding
	 *            pre-binding.
	 * @return found {@link ITemplateBindings}.
	 * @throws BindingException
	 *             if binding fails.
	 * @throws ParserException
	 *             if condition evaluation fails.
	 */
	public ITemplateBindings findTemplateBindings(
			final ITemplateBinding preBinding) throws ParserException,
			BindingException {
		return findTemplateBindings(preBinding, null);
	}

	/**
	 * Finds valid {@link ITemplateBindings} for the specified
	 * <code>preBinding</code>.
	 * 
	 * @param preBinding
	 *            pre-binding.
	 * @param candidateBinding
	 *            may be used to restrict candidate selection to the specified
	 *            binding. Might be <code>null</code> or empty.
	 * @return found {@link ITemplateBindings}.
	 * @throws BindingException
	 *             if binding fails.
	 * @throws ParserException
	 *             if condition evaluation fails.
	 */
	public ITemplateBindings findTemplateBindings(
			final ITemplateBinding preBinding,
			final ITemplateBinding candidateBinding) throws ParserException,
			BindingException {
		Collection<ITemplateBinding> foundBindings = new ArrayList<ITemplateBinding>();
		evaluationResults = new BasicEList<EvaluationResult>();

		findTemplateBindings(preBinding, createEmptyTemplateBinding(), null,
				candidateBinding, foundBindings);

		TemplateBindingsImpl bindings = new TemplateBindingsImpl(evaluator
				.getConditionsModel().getRootTemplate());

		boolean isSatisfied = foundBindings.size() > 0;
		EvaluationResult evaluationResult = evaluator
				.createEvaluationResult(isSatisfied);
		if (!isSatisfied) {
			evaluationResult.getSubResults().addAll(evaluationResults);
		}
		bindings.setEvaluationResult(evaluationResult);

		bindings.setPossibleBindings(foundBindings);

		return bindings;
	}

	/**
	 * Returns a list of {@link Template Templates} to evaluate.
	 * 
	 * @return list of {@link Template Templates}.
	 */
	private List<Template> getTemplates() {
		return this.evaluator.getTemplatesToEvaluate();
	}

	/**
	 * Finds recursively valid {@link ITemplateBinding}s starting with the
	 * <code>nextTemplate</code> and <code>currentBinding</code> based on the
	 * specified <code>preBinding</code>, the pre-calculated
	 * <code>candidates</code>.
	 * 
	 * @param preBinding
	 *            pre-bound objects.
	 * @param currentBinding
	 *            current binding to find a completed binding from.
	 * @param nextObject
	 *            next template to handle. May be <code>null</code>.
	 * @param candidateBinding
	 *            binding used to select candidates from.
	 * @param foundBindings
	 *            collection to add found bindings to.
	 * @throws ParserException
	 *             if evaluation fails.
	 */
	private void findTemplateBindings(final ITemplateBinding preBinding,
			final ITemplateBinding currentBinding, Template nextTemplate,
			ITemplateBinding candidateBinding,
			final Collection<ITemplateBinding> foundBindings)
			throws ParserException {

		// if nextTemplate is null start with root template
		if (nextTemplate == null) {
			nextTemplate = evaluator.getConditionsModel().getRootTemplate();
		}

		// call recursion for each candidate of next template
		int index = getTemplates().indexOf(nextTemplate);
		Template currentTemplate = getTemplates().get(index);

		// get candidates of current template
		Collection<EObject> currentCandidates = getCurrentCandidates(
				currentTemplate, preBinding, candidateBinding, currentBinding);

		// evaluate referenced candidates for each candidate
		for (EObject currentCandidate : currentCandidates) {
			// set current candidate to current template
			currentBinding.remove(currentTemplate);
			currentBinding.add(currentTemplate, currentCandidate);

			// get valid bindings for current candidate
			try {
				List<ITemplateBinding> referenceBindings = findValidReferenceBindings(
						currentCandidate, currentTemplate, preBinding,
						candidateBinding, currentBinding);
				if (referenceBindings.size() > 0) {
					// recurse with each valid reference binding
					for (ITemplateBinding referenceBinding : referenceBindings) {
						ITemplateBinding newBinding = currentBinding.clone();
						newBinding.add(currentTemplate, currentCandidate);
						newBinding.addAll(referenceBinding);

						// get next template to evaluate
						if (index < getTemplates().size() - 1) {
							Template nextTemplateToEvaluate = getTemplates()
									.get(index + 1);
							// call recursion with new binding
							findTemplateBindings(preBinding, newBinding,
									nextTemplateToEvaluate, candidateBinding,
									foundBindings);
						} else {
							// this is last template so just check for
							// completeness and add it if complete
							if (evaluator.isComplete(currentBinding)) {
								foundBindings.add(newBinding);
							}
						}
					}
				} else {
					// get next template to evaluate
					if (index < getTemplates().size() - 1) {
						Template nextTemplateToEvaluate = getTemplates().get(
								index + 1);
						// call recursion with new binding
						findTemplateBindings(preBinding,
								currentBinding.clone(), nextTemplateToEvaluate,
								candidateBinding, foundBindings);
					} else {
						// this is last template so just check for
						// completeness and add it if complete
						if (evaluator.isComplete(currentBinding)) {
							foundBindings.add(currentBinding.clone());
						}
					}
				}

			} catch (BindingException e) {
				// just continue with next candidate
				continue;
			}
		}
		return;
	}

	/**
	 * Returns a set of candidates for the specified <code>template</code> using
	 * the following priority:
	 * 
	 * <ol>
	 * <li><code>preBinding</code></li>
	 * <li><code>currentBinding</code></li>
	 * <li>other suitable model elements in the model</li>
	 * </ol>
	 * 
	 * @param template
	 *            template to get candidates for.
	 * @param preBinding
	 *            pre-bound bindings.
	 * @param candidateBinding
	 *            candidate binding to restrict candidate selection to. Might be
	 *            <code>null</code> or empty.
	 * @param currentBinding
	 *            candidates of the current binding.
	 */
	private Collection<EObject> getCurrentCandidates(final Template template,
			final ITemplateBinding preBinding,
			final ITemplateBinding candidateBinding,
			final ITemplateBinding currentBinding) {

		// add candidates according to priority scheme
		List<EObject> candidateSet = new ArrayList<EObject>();
		if (preBinding.getBoundObjects(template) != null
				&& preBinding.getBoundObjects(template).size() > 0) {

			candidateSet.addAll(preBinding.getBoundObjects(template));

		} else if (currentBinding.getBoundObjects(template) != null
				&& currentBinding.getBoundObjects(template).size() > 0) {

			candidateSet.addAll(currentBinding.getBoundObjects(template));

		} else if (candidateBinding != null
				&& candidateBinding.getBoundObjects(template) != null
				&& candidateBinding.getBoundObjects(template).size() > 0) {

			// remove already used objects
			Collection<EObject> boundObjects = new HashSet<EObject>(
					candidateBinding.getBoundObjects(template));
			boundObjects.removeAll(preBinding.getBoundObjects());
			boundObjects.removeAll(currentBinding.getBoundObjects());

			Collection<EObject> locallyValidEObjects = getLocallyValidEObjects(
					boundObjects, template);

			if (locallyValidEObjects.size() > 0) {
				candidateSet.addAll(locallyValidEObjects);
			}

		}
		if (candidateSet.size() < 1) {
			// Not pre-bound and not in currentBinding. So select suitable
			// candidates based on the containment relationship.

			List<List<EObject>> candidateLists = new ArrayList<List<EObject>>();
			Collection<EObject> handledObjects = new HashSet<EObject>();

			// iterate through preBinding and determine candidates on the
			// base of the containment relationship according to the template
			// hierarchy.
			for (Template boundTemplate : preBinding.getTemplates()) {
				EList<EObject> candidateList = new BasicEList<EObject>();
				for (EObject eObject : preBinding
						.getBoundObjects(boundTemplate)) {
					if (!handledObjects.contains(eObject)) {
						candidateList.addAll(getCurrentCandidates(template,
								eObject, boundTemplate, preBinding,
								candidateBinding, currentBinding));
						handledObjects.add(eObject);
					}
				}
				if (candidateList.size() > 0) {
					candidateLists.add(candidateList);
				}
			}

			// iterate through currentBinding and determine candidates on the
			// base of the containment relationship according to the template
			// hierarchy.
			for (Template boundTemplate : currentBinding.getTemplates()) {
				EList<EObject> candidateList = new BasicEList<EObject>();
				for (EObject eObject : currentBinding
						.getBoundObjects(boundTemplate)) {
					if (!handledObjects.contains(eObject)) {
						candidateList.addAll(getCurrentCandidates(template,
								eObject, boundTemplate, preBinding,
								candidateBinding, currentBinding));
						handledObjects.add(eObject);
					}
				}
				if (candidateList.size() > 0) {
					candidateLists.add(candidateList);
				}
			}

			if (candidateBinding != null) {
				// iterate through candidatetBinding and determine candidates on
				// the base of the containment relationship according to the
				// template hierarchy.
				for (Template boundTemplate : candidateBinding.getTemplates()) {
					EList<EObject> candidateList = new BasicEList<EObject>();
					for (EObject eObject : candidateBinding
							.getBoundObjects(boundTemplate)) {
						if (!handledObjects.contains(eObject)) {
							candidateList.addAll(getCurrentCandidates(template,
									eObject, boundTemplate, preBinding,
									candidateBinding, currentBinding));
							handledObjects.add(eObject);
						}
					}
					if (candidateList.size() > 0) {
						candidateLists.add(candidateList);
					}
				}
			}

			// get intersection of all suitable candidates in candidate lists.
			Collection<EObject> foundCandidates = getIntersection(candidateLists);

			// remove already bound objects
			foundCandidates.removeAll(currentBinding.getBoundObjects());
			candidateSet.addAll(foundCandidates);
		}
		return candidateSet;
	}

	/**
	 * Returns the intersection of all {@link EObject}s contained by
	 * <code>lists</code> in one new {@link List} of {@link EObject}s.
	 * 
	 * @param candidateLists
	 *            lists to create intersection of.
	 * @return the intersection of all {@link EObject}s contained by
	 *         <code>lists</code> in one new {@link List} of {@link EObject}s
	 */
	private Collection<EObject> getIntersection(final List<List<EObject>> lists) {
		Collection<EObject> intersection = new HashSet<EObject>();
		// get smallest list to start with
		List<EObject> smallestList = null;
		for (List<EObject> eObjectList : lists) {
			if (smallestList == null
					|| eObjectList.size() < smallestList.size()) {
				smallestList = eObjectList;
			}
		}
		// clear all objects from smallest list which are not contained in every
		// other list.
		if (smallestList != null) {
			for (EObject eObject : smallestList) {
				boolean containedByAll = true;
				for (List<EObject> list : lists) {
					if (!list.contains(eObject)) {
						containedByAll = false;
					}
				}
				if (containedByAll) {
					intersection.add(eObject);
				}
			}
		}
		return intersection;
	}

	/**
	 * Finds suitable candidates for <code>forTemplate</code> on the base of the
	 * containment hierarchy from <code>baseObject</code> and
	 * <code>baseTemplate</code>.
	 * 
	 * @param forTemplate
	 *            {@link Template} to find candidates for.
	 * @param baseObject
	 *            base object.
	 * @param baseTemplate
	 *            base template.
	 * @param currentBinding
	 *            used to resolve already bound templates.
	 * @param preBinding
	 *            used to resolve pre-bound templates.
	 * @return {@link List} of {@link EObject}s.
	 */
	private List<EObject> getCurrentCandidates(final Template forTemplate,
			final EObject baseObject, final Template baseTemplate,
			final ITemplateBinding preBinding,
			final ITemplateBinding candidateBinding,
			final ITemplateBinding currentBinding) {

		List<EObject> candidates = new BasicEList<EObject>();
		// find common parent
		Template commonParentTemplate = ConditionsUtil
				.findLeastCommonParentTemplate(forTemplate, baseTemplate);

		if (commonParentTemplate != null) {
			// create parent list
			List<Template> parents = ConditionsUtil
					.createParentList(forTemplate);
			// shortest path to forTemplate from baseTemplate
			// navigate up to common parent
			EObject currentParentObject = baseObject;
			Template currentParentTemplate = baseTemplate;
			boolean commonParentReached = commonParentTemplate
					.equals(currentParentTemplate);
			while (!commonParentReached
					&& (currentParentTemplate = currentParentTemplate
							.getParentTemplate()) != null) {
				currentParentObject = currentParentObject.eContainer();
				if (currentParentTemplate.equals(commonParentTemplate)) {
					commonParentReached = true;
				}
			}

			// add targetObject to parents list
			parents.add(0, forTemplate);
			Collection<EObject> parentsCandidates = new BasicEList<EObject>();
			parentsCandidates.add(currentParentObject);
			// navigate down to forTemplate collecting all possible objects
			for (int i = parents.indexOf(commonParentTemplate) - 1; i >= 0; i--) {

				Template template = parents.get(i);
				// check if template is pre or currently bound
				if (preBinding.getBoundObjects(template).size() > 0
						|| currentBinding.getBoundObjects(template).size() > 0
						|| (candidateBinding != null && candidateBinding
								.getBoundObjects(template).size() > 0)) {
					if (preBinding.getBoundObjects(template).size() > 0) {
						parentsCandidates = preBinding
								.getBoundObjects(template);
					} else if (currentBinding.getBoundObjects(template).size() > 0) {
						parentsCandidates = currentBinding
								.getBoundObjects(template);
					} else {
						parentsCandidates = candidateBinding
								.getBoundObjects(template);
					}
				} else {
					// if not select all children and collect possible
					// candidates
					Collection<EObject> tempCandidates = new BasicEList<EObject>();
					for (EObject currentParent : parentsCandidates) {
						// select all objects according to feature
						Collection<EObject> values = getEObjectValues(
								currentParent, template.getParentsFeature(),
								true);
						tempCandidates.addAll(getLocallyValidEObjects(values,
								template));
					}
					parentsCandidates = tempCandidates;
				}
			}
			candidates.addAll(parentsCandidates);
		}
		Collection<EObject> finalCandidates = getLocallyValidEObjects(
				candidates, forTemplate);
		finalCandidates.removeAll(preBinding.getBoundObjects());
		finalCandidates.removeAll(currentBinding.getBoundObjects());
		if (candidateBinding != null) {
			finalCandidates.removeAll(candidateBinding.getBoundObjects());
		}
		return new BasicEList<EObject>(finalCandidates);
	}

	/**
	 * Returns all locally valid {@link EObject}s out of the list of
	 * <code>candidates</code> for the specified <code>template</code>.
	 * 
	 * @param candidates
	 *            to get locally valid from.
	 * @param template
	 *            to evaluate against.
	 * @return locally valid {@link EObject}s.
	 */
	private Collection<EObject> getLocallyValidEObjects(
			Collection<EObject> candidates, Template template) {
		Collection<EObject> tempCandidates = new BasicEList<EObject>();
		for (EObject eObject : candidates) {
			if (eObject != null
					&& evaluator.evaluate(template, eObject, true).isOK()) {
				tempCandidates.add(eObject);
			}
		}
		return tempCandidates;
	}

	/**
	 * Gets the values of the specified <code>feature</code> from the
	 * <code>eObject</code> and returns them in a list of {@link EObject}s. If
	 * these values are raw values they will be omitted.
	 * 
	 * @param eObject
	 * @param feature
	 * @return {@link EObject} values of <code>feature</code> in
	 *         <code>eObject</code>.
	 */
	private Collection<EObject> getEObjectValues(EObject eObject,
			EStructuralFeature feature, boolean tryToResolve) {
		List<EObject> values = new BasicEList<EObject>();
		try {
			if (feature.getUpperBound() != 1) {
				// multi valued
				EList<?> objectList = (EList<?>) eObject.eGet(feature, true);
				for (Object object : objectList) {
					if (object instanceof EObject) {
						values.add((EObject) object);
					}
				}
			} else {
				// single valued
				Object object = eObject.eGet(feature, true);
				if (object instanceof EObject) {
					values.add((EObject) object);
				}
			}
		} catch (IllegalArgumentException e) {
			if (tryToResolve) {
				return tryToResolveByFeatureName(eObject, feature);
			}
			// ignore object if eGet results in exception (due to wrong type)
		} catch (NullPointerException e) {
			if (tryToResolve) {
				return tryToResolveByFeatureName(eObject, feature);
			}
		}
		return values;
	}

	/**
	 * Tries to resolve the value of the specified <code>feature</code> by name
	 * at the specified <code>eObject</code>.
	 * 
	 * @param eObject
	 *            to get value from.
	 * @param feature
	 *            to resolve.
	 */
	private Collection<EObject> tryToResolveByFeatureName(EObject eObject,
			EStructuralFeature feature) {
		// try to match with different feature instance which is still
		// equal
		for (EStructuralFeature feature2 : eObject.eClass()
				.getEStructuralFeatures()) {
			if (EcoreUtil.equals(feature, feature2)) {
				return getEObjectValues(eObject, feature2, false);
			}
		}
		return Collections.emptySet();
	}

	/**
	 * Finds and returns valid and unique referenced object combination by the
	 * conditions contained by <code>currentTemplate</code> for the
	 * <code>currentCandidate</code>. The selection of candidates is done using
	 * {@link #getCurrentCandidates(Template, ITemplateBinding, ITemplateBinding, ITemplateBinding)}
	 * with the provided <code>preBinding</code>, <code>currentBinding</code>,
	 * and <code>candidates</code>.
	 * 
	 * <p>
	 * Found valid reference bindings will be returned. This might be empty if
	 * and only if <code>currentTemplate</code> contains no conditions which
	 * refer to other templates. If no valid reference binding could be found,
	 * this method throws a {@link BindingException}.
	 * </p>
	 * 
	 * @param currentCandidate
	 *            object to find valid referenced objects for.
	 * @param currentTemplate
	 *            template of <code>currentCandidate</code>.
	 * @param preBinding
	 *            fixed, pre-binding.
	 * @param candidateBinding
	 *            used to restrict candidate selection.
	 * @param currentBinding
	 *            currently built binding.
	 * @return found valid rerenced {@link ITemplateBinding}s.
	 * @throws ParserException
	 *             if evaluation fails.
	 * @throws BindingException
	 *             if no valid reference binding could be found.
	 */
	private List<ITemplateBinding> findValidReferenceBindings(
			EObject currentCandidate, Template currentTemplate,
			ITemplateBinding preBinding, ITemplateBinding candidateBinding,
			ITemplateBinding currentBinding) throws ParserException,
			BindingException {

		List<ITemplateBinding> validReferenceBindings = new ArrayList<ITemplateBinding>();

		// if containment aware add parent template condition
		Condition containmentCondition = null;
		if (evaluator.isContainmentAware()
				&& currentTemplate.getParentTemplate() != null) {
			containmentCondition = evaluator
					.createContainmentCondition(currentTemplate);
		}

		// get conditions which reference another template
		Set<Condition> conditions = evaluator
				.getTemplateInvolvingConditionsToEvaluate(currentTemplate);
		if (conditions.size() == 0) {
			return Collections.emptyList();
		}

		// get referenced templates
		Collection<Template> referencedTemplates = evaluator
				.extractInvolvedTemplates(conditions);

		// get binding combinations (permutation of each possible candidate
		// object to template)
		Collection<ITemplateBinding> allBindingCombinations = calcTemplateObjectPermutation(
				referencedTemplates, preBinding, candidateBinding,
				currentBinding);

		// check for each binding combination each condition and save all valid
		// combinations
		for (ITemplateBinding bindingCombination : allBindingCombinations) {
			boolean currentCombinationSatisfied = true;
			for (Condition condition : conditions) {
				// check whether condition is satisfied
				if (!evaluator.isSatisfied(currentCandidate,
						bindingCombination.getSingleBindingMap(), condition)) {
					currentCombinationSatisfied = false;
					// save trace info
					EvaluationResult evaluationResult = ConditionsFactory.eINSTANCE
							.createEvaluationResult();
					evaluationResult.setStatus(EvaluationStatus.UNSATISFIED);
					evaluationResult
							.setEvaluator(ConditionsEvaluationEngineImpl.EVALUATOR);
					evaluationResult.setMessage("Condition failed.");
					evaluationResult.setFailedCandidate(currentCandidate);
					evaluationResult.setFailedCondition(condition);
					evaluationResults.add(evaluationResult);
					// stop evaluating any further
					break;
				}
			}
			// if current binding combination is valid save it
			if (currentCombinationSatisfied) {
				validReferenceBindings.add(bindingCombination);
			}
		}

		// remove containment condition if we added it
		if (containmentCondition != null) {
			currentTemplate.getSpecifications().remove(containmentCondition);
		}

		// if there are referenced templates but no valid binding combination
		// throw binding exception
		if (referencedTemplates.size() > 0
				&& validReferenceBindings.size() == 0) {
			throw new BindingException(NO_REFERENCE_BINDING,
					referencedTemplates.iterator().next());
		} else {
			return validReferenceBindings;
		}
	}

	/**
	 * Calculates a permutation of all possible objects to template
	 * combinations.
	 * 
	 * @param templates
	 *            templates to create candidate permutation for.
	 * @param candidatesToIgnore
	 *            candidates to ignore.
	 * @param preBinding
	 *            user-defined fixed binding.
	 * @param candidateBinding
	 *            used to restrict candidate selection.
	 * @param currentBinding
	 *            the current binding.
	 * @return template candidate permutation in terms of
	 *         {@link ITemplateBinding}s.
	 */
	protected Collection<ITemplateBinding> calcTemplateObjectPermutation(
			Collection<Template> templates, ITemplateBinding preBinding,
			ITemplateBinding candidateBinding, ITemplateBinding currentBinding) {

		// create overall binding containing best candidates for each template
		ITemplateBinding overallBinding = createEmptyTemplateBinding();
		for (Template template : templates) {
			Collection<EObject> referencedCandidates = getCurrentCandidates(
					template, preBinding, candidateBinding, currentBinding);
			overallBinding.add(template, referencedCandidates);
		}

		// if there was no candidate for each template in templates there is no
		// valid combination
		if (overallBinding.getTemplates().size() < templates.size()) {
			return Collections.emptySet();
		}

		// call recursion to create a permutation of each object per each
		// template
		Set<ITemplateBinding> permutations = new HashSet<ITemplateBinding>();
		permutations
				.addAll(calcTemplateObjectPermutation(overallBinding, null));

		return permutations;
	}

	/**
	 * Creates a {@link ITemplateBinding} for each permutation of template to
	 * object combination and returns all resulting combinations.
	 * 
	 * @param referencedTemplates
	 * 
	 * @param overallBinding
	 *            to create permutation for.
	 * @param currentCombination
	 *            current combination which has to be completed. May be
	 *            <code>null</code>.
	 * @return set of {@link ITemplateBinding} containing a
	 *         {@link ITemplateBinding} for each permutation.
	 */
	private Collection<ITemplateBinding> calcTemplateObjectPermutation(
			ITemplateBinding overallBinding, ITemplateBinding currentCombination) {
		// create set to return
		Set<ITemplateBinding> combinations = new HashSet<ITemplateBinding>();

		// create new combination if null
		if (currentCombination == null) {
			currentCombination = createEmptyTemplateBinding();
		}

		// get list of templates which has not been processed
		Set<Template> templates = new HashSet<Template>();
		templates.addAll(overallBinding.getTemplates());
		templates.removeAll(currentCombination.getTemplates());

		if (templates.size() > 0) {
			// recursive with each candidate for next template
			Template template = templates.iterator().next();
			for (EObject eObject : overallBinding.getBoundObjects(template)) {
				currentCombination.remove(template);
				currentCombination.add(template, eObject);
				combinations.addAll(calcTemplateObjectPermutation(
						overallBinding, currentCombination.clone()));
			}
		} else {
			combinations.add(currentCombination);
		}

		return combinations;
	}
}