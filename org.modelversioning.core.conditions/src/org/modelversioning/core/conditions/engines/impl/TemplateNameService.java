/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;

/**
 * Provides the functionality to create names for {@link Template}s.
 * 
 * <p>
 * The names created by this service are conform to the syntax of OCL and are
 * unique within one instance of this class.
 * </p>
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateNameService {
	
	/**
	 * Constant saving the name of the <em>name</em> attribute.
	 */
	private static final String NAME = "name";
	/**
	 * Constant saving the delimiter for titles between name and type.
	 */
	private static final String DELIM = " : ";
	/**
	 * The name cache to check for duplicate names.
	 */
	private Map<String, Integer> nameCache = new HashMap<String, Integer>();
	
	/**
	 * Creates a human readable title for the specified <code>eObject</code>.
	 * @param eObject {@link EObject} to create title for.
	 * @return created title.
	 */
	public String createTitle(EObject eObject) {
		String name = String.valueOf(eObject.hashCode());
		// try to find out name
		for (EAttribute eAtt : eObject.eClass().getEAllAttributes()) {
			if (eAtt.getName().equals(NAME)) {
				if (eObject.eGet(eAtt) != null) {
					name = eObject.eGet(eAtt).toString();
				}
			}
		}
		return name + DELIM + eObject.eClass().getName();
	}
	
	/**
	 * Creates a name conform to the syntax of OCL and  unique within one instance of this class.
	 * @param eObject {@link EObject} to create name for.
	 * @return created name.
	 */
	public String createName(EObject eObject) {
		String nameCandidate = eObject.eClass().getName();
		nameCandidate = cleanForOCL(nameCandidate);
		int count = 0;
		if (nameCache.get(nameCandidate) == null) {
			nameCache.put(nameCandidate, count);
		} else {
			count = nameCache.get(nameCandidate);
			count++;
			nameCache.put(nameCandidate, count);
		}
		nameCandidate = nameCandidate + "_" + (count);
		return nameCandidate;
	}
	
	/**
	 * Cleans the specified <code>string</code> to be conform to the syntax of OCL attributes.
	 * 
	 * @param string {@link String} to clean.
	 * @return cleaned <code>string</code>.
	 */
	public String cleanForOCL(String string) {
		string.replaceAll(" ", "_");
		return string;
	}

}
