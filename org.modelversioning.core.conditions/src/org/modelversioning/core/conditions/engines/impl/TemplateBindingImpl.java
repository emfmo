/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.ITemplateBinding;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateBindingImpl implements ITemplateBinding {

	/**
	 * The actual binding.
	 */
	private Map<Template, Collection<EObject>> binding = new HashMap<Template, Collection<EObject>>();

	/**
	 * Creates a new {@link Set}.
	 * 
	 * @return newly created {@link Set}.
	 */
	private Collection<EObject> createSet() {
		return new ArrayList<EObject>();
	}

	/**
	 * Cleans the {@link Set} of <code>template</code> if necessary.
	 * 
	 * @param template
	 *            {@link Template} of which the {@link Set} to clean.
	 */
	private void cleanSet(Template template) {
		if (binding.get(template) != null && binding.get(template).size() == 0) {
			binding.remove(template);
		}
	}

	/**
	 * Returns the set of bound {@link EObject}s of the <code>template</code>.
	 * If there is no {@link Set} this will add a set and returns it.
	 * 
	 * @param template
	 *            {@link Template} of which the {@link Set} is requested.
	 * @return the {@link Set} of <code>template</code>.
	 */
	private Collection<EObject> getSet(Template template) {
		if (binding.get(template) == null) {
			binding.put(template, createSet());
		}
		return binding.get(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<EObject> getBoundObjects() {
		Collection<EObject> returnCollection = new ArrayList<EObject>();
		for (Collection<EObject> col : binding.values()) {
			returnCollection.addAll(col);
		}
		return returnCollection;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<EObject> getBoundObjects(Template template) {
		if (binding.containsKey(template)) {
			return Collections.unmodifiableCollection(binding.get(template));
		} else {
			return Collections.emptySet();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Template getBoundTemplate(EObject eObject) {
		for (Template template : binding.keySet()) {
			if (binding.get(template).contains(eObject)) {
				return template;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(Template template, Collection<EObject> eObjects) {
		for (EObject eObject : eObjects) {
			add(template, eObject);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(Template template, EObject eObject) {
		if (eObject != null && !getSet(template).contains(eObject)) {
			getSet(template).add(eObject);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(Template template) {
		binding.remove(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(Template template, EObject eObject) {
		getSet(template).remove(eObject);
		cleanSet(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(Template template, Collection<EObject> eObjects) {
		getSet(template).removeAll(eObjects);
		cleanSet(template);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		binding.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return binding.size() < 1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return binding.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Template> getTemplates() {
		return binding.keySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITemplateBinding clone() {
		TemplateBindingImpl clone = new TemplateBindingImpl();
		for (Template template : binding.keySet()) {
			Set<EObject> clonedSet = new HashSet<EObject>();
			for (EObject eObject : binding.get(template)) {
				clonedSet.add(eObject);
			}
			clone.add(template, clonedSet);
		}
		return clone;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Template, EObject> getSingleBindingMap() {
		Map<Template, EObject> singleBinding = new HashMap<Template, EObject>();
		for (Template template : binding.keySet()) {
			if (binding.get(template) != null
					&& binding.get(template).size() > 0) {
				singleBinding.put(template, binding.get(template).iterator()
						.next());
			}
		}
		return Collections.unmodifiableMap(singleBinding);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addAll(ITemplateBinding bindingCombination) {
		for (Template template : bindingCombination.getTemplates()) {
			add(template, bindingCombination.getBoundObjects(template));
		}
	}
}
