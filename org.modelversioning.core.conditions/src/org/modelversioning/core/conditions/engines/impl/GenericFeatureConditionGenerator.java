/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.ecore.OCL;
import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.ConditionsPlugin;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.State;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.TemplateMaskLiterals;
import org.modelversioning.core.conditions.engines.IFeatureConditionGenerator;

/**
 * Generically generates {@link FeatureCondition}s in {@link OCL}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class GenericFeatureConditionGenerator implements
		IFeatureConditionGenerator {

	/**
	 * Names of features to omit.
	 * 
	 * <p>
	 * This generator omits the generation of conditions for the feature
	 * <code>iD</code> as this features can not be evaluated directly using
	 * {@link OCL}.
	 * </p>
	 */
	private static final List<String> OMIT_FEATURE_NAMES = Arrays.asList("iD");

	/**
	 * Error message: Unkown ERawType.
	 */
	private static final String UNKNOWN_ERAW_TYPE = "FeatureConditionImpl:"
			+ "ERawType which is not an instance of EEnum, EDataType, "
			+ "or EClass: ";

	/**
	 * Generates {@link FeatureCondition}s for the specified
	 * <code>feature</code> and <code>value</code>(s).
	 * 
	 * @param feature
	 *            Feature to create {@link FeatureCondition} for.
	 * @param template
	 *            The {@link Template} for which the {@link FeatureCondition} is
	 *            generated. It is used to retrieve the representative's value
	 *            as well as neighbor templates.
	 * 
	 * @return Generated {@link FeatureCondition}.
	 * 
	 * @see IFeatureConditionGenerator#generateFeatureCondition(EStructuralFeature,
	 *      Template))
	 */
	@Override
	public List<FeatureCondition> generateFeatureCondition(
			EStructuralFeature feature, Template template) {
		if (OMIT_FEATURE_NAMES.contains(feature.getName())) {
			return Collections.emptyList();
		} else {
			return deriveFeatureConditions(feature, template);
		}
	}

	/**
	 * Creates an empty {@link FeatureCondition} for the specified
	 * <code>feature</code>.
	 * 
	 * @param feature
	 *            {@link EStructuralFeature} to create conditions for.
	 * @return empty {@link FeatureCondition} for <code>feature</code>.
	 */
	private FeatureCondition createEmptyFeatureCondition(
			EStructuralFeature feature) {
		FeatureCondition condition = ConditionsFactory.eINSTANCE
				.createFeatureCondition();
		condition.setFeature(feature);
		condition.setState(State.GENERATED);
		condition.setInvolvesTemplate(false);
		return condition;
	}

	/**
	 * Returns the condition language the generated {@link FeatureCondition}s by
	 * this class are using.
	 * 
	 * @return the condition language.
	 * 
	 * @see IFeatureConditionGenerator#getConditionLanguage()
	 */
	@Override
	public String getConditionLanguage() {
		return OCLLiterals.CONDITION_LANGUAGE;
	}

	/**
	 * Returns the namespace URIs of the meta model this
	 * {@link IFeatureConditionGenerator} is specialized for.
	 * 
	 * @return the namespace URIs of the meta model.
	 * 
	 * @see IFeatureConditionGenerator#getMetaModelNamespace()
	 */
	@Override
	public List<String> getMetaModelNamespace() {
		return Arrays.asList(IFeatureConditionGenerator.GENERAL);
	}

	/**
	 * Derives FeatureConditions for the specified <code>template</code> and the
	 * specified {@link EStructuralFeature}.
	 * 
	 * TODO improve code
	 * 
	 * @param feature
	 *            {@link EStructuralFeature} to derive OCL {@link String}s s.
	 * @param template
	 *            {@link Template} the OCL expression is built for.
	 */
	private List<FeatureCondition> deriveFeatureConditions(
			EStructuralFeature feature, Template template) {

		Object value = template.getRepresentative().eGet(feature);
		List<FeatureCondition> conditions = new ArrayList<FeatureCondition>();

		if (value == null) {
			// if value is null just write = null
			FeatureCondition condition = createEmptyFeatureCondition(feature);
			condition.setExpression(OCLLiterals.EQUAL + value);
			// deactivate by default
			condition.setActive(false);
			conditions.add(condition);

		} else {

			EClassifier rawFeatureType = feature.getEGenericType()
					.getERawType();

			if (feature.getUpperBound() != 1) {
				// handle multi valued
				EList<?> valueList = (EList<?>) value;

				if (valueList.size() == 0) {
					// list is empty so add isEmpty-condition
					FeatureCondition condition = createEmptyFeatureCondition(feature);
					condition.setExpression(OCLLiterals.IS_EMPTY);
					// deactivate by default
					condition.setActive(false);
					conditions.add(condition);

				} else {
					// list is not empty so add a contains-condition for each
					// value element
					for (Object object : valueList) {
						// check feature type
						if (rawFeatureType instanceof EEnum) {
							FeatureCondition condition = createEmptyFeatureCondition(feature);
							// handle EEnums
							condition.setExpression(OCLLiterals.INCLUDES_START
									+ rawFeatureType.getName()
									+ OCLLiterals.ENUM_SEP + object
									+ OCLLiterals.INCLUDES_END);
							conditions.add(condition);

						} else if (rawFeatureType instanceof EClass) {
							// handle EClasses
							String templateName = getTemplateName(template,
									object);

							if (templateName != null) {
								FeatureCondition condition = createEmptyFeatureCondition(feature);
								condition.setInvolvesTemplate(true);
								condition
										.setExpression(OCLLiterals.INCLUDES_START
												+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
												+ templateName
												+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
												+ OCLLiterals.INCLUDES_END);
								conditions.add(condition);
							} else if (value instanceof EDataType) {
								FeatureCondition condition = createEmptyFeatureCondition(feature);
								condition
										.setExpression(OCLLiterals.INCLUDES_START
												+ ((EDataType) value).getName()
												+ OCLLiterals.INCLUDES_END);
								conditions.add(condition);
							}

						} else if (rawFeatureType instanceof EDataType) {
							// handle simple EDataTypes
							EDataType dataType = (EDataType) rawFeatureType;

							if (String.class
									.equals(dataType.getInstanceClass())) {
								// put Strings in apostrophs
								FeatureCondition condition = createEmptyFeatureCondition(feature);
								condition
										.setExpression(OCLLiterals.INCLUDES_START
												+ OCLLiterals.APO
												+ object
												+ OCLLiterals.APO
												+ OCLLiterals.INCLUDES_END);
								conditions.add(condition);

							} else {
								// just output value
								FeatureCondition condition = createEmptyFeatureCondition(feature);
								condition
										.setExpression(OCLLiterals.INCLUDES_START
												+ object
												+ OCLLiterals.INCLUDES_END);
								conditions.add(condition);
							}

						} else {
							// don't know what type of RawType this is, so log
							// it
							ConditionsPlugin.getDefault().getLog().log(
									new Status(IStatus.WARNING,
											ConditionsPlugin.PLUGIN_ID,
											UNKNOWN_ERAW_TYPE + feature));
						}
					}

				}

			} else {
				// handle single valued
				// check feature type
				if (rawFeatureType instanceof EEnum) {
					// handle EEnums
					FeatureCondition condition = createEmptyFeatureCondition(feature);
					condition.setExpression(OCLLiterals.EQUAL
							+ rawFeatureType.getName() + OCLLiterals.ENUM_SEP
							+ value);
					conditions.add(condition);

				} else if (rawFeatureType instanceof EClass) {
					// handle EClasses
					String templateName = getTemplateName(template, value);
					if (templateName != null) {
						FeatureCondition condition = createEmptyFeatureCondition(feature);
						condition.setInvolvesTemplate(true);
						condition
								.setExpression(OCLLiterals.EQUAL
										+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
										+ templateName
										+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END);
						conditions.add(condition);
					} else if (value instanceof EDataType) {
						FeatureCondition condition = createEmptyFeatureCondition(feature);
						condition.setExpression(OCLLiterals.EQUAL
								+ ((EDataType) value).getName());
						conditions.add(condition);
					}

				} else if (rawFeatureType instanceof EDataType) {
					// handle simple EDataTypes
					EDataType dataType = (EDataType) rawFeatureType;

					if (String.class.equals(dataType.getInstanceClass())) {
						// put Strings in apostrophs
						FeatureCondition condition = createEmptyFeatureCondition(feature);
						condition.setExpression(OCLLiterals.EQUAL
								+ OCLLiterals.APO + value + OCLLiterals.APO);
						// deactivate by default
						condition.setActive(false);
						conditions.add(condition);

					} else {
						// just output value
						FeatureCondition condition = createEmptyFeatureCondition(feature);
						condition.setExpression(OCLLiterals.EQUAL + value);
						conditions.add(condition);
						// deactivate by default
						condition.setActive(false);
					}
				} else {
					// don't know what type of RawType this is, so log it
					ConditionsPlugin.getDefault().getLog().log(
							new Status(IStatus.WARNING,
									ConditionsPlugin.PLUGIN_ID,
									UNKNOWN_ERAW_TYPE + feature));
				}
			}
		}
		return conditions;
	}

	/**
	 * Tries to find a {@link Template} representing the specified
	 * <code>object</code> starting the search at the specified
	 * <code>template</code>. If no {@link Template} representing
	 * <code>object</code> could be found, this method returns <code>null</code>
	 * .
	 * 
	 * @param template
	 *            the {@link Template} to start searching.
	 * @param object
	 *            {@link Object} to find {@link Template} for.
	 * @return the {@link Template} name if available or <code>null</code>
	 */
	private String getTemplateName(Template template, Object object) {
		String templateName = null;
		// try template itself
		if (template.getRepresentative().equals(object)) {
			templateName = template.getName();
		}

		// try direct parent
		if (template.getParentTemplate() != null
				&& template.getParentTemplate().getRepresentative().equals(
						object)) {
			templateName = template.getParentTemplate().getName();
		}

		// try first children
		for (Template subTemplate : template.getSubTemplates()) {
			if (subTemplate.getRepresentative().equals(object)) {
				templateName = subTemplate.getName();
			}
		}

		// try to iterate all templates
		// get root template
		Template currentTemplate = template;
		while (currentTemplate.getParentTemplate() != null) {
			currentTemplate = currentTemplate.getParentTemplate();
		}

		TreeIterator<EObject> treeIter = currentTemplate.eAllContents();
		while (treeIter.hasNext()) {
			EObject content = treeIter.next();
			if (content instanceof Template) {
				Template subTemplate = (Template) content;
				if (subTemplate.getRepresentative().equals(object)) {
					templateName = subTemplate.getName();
				}
			}
		}

		return templateName;
	}

}
