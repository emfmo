/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.Template;

/**
 * Represents a binding from objects to {@link Template}s.
 * 
 * A template binding is valid if there is at least one object bound to each
 * template fulfilling all constraints of the respective template.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ITemplateBindings {

	/**
	 * Returns the root template of this binding.
	 * 
	 * @return the root template.
	 */
	public Template getRootTemplate();

	/**
	 * Returns the objects bound to the specified <code>template</code>.
	 * 
	 * @param template
	 *            template to which the objects in question are bound.
	 * @return bound objects.
	 */
	public Set<EObject> getBoundObjects(Template template);

	/**
	 * Returns the {@link Template}s bound to the specified <code>eObject</code>.
	 * 
	 * @param eObject
	 *            {@link EObject} which is bound to the template to find.
	 * @return the bound templates or an empty {@link Set}.
	 */
	public Set<Template> getBoundTemplates(EObject eObject);

	/**
	 * Returns <code>true</code> if the specified <code>object</code> may be
	 * removed from its current binding to <code>template</code> without
	 * affecting the validity of this template binding.
	 * 
	 * @param object
	 *            object in question.
	 * @param template
	 *            template of the object binding.
	 * @return <code>true</code> if removable, otherwise <code>false</code>.
	 */
	public boolean isRemovable(EObject object, Template template);

	/**
	 * Removes the specified <code>object</code> from the binding. If the
	 * <code>object</code> could be removed this method returns
	 * <code>true</code> after it removed the <code>object</code>. Note that it
	 * might be necessary to remove other objects as well to keep the binding
	 * valid. These dependent objects are removed as well if necessary.
	 * 
	 * @param object
	 *            object to remove.
	 * @param template
	 *            template the object binding has to be removed from.
	 * @return <code>true</code> if the object is removed. <code>false</code>
	 *         otherwise.
	 */
	public boolean remove(EObject object, Template template);

	/**
	 * Undo the previously remove ({@link #remove(EObject, Template)}) action of
	 * <code>object</code>.
	 * 
	 * @param object
	 *            object to re-add.
	 * @return <code>true</code> if it was possible. <code>false</code>
	 *         otherwise.
	 */
	public boolean undoRemove(EObject object);

	/**
	 * Returns an evaluation result keeping information about the validity of
	 * this binding. If a binding generator could not create a successful
	 * binding, it should put the reason for it in this evaluation result.
	 * 
	 * @return the evaluation result.
	 */
	public EvaluationResult validate();

	/**
	 * Returns all single and valid bindings.
	 * 
	 * @return the possible bindings.
	 */
	public Set<ITemplateBinding> getAllPossibleBindings();

	/**
	 * Returns all bound {@link Template}s.
	 * @return all bound {@link Template}s.
	 */
	public Set<Template> getTemplates();

	/**
	 * Extracts a {@link ITemplateBindings} in which only
	 * <code>boundObject</code> is bound to <code>template</code>.
	 * 
	 * @param template
	 *            to extract bindings for.
	 * @param boundObject
	 *            bound object which is uniquely bound to <code>template</code>
	 *            in the returned {@link ITemplateBindings}.
	 * @param removeExtracted
	 *            specifies whether the extracted bindings should be removed
	 *            from this binding.
	 * @return a {@link ITemplateBindings} in which only
	 *         <code>boundObject</code> is bound to <code>template</code>.
	 */
	public ITemplateBindings extractSubBindings(Template template,
			EObject boundObject, boolean removeExtracted);

}
