/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import java.util.List;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.RefinementTemplate;
import org.modelversioning.core.conditions.Template;

/**
 * Engine that provides the functionality to generate an instance of a
 * {@link ConditionsModel} for an {@link EObject}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IConditionGenerationEngine {

	/**
	 * Returns the condition language this generator will use to express
	 * conditions.
	 * 
	 * @return condition language this generator will use to express conditions.
	 */
	public String getConditionLanguage();

	/**
	 * Adds the specified {@link IFeatureConditionGenerator} to the used
	 * {@link IFeatureConditionGenerator}s by this generation engine.
	 * <p>
	 * Clients may add specific {@link IFeatureConditionGenerator}s to adapt the
	 * generation of the {@link ConditionsModel}. Nevertheless, the condition
	 * language of each provided {@link IFeatureConditionGenerator} must be
	 * equal to the condition language of this generator.
	 * </p>
	 * 
	 * @param featureConditionGenerator
	 *            {@link IFeatureConditionGenerator} to add.
	 */
	public void addFeatureConditionGenerator(
			IFeatureConditionGenerator featureConditionGenerator);

	/**
	 * Removes the specified {@link IFeatureConditionGenerator} from the list of
	 * {@link IFeatureConditionGenerator}s used by this generation engine.
	 * 
	 * @param featureConditionGenerator
	 *            {@link IFeatureConditionGenerator} to remove.
	 */
	public void removeFeatureConditionGenerator(
			IFeatureConditionGenerator featureConditionGenerator);

	/**
	 * Returns the list of {@link IFeatureConditionGenerator}s currently used by
	 * this generation engine.
	 * 
	 * @return immutable list of {@link IFeatureConditionGenerator}s currently
	 *         used by this generation engine.
	 */
	public List<IFeatureConditionGenerator> getFeatureConditionGenerators();

	/**
	 * Generates an instance of the {@link ConditionsModel} for the specified
	 * <code>eObject</code>.
	 * 
	 * @param eObject
	 *            the {@link EObject} to create the model for.
	 * @return the created {@link ConditionsModel}.
	 */
	public ConditionsModel generateConditionsModel(EObject eObject);

	/**
	 * Generates a refined {@link ConditionsModel} for the specified root
	 * <code>eObject</code>.
	 * 
	 * <p>
	 * The generated {@link ConditionsModel} will refine the specified
	 * <code>baseConditionsModel</code>. That means, that for every model
	 * element represented by templates in the <code>baseConditionsModel</code>
	 * this method creates a {@link RefinementTemplate}. Based on the specified
	 * <code>differences</code>, for every different value a condition will be
	 * added to the {@link RefinementTemplate}. For new model elements, a usual
	 * {@link Template} will be created.
	 * </p>
	 * 
	 * @param eObject
	 *            the {@link EObject} to create the model for.
	 * @param baseConditionsModel
	 *            {@link ConditionsModel} acting as the basis.
	 * @param differences
	 *            to derive which parts to refine.
	 * @return the created refined {@link ConditionsModel}.
	 */
	public ConditionsModel generateRefinedConditionsModel(EObject eObject,
			ConditionsModel baseConditionsModel,
			ComparisonResourceSnapshot differences);

}
