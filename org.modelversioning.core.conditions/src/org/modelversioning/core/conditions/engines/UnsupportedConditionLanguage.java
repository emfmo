/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import org.modelversioning.core.conditions.ConditionsModel;

/**
 * Thrown if the condition language of the {@link ConditionsModel} is not supported.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class UnsupportedConditionLanguage extends Exception {

	/**
	 * Generated id.
	 */
	private static final long serialVersionUID = 6651875986708685736L;

}
