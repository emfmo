/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import org.modelversioning.core.conditions.Template;

/**
 * Thrown if either the creation or the application of an
 * {@link IOperationBinding} failed. Normally, this occurs if no object matched
 * a {@link Template} or an {@link ITemplateBindings} is incomplete.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class BindingException extends Throwable {

	/**
	 * Generated serial version id.
	 */
	private static final long serialVersionUID = 5981739570436744626L;

	/**
	 * The template causing this exception.
	 */
	private Template template;
	

	/**
	 * Creates a new exception specifying a message and the template.
	 * 
	 * @param msg excpetion message.
	 * @param template failed template.
	 */
	public BindingException(String msg, Template template) {
		super(msg);
		this.template = template;
	}

	/**
	 * Returns the {@link Template} causing this exception.
	 * 
	 * @return the template
	 */
	public Template getTemplate() {
		return template;
	}

}
