/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;

/**
 * A binding from {@link Template}s to {@link EObject}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ITemplateBinding {

	/**
	 * Returns all bound {@link EObject}s.
	 * 
	 * @return all bound {@link EObject}s.
	 */
	Collection<EObject> getBoundObjects();

	/**
	 * Returns the {@link EObject}s bound to <code>template</code>.
	 * 
	 * @param template
	 *            to get bound {@link EObject}s of.
	 * @return bound {@link EObject}s.
	 */
	Collection<EObject> getBoundObjects(Template template);

	/**
	 * Adds the binding of <code>template</code> to <code>eObjects</code>.
	 * 
	 * @param template
	 *            {@link Template} to add binding for.
	 * @param eObjects
	 *            bound {@link EObject}s.
	 */
	void add(Template template, Collection<EObject> eObjects);

	/**
	 * Adds the binding of <code>template</code> to <code>eObject</code>.
	 * 
	 * @param template
	 *            {@link Template} to add binding for.
	 * @param eObjects
	 *            bound {@link EObject}.
	 */
	void add(Template template, EObject eObject);

	/**
	 * Removes the binding of <code>template</code>.
	 * 
	 * @param template
	 *            {@link Template} of which the binding is to remove.
	 */
	void remove(Template template);

	/**
	 * Removes <code>eObject</code> which is currently bound to
	 * <code>template</code> out of the bound {@link EObject}s.
	 * 
	 * @param template
	 *            {@link Template} to which <code>eObject</code> is currently
	 *            bound.
	 * @param eObject
	 *            {@link EObject} to remove.
	 */
	void remove(Template template, EObject eObject);

	/**
	 * Removes <code>eObjects</code> which are currently bound to
	 * <code>template</code> out of the bound {@link EObject}s.
	 * 
	 * @param template
	 *            {@link Template} to which <code>eObject</code> is currently
	 *            bound.
	 * @param eObjects
	 *            {@link EObject}s to remove.
	 */
	void remove(Template template, Collection<EObject> eObjects);

	/**
	 * Clears this binding.
	 */
	void clear();

	/**
	 * Specifies if this binding is empty.
	 * 
	 * @return <code>true</code> if empty, <code>false</code> otherwise.
	 */
	boolean isEmpty();

	/**
	 * Specifies the size of this binding, i.e., the amount of templates in this
	 * binding.
	 * 
	 * @return the size.
	 */
	int size();

	/**
	 * The {@link Template} for which a binding exists.
	 * 
	 * @return {@link Template}s.
	 */
	Set<Template> getTemplates();

	/**
	 * Creates a clone of this binding.
	 * 
	 * @return cloned binding.
	 */
	ITemplateBinding clone();

	/**
	 * Creates a {@link Map} containing the single bindings, i.e., only the
	 * first bound EObject.
	 * 
	 * @return single binding map.
	 */
	Map<Template, EObject> getSingleBindingMap();

	/**
	 * Adds all bindings in specified <code>bindingCombination</code> to this
	 * template binding.
	 * 
	 * @param bindingCombination
	 *            to add bindings from.
	 */
	void addAll(ITemplateBinding bindingCombination);

	/**
	 * Returns the {@link Template} to which the specified <code>eObject</code>
	 * is bound to.
	 * 
	 * @param eObject
	 *            to find {@link Template} to which it is bound to.
	 * @return {@link Template} to which the specified <code>eObject</code> is
	 *         bound to.
	 */
	Template getBoundTemplate(EObject eObject);

}
