/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons.wizards;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.ide.IDE;
import org.modelversioning.core.util.ModelDiagramUtil;

/**
 * Wizard page for model selection. Based on the implementation of Felix Rinker
 * (formally ORNewSelectModelWizardPage).
 * 
 * @author Felix Rinker
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ModelDiagramSelectionPage extends WizardPage {

	private static final String REFLECTIVE_EDITOR_ID = "org.eclipse.emf.ecore.presentation.ReflectiveEditorID"; //$NON-NLS-1$
	private static final String ECORE_EDITOR_ID = "org.eclipse.emf.ecore.presentation.EcoreEditorID"; //$NON-NLS-1$
	private static final Object ECORE_EXT = "ecore"; //$NON-NLS-1$
	private Text fileField;
	private Combo editorCombo;
	@SuppressWarnings("unused")
	private IStructuredSelection selection;
	private Button btnBrowse;
	private Map<String, IEditorDescriptor> availableEditorMap;
	private Map<String, IFile> availableModelMap;
	private IEditorDescriptor selectedEditorDescriptor;
	private IFile selectedFile;
	private IWorkbench workbench;
	private ResourceSet resourceSet = new ResourceSetImpl();
	private Combo modelCombo;
	private boolean containsDiagram = false;

	/**
	 * Constructor.
	 * 
	 * @param pageName
	 *            name of page.
	 * @param selection
	 *            selection.
	 * @param workbench
	 *            for opening dialogs.
	 */
	public ModelDiagramSelectionPage(String pageName,
			IStructuredSelection selection, IWorkbench workbench) {
		super(pageName);
		this.workbench = workbench;
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	@Override
	public void createControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		container.setLayout(gridLayout);

		// Model section
		Group modelGroup = new Group(container, SWT.NULL);
		modelGroup.setText("Select model or diagram file"); //$NON-NLS-1$
		GridLayout gridLayout2 = new GridLayout();
		gridLayout2.numColumns = 3;
		modelGroup.setLayout(gridLayout2);
		GridData gridData1 = new GridData(GridData.FILL, GridData.CENTER, true,
				false);
		modelGroup.setLayoutData(gridData1);

		Label label = new Label(modelGroup, SWT.NULL);
		label.setText("&File:");

		fileField = new Text(modelGroup, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		fileField.setLayoutData(gd);
		fileField.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				handleSelectedFile();
				dialogChanged();
			}
		});

		this.btnBrowse = new Button(modelGroup, SWT.PUSH);
		this.btnBrowse.setText("Browse...");
		this.btnBrowse.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browseForFile();
			}
		});

		Group comboGroup = new Group(container, SWT.NULL);
		comboGroup.setText("Editor and model"); //$NON-NLS-1$
		gridLayout2 = new GridLayout();
		gridLayout2.numColumns = 2;
		comboGroup.setLayout(gridLayout2);
		gridData1 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		comboGroup.setLayoutData(gridData1);

		// Editor section
		label = new Label(comboGroup, SWT.NULL);
		label.setText("&Editor:");
		editorCombo = new Combo(comboGroup, SWT.READ_ONLY);
		editorCombo.setText("<select editor>");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		editorCombo.setLayoutData(gd);
		editorCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleEditorSelected();
				dialogChanged();
			}
		});

		// Model selection
		label = new Label(comboGroup, SWT.NULL);
		label.setText("&Model:");
		modelCombo = new Combo(comboGroup, SWT.READ_ONLY);
		modelCombo.setText("<select model file>");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		modelCombo.setLayoutData(gd);
		modelCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				dialogChanged();
			}
		});

		// Disable file browse and editor selection elements
		this.editorCombo.setEnabled(false);
		this.modelCombo.setEnabled(false);
		this.fileField.setEditable(false);

		dialogChanged();
		setControl(container);
	}

	/**
	 * Uses the file dialog to choose the new value for the model file field.
	 */
	private void browseForFile() {
		FileDialog fileDialog = new FileDialog(workbench
				.getActiveWorkbenchWindow().getShell());
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		fileDialog.setFilterPath(root.getLocation().toString());
		fileDialog.setText("Select model or diagram file");
		String selectedFile = fileDialog.open();
		if (selectedFile != null) {
			this.selectedFile = root.getFileForLocation(new Path(fileDialog
					.getFilterPath() + "/" + fileDialog.getFileName())); //$NON-NLS-1$
			this.fileField.setText(this.selectedFile.getName());
		}
	}

	/**
	 * Activate and rebuilt the model editor combo then set the model file as
	 * editor file.
	 */
	private void handleSelectedFile() {
		// update editor combo
		handleEditorCombo();

		// check selected file
		Resource resource = getResource(this.selectedFile);
		containsDiagram = ModelDiagramUtil.containsDiagram(resource);

		if (containsDiagram) {
			Collection<IFile> modelFiles = ModelDiagramUtil
					.getModelFilesFromDiagram(resource);
			updateModelCombo(modelFiles);
		} else {
			emptyModelCombo();
		}
	}

	/**
	 * Returns a {@link Resource} for the specified file.
	 * 
	 * @param file
	 *            the file to get resource from.
	 * @return a resource for the selected file.
	 */
	private Resource getResource(IFile file) {
		return resourceSet.getResource(
				URI.createURI(file.getLocationURI().toString()), true);
	}

	/**
	 * Updates {@link #availableModelMap} and updates the combo.
	 * 
	 * @param modelFiles
	 */
	private void updateModelCombo(Collection<IFile> modelFiles) {
		// create map for specified ifiles
		this.availableModelMap = new HashMap<String, IFile>();
		String[] labels = new String[modelFiles.size()];
		int index = 0;
		for (IFile modelFile : modelFiles) {
			this.availableModelMap.put(modelFile.getName(), modelFile);
			labels[index++] = modelFile.getName();
		}

		// update combo
		if (availableModelMap.size() > 0) {
			modelCombo.setItems(labels);
			modelCombo.setEnabled(true);
			modelCombo.select(0);
		} else {
			emptyModelCombo();
		}
	}

	/**
	 * Get all registered editors for the selected file and update the combo.
	 */
	private void handleEditorCombo() {
		// get available editors
		IContentType contentType = IDE.guessContentType(selectedFile);
		IEditorRegistry editorRegistry = this.workbench.getEditorRegistry();
		IEditorDescriptor[] editorArray = editorRegistry.getEditors(
				this.selectedFile.getName(), contentType);

		// create map
		availableEditorMap = new HashMap<String, IEditorDescriptor>();
		// String[] labels = new String[editorArray.length + 1];
		for (int i = 0; i < editorArray.length; i++) {
			availableEditorMap.put(editorArray[i].getLabel(), editorArray[i]);
		}

		// add ecore editor if appropriate extension
		if (ECORE_EXT.equals(selectedFile.getFileExtension())) {
			IEditorDescriptor ecoreEditor = editorRegistry
					.findEditor(ECORE_EDITOR_ID);
			availableEditorMap.put(ecoreEditor.getLabel(), ecoreEditor);
		}

		// add reflective editor in any case
		IEditorDescriptor reflectiveEditor = editorRegistry
				.findEditor(REFLECTIVE_EDITOR_ID);
		if (reflectiveEditor != null) {
			availableEditorMap.put(reflectiveEditor.getLabel(),
					reflectiveEditor);
		}

		// update combo
		if (availableEditorMap.size() > 0) {
			editorCombo.setItems(availableEditorMap.keySet().toArray(
					new String[availableEditorMap.keySet().size()]));
			editorCombo.setEnabled(true);
			editorCombo.select(0);
			handleEditorSelected();
		} else {
			emptyEditorCombo();
		}
	}

	/**
	 * Empties and disables the editor combo.
	 */
	private void emptyEditorCombo() {
		editorCombo.setItems(new String[0]);
		editorCombo.setEnabled(false);
	}

	/**
	 * Empties the model combo.
	 */
	private void emptyModelCombo() {
		modelCombo.setItems(new String[0]);
		modelCombo.setEnabled(false);
	}

	/**
	 * If an item is selected in the Editor Combo, find the related editor
	 * descriptor.
	 */
	private void handleEditorSelected() {
		this.selectedEditorDescriptor = availableEditorMap.get(editorCombo
				.getText());
	}

	/**
	 * Check all fields. If a field is empty, then give the user a message. This
	 * method is called if a control is changed.
	 */
	private void dialogChanged() {

		if (this.selectedFile == null) {
			updateStatus("A model or diagram file must be selected.");
			emptyEditorCombo();
			emptyModelCombo();
		} else if (editorCombo.getText().isEmpty()) {
			updateStatus("There is no registered editor for the selected file.");
		} else if (containsDiagram && this.modelCombo.getText().isEmpty()) {
			updateStatus("The selected file does not contain a model.");
		} else {
			updateStatus(null);
		}
	}

	/**
	 * Method for updating the statusbar at the top of the wizard
	 * 
	 * @param message
	 *            to set.
	 */
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	/**
	 * Specifies whether the user selected a model only and no additional
	 * diagram.
	 * 
	 * @return <code>true</code> if user selected model and model editor only.
	 *         <code>false</code> if user selected a diagram as well.
	 */
	public boolean isModelOnly() {
		return !containsDiagram;
	}

	/**
	 * Returns the selected diagram file. Might be <code>null</code> if
	 * {@link #isModelOnly()}.
	 * 
	 * @return the selected diagram file.
	 */
	public IFile getDiagramFile() {
		if (containsDiagram) {
			return this.selectedFile;
		} else {
			return null;
		}
	}

	/**
	 * Returns the selected model file.
	 * 
	 * @return the selected model file.
	 */
	public IFile getModelFile() {
		if (containsDiagram) {
			return availableModelMap.get(this.modelCombo.getText());
		} else {
			return this.selectedFile;
		}
	}

	/**
	 * Returns the selected editor.
	 * 
	 * @return the selected editor.
	 */
	public IEditorDescriptor getEditorFileDescriptor() {
		return selectedEditorDescriptor;
	}

}