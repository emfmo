/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons;

import org.eclipse.swt.widgets.Tree;

/**
 * A tree view providing a {@link Tree}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ITreeView {

	/**
	 * Returns the displayed {@link Tree}.
	 * 
	 * @return the displayed {@link Tree}.
	 */
	public Tree getTree();

}
