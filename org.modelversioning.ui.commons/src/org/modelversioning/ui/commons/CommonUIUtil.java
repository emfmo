/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Helper methods for common UI tasks.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class CommonUIUtil {

	/**
	 * Returns an {@link IFile} handle for the specified {@link Resource}.
	 * 
	 * @param resource
	 *            to get IFile for.
	 * @return the {@link IFile}.
	 */
	public static IFile getIFile(Resource resource) {
		return ResourcesPlugin.getWorkspace().getRoot()
				.getFile(new Path(resource.getURI().toPlatformString(true)));
	}

	/**
	 * Deletes the specified file.
	 * 
	 * @param file
	 *            file to delete.
	 */
	public static void deleteFile(IFile file, IProgressMonitor monitor) {
		if (file != null && file.exists()) {
			try {
				file.delete(true, monitor);
			} catch (CoreException e) {
				// ignore.
			}
		}
	}

	/**
	 * Sets the specified resource to be read-only.
	 * 
	 * @param file
	 *            to set read-only.
	 * @param readOnly
	 *            <code>true</code> to set it read only, <code>false</code> to
	 *            set writable.
	 */
	public static void setReadOnly(IFile file, boolean readOnly) {
		try {
			file.setHidden(true);
			ResourceAttributes attributes = file.getResourceAttributes();
			if (attributes == null) {
				attributes = new ResourceAttributes();
			}
			// attributes.setHidden(true);
			attributes.setReadOnly(readOnly);
			file.setResourceAttributes(attributes);
		} catch (CoreException e) {
			// ignore
		}
	}

}
