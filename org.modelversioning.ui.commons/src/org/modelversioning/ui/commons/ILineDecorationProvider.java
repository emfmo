/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Provides information on decoration of lines represinting a connection between
 * two {@link TreeItem}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ILineDecorationProvider {

	/**
	 * Returns the decoration for the specified connection between
	 * <code>leftItem</code> and <code>rightItem</code>.
	 * 
	 * @param leftItem
	 *            left item (begin of the line).
	 * @param rightItem
	 *            right item (end of the line).
	 * @param isOneHidden
	 *            specifies if one of the items is hidden.
	 * @return decoration for the specified connection.
	 */
	public ILineDecoration getLineDecoration(TreeItem leftItem,
			TreeItem rightItem, boolean isOneHidden);

	/**
	 * Specifies the line decoration.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	public interface ILineDecoration {

		/**
		 * The width of the line.
		 * 
		 * @return width of the line.
		 */
		public int getWidth();

		/**
		 * The color of the line.
		 * 
		 * @return color of the line.
		 */
		public RGB getColor();

		/**
		 * The line style.
		 * 
		 * @see SWT#LINE_DASH
		 * 
		 * @return line style.
		 */
		public int getLineStyle();

	}

	/**
	 * Basic implementation of the {@link ILineDecoration} interface.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 */
	public class LineDecoration implements ILineDecoration {

		final RGB color;
		final int lineStyle;
		final int width;

		/**
		 * Creates a new {@link LineDecoration} specifying all the fields.
		 * 
		 * @param color
		 *            color to set.
		 * @param lineStyle
		 *            lineStyle to set.
		 * @param width
		 *            width to set.
		 */
		public LineDecoration(RGB color, int lineStyle, int width) {
			super();
			this.color = color;
			this.lineStyle = lineStyle;
			this.width = width;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public RGB getColor() {
			return color;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getLineStyle() {
			return lineStyle;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getWidth() {
			return width;
		}

	}

}
