/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons;

import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Provides information on whether a {@link TreeItem} of a <code>leftTree</code>
 * should be connected or not.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ITreeConnectionProvider {

	/**
	 * Returns the {@link TreeItem}s contained by <code>rightTree</code> which
	 * should be connected to the specified <code>leftItem</code>.
	 * 
	 * @param leftItem
	 *            {@link TreeItem} to get {@link TreeItem} contained by
	 *            <code>rightTree</code> to connect.
	 * @param rightTree
	 *            right {@link Tree}.
	 * @return {@link TreeItem}s contained by <code>rightTree</code> which should
	 *         be connected or <code>null</code> if it should not be connected.
	 */
	public TreeItem[] getConnectedItems(TreeItem leftItem, Tree rightTree);

}
