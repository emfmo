/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.ui.commons.parts;

import org.eclipse.emf.compare.util.AdapterUtils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.part.ViewPart;
import org.modelversioning.ui.commons.ITreeView;

/**
 * View showing the specified {@link EObject} and its children.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class EcoreTreeView extends ViewPart implements ITreeView {

	/**
	 * Tree viewer of this view part.
	 */
	private TreeViewer viewer;
	/**
	 * The root object
	 */
	private EObject rootObject;

	/**
	 * The constructor.
	 * 
	 * @param the
	 *            root object to visualize.
	 */
	public EcoreTreeView(EObject rootObject) {
		this.rootObject = rootObject;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL
				| SWT.BORDER);
		viewer.setContentProvider(new AdapterFactoryContentProvider(
				AdapterUtils.getAdapterFactory()));
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(AdapterUtils
				.getAdapterFactory()));
		viewer.setInput(rootObject.eResource());
		viewer.expandAll();
	}

	/**
	 * Sets the root object to visualize.
	 * 
	 * @param eObject
	 *            object to visualize.
	 */
	public void setRootObject(EObject eObject) {
		this.rootObject = eObject;
		viewer.setInput(eObject);
		viewer.refresh();
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	/**
	 * Returns the currently selected {@link EObject}s.
	 * 
	 * @return the currently selected {@link EObject}s.
	 */
	public EObject[] getSelectedEObjects() {
		TreeItem[] selection = getTree().getSelection();
		EObject[] eObjects = new EObject[selection.length];
		int i = 0;
		for (TreeItem treeItem : selection) {
			if (treeItem.getData() instanceof EObject) {
				eObjects[i++] = (EObject) treeItem.getData();
			}
		}
		return eObjects;
	}

	/**
	 * Returns the tree contained by this view.
	 * 
	 * @return the tree.
	 */
	public Tree getTree() {
		return viewer.getTree();
	}
	
	/**
	 * Redraws this component.
	 */
	public void layout() {
		viewer.getControl().redraw();
	}
	
}