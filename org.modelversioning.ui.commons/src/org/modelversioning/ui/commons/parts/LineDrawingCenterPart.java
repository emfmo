/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.ui.commons.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;
import org.modelversioning.ui.commons.ILineDecorationProvider;
import org.modelversioning.ui.commons.ITreeConnectionProvider;
import org.modelversioning.ui.commons.ITreeView;
import org.modelversioning.ui.commons.ILineDecorationProvider.ILineDecoration;
import org.modelversioning.ui.commons.ILineDecorationProvider.LineDecoration;

/**
 * Canvas showing lines between two trees.
 * 
 * Inspired by {@link AbstractCenterPart} by <a
 * href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>.
 * 
 * FIXME show multiply bound elements.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class LineDrawingCenterPart extends Canvas {

	/**
	 * The offset elements to hidden lines should be lowered.
	 */
	private static final int HIDDEN_OFFSET = 2;

	/** Buffer used by this {@link Canvas} to smoothly paint its content. */
	protected Image buffer;

	/**
	 * This array is used to compute the curve to draw between left and right
	 * elements.
	 */
	private double[] baseCenterCurve;

	/**
	 * The left tree.
	 */
	private ITreeView leftTreeView;

	/**
	 * The right tree.
	 */
	private ITreeView rightTreeView;

	/**
	 * Connection provider to use.
	 */
	private ITreeConnectionProvider connectionProvider;

	/**
	 * The line decoration provider.
	 */
	private ILineDecorationProvider lineDecorationProvider = null;

	/**
	 * The width of this center part.
	 */
	private int centerPartWidth;

	/**
	 * Default constructor, instantiates the canvas given its parent.
	 * 
	 * @param parent
	 *            Parent of the canvas.
	 * @param leftTreeView
	 *            the left tree.
	 * @param rightTreeView
	 *            the the right tree.
	 * @param centerPartWidth
	 *            the width of the center part.
	 * @param connectionProvider
	 *            the {@link ITreeConnectionProvider} to ask for connections to
	 *            draw.
	 */
	public LineDrawingCenterPart(Composite parent, ITreeView leftTreeView,
			ITreeView rightTreeView, int centerPartWidth,
			ITreeConnectionProvider connectionProvider) {
		super(parent, SWT.BACKGROUND | SWT.NO_MERGE_PAINTS
				| SWT.NO_REDRAW_RESIZE);

		this.leftTreeView = leftTreeView;
		this.rightTreeView = rightTreeView;
		this.connectionProvider = connectionProvider;
		this.centerPartWidth = centerPartWidth;

		init();
	}

	/**
	 * Default constructor, instantiates the canvas given its parent.
	 * 
	 * @param parent
	 *            Parent of the canvas.
	 * @param leftTreeView
	 *            the left tree.
	 * @param tree
	 *            the right tree.
	 * @param centerPartWidth
	 *            the width of the center part.
	 * @param connectionProvider
	 *            the {@link ITreeConnectionProvider} to ask for connections to
	 *            draw.
	 * @param lineDecorationProvider
	 *            the {@link ILineDecorationProvider} to use to decorate lines.
	 */
	public LineDrawingCenterPart(Composite parent, ITreeView leftTreeView,
			ITreeView rightTreeView, int centerPartWidth,
			ITreeConnectionProvider connectionProvider,
			ILineDecorationProvider lineDecorationProvider) {
		super(parent, SWT.BACKGROUND | SWT.NO_MERGE_PAINTS
				| SWT.NO_REDRAW_RESIZE);

		this.leftTreeView = leftTreeView;
		this.rightTreeView = rightTreeView;
		this.connectionProvider = connectionProvider;
		this.lineDecorationProvider = lineDecorationProvider;
		this.centerPartWidth = centerPartWidth;

		init();
	}

	/**
	 * Initializes this {@link LineDrawingCenterPart}.
	 */
	private void init() {
		final PaintListener paintListener = new PaintListener() {
			public void paintControl(PaintEvent event) {
				doubleBufferedPaint(event.gc);
			}
		};
		addPaintListener(paintListener);

		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				if (buffer != null) {
					buffer.dispose();
					buffer = null;
				}
				removePaintListener(paintListener);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		super.dispose();
		if (buffer != null) {
			buffer.dispose();
		}
		baseCenterCurve = null;
	}

	/**
	 * Clients should implement this method for the actual drawing.
	 * 
	 * @see ModelContentMergeViewer#getCenterPart()
	 * 
	 * @param gc
	 *            {@link GC} used for the painting.
	 */
	public void doPaint(GC gc) {
		paintLines(gc, leftTreeView.getTree().getItems());
	}

	/**
	 * Calls {@link #drawLine(GC, TreeItem, TreeItem)} for each specified
	 * {@link TreeItem} in <code>items</code> including their children.
	 * 
	 * @param gc
	 *            {@link GC} used for the painting.
	 * @param items
	 *            items to paint lines for.
	 */
	private void paintLines(GC gc, TreeItem[] items) {
		for (TreeItem item : items) {
			TreeItem[] itemsToConnect = connectionProvider.getConnectedItems(
					item, rightTreeView.getTree());
			for (TreeItem itemToConnect : itemsToConnect) {
				drawLine(gc, item, itemToConnect);
			}

			paintLines(gc, item.getItems());
		}
	}

	/**
	 * @return the lineDecorationProvider
	 */
	public ILineDecorationProvider getLineDecorationProvider() {
		return lineDecorationProvider;
	}

	/**
	 * @param lineDecorationProvider
	 *            the lineDecorationProvider to set
	 */
	public void setLineDecorationProvider(
			ILineDecorationProvider lineDecorationProvider) {
		this.lineDecorationProvider = lineDecorationProvider;
	}

	/**
	 * Draws a line connecting the given right and left items.
	 * 
	 * @param gc
	 *            {@link GC graphics configuration} on which to actually draw.
	 * @param leftItem
	 *            Left of the two items to connect.
	 * @param rightItem
	 *            Right of the items to connect.
	 */
	protected void drawLine(GC gc, TreeItem leftItem, TreeItem rightItem) {
		if (leftItem == null || rightItem == null)
			return;

		// handle hidden elements
		TreeItem originalLeftItem = leftItem;
		TreeItem originalRightItem = rightItem;
		boolean isRightItemHidden = isHidden(rightItem);
		boolean isLeftItemHidden = isHidden(leftItem);
		if (isRightItemHidden) {
			rightItem = getLeastVisibleParent(rightItem);
		} else if (isLeftItemHidden) {
			leftItem = getLeastVisibleParent(leftItem);
		}

		// create or retrieve line decoration for the line to draw
		ILineDecoration lineDecoration = null;
		if (lineDecorationProvider != null) {
			lineDecoration = lineDecorationProvider.getLineDecoration(
					originalLeftItem, originalRightItem,
					(isRightItemHidden || isLeftItemHidden));
		} else {
			lineDecoration = new LineDecoration(new RGB(10, 10, 10),
					SWT.LINE_SOLID, 1);
		}

		final Rectangle drawingBounds = getBounds();

		// Defines all variables needed for drawing the line.
		int hiddenOffsetLeft = 0;
		int hiddenOffsetRight = 0;
		final int leftX = 0;
		final int rightX = drawingBounds.width;
		final int leftY;
		final int rightY;

		// if left is hidden set hiddenOffsetLeft
		if (isLeftItemHidden) {
			hiddenOffsetLeft = HIDDEN_OFFSET;
		}
		if (isRightItemHidden) {
			hiddenOffsetRight = HIDDEN_OFFSET;
		}

		if (System.getProperty("os.name").contains("Windows")) { //$NON-NLS-1$ //$NON-NLS-2$
			// TODO check

			leftY = leftItem.getBounds().y + (leftItem.getBounds().height / 2)
					+ hiddenOffsetLeft;
			rightY = rightItem.getBounds().y
					+ (rightItem.getBounds().height / 2) + hiddenOffsetRight;

			// leftY = leftItem.getBounds().y - hiddenOffsetLeft;
			// rightY = rightItem.getBounds().y - hiddenOffsetRight;
			// // leftY = leftItem.getCurveY() + treeTabBorder;
			// // rightY = rightItem.getCurveY() + treeTabBorder;
		} else {
			leftY = leftItem.getBounds().y + (leftItem.getBounds().height / 2)
					+ hiddenOffsetLeft;
			rightY = rightItem.getBounds().y
					+ (rightItem.getBounds().height / 2) + hiddenOffsetRight;
		}
		// Performs the actual drawing
		gc.setForeground(new Color(getDisplay(), lineDecoration.getColor()));
		gc.setLineWidth(lineDecoration.getWidth());
		gc.setLineStyle(lineDecoration.getLineStyle());
		final int[] points = getCenterCurvePoints(leftX, leftY, rightX, rightY);
		for (int i = 1; i < points.length; i++) {
			gc.drawLine(leftX + i - 1, points[i - 1], leftX + i, points[i]);
		}
	}

	/**
	 * Returns the least visible parent of <code>item</code>. If
	 * <code>item</code> is visible this method will return <code>item</code>.
	 * If there is no visible parent this method returns <code>null</code>.
	 * 
	 * @param item
	 *            item to get least visible parent of.
	 * @return least visible parent.
	 */
	private TreeItem getLeastVisibleParent(TreeItem item) {
		if (!isHidden(item)) {
			return item;
		} else {
			TreeItem parentItem = item;
			while ((parentItem = parentItem.getParentItem()) != null) {
				if (!isHidden(parentItem)) {
					return parentItem;
				}
			}
		}
		return null;
	}

	/**
	 * Specifies whether the <code>item</code> is hidden (not visible, i.e.,
	 * collapsed) or not.
	 * 
	 * @param item
	 *            item to check.
	 * @return <code>true</code> if hidden, <code>false</code> otherwise.
	 */
	private boolean isHidden(TreeItem item) {
		if (item.getParentItem() == null) {
			return false;
		} else if (item.getBounds().y == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Paints this component using double-buffering.
	 * 
	 * @param dest
	 *            Destination {@link GC graphics}.
	 */
	void doubleBufferedPaint(GC dest) {
		final Point size = getSize();

		if (size.x <= 0 || size.y <= 0)
			return;

		if (buffer != null) {
			final Rectangle bufferBounds = buffer.getBounds();
			if (bufferBounds.width != size.x || bufferBounds.height != size.y) {
				buffer.dispose();
				buffer = null;
			}
		}

		if (buffer == null) {
			buffer = new Image(getDisplay(), size.x, size.y);
		}

		final GC gc = new GC(buffer);
		// Draw lines on the left and right edges
		gc.setForeground(getDisplay().getSystemColor(
				SWT.COLOR_WIDGET_NORMAL_SHADOW));
		gc.drawLine(0, 0, 0, getBounds().height);
		gc.drawLine(getBounds().width - 1, 0, getBounds().width - 1,
				getBounds().height);
		try {
			gc.setBackground(getBackground());
			gc.fillRectangle(0, 0, size.x, size.y);
			doPaint(gc);
		} finally {
			gc.dispose();
		}

		dest.drawImage(buffer, 0, 0);
	}

	/**
	 * Computes the points needed to draw a curve of the given width.
	 * 
	 * @param width
	 *            This is the width of the curve to build.
	 */
	private void buildBaseCenterCurve(int width) {
		final double doubleWidth = width;
		baseCenterCurve = new double[centerPartWidth];
		for (int i = 0; i < centerPartWidth; i++) {
			final double r = i / doubleWidth;
			baseCenterCurve[i] = Math.cos(Math.PI * r);
		}
	}

	/**
	 * Computes the points to connect for the curve between the two items to
	 * connect.
	 * 
	 * @param startx
	 *            X coordinate of the starting point.
	 * @param starty
	 *            Y coordinate of the starting point.
	 * @param endx
	 *            X coordinate of the ending point.
	 * @param endy
	 *            Y coordinate of the ending point.
	 * @return The points to connect to draw the curve between the two items to
	 *         connect.
	 */
	private int[] getCenterCurvePoints(int startx, int starty, int endx,
			int endy) {
		if (baseCenterCurve == null) {
			buildBaseCenterCurve(endx - startx);
		}
		double height = endy - starty;
		height = height / 2;
		final int width = endx - startx;
		final int[] points = new int[width];
		// for (int i = 0; i < width; i++) {
		for (int i = 0; i < baseCenterCurve.length; i++) {
			points[i] = (int) (-height * baseCenterCurve[i] + height + starty);
		}
		return points;
	}
}
