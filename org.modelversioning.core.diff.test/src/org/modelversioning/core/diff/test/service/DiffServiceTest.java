/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.test.service;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.MoveModelElement;
import org.eclipse.emf.compare.diff.metamodel.UpdateReference;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.diff.service.DiffService;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffServiceTest extends TestCase {

	Resource origin = null;
	Resource workingCopy1 = null;
	Resource workingCopy2 = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		super.setUp();

		// load id resources
		ResourceSet resourceSet_id = new ResourceSetImpl();
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		URI fileURI_id = URI.createPlatformPluginURI(
				"/org.modelversioning.core.diff.test/models/origin_ids.ecore",
				true);
		origin = resourceSet_id.getResource(fileURI_id, true);
		URI fileURI1_id = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.diff.test/models/working_copy_1_ids.ecore",
						true);
		workingCopy1 = resourceSet_id.getResource(fileURI1_id, true);
		URI fileURI2_id = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.diff.test/models/working_copy_2_ids.ecore",
						true);
		workingCopy2 = resourceSet_id.getResource(fileURI2_id, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		this.origin.unload();
		this.workingCopy1.unload();
		this.workingCopy2.unload();
		super.tearDown();
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.core.diff.service.DiffService#generateModelInputSnapshot(org.eclipse.emf.compare.match.metamodel.MatchModel)}
	 * .
	 * 
	 * @throws MatchException
	 * @throws IOException
	 */
	public void testGenerateModelInputSnapshot() throws MatchException,
			IOException {
		// do 2way match
		MatchService matchService_2way = new MatchService();
		MatchModel matchModel_2way = matchService_2way.generateMatchModel(
				origin, workingCopy1);
		// do 2way diff
		DiffService diffService = new DiffService();
		ComparisonResourceSnapshot comparisonResourceSnapshot_2way = diffService
				.generateComparisonResourceSnapshot(matchModel_2way);
		// check results
		// TODO redo checks
		assertTrue(comparisonResourceSnapshot_2way.getDiff().getOwnedElements()
				.get(0).getSubDiffElements().get(0).getSubDiffElements().get(0)
				.getSubDiffElements().get(0).getSubDiffElements().get(0) instanceof MoveModelElement);
		
		

		// do 3way match
		MatchService matchService = new MatchService();
		MatchModel matchModel = matchService.generateMatchModel(origin,
				workingCopy1, workingCopy2);
		// do 3way diff
		ComparisonResourceSnapshot comparisonResourceSnapshot = diffService
				.generateComparisonResourceSnapshot(matchModel);
		// check result
		// TODO redo checks
		assertEquals(1, comparisonResourceSnapshot.getDiff().getOwnedElements()
				.size());
		assertEquals(1, comparisonResourceSnapshot.getDiff().getOwnedElements()
				.get(0).getSubDiffElements().get(0).getSubDiffElements().size());
		assertEquals(2, comparisonResourceSnapshot.getDiff().getOwnedElements()
				.get(0).getSubDiffElements().get(0).getSubDiffElements().get(0)
				.getSubDiffElements().size());
		assertTrue(comparisonResourceSnapshot.getDiff().getOwnedElements().get(
				0).getSubDiffElements().get(0).getSubDiffElements().get(0)
				.getSubDiffElements().get(0).getSubDiffElements().get(0) instanceof UpdateReference);
	}
}
