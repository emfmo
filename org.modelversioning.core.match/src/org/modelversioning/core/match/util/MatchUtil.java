/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.util;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.match.metamodel.Match2Elements;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.compare.match.metamodel.Side;
import org.eclipse.emf.compare.match.metamodel.UnmatchElement;
import org.eclipse.emf.ecore.EObject;

/**
 * Utilities for {@link MatchModel}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class MatchUtil {

	/**
	 * Returns the matching object in the match model of the specified
	 * <code>eObject</code>. If there is no matching object this method returns
	 * <code>null</code>.
	 * 
	 * @param eObject
	 *            to find match of.
	 * @return the matching object or <code>null</code> if there is none.
	 */
	public static EObject getMatchingObject(EObject eObject,
			MatchModel matchModel) {
		TreeIterator<EObject> matches = matchModel.eAllContents();
		while (matches.hasNext()) {
			EObject match = matches.next();
			if (match instanceof Match2Elements) {
				Match2Elements match2Elements = (Match2Elements) match;
				if (eObject.equals(match2Elements.getLeftElement())) {
					return match2Elements.getRightElement();
				} else if (eObject.equals(match2Elements.getRightElement())) {
					return match2Elements.getLeftElement();
				}
			}
		}
		return null;
	}

	/**
	 * Returns the {@link Side} on which the specified <code>eObject</code>
	 * resides.
	 * 
	 * @param eObject
	 *            in question.
	 * @param matchModel
	 *            to search in.
	 * @return the {@link Side} or <code>null</code> if it couldn't been found.
	 */
	public static Side getSide(EObject eObject, MatchModel matchModel) {
		TreeIterator<EObject> matches = matchModel.eAllContents();
		while (matches.hasNext()) {
			EObject match = matches.next();
			if (match instanceof Match2Elements) {
				Match2Elements match2Elements = (Match2Elements) match;
				if (eObject.equals(match2Elements.getLeftElement())) {
					return Side.LEFT;
				} else if (eObject.equals(match2Elements.getRightElement())) {
					return Side.RIGHT;
				}
			} else if (match instanceof UnmatchElement) {
				UnmatchElement unmatchElement = (UnmatchElement) match;
				if (eObject.equals(unmatchElement.getElement())) {
					return unmatchElement.getSide();
				}
			}
		}
		return null;
	}

}
