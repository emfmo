/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.engine.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.match.metamodel.Match2Elements;
import org.eclipse.emf.compare.match.metamodel.Match3Elements;
import org.eclipse.emf.compare.match.metamodel.MatchFactory;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.compare.match.metamodel.Side;
import org.eclipse.emf.compare.match.metamodel.UnmatchElement;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.engine.IMatchEngine;
import org.modelversioning.core.util.EcoreUtil;
import org.modelversioning.core.util.UUIDUtil;

/**
 * Generic {@link IMatchEngine} that matches based on {@link UUIDUtil 
 * UUIDUtil's} ID functionality.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UUIDMatchEngine implements IMatchEngine {

	private Set<EObject> unmatched = new HashSet<EObject>();

	/**
	 * {@inheritDoc}
	 * 
	 * This match engine will accept with a priority of 2 if the
	 * <code>eObjects</code> have a UUID (using {@link UUIDUtil}).
	 */
	@Override
	public int accept(EObject... eObjects) {
		for (EObject root : eObjects) {
			String uuid = UUIDUtil.getUUID(root);
			if (uuid == null) {
				return 0;
			}
		}
		return 2;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatchModel generateMatchModel(Resource resource1, Resource resource2)
			throws MatchException {

		unmatched.clear();

		// create MatchModel
		MatchModel matchModel = MatchFactory.eINSTANCE.createMatchModel();
		matchModel.getLeftRoots().addAll(resource1.getContents());
		matchModel.getRightRoots().addAll(resource2.getContents());
		// matchModel.setLeftModel(resource1.getURI().toString());
		// matchModel.setRightModel(resource2.getURI().toString());

		// create and add Match2Elements and UnMatchedElements
		matchModel.getMatchedElements().addAll(
				createMatch2Elements(resource2.getContents(), resource1,
						matchModel.getUnmatchedElements(), 0));

		// find and add unmatched elements on the right side
		matchModel.getUnmatchedElements()
				.addAll(findUnMatchedElements(resource1.getContents(),
						resource2, false));

		return matchModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatchModel generateMatchModel(EObject eObject1, EObject eObject2)
			throws MatchException {

		unmatched.clear();

		// create MatchModel
		MatchModel matchModel = MatchFactory.eINSTANCE.createMatchModel();
		matchModel.getLeftRoots().add(eObject1);
		matchModel.getRightRoots().add(eObject2);
		// matchModel.setLeftModel(resource1.getURI().toString());
		// matchModel.setRightModel(resource2.getURI().toString());

		// create and add Match2Elements and UnMatchedElements
		EList<EObject> list2 = new BasicEList<EObject>();
		list2.add(eObject2);
		matchModel.getMatchedElements().addAll(
				createMatch2Elements(list2, eObject1.eResource(),
						matchModel.getUnmatchedElements(), 0, eObject1));

		// find and add unmatched elements on the right side
		EList<EObject> list1 = new BasicEList<EObject>();
		list1.add(eObject1);
		matchModel.getUnmatchedElements().addAll(
				findUnMatchedElements(list1, eObject2.eResource(), false,
						eObject2));

		return matchModel;
	}

	/**
	 * Creates and returns a {@link EList} of {@link Match2Elements} matching
	 * the specified <code>leftObjects</code> based on their <code>xmi:id</code>
	 * s to their opposites in <code>rightResource</code>.
	 * 
	 * <p>
	 * This method goes down recursively adding the sub matches to the returned
	 * {@link EList} of {@link Match2Elements}.
	 * </p>
	 * 
	 * @param leftObjects
	 *            {@link EObject}s to match.
	 * @param rightResource
	 *            {@link Resource} containing {@link EObject}s to match with.
	 * @param unmatchedElements
	 *            {@link EList} to add unmatched elements to.
	 * @param level
	 *            recursion level hint.
	 * @return the {@link EList} of {@link Match2Elements}.
	 */
	private EList<Match2Elements> createMatch2Elements(
			EList<EObject> leftObjects, Resource rightResource,
			EList<UnmatchElement> unmatchedElements, int level) {
		return createMatch2Elements(leftObjects, rightResource,
				unmatchedElements, level, null);
	}

	/**
	 * Creates and returns a {@link EList} of {@link Match2Elements} matching
	 * the specified <code>leftObjects</code> based on their <code>xmi:id</code>
	 * s to their opposites in <code>rightResource</code>.
	 * 
	 * <p>
	 * This method goes down recursively adding the sub matches to the returned
	 * {@link EList} of {@link Match2Elements}.
	 * </p>
	 * 
	 * @param leftObjects
	 *            {@link EObject}s to match.
	 * @param rightResource
	 *            {@link Resource} containing {@link EObject}s to match with.
	 * @param unmatchedElements
	 *            {@link EList} to add unmatched elements to.
	 * @param level
	 *            recursion level hint.
	 * @param matchOnlyRoot2
	 *            Indicates that only objects should be considered as matched
	 *            which are contained by the specified
	 *            <code>matchOnlyRoot2</code>. This parameter may be
	 *            <code>null</code> to ignore containment.
	 * @return the {@link EList} of {@link Match2Elements}.
	 */
	private EList<Match2Elements> createMatch2Elements(
			EList<EObject> leftObjects, Resource rightResource,
			EList<UnmatchElement> unmatchedElements, int level,
			EObject matchOnlyRoot2) {
		// create list to return
		EList<Match2Elements> match2Elements = new BasicEList<Match2Elements>();
		// guard empty or null lists
		if (leftObjects != null && leftObjects.size() > 0) {
			// iterate leftObjects
			for (EObject leftObject : leftObjects) {
				// get right opposite
				EObject rightObject = UUIDUtil.getObject(rightResource,
						UUIDUtil.getUUID(leftObject));

				// check if right opposite existing
				boolean containedByRoot2 = matchOnlyRoot2 == null
						|| rightObject == null // if rightObject is null, we
												// don't care about root
												// containment
						|| rightObject.equals(matchOnlyRoot2)
						|| EcoreUtil.createParentList(rightObject).contains(
								matchOnlyRoot2);
				if (rightObject != null && containedByRoot2) {
					// if existing create Match2Elements
					Match2Elements match2Element = MatchFactory.eINSTANCE
							.createMatch2Elements();
					match2Element.setLeftElement(leftObject);
					match2Element.setRightElement(rightObject);
					match2Element.setSimilarity(1d);
					// // go deeper eventually and add children
					if (level < 1) {
						// if root level add them as sub elements
						match2Element.getSubMatchElements().addAll(
								createMatch2Elements(leftObject.eContents(),
										rightResource, unmatchedElements,
										level + 1, matchOnlyRoot2));
					}
					// add created Match2Element
					match2Elements.add(match2Element);

				} else if (leftObject instanceof EGenericType
						&& containedByRoot2) {
					// try to match Generic Type
					Match2Elements match2ElementGeneric = matchGenericType(
							(EGenericType) leftObject, rightResource);
					// if Generic Type could be matched add it
					if (match2ElementGeneric != null) {
						match2Elements.add(match2ElementGeneric);
					} else {
						// if not create UnMatchElement
						if (!isUnmatchedAlready(EcoreUtil
								.createParentList(leftObject))) {
							unmatchedElements.add(createUnMatchElement(
									leftObject, Side.LEFT, false));
						}
					}

				} else {
					// if not existing create UnMatchElement
					if (!isUnmatchedAlready(EcoreUtil
							.createParentList(leftObject))) {
						unmatchedElements.add(createUnMatchElement(leftObject,
								Side.LEFT, false));
					}
				}
				// process children anyway
				if (level >= 1) {
					match2Elements.addAll(createMatch2Elements(
							leftObject.eContents(), rightResource,
							unmatchedElements, level + 1, matchOnlyRoot2));
				}
			}
		}

		return match2Elements;
	}

	/**
	 * Tries to match the {@link EGenericType}s <code>leftObject</code> with the
	 * opposite in <code>rightResource</code>.
	 * 
	 * @param leftObject
	 *            the {@link EGenericType} to match.
	 * @param rightResource
	 *            the right {@link Resource} to find right opposite.
	 * @return the {@link Match2Elements} instance representing the match or
	 *         <code>null</code> if no match could be obtained.
	 */
	private Match2Elements matchGenericType(EGenericType leftObject,
			Resource rightResource) {
		// get left parent
		EObject leftParent = leftObject.eContainer();
		// get right parent
		EObject rightParent = UUIDUtil.getObject(rightResource,
				UUIDUtil.getUUID(leftParent));
		// get containment feature
		EStructuralFeature feature = leftObject.eContainingFeature();

		// if left and right parent aren't null find their value of the feature
		// and match them.
		if (leftParent != null && rightParent != null) {
			boolean isEqual = false;
			Object rightObject = rightParent.eGet(feature);
			if (leftObject.getEClassifier() == null) {
				return null;
			}
			String leftName = leftObject.getEClassifier().getName();
			// fetch inner object if multi-valued feature
			if (feature.isMany() && rightObject != null) {
				int leftIndex = ((List<?>) leftParent.eGet(feature))
						.indexOf(leftObject);
				List<?> rightList = ((List<?>) rightObject);
				if (rightList.size() > leftIndex) {
					rightObject = rightList.get(leftIndex);
				}
			}
			if (rightObject != null && rightObject instanceof EGenericType) {
				String rightName = ((EGenericType) rightObject)
						.getEClassifier().getName();
				if (leftName.equals(rightName)) {
					isEqual = true;
				}
			}

			if (isEqual) {
				Match2Elements match2Elements = MatchFactory.eINSTANCE
						.createMatch2Elements();
				match2Elements.setLeftElement(leftObject);
				match2Elements.setRightElement((EObject) rightObject);
				match2Elements.setSimilarity(1d);
				return match2Elements;
			}
		}

		// if nothing helped, return null
		return null;
	}

	/**
	 * Specifies whether <code>eObject</code> is already in the list of
	 * unmatched elements.
	 * 
	 * @param eObject
	 *            to check.
	 * @return <code>true</code> if already marked as unmatched, otherwise
	 *         <code>false</code>.
	 */
	private boolean isUnmatchedAlready(EObject eObject) {
		return unmatched.contains(eObject);
	}

	/**
	 * Specifies whether one of the specified <code>eObjects</code> is already
	 * in the list of unmatched elements.
	 * 
	 * @param eObjects
	 *            to check.
	 * @return <code>true</code> if one is already marked as unmatched,
	 *         otherwise <code>false</code>.
	 */
	private boolean isUnmatchedAlready(Collection<EObject> eObjects) {
		for (EObject eObject : eObjects) {
			if (isUnmatchedAlready(eObject)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds {@link EObject} out of the specified {@link EList}
	 * <code>eObjects</code> which have no matching opposite in the specified
	 * <code>resource</code>.
	 * 
	 * <p>
	 * If <code>remote</code> is set to <code>true</code>, then
	 * {@link RemoteUnMatchElement}s will be created instead of
	 * {@link UnMatchElement}s.
	 * </p>
	 * 
	 * @param eObjects
	 *            {@link EList} of {@link EObject} to find unmatched elements.
	 * @param resource
	 *            {@link Resource} to search for matching elements.
	 * @param remote
	 *            specifies if {@link RemoteUnMatchElement}s or
	 *            {@link UnMatchElement}s should be created.
	 * @return the {@link EList} of {@link UnMatchElement}s or
	 *         {@link RemoteUnMatchElement}s.
	 */
	private EList<UnmatchElement> findUnMatchedElements(
			EList<EObject> eObjects, Resource resource, boolean remote) {
		return findUnMatchedElements(eObjects, resource, remote, null);
	}

	/**
	 * Finds {@link EObject} out of the specified {@link EList}
	 * <code>eObjects</code> which have no matching opposite in the specified
	 * <code>resource</code>.
	 * 
	 * <p>
	 * If <code>remote</code> is set to <code>true</code>, then
	 * {@link RemoteUnMatchElement}s will be created instead of
	 * {@link UnMatchElement}s.
	 * </p>
	 * 
	 * @param eObjects
	 *            {@link EList} of {@link EObject} to find unmatched elements.
	 * @param resource
	 *            {@link Resource} to search for matching elements.
	 * @param remote
	 *            specifies if {@link RemoteUnMatchElement}s or
	 *            {@link UnMatchElement}s should be created.
	 * @param matchOnlyRoot
	 *            Indicates that only objects should be considered as matched
	 *            which are contained by the specified
	 *            <code>matchOnlyRoot</code>. This parameter may be
	 *            <code>null</code> to ignore containment.
	 * @return the {@link EList} of {@link UnMatchElement}s or
	 *         {@link RemoteUnMatchElement}s.
	 */
	private EList<UnmatchElement> findUnMatchedElements(
			EList<EObject> eObjects, Resource resource, boolean remote,
			EObject matchOnlyRoot) {
		EList<UnmatchElement> unMatchElements = new BasicEList<UnmatchElement>();
		for (EObject eObject : eObjects) {
			EObject matchingObject = UUIDUtil.getObject(resource,
					UUIDUtil.getUUID(eObject));
			boolean haveMatch = matchingObject != null
					&& (matchOnlyRoot == null
							|| matchingObject.equals(matchOnlyRoot) || EcoreUtil
							.createParentList(matchingObject).contains(
									matchOnlyRoot));
			// check for match and if is a GenericType (for which we do not want
			// to create a unmatch element)
			if (!(eObject instanceof EGenericType) && !haveMatch) {
				if (!isUnmatchedAlready(EcoreUtil.createParentList(eObject))) {
					unMatchElements.add(createUnMatchElement(eObject,
							Side.RIGHT, remote));
				}
			}
			// check children
			TreeIterator<EObject> eObjectIterator = eObject.eAllContents();
			while (eObjectIterator.hasNext()) {
				EObject subEObject = eObjectIterator.next();
				EObject matchingSubObject = UUIDUtil.getObject(resource,
						UUIDUtil.getUUID(subEObject));
				boolean haveSubMatch = matchingSubObject != null
						&& (matchOnlyRoot == null || EcoreUtil
								.createParentList(matchingSubObject).contains(
										matchOnlyRoot));
				if (!(subEObject instanceof EGenericType) && !haveSubMatch) {
					if (!isUnmatchedAlready(EcoreUtil
							.createParentList(subEObject))) {
						unMatchElements.add(createUnMatchElement(subEObject,
								Side.RIGHT, remote));
					}
				}
			}
		}
		return unMatchElements;
	}

	/**
	 * Creates a new {@link UnMatchElement} if <code>remote</code> is
	 * <code>false</code> or a {@link RemoteUnMatchElement} if
	 * <code>remote</code> is <code>true</code> for the specified
	 * <code>object</code>.
	 * 
	 * @param object
	 *            {@link EObject} to create {@link UnMatchElement} or
	 *            {@link RemoteUnMatchElement} for.
	 * @param side
	 *            Side to set.
	 * @param remote
	 *            if <code>true</code> a {@link RemoteUnMatchElement} will be
	 *            created. If <code>false</code> a {@link UnMatchElement} will
	 *            be created.
	 * @return the created {@link UnMatchElement}.
	 */
	private UnmatchElement createUnMatchElement(EObject object, Side side,
			boolean remote) {
		UnmatchElement unMatchElement = null;
		unMatchElement = MatchFactory.eINSTANCE.createUnmatchElement();
		unMatchElement.setRemote(remote);
		unMatchElement.setSide(side);
		unMatchElement.setElement(object);
		unmatched.add(object);
		return unMatchElement;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngine#generateMatchModel
	 * (org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public MatchModel generateMatchModel(Resource origin, Resource resource1,
			Resource resource2) throws MatchException {

		unmatched.clear();

		// create MatchModel
		MatchModel matchModel = MatchFactory.eINSTANCE.createMatchModel();
		// matchModel.setOriginModel(origin.getURI().toString());
		// matchModel.setLeftModel(resource1.getURI().toString());
		// matchModel.setRightModel(resource2.getURI().toString());

		// create and add Match2Elements and UnMatchedElements
		matchModel.getMatchedElements().addAll(
				createMatch3Elements(origin.getContents(), resource1,
						resource2, matchModel.getUnmatchedElements(), 0));

		// find and add unmatched elements in the resource1
		matchModel.getUnmatchedElements().addAll(
				findUnMatchedElements(resource1.getContents(), origin, false));

		// find and add unmatched elements in the resource2
		matchModel.getUnmatchedElements().addAll(
				findUnMatchedElements(resource2.getContents(), origin, false));

		return matchModel;
	}

	/**
	 * Creates and returns a {@link EList} of {@link Match3Elements} matching
	 * the specified <code>originObjects</code> based on their
	 * <code>xmi:id</code> s to their opposites in <code>leftResource</code> and
	 * <code>rightResource</code>.
	 * 
	 * <p>
	 * This method goes down recursively adding the sub matches to the returned
	 * {@link EList} of {@link Match3Elements}.
	 * </p>
	 * 
	 * @param originObjects
	 *            {@link EObject}s in origin to match.
	 * @param leftResource
	 *            {@link Resource} containing {@link EObject}s from the left
	 *            side to match with.
	 * @param rightResource
	 *            {@link Resource} containing {@link EObject}s from the right
	 *            side to match with.
	 * @param unmatchedElements
	 *            {@link EList} to add unmatched elements to.
	 * @return the {@link EList} of {@link Match3Elements}.
	 */
	private EList<Match3Elements> createMatch3Elements(
			EList<EObject> originObjects, Resource leftResource,
			Resource rightResource, EList<UnmatchElement> unmatchedElements,
			int level) {
		// create list to return
		EList<Match3Elements> match3Elementss = new BasicEList<Match3Elements>();
		// guard empty or null lists
		if (originObjects != null && originObjects.size() > 0) {
			// iterate originObjects
			for (EObject originObject : originObjects) {
				// get left opposite
				EObject leftObject = UUIDUtil.getObject(leftResource,
						UUIDUtil.getUUID(originObject));
				// get right opposite
				EObject rightObject = UUIDUtil.getObject(rightResource,
						UUIDUtil.getUUID(originObject));
				// check if right opposite existing
				if (rightObject != null && leftObject != null) {
					// if existing create Match2Elements
					Match3Elements match3Elements = MatchFactory.eINSTANCE
							.createMatch3Elements();
					match3Elements.setOriginElement(originObject);
					match3Elements.setLeftElement(leftObject);
					match3Elements.setRightElement(rightObject);
					match3Elements.setSimilarity(1d);
					// go deeper eventually and add children
					if (level < 1) {
						// if root level add them as sub elements
						match3Elements.getSubMatchElements().addAll(
								createMatch3Elements(originObject.eContents(),
										leftResource, rightResource,
										unmatchedElements, level + 1));
					} else {
						// if deeper just add them directly
						match3Elementss.addAll(createMatch3Elements(
								originObject.eContents(), leftResource,
								rightResource, unmatchedElements, level + 1));
					}
					// add created Match3Elements
					match3Elementss.add(match3Elements);
				} else if (originObject instanceof EGenericType) {
					// try to match Generic Type
					Match3Elements Match3Elements = matchGenericType(
							(EGenericType) originObject, leftResource,
							rightResource);
					// if Generic Type could be matched add it
					if (Match3Elements != null) {
						match3Elementss.add(Match3Elements);
					} else {
						// if not create UnMatchElement
						if (!isUnmatchedAlready(EcoreUtil
								.createParentList(originObject))) {
							unmatchedElements.add(createUnMatchElement(
									originObject, Side.RIGHT, false));
						}
					}
				} else if (leftObject != null) {
					// if not existing left create UnMatchElement
					if (!isUnmatchedAlready(EcoreUtil
							.createParentList(originObject))) {
						unmatchedElements.add(createUnMatchElement(
								originObject, Side.RIGHT, true));
					}
				} else if (rightObject != null) {
					// if not existing create UnMatchElement
					if (!isUnmatchedAlready(EcoreUtil
							.createParentList(originObject))) {
						unmatchedElements.add(createUnMatchElement(
								originObject, Side.LEFT, false));
					}
				}
			}
		}

		return match3Elementss;
	}

	/**
	 * Tries to match the {@link EGenericType}s <code>originObject</code> with
	 * the opposite in <code>leftResource</code> and <code>rightResource</code>.
	 * 
	 * @param originObject
	 *            the {@link EGenericType} to match.
	 * @param leftResource
	 *            the left {@link Resource} to find left opposite.
	 * @param rightResource
	 *            the right {@link Resource} to find right opposite.
	 * @return the {@link Match3Elements} instance representing the match or
	 *         <code>null</code> if no match could be obtained.
	 */
	private Match3Elements matchGenericType(EGenericType originObject,
			Resource leftResource, Resource rightResource) {
		// get parent
		EObject parent = originObject.eContainer();
		// get left and right parent
		EObject leftParent = UUIDUtil.getObject(leftResource,
				UUIDUtil.getUUID(parent));
		EObject rightParent = UUIDUtil.getObject(rightResource,
				UUIDUtil.getUUID(parent));

		// if left and right parent aren't null find their value of the feature
		// and match them.
		if (leftParent != null && rightParent != null) {
			Object leftObject = leftParent.eGet(originObject
					.eContainingFeature());
			Object rightObject = rightParent.eGet(originObject
					.eContainingFeature());
			if (leftObject != null && rightObject != null
					&& leftObject instanceof EObject
					&& rightObject instanceof EObject) {
				Match3Elements Match3Elements = MatchFactory.eINSTANCE
						.createMatch3Elements();
				Match3Elements.setOriginElement(originObject);
				Match3Elements.setLeftElement((EObject) leftObject);
				Match3Elements.setRightElement((EObject) rightObject);
				Match3Elements.setSimilarity(1d);
				return Match3Elements;
			}
		}

		// if nothing helped, return null
		return null;
	}

}