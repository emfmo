/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.engine.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.match.metamodel.Match2Elements;
import org.eclipse.emf.compare.match.metamodel.MatchElement;
import org.eclipse.emf.compare.match.metamodel.MatchFactory;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.compare.match.metamodel.Side;
import org.eclipse.emf.compare.match.metamodel.UnmatchElement;
import org.eclipse.emf.compare.match.service.MatchService;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.engine.IMatchEngine;
import org.modelversioning.core.util.EcoreUtil;
import org.modelversioning.core.util.UUIDUtil;

/**
 * Generates the {@link MatchModel} using the EMF Compare MatchEngine.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class EMFCompareMatchEngine implements IMatchEngine {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatchModel generateMatchModel(Resource resource1, Resource resource2)
			throws MatchException {
		org.eclipse.emf.compare.match.engine.IMatchEngine engine = getBestMatchEngine(
				resource1, resource2);
		try {
			MatchModel matchModel = engine.modelMatch(
					EcoreUtil.getFirstRootObject(resource2),
					EcoreUtil.getFirstRootObject(resource1), null);

			return improveMatchModel(matchModel, resource2, resource1);
		} catch (InterruptedException e) {
			throw new MatchException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatchModel generateMatchModel(EObject eObject1, EObject eObject2)
			throws MatchException {
		org.eclipse.emf.compare.match.engine.IMatchEngine engine = getBestMatchEngine(
				eObject1.eResource(), eObject2.eResource());
		try {
			MatchModel matchModel = engine.contentMatch(eObject2, eObject1,
					null);

			// reset model roots since EMF Compare sets the real model root and
			// not the specified objects as root
			matchModel.getRightRoots().clear();
			matchModel.getRightRoots().add(eObject2);
			matchModel.getLeftRoots().clear();
			matchModel.getLeftRoots().add(eObject1);

			return improveMatchModel(matchModel, eObject2.eResource(),
					eObject1.eResource());
		} catch (InterruptedException e) {
			throw new MatchException(e);
		}
	}

	/**
	 * Since EMF Compare Match does not match elements which have been moved to
	 * newly created elements, we have to iterate through all unmatched elements
	 * and try to find a match again.
	 * 
	 * @param matchModel
	 *            match model to improve.
	 * @param resource1
	 *            left resource.
	 * @param resource2
	 *            right resource.
	 * @return improved match model.
	 */
	private MatchModel improveMatchModel(MatchModel matchModel,
			Resource resource2, Resource resource1) {
		EList<UnmatchElement> unmatchedElements = new BasicEList<UnmatchElement>();
		unmatchedElements.addAll(matchModel.getUnmatchedElements());
		for (UnmatchElement unmatchElement : unmatchedElements) {
			EObject oppositeObject = null;
			// try to find by ID
			if (Side.RIGHT.equals(unmatchElement.getSide())) {
				oppositeObject = UUIDUtil.getObject(resource2,
						UUIDUtil.getUUID(unmatchElement.getElement()));
			} else {
				oppositeObject = UUIDUtil.getObject(resource1,
						UUIDUtil.getUUID(unmatchElement.getElement()));
			}
			// if we found a match add the Match2Elements
			if (oppositeObject != null) {
				Match2Elements match2Elements = null;
				if (Side.LEFT.equals(unmatchElement.getSide())) {
					match2Elements = addMatch2Elements(
							unmatchElement.getElement(), oppositeObject,
							matchModel);
				} else {
					match2Elements = addMatch2Elements(oppositeObject,
							unmatchElement.getElement(), matchModel);
				}

				// add match or unmatch for children of wrongly unmatched
				// element for left element
				TreeIterator<EObject> leftContents = match2Elements
						.getLeftElement().eAllContents();
				while (leftContents.hasNext()) {
					EObject left = leftContents.next();
					EObject right = UUIDUtil.getObject(resource1,
							UUIDUtil.getUUID(left));
					if (right != null) {
						// add match
						addMatch2Elements(left, right, matchModel);
					} else {
						// add unmatch
						addUnmatchElement(left, Side.LEFT, matchModel);
					}
				}

				// add match or unmatch for children of wrongly unmatched
				// element for right element
				TreeIterator<EObject> rightContents = match2Elements
						.getRightElement().eAllContents();
				while (rightContents.hasNext()) {
					EObject right = rightContents.next();
					EObject left = UUIDUtil.getObject(resource2,
							UUIDUtil.getUUID(right));
					if (left != null) {
						// add match
						addMatch2Elements(left, right, matchModel);
					} else {
						// add unmatch
						addUnmatchElement(right, Side.RIGHT, matchModel);
					}
				}

				// remove unmatched element
				matchModel.getUnmatchedElements().remove(unmatchElement);
			}// else if (unmatchElement.getElement() instanceof EGenericType) {
				// remove unmatched EGenericTypes
				// matchModel.getUnmatchedElements().remove(unmatchElement);
				// }
		}
		return matchModel;
	}

	/**
	 * Adds an {@link UnmatchElement} for the specified <code>eObject</code> in
	 * the <code>side</code> to the specified <code>matchModel</code>.
	 * 
	 * @param eObject
	 *            to create {@link UnmatchElement} for.
	 * @param side
	 *            side of <code>eObject</code>.
	 * @param matchModel
	 *            {@link MatchModel} to add {@link UnmatchElement} to.
	 */
	private void addUnmatchElement(EObject eObject, Side side,
			MatchModel matchModel) {
		UnmatchElement unmatchElement = MatchFactory.eINSTANCE
				.createUnmatchElement();
		unmatchElement.setSide(side);
		unmatchElement.setElement(eObject);
		matchModel.getUnmatchedElements().add(unmatchElement);
	}

	/**
	 * Adds a {@link Match2Elements} for the <code>left</code> and the
	 * <code>right</code> object to <code>matchModel</code>.
	 * 
	 * @param left
	 *            left {@link EObject} of {@link Match2Elements} to create.
	 * @param right
	 *            right {@link EObject} of {@link Match2Elements} to create.
	 * @param matchModel
	 *            {@link MatchModel} to add {@link Match2Elements} to.
	 * @return created {@link Match2Elements}.
	 */
	private Match2Elements addMatch2Elements(EObject left, EObject right,
			MatchModel matchModel) {
		Match2Elements match2Elements = MatchFactory.eINSTANCE
				.createMatch2Elements();
		match2Elements.setSimilarity(1d);
		match2Elements.setLeftElement(left);
		match2Elements.setRightElement(right);
		EObject containerOfRightElm = match2Elements.getRightElement()
				.eContainer();
		Match2Elements match = getMatch2ElementsOfRightObject(
				containerOfRightElm, matchModel);
		if (match == null) {
			// right container seems to be removed, so take left container
			EObject containerOfLeftElm = match2Elements.getLeftElement()
					.eContainer();
			match = getMatch2ElementsOfLeftObject(containerOfLeftElm,
					matchModel);
			if (match != null) {
				match.getSubMatchElements().add(match2Elements);
			} else {
				// if match is still null add it to root
				matchModel.getMatchedElements().add(match2Elements);
			}
		} else {
			match.getSubMatchElements().add(match2Elements);
		}
		return match2Elements;
	}

	/**
	 * Returns the {@link Match2Elements} matching <code>eObject</code> as right
	 * element.
	 * 
	 * @param eObject
	 *            object to find {@link Match2Elements} of.
	 * @param matchModel
	 *            match model to search in.
	 * @return {@link Match2Elements} or <code>null</code> if not found.
	 */
	private Match2Elements getMatch2ElementsOfRightObject(EObject eObject,
			MatchModel matchModel) {
		for (MatchElement matchElement : matchModel.getMatchedElements()) {
			// search in matchElement
			if (matchElement instanceof Match2Elements) {
				if (UUIDUtil.getUUID(eObject).equals(
						UUIDUtil.getUUID(((Match2Elements) matchElement)
								.getRightElement()))) {
					return ((Match2Elements) matchElement);
				}
			}
			// search in children of matchElement
			TreeIterator<EObject> treeIterator = matchElement.eAllContents();
			while (treeIterator.hasNext()) {
				EObject nextChild = treeIterator.next();
				if (nextChild instanceof Match2Elements) {
					Match2Elements match2Elements = (Match2Elements) nextChild;
					if (UUIDUtil.getUUID(eObject).equals(
							UUIDUtil.getUUID(match2Elements.getRightElement()))) {
						return match2Elements;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Returns the {@link Match2Elements} matching <code>eObject</code> as left
	 * element.
	 * 
	 * @param eObject
	 *            object to find {@link Match2Elements} of.
	 * @param matchModel
	 *            match model to search in.
	 * @return {@link Match2Elements} or <code>null</code> if not found.
	 */
	private Match2Elements getMatch2ElementsOfLeftObject(EObject eObject,
			MatchModel matchModel) {
		for (MatchElement matchElement : matchModel.getMatchedElements()) {
			// search in matchElement
			if (matchElement instanceof Match2Elements) {
				if (UUIDUtil.getUUID(eObject).equals(
						UUIDUtil.getUUID(((Match2Elements) matchElement)
								.getLeftElement()))) {
					return ((Match2Elements) matchElement);
				}
			}
			// search in children of matchElement
			TreeIterator<EObject> treeIterator = matchElement.eAllContents();
			while (treeIterator.hasNext()) {
				EObject nextChild = treeIterator.next();
				if (nextChild instanceof Match2Elements) {
					Match2Elements match2Elements = (Match2Elements) nextChild;
					if (UUIDUtil.getUUID(eObject).equals(
							UUIDUtil.getUUID(match2Elements.getLeftElement()))) {
						return match2Elements;
					}
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngine#generateMatchModel
	 * (org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public MatchModel generateMatchModel(Resource origin, Resource resource1,
			Resource resource2) throws MatchException {
		org.eclipse.emf.compare.match.engine.IMatchEngine engine = getBestMatchEngine(
				origin, resource1, resource2);
		try {
			return engine.modelMatch(EcoreUtil.getFirstRootObject(resource1),
					EcoreUtil.getFirstRootObject(resource2),
					EcoreUtil.getFirstRootObject(origin), null);
		} catch (InterruptedException e) {
			throw new MatchException(e);
		}
	}

	/**
	 * Returns the best {@link org.eclipse.emf.compare.match.api.IMatchEngine}
	 * for the specified <code>resources</code>.
	 * 
	 * @param resources
	 *            resources to find best
	 *            {@link org.eclipse.emf.compare.match.api.IMatchEngine}.
	 * @return the best {@link org.eclipse.emf.compare.match.api.IMatchEngine}
	 */
	private static org.eclipse.emf.compare.match.engine.IMatchEngine getBestMatchEngine(
			Resource... resources) {
		return MatchService.getBestMatchEngine(resources);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This match engine will accept everything with a priority of 1.
	 */
	@Override
	public int accept(EObject... eObjects) {
		return 1;
	}

}
