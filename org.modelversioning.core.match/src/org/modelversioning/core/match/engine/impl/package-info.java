/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 *
 * This package contains implementations of the interfaces and other classes
 * that provide facilities to select an appropriate {@link IMatchEngine}s,
 * generating a {@link org.eclipse.emf.compare.match.metamodel.MatchModel} etc.
 */
package org.modelversioning.core.match.engine.impl;