/* ************************************************************************
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 /*************************************************************************/

package org.modelversioning.core.match.engine.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.match.MatchPlugin;
import org.modelversioning.core.match.engine.IMatchEngine;
import org.modelversioning.core.match.engine.IMatchEngineSelector;

/**
 * Default implementation of the {@link IMatchEngineSelector}.
 * 
 * <p>
 * This match engine selector will examine the registered {@link IMatchEngine
 * IMatchEngines} provided by other plug-ins using the extension point
 * <code>org.modelversioning.core.match.engine</code>. As a fall back, this
 * selector selects the {@link UUIDMatchEngine} if UUIDs are available or the
 * {@link EMFCompareMatchEngine} if no UUIDs are available.
 * </p>
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DefaultMatchEngineSelector implements IMatchEngineSelector {

	/**
	 * The ID of the extension point for contributing {@link IMatchEngine
	 * IMatchEngines}.
	 */
	public final static String IMATCH_ENGINE_ID = "org.modelversioning.core.match.engine";

	/**
	 * The instance of the {@link UUIDMatchEngine}.
	 */
	private IMatchEngine uuidMatchEngine = new UUIDMatchEngine();
	/**
	 * The instance of the {@link EMFCompareMatchEngine}.
	 */
	private IMatchEngine emfCompareMatchEngine = new EMFCompareMatchEngine();
	/**
	 * All match engines including externally contributed ones.
	 */
	private Set<IMatchEngine> matchEngines = new HashSet<IMatchEngine>();

	/**
	 * Creates a new selector which will include registered extensions.
	 */
	public DefaultMatchEngineSelector() {
		init(true);
	}

	/**
	 * Creates a new selector specifying whether to include registered
	 * extensions.
	 * 
	 * @param includeExtensions
	 *            whether to include registered extensions.
	 */
	public DefaultMatchEngineSelector(boolean includeExtensions) {
		init(includeExtensions);
	}

	private void init(boolean includeExtensions) {
		matchEngines.add(uuidMatchEngine);
		matchEngines.add(emfCompareMatchEngine);
		if (includeExtensions) {
			IConfigurationElement[] config = Platform.getExtensionRegistry()
					.getConfigurationElementsFor(IMATCH_ENGINE_ID);
			for (IConfigurationElement configElement : config) {
				try {
					final Object matchEngine = configElement
							.createExecutableExtension("class");
					matchEngines.add((IMatchEngine) matchEngine);
				} catch (CoreException e) {
					IStatus status = new Status(IStatus.ERROR,
							MatchPlugin.PLUGIN_ID,
							"Failed to load contributed match engine", e);
					MatchPlugin.getDefault().getLog().log(status);
				} catch (ClassCastException e) {
					IStatus status = new Status(IStatus.ERROR,
							MatchPlugin.PLUGIN_ID,
							"Failed to load contributed match engine", e);
					MatchPlugin.getDefault().getLog().log(status);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngineSelector#selectEngine
	 * (org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public IMatchEngine selectEngine(Resource resource1, Resource resource2) {
		return this.getBestMatchEngine(resource1, resource2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngineSelector#selectEngine
	 * (org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource,
	 * org.eclipse.emf.ecore.resource.Resource)
	 */
	@Override
	public IMatchEngine selectEngine(Resource origin, Resource resource1,
			Resource resource2) {
		return this.getBestMatchEngine(origin, resource1, resource2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngineSelector#selectEngine
	 * (org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject,
	 * org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public IMatchEngine selectEngine(EObject origin, EObject eObject1,
			EObject eObject2) {
		return this.getBestMatchEngine(origin, eObject1, eObject2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.modelversioning.core.match.engine.IMatchEngineSelector#selectEngine
	 * (org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public IMatchEngine selectEngine(EObject eObject1, EObject eObject2) {
		return this.getBestMatchEngine(eObject1, eObject2);
	}

	/**
	 * Returns the most suitable instance of an {@link IMatchEngine} considering
	 * the specified <code>resources</code>.
	 * 
	 * @param resources
	 *            {@link Resource}s to consider.
	 * @return a suitable instance of a {@link IMatchEngine}.
	 */
	private IMatchEngine getBestMatchEngine(Resource... resources) {
		List<EObject> rootObjects = new BasicEList<EObject>();
		for (Resource resource : resources) {
			rootObjects.addAll(resource.getContents());
		}
		return getBestMatchEngine(rootObjects.toArray(new EObject[rootObjects
				.size()]));
	}

	/**
	 * Returns the most suitable instance of an {@link IMatchEngine} considering
	 * the specified <code>eObjects</code>.
	 * 
	 * @param eObjects
	 *            to consider.
	 * @return a suitable instance of a {@link IMatchEngine}.
	 */
	private IMatchEngine getBestMatchEngine(EObject... eObjects) {
		IMatchEngine bestMatchEngine = null;
		int bestAcceptRate = 0;
		for (IMatchEngine matchEngine : matchEngines) {
			int acceptRate = matchEngine.accept(eObjects);
			if (acceptRate > 0
					&& (bestMatchEngine == null || acceptRate > bestAcceptRate)) {
				bestMatchEngine = matchEngine;
				bestAcceptRate = acceptRate;
			}
		}
		return bestMatchEngine;
	}

}
