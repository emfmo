/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.engine;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Provides an interface to select an appropriate implementation of a
 * {@link IMatchEngine}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IMatchEngineSelector {

	/**
	 * Returns an instance of a {@link IMatchEngine} suitable for the specified
	 * {@link Resource}s <code>resource1</code> and <code>resource2</code>.
	 * 
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the suitable instance of a {@link IMatchEngine}.
	 */
	public IMatchEngine selectEngine(Resource resource1, Resource resource2);

	/**
	 * Returns an instance of a {@link IMatchEngine} suitable for the specified
	 * {@link Resource}s <code>resource1</code> and <code>resource2</code> as
	 * well as for their common origin model in <code>origin</code>.
	 * 
	 * @param origin
	 *            {@link Resource} of the common origin model.
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the suitable instance of a {@link IMatchEngine}.
	 */
	public IMatchEngine selectEngine(Resource origin, Resource resource1,
			Resource resource2);

	/**
	 * Returns an instance of a {@link IMatchEngine} suitable for the specified
	 * {@link EObject EObjects} <code>eObject1</code> and <code>eObject2</code>
	 * as well as for their common <code>origin</code>.
	 * 
	 * @param origin
	 *            Common origin {@link EObject}.
	 * @param eObject1
	 *            First {@link EObject}.
	 * @param eObject2
	 *            Second {@link EObject}.
	 * @return the suitable instance of a {@link IMatchEngine}.
	 */
	public IMatchEngine selectEngine(EObject origin, EObject eObject1,
			EObject eObject2);

	/**
	 * Returns an instance of a {@link IMatchEngine} suitable for the specified
	 * {@link EObject EObjects} <code>eObject1</code> and <code>eObject2</code>.
	 * 
	 * @param eObject1
	 *            First {@link EObject}.
	 * @param eObject2
	 *            Second {@link EObject}.
	 * @return the suitable instance of a {@link IMatchEngine}.
	 */
	public IMatchEngine selectEngine(EObject eObject1, EObject eObject2);

}
