/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.service;

import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.engine.IMatchEngineSelector;
import org.modelversioning.core.match.engine.impl.DefaultMatchEngineSelector;

/**
 * This class offers services to create a {@link MatchModel} out of two or three
 * {@link Resource}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class MatchService {

	/**
	 * An instance of the {@link IMatchEngineSelector} to use.
	 */
	private IMatchEngineSelector matchEngineSelector;

	/**
	 * Empty Default Constructor.
	 */
	public MatchService() {
		super();
		this.matchEngineSelector = new DefaultMatchEngineSelector();
	}

	/**
	 * Constructor specifying the {@link IMatchEngineSelector} to use.
	 * 
	 * @param matchEngineSelector
	 *            the {@link IMatchEngineSelector} to use.
	 */
	public MatchService(IMatchEngineSelector matchEngineSelector) {
		super();
		this.matchEngineSelector = matchEngineSelector;
	}

	/**
	 * Generates a <em>two way</em> {@link MatchModel} for the specified
	 * {@link Resource}s <code>resource1</code> and <code>resourse2</code>.
	 * 
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the {@link MatchModel} matching both models.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(Resource resource1, Resource resource2)
			throws MatchException {
		return this.matchEngineSelector.selectEngine(resource1, resource2)
				.generateMatchModel(resource1, resource2);
	}

	/**
	 * Generates a <em>two way</em> {@link MatchModel} for the specified
	 * {@link EObject}s <code>eObject1</code> and <code>eObject2</code>.
	 * 
	 * @param eObject1
	 *            {@link EObject} root of the first model.
	 * @param eObject2
	 *            {@link EObject} root of the second model.
	 * @return the {@link MatchModel} matching both models.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(EObject eObject1, EObject eObject2)
			throws MatchException {
		return this.matchEngineSelector.selectEngine(eObject1,
				eObject2).generateMatchModel(eObject1, eObject2);
	}

	/**
	 * Generates a <em>three way</em> {@link MatchModel} between the models in
	 * the specified {@link Resource}s <code>resource1</code> and
	 * <code>resource2</code> as well as their common origin model
	 * <code>origin</code>.
	 * 
	 * @param origin
	 *            {@link Resource} of the common origin model.
	 * @param resource1
	 *            {@link Resource} of the first model.
	 * @param resource2
	 *            {@link Resource} of the second model.
	 * @return the {@link MatchModel} matching the two models and the common
	 *         origin model.
	 * @throws MatchException
	 *             if the match could not be successfully executes.
	 */
	public MatchModel generateMatchModel(Resource origin, Resource resource1,
			Resource resource2) throws MatchException {
		return this.matchEngineSelector.selectEngine(origin, resource1,
				resource2).generateMatchModel(origin, resource1, resource2);
	}

	/**
	 * Returns the currently used {@link IMatchEngineSelector}.
	 * 
	 * @return the currently used {@link IMatchEngineSelector}.
	 */
	public IMatchEngineSelector getMatchEngineSelector() {
		return matchEngineSelector;
	}

	/**
	 * Sets the {@link IMatchEngineSelector} to use.
	 * 
	 * @param matchEngineSelector
	 *            the {@link IMatchEngineSelector} to use.
	 */
	public void setMatchEngineSelector(IMatchEngineSelector matchEngineSelector) {
		this.matchEngineSelector = matchEngineSelector;
	}

}
