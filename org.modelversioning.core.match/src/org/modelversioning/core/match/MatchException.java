/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match;

/**
 * This exception is thrown whenever the match process went wrong.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class MatchException extends Exception {

	/**
	 * Serial used for unserialization checks.
	 */
	private static final long serialVersionUID = -3694065254672789224L;
	
	/**
	 * Creates a {@link MatchException} with the specified {@link Exception} <code>e</code>.
	 * @param e inner {@link Exception}.
	 */
	public MatchException(Exception e) {
		super(e);
	}

}
