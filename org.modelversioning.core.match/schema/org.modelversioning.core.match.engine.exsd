<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.modelversioning.core.match" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appinfo>
         <meta.schema plugin="org.modelversioning.core.match" id="org.modelversioning.core.match.engine" name="Match Engine"/>
      </appinfo>
      <documentation>
         Using this extension point, external plug-ins may contribute specific match engines to be used by this plug-in.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appinfo>
            <meta.element />
         </appinfo>
      </annotation>
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="matchengine"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute translatable="true"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="matchengine">
      <complexType>
         <attribute name="class" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appinfo>
                  <meta.attribute kind="java" basedOn=":org.modelversioning.core.match.engine.IMatchEngine"/>
               </appinfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appinfo>
         <meta.section type="since"/>
      </appinfo>
      <documentation>
         1.0.0
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="examples"/>
      </appinfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="apiinfo"/>
      </appinfo>
      <documentation>
         All contributions must implement the IMatchEngine and will get asked whether they accept a set of EObjects to be matched using the accept(EObject... eObjects) method. If this method returns 0, the match engine will be ignored. Otherwise, the match engine returning the highest value will be choosen.
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="implementation"/>
      </appinfo>
      <documentation>
         This plug-in is shipped with two match engines. One for XMI resources having XMI:IDs (UUIDMatchEngine) and one using EMF Compare (EMFCompareMatchEngine).
      </documentation>
   </annotation>

   <annotation>
      <appinfo>
         <meta.section type="copyright"/>
      </appinfo>
      <documentation>
         Copyright (c) 2011 modelversioning.org
All rights reserved. This program and the accompanying materials are
made available under the terms of the Eclipse Public License v1.0 which
accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html
      </documentation>
   </annotation>

</schema>
