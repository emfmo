/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.ConditionsModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Represents an operation.
 * 
 * <p>
 * This class contains all essential information needed, to define a composite operation. This operation definition may be used for detection in a DiffModel or to execute this operation in a suitable modeling scenario.
 * </p>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getDescription <em>Description</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getTitleTemplate <em>Title Template</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getVersion <em>Version</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#isIsRefactoring <em>Is Refactoring</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getModelingLanguage <em>Modeling Language</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getModelFileExtension <em>Model File Extension</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getDiagramFileExtension <em>Diagram File Extension</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getEditorId <em>Editor Id</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getWorkingModelRoot <em>Working Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getWorkingDiagram <em>Working Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getOriginModelRoot <em>Origin Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getOriginDiagram <em>Origin Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getDifferenceModel <em>Difference Model</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getIterations <em>Iterations</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getUserInputs <em>User Inputs</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getPostconditions <em>Postconditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getNegativeApplicationConditions <em>Negative Application Conditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.OperationSpecification#getState <em>State</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification()
 * @model
 * @generated
 */
public interface OperationSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Title Template</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The title template saves the title of this operation and may inlcude symbol values (i.e. #{code #{symbolname}.name}) which will be replaced with the current occurrance of this operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Title Template</em>' attribute.
	 * @see #setTitleTemplate(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_TitleTemplate()
	 * @model
	 * @generated
	 */
	String getTitleTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getTitleTemplate <em>Title Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title Template</em>' attribute.
	 * @see #getTitleTemplate()
	 * @generated
	 */
	void setTitleTemplate(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * The default value is <code>"1.0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A string identifiying the version of this operation definition.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Version()
	 * @model default="1.0.0"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Modeling Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A string identifying the modeling language (i.e. &quot;ecore&quot;) this operation definition is aiming at.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Modeling Language</em>' attribute.
	 * @see #setModelingLanguage(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_ModelingLanguage()
	 * @model
	 * @generated
	 */
	String getModelingLanguage();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getModelingLanguage <em>Modeling Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modeling Language</em>' attribute.
	 * @see #getModelingLanguage()
	 * @generated
	 */
	void setModelingLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Model File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model File Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model File Extension</em>' attribute.
	 * @see #setModelFileExtension(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_ModelFileExtension()
	 * @model required="true"
	 * @generated
	 */
	String getModelFileExtension();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getModelFileExtension <em>Model File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model File Extension</em>' attribute.
	 * @see #getModelFileExtension()
	 * @generated
	 */
	void setModelFileExtension(String value);

	/**
	 * Returns the value of the '<em><b>Diagram File Extension</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram File Extension</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram File Extension</em>' attribute.
	 * @see #setDiagramFileExtension(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_DiagramFileExtension()
	 * @model
	 * @generated
	 */
	String getDiagramFileExtension();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getDiagramFileExtension <em>Diagram File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram File Extension</em>' attribute.
	 * @see #getDiagramFileExtension()
	 * @generated
	 */
	void setDiagramFileExtension(String value);

	/**
	 * Returns the value of the '<em><b>Editor Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Id</em>' attribute.
	 * @see #setEditorId(String)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_EditorId()
	 * @model required="true"
	 * @generated
	 */
	String getEditorId();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getEditorId <em>Editor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Id</em>' attribute.
	 * @see #getEditorId()
	 * @generated
	 */
	void setEditorId(String value);

	/**
	 * Returns the value of the '<em><b>Is Refactoring</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Specifies whether this composite operation is a refactoring (improving the design preserving the semantics).
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Refactoring</em>' attribute.
	 * @see #setIsRefactoring(boolean)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_IsRefactoring()
	 * @model
	 * @generated
	 */
	boolean isIsRefactoring();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#isIsRefactoring <em>Is Refactoring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Refactoring</em>' attribute.
	 * @see #isIsRefactoring()
	 * @generated
	 */
	void setIsRefactoring(boolean value);

	/**
	 * Returns the value of the '<em><b>Working Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Working Model Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Working Model Root</em>' containment reference.
	 * @see #setWorkingModelRoot(EObject)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_WorkingModelRoot()
	 * @model containment="true"
	 * @generated
	 */
	EObject getWorkingModelRoot();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getWorkingModelRoot <em>Working Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Working Model Root</em>' containment reference.
	 * @see #getWorkingModelRoot()
	 * @generated
	 */
	void setWorkingModelRoot(EObject value);

	/**
	 * Returns the value of the '<em><b>Working Diagram</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Working Diagram</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Working Diagram</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_WorkingDiagram()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getWorkingDiagram();

	/**
	 * Returns the value of the '<em><b>Origin Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin Model Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin Model Root</em>' containment reference.
	 * @see #setOriginModelRoot(EObject)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_OriginModelRoot()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EObject getOriginModelRoot();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getOriginModelRoot <em>Origin Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin Model Root</em>' containment reference.
	 * @see #getOriginModelRoot()
	 * @generated
	 */
	void setOriginModelRoot(EObject value);

	/**
	 * Returns the value of the '<em><b>Origin Diagram</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin Diagram</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin Diagram</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_OriginDiagram()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getOriginDiagram();

	/**
	 * Returns the value of the '<em><b>Difference Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Difference Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Difference Model</em>' containment reference.
	 * @see #setDifferenceModel(ComparisonResourceSnapshot)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_DifferenceModel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ComparisonResourceSnapshot getDifferenceModel();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getDifferenceModel <em>Difference Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Difference Model</em>' containment reference.
	 * @see #getDifferenceModel()
	 * @generated
	 */
	void setDifferenceModel(ComparisonResourceSnapshot value);

	/**
	 * Returns the value of the '<em><b>Iterations</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.operations.Iteration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterations</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Iterations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Iteration> getIterations();

	/**
	 * Returns the value of the '<em><b>User Inputs</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.operations.UserInput}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Inputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Inputs</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_UserInputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<UserInput> getUserInputs();

	/**
	 * Returns the value of the '<em><b>Preconditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preconditions</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preconditions</em>' containment reference.
	 * @see #setPreconditions(ConditionsModel)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Preconditions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ConditionsModel getPreconditions();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getPreconditions <em>Preconditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preconditions</em>' containment reference.
	 * @see #getPreconditions()
	 * @generated
	 */
	void setPreconditions(ConditionsModel value);

	/**
	 * Returns the value of the '<em><b>Postconditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Postconditions</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postconditions</em>' containment reference.
	 * @see #setPostconditions(ConditionsModel)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_Postconditions()
	 * @model containment="true"
	 * @generated
	 */
	ConditionsModel getPostconditions();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getPostconditions <em>Postconditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postconditions</em>' containment reference.
	 * @see #getPostconditions()
	 * @generated
	 */
	void setPostconditions(ConditionsModel value);

	/**
	 * Returns the value of the '<em><b>Negative Application Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.operations.NegativeApplicationCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Application Conditions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Application Conditions</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_NegativeApplicationConditions()
	 * @model containment="true"
	 * @generated
	 */
	EList<NegativeApplicationCondition> getNegativeApplicationConditions();

	/**
	 * Returns the value of the '<em><b>State</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * The literals are from the enumeration {@link org.modelversioning.operations.SpecificationState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.operations.SpecificationState
	 * @see #setState(SpecificationState)
	 * @see org.modelversioning.operations.OperationsPackage#getOperationSpecification_State()
	 * @model default="0"
	 * @generated
	 */
	SpecificationState getState();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.OperationSpecification#getState <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State</em>' attribute.
	 * @see org.modelversioning.operations.SpecificationState
	 * @see #getState()
	 * @generated
	 */
	void setState(SpecificationState value);

} // OperationSpecification
