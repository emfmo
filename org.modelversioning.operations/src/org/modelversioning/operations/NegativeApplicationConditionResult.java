/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations;

import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.templatebindings.TemplateBindingCollection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Negative Application Condition Result</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationConditionResult#getNegativeApplicationCondition <em>Negative Application Condition</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationConditionResult#getBinding <em>Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationConditionResult()
 * @model
 * @generated
 */
public interface NegativeApplicationConditionResult extends EvaluationResult {
	/**
	 * Returns the value of the '<em><b>Negative Application Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negative Application Condition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negative Application Condition</em>' reference.
	 * @see #setNegativeApplicationCondition(NegativeApplicationCondition)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationConditionResult_NegativeApplicationCondition()
	 * @model required="true"
	 * @generated
	 */
	NegativeApplicationCondition getNegativeApplicationCondition();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationConditionResult#getNegativeApplicationCondition <em>Negative Application Condition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Negative Application Condition</em>' reference.
	 * @see #getNegativeApplicationCondition()
	 * @generated
	 */
	void setNegativeApplicationCondition(NegativeApplicationCondition value);

	/**
	 * Returns the value of the '<em><b>Binding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding</em>' containment reference.
	 * @see #setBinding(TemplateBindingCollection)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationConditionResult_Binding()
	 * @model containment="true"
	 * @generated
	 */
	TemplateBindingCollection getBinding();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationConditionResult#getBinding <em>Binding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding</em>' containment reference.
	 * @see #getBinding()
	 * @generated
	 */
	void setBinding(TemplateBindingCollection value);

} // NegativeApplicationConditionResult
