/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;

import org.eclipse.emf.ecore.EObject;

import org.modelversioning.core.conditions.ConditionsModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Negative Application Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getDescription <em>Description</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getErrorMessage <em>Error Message</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getModelRoot <em>Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getDifferenceModel <em>Difference Model</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getConditions <em>Conditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.NegativeApplicationCondition#getOperationSpecification <em>Operation Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition()
 * @model
 * @generated
 */
public interface NegativeApplicationCondition extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Error Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Message</em>' attribute.
	 * @see #setErrorMessage(String)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_ErrorMessage()
	 * @model
	 * @generated
	 */
	String getErrorMessage();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getErrorMessage <em>Error Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Message</em>' attribute.
	 * @see #getErrorMessage()
	 * @generated
	 */
	void setErrorMessage(String value);

	/**
	 * Returns the value of the '<em><b>Model Root</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Root</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Root</em>' containment reference.
	 * @see #setModelRoot(EObject)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_ModelRoot()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EObject getModelRoot();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getModelRoot <em>Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Root</em>' containment reference.
	 * @see #getModelRoot()
	 * @generated
	 */
	void setModelRoot(EObject value);

	/**
	 * Returns the value of the '<em><b>Diagram</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_Diagram()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getDiagram();

	/**
	 * Returns the value of the '<em><b>Difference Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Difference Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Difference Model</em>' containment reference.
	 * @see #setDifferenceModel(ComparisonResourceSnapshot)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_DifferenceModel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ComparisonResourceSnapshot getDifferenceModel();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getDifferenceModel <em>Difference Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Difference Model</em>' containment reference.
	 * @see #getDifferenceModel()
	 * @generated
	 */
	void setDifferenceModel(ComparisonResourceSnapshot value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditions</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' containment reference.
	 * @see #setConditions(ConditionsModel)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_Conditions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ConditionsModel getConditions();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getConditions <em>Conditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conditions</em>' containment reference.
	 * @see #getConditions()
	 * @generated
	 */
	void setConditions(ConditionsModel value);

	/**
	 * Returns the value of the '<em><b>Operation Specification</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.modelversioning.operations.OperationSpecification#getNegativeApplicationConditions <em>Negative Application Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation Specification</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation Specification</em>' container reference.
	 * @see #setOperationSpecification(OperationSpecification)
	 * @see org.modelversioning.operations.OperationsPackage#getNegativeApplicationCondition_OperationSpecification()
	 * @see org.modelversioning.operations.OperationSpecification#getNegativeApplicationConditions
	 * @model opposite="negativeApplicationConditions" required="true" transient="false"
	 * @generated
	 */
	OperationSpecification getOperationSpecification();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.NegativeApplicationCondition#getOperationSpecification <em>Operation Specification</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation Specification</em>' container reference.
	 * @see #getOperationSpecification()
	 * @generated
	 */
	void setOperationSpecification(OperationSpecification value);

} // NegativeApplicationCondition
