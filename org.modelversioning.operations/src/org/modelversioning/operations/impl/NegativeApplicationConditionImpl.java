/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import org.modelversioning.core.conditions.ConditionsModel;

import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Negative Application Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getErrorMessage <em>Error Message</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getModelRoot <em>Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getDifferenceModel <em>Difference Model</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.NegativeApplicationConditionImpl#getOperationSpecification <em>Operation Specification</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NegativeApplicationConditionImpl extends EObjectImpl implements NegativeApplicationCondition {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getErrorMessage() <em>Error Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String ERROR_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getErrorMessage() <em>Error Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorMessage()
	 * @generated
	 * @ordered
	 */
	protected String errorMessage = ERROR_MESSAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModelRoot() <em>Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelRoot()
	 * @generated
	 * @ordered
	 */
	protected EObject modelRoot;

	/**
	 * The cached value of the '{@link #getDiagram() <em>Diagram</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagram()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> diagram;

	/**
	 * The cached value of the '{@link #getDifferenceModel() <em>Difference Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDifferenceModel()
	 * @generated
	 * @ordered
	 */
	protected ComparisonResourceSnapshot differenceModel;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected ConditionsModel conditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NegativeApplicationConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationsPackage.Literals.NEGATIVE_APPLICATION_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorMessage(String newErrorMessage) {
		String oldErrorMessage = errorMessage;
		errorMessage = newErrorMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE, oldErrorMessage, errorMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getModelRoot() {
		return modelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelRoot(EObject newModelRoot, NotificationChain msgs) {
		EObject oldModelRoot = modelRoot;
		modelRoot = newModelRoot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT, oldModelRoot, newModelRoot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelRoot(EObject newModelRoot) {
		if (newModelRoot != modelRoot) {
			NotificationChain msgs = null;
			if (modelRoot != null)
				msgs = ((InternalEObject)modelRoot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT, null, msgs);
			if (newModelRoot != null)
				msgs = ((InternalEObject)newModelRoot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT, null, msgs);
			msgs = basicSetModelRoot(newModelRoot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT, newModelRoot, newModelRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getDiagram() {
		if (diagram == null) {
			diagram = new EObjectContainmentEList<EObject>(EObject.class, this, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM);
		}
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonResourceSnapshot getDifferenceModel() {
		return differenceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDifferenceModel(ComparisonResourceSnapshot newDifferenceModel, NotificationChain msgs) {
		ComparisonResourceSnapshot oldDifferenceModel = differenceModel;
		differenceModel = newDifferenceModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL, oldDifferenceModel, newDifferenceModel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDifferenceModel(ComparisonResourceSnapshot newDifferenceModel) {
		if (newDifferenceModel != differenceModel) {
			NotificationChain msgs = null;
			if (differenceModel != null)
				msgs = ((InternalEObject)differenceModel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL, null, msgs);
			if (newDifferenceModel != null)
				msgs = ((InternalEObject)newDifferenceModel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL, null, msgs);
			msgs = basicSetDifferenceModel(newDifferenceModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL, newDifferenceModel, newDifferenceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsModel getConditions() {
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditions(ConditionsModel newConditions, NotificationChain msgs) {
		ConditionsModel oldConditions = conditions;
		conditions = newConditions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS, oldConditions, newConditions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditions(ConditionsModel newConditions) {
		if (newConditions != conditions) {
			NotificationChain msgs = null;
			if (conditions != null)
				msgs = ((InternalEObject)conditions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS, null, msgs);
			if (newConditions != null)
				msgs = ((InternalEObject)newConditions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS, null, msgs);
			msgs = basicSetConditions(newConditions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS, newConditions, newConditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationSpecification getOperationSpecification() {
		if (eContainerFeatureID() != OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION) return null;
		return (OperationSpecification)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperationSpecification(OperationSpecification newOperationSpecification, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newOperationSpecification, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationSpecification(OperationSpecification newOperationSpecification) {
		if (newOperationSpecification != eInternalContainer() || (eContainerFeatureID() != OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION && newOperationSpecification != null)) {
			if (EcoreUtil.isAncestor(this, newOperationSpecification))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOperationSpecification != null)
				msgs = ((InternalEObject)newOperationSpecification).eInverseAdd(this, OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS, OperationSpecification.class, msgs);
			msgs = basicSetOperationSpecification(newOperationSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION, newOperationSpecification, newOperationSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetOperationSpecification((OperationSpecification)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT:
				return basicSetModelRoot(null, msgs);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM:
				return ((InternalEList<?>)getDiagram()).basicRemove(otherEnd, msgs);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL:
				return basicSetDifferenceModel(null, msgs);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS:
				return basicSetConditions(null, msgs);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				return basicSetOperationSpecification(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				return eInternalContainer().eInverseRemove(this, OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS, OperationSpecification.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__NAME:
				return getName();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DESCRIPTION:
				return getDescription();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE:
				return getErrorMessage();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT:
				return getModelRoot();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM:
				return getDiagram();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL:
				return getDifferenceModel();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS:
				return getConditions();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				return getOperationSpecification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__NAME:
				setName((String)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE:
				setErrorMessage((String)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT:
				setModelRoot((EObject)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM:
				getDiagram().clear();
				getDiagram().addAll((Collection<? extends EObject>)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL:
				setDifferenceModel((ComparisonResourceSnapshot)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS:
				setConditions((ConditionsModel)newValue);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				setOperationSpecification((OperationSpecification)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE:
				setErrorMessage(ERROR_MESSAGE_EDEFAULT);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT:
				setModelRoot((EObject)null);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM:
				getDiagram().clear();
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL:
				setDifferenceModel((ComparisonResourceSnapshot)null);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS:
				setConditions((ConditionsModel)null);
				return;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				setOperationSpecification((OperationSpecification)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__ERROR_MESSAGE:
				return ERROR_MESSAGE_EDEFAULT == null ? errorMessage != null : !ERROR_MESSAGE_EDEFAULT.equals(errorMessage);
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__MODEL_ROOT:
				return modelRoot != null;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIAGRAM:
				return diagram != null && !diagram.isEmpty();
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__DIFFERENCE_MODEL:
				return differenceModel != null;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__CONDITIONS:
				return conditions != null;
			case OperationsPackage.NEGATIVE_APPLICATION_CONDITION__OPERATION_SPECIFICATION:
				return getOperationSpecification() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", errorMessage: ");
		result.append(errorMessage);
		result.append(')');
		return result.toString();
	}

} //NegativeApplicationConditionImpl
