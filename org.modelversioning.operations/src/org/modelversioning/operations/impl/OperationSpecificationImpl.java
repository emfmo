/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsPackage;
import org.modelversioning.operations.SpecificationState;
import org.modelversioning.operations.UserInput;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getTitleTemplate <em>Title Template</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#isIsRefactoring <em>Is Refactoring</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getModelingLanguage <em>Modeling Language</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getModelFileExtension <em>Model File Extension</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getDiagramFileExtension <em>Diagram File Extension</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getEditorId <em>Editor Id</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getWorkingModelRoot <em>Working Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getWorkingDiagram <em>Working Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getOriginModelRoot <em>Origin Model Root</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getOriginDiagram <em>Origin Diagram</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getDifferenceModel <em>Difference Model</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getIterations <em>Iterations</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getUserInputs <em>User Inputs</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getPreconditions <em>Preconditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getPostconditions <em>Postconditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getNegativeApplicationConditions <em>Negative Application Conditions</em>}</li>
 *   <li>{@link org.modelversioning.operations.impl.OperationSpecificationImpl#getState <em>State</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OperationSpecificationImpl extends EObjectImpl implements OperationSpecification {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitleTemplate() <em>Title Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitleTemplate()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_TEMPLATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitleTemplate() <em>Title Template</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitleTemplate()
	 * @generated
	 * @ordered
	 */
	protected String titleTemplate = TITLE_TEMPLATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = "1.0.0";

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #isIsRefactoring() <em>Is Refactoring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRefactoring()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_REFACTORING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsRefactoring() <em>Is Refactoring</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRefactoring()
	 * @generated
	 * @ordered
	 */
	protected boolean isRefactoring = IS_REFACTORING_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelingLanguage() <em>Modeling Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelingLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String MODELING_LANGUAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelingLanguage() <em>Modeling Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelingLanguage()
	 * @generated
	 * @ordered
	 */
	protected String modelingLanguage = MODELING_LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelFileExtension() <em>Model File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelFileExtension()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_FILE_EXTENSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelFileExtension() <em>Model File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelFileExtension()
	 * @generated
	 * @ordered
	 */
	protected String modelFileExtension = MODEL_FILE_EXTENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getDiagramFileExtension() <em>Diagram File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagramFileExtension()
	 * @generated
	 * @ordered
	 */
	protected static final String DIAGRAM_FILE_EXTENSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDiagramFileExtension() <em>Diagram File Extension</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagramFileExtension()
	 * @generated
	 * @ordered
	 */
	protected String diagramFileExtension = DIAGRAM_FILE_EXTENSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getEditorId() <em>Editor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorId()
	 * @generated
	 * @ordered
	 */
	protected static final String EDITOR_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEditorId() <em>Editor Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditorId()
	 * @generated
	 * @ordered
	 */
	protected String editorId = EDITOR_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWorkingModelRoot() <em>Working Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkingModelRoot()
	 * @generated
	 * @ordered
	 */
	protected EObject workingModelRoot;

	/**
	 * The cached value of the '{@link #getWorkingDiagram() <em>Working Diagram</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorkingDiagram()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> workingDiagram;

	/**
	 * The cached value of the '{@link #getOriginModelRoot() <em>Origin Model Root</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginModelRoot()
	 * @generated
	 * @ordered
	 */
	protected EObject originModelRoot;

	/**
	 * The cached value of the '{@link #getOriginDiagram() <em>Origin Diagram</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginDiagram()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> originDiagram;

	/**
	 * The cached value of the '{@link #getDifferenceModel() <em>Difference Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDifferenceModel()
	 * @generated
	 * @ordered
	 */
	protected ComparisonResourceSnapshot differenceModel;

	/**
	 * The cached value of the '{@link #getIterations() <em>Iterations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterations()
	 * @generated
	 * @ordered
	 */
	protected EList<Iteration> iterations;

	/**
	 * The cached value of the '{@link #getUserInputs() <em>User Inputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<UserInput> userInputs;

	/**
	 * The cached value of the '{@link #getPreconditions() <em>Preconditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreconditions()
	 * @generated
	 * @ordered
	 */
	protected ConditionsModel preconditions;

	/**
	 * The cached value of the '{@link #getPostconditions() <em>Postconditions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPostconditions()
	 * @generated
	 * @ordered
	 */
	protected ConditionsModel postconditions;

	/**
	 * The cached value of the '{@link #getNegativeApplicationConditions() <em>Negative Application Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNegativeApplicationConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<NegativeApplicationCondition> negativeApplicationConditions;

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final SpecificationState STATE_EDEFAULT = SpecificationState.INITIAL;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected SpecificationState state = STATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperationsPackage.Literals.OPERATION_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTitleTemplate() {
		return titleTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTitleTemplate(String newTitleTemplate) {
		String oldTitleTemplate = titleTemplate;
		titleTemplate = newTitleTemplate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__TITLE_TEMPLATE, oldTitleTemplate, titleTemplate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelingLanguage() {
		return modelingLanguage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelingLanguage(String newModelingLanguage) {
		String oldModelingLanguage = modelingLanguage;
		modelingLanguage = newModelingLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__MODELING_LANGUAGE, oldModelingLanguage, modelingLanguage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelFileExtension() {
		return modelFileExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelFileExtension(String newModelFileExtension) {
		String oldModelFileExtension = modelFileExtension;
		modelFileExtension = newModelFileExtension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION, oldModelFileExtension, modelFileExtension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDiagramFileExtension() {
		return diagramFileExtension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagramFileExtension(String newDiagramFileExtension) {
		String oldDiagramFileExtension = diagramFileExtension;
		diagramFileExtension = newDiagramFileExtension;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION, oldDiagramFileExtension, diagramFileExtension));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEditorId() {
		return editorId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditorId(String newEditorId) {
		String oldEditorId = editorId;
		editorId = newEditorId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__EDITOR_ID, oldEditorId, editorId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsRefactoring() {
		return isRefactoring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsRefactoring(boolean newIsRefactoring) {
		boolean oldIsRefactoring = isRefactoring;
		isRefactoring = newIsRefactoring;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__IS_REFACTORING, oldIsRefactoring, isRefactoring));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getWorkingModelRoot() {
		return workingModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWorkingModelRoot(EObject newWorkingModelRoot, NotificationChain msgs) {
		EObject oldWorkingModelRoot = workingModelRoot;
		workingModelRoot = newWorkingModelRoot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT, oldWorkingModelRoot, newWorkingModelRoot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorkingModelRoot(EObject newWorkingModelRoot) {
		if (newWorkingModelRoot != workingModelRoot) {
			NotificationChain msgs = null;
			if (workingModelRoot != null)
				msgs = ((InternalEObject)workingModelRoot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT, null, msgs);
			if (newWorkingModelRoot != null)
				msgs = ((InternalEObject)newWorkingModelRoot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT, null, msgs);
			msgs = basicSetWorkingModelRoot(newWorkingModelRoot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT, newWorkingModelRoot, newWorkingModelRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getWorkingDiagram() {
		if (workingDiagram == null) {
			workingDiagram = new EObjectContainmentEList<EObject>(EObject.class, this, OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM);
		}
		return workingDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getOriginModelRoot() {
		return originModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOriginModelRoot(EObject newOriginModelRoot, NotificationChain msgs) {
		EObject oldOriginModelRoot = originModelRoot;
		originModelRoot = newOriginModelRoot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT, oldOriginModelRoot, newOriginModelRoot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginModelRoot(EObject newOriginModelRoot) {
		if (newOriginModelRoot != originModelRoot) {
			NotificationChain msgs = null;
			if (originModelRoot != null)
				msgs = ((InternalEObject)originModelRoot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT, null, msgs);
			if (newOriginModelRoot != null)
				msgs = ((InternalEObject)newOriginModelRoot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT, null, msgs);
			msgs = basicSetOriginModelRoot(newOriginModelRoot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT, newOriginModelRoot, newOriginModelRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getOriginDiagram() {
		if (originDiagram == null) {
			originDiagram = new EObjectContainmentEList<EObject>(EObject.class, this, OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM);
		}
		return originDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonResourceSnapshot getDifferenceModel() {
		return differenceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDifferenceModel(ComparisonResourceSnapshot newDifferenceModel, NotificationChain msgs) {
		ComparisonResourceSnapshot oldDifferenceModel = differenceModel;
		differenceModel = newDifferenceModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL, oldDifferenceModel, newDifferenceModel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDifferenceModel(ComparisonResourceSnapshot newDifferenceModel) {
		if (newDifferenceModel != differenceModel) {
			NotificationChain msgs = null;
			if (differenceModel != null)
				msgs = ((InternalEObject)differenceModel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL, null, msgs);
			if (newDifferenceModel != null)
				msgs = ((InternalEObject)newDifferenceModel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL, null, msgs);
			msgs = basicSetDifferenceModel(newDifferenceModel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL, newDifferenceModel, newDifferenceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Iteration> getIterations() {
		if (iterations == null) {
			iterations = new EObjectContainmentEList<Iteration>(Iteration.class, this, OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS);
		}
		return iterations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UserInput> getUserInputs() {
		if (userInputs == null) {
			userInputs = new EObjectContainmentEList<UserInput>(UserInput.class, this, OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS);
		}
		return userInputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsModel getPreconditions() {
		return preconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreconditions(ConditionsModel newPreconditions, NotificationChain msgs) {
		ConditionsModel oldPreconditions = preconditions;
		preconditions = newPreconditions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS, oldPreconditions, newPreconditions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreconditions(ConditionsModel newPreconditions) {
		if (newPreconditions != preconditions) {
			NotificationChain msgs = null;
			if (preconditions != null)
				msgs = ((InternalEObject)preconditions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS, null, msgs);
			if (newPreconditions != null)
				msgs = ((InternalEObject)newPreconditions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS, null, msgs);
			msgs = basicSetPreconditions(newPreconditions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS, newPreconditions, newPreconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsModel getPostconditions() {
		return postconditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPostconditions(ConditionsModel newPostconditions, NotificationChain msgs) {
		ConditionsModel oldPostconditions = postconditions;
		postconditions = newPostconditions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS, oldPostconditions, newPostconditions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPostconditions(ConditionsModel newPostconditions) {
		if (newPostconditions != postconditions) {
			NotificationChain msgs = null;
			if (postconditions != null)
				msgs = ((InternalEObject)postconditions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS, null, msgs);
			if (newPostconditions != null)
				msgs = ((InternalEObject)newPostconditions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS, null, msgs);
			msgs = basicSetPostconditions(newPostconditions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS, newPostconditions, newPostconditions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NegativeApplicationCondition> getNegativeApplicationConditions() {
		if (negativeApplicationConditions == null) {
			negativeApplicationConditions = new EObjectContainmentEList<NegativeApplicationCondition>(NegativeApplicationCondition.class, this, OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS);
		}
		return negativeApplicationConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationState getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(SpecificationState newState) {
		SpecificationState oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperationsPackage.OPERATION_SPECIFICATION__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT:
				return basicSetWorkingModelRoot(null, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM:
				return ((InternalEList<?>)getWorkingDiagram()).basicRemove(otherEnd, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT:
				return basicSetOriginModelRoot(null, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM:
				return ((InternalEList<?>)getOriginDiagram()).basicRemove(otherEnd, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL:
				return basicSetDifferenceModel(null, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS:
				return ((InternalEList<?>)getIterations()).basicRemove(otherEnd, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS:
				return ((InternalEList<?>)getUserInputs()).basicRemove(otherEnd, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS:
				return basicSetPreconditions(null, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS:
				return basicSetPostconditions(null, msgs);
			case OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS:
				return ((InternalEList<?>)getNegativeApplicationConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperationsPackage.OPERATION_SPECIFICATION__NAME:
				return getName();
			case OperationsPackage.OPERATION_SPECIFICATION__DESCRIPTION:
				return getDescription();
			case OperationsPackage.OPERATION_SPECIFICATION__TITLE_TEMPLATE:
				return getTitleTemplate();
			case OperationsPackage.OPERATION_SPECIFICATION__VERSION:
				return getVersion();
			case OperationsPackage.OPERATION_SPECIFICATION__IS_REFACTORING:
				return isIsRefactoring();
			case OperationsPackage.OPERATION_SPECIFICATION__MODELING_LANGUAGE:
				return getModelingLanguage();
			case OperationsPackage.OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION:
				return getModelFileExtension();
			case OperationsPackage.OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION:
				return getDiagramFileExtension();
			case OperationsPackage.OPERATION_SPECIFICATION__EDITOR_ID:
				return getEditorId();
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT:
				return getWorkingModelRoot();
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM:
				return getWorkingDiagram();
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT:
				return getOriginModelRoot();
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM:
				return getOriginDiagram();
			case OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL:
				return getDifferenceModel();
			case OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS:
				return getIterations();
			case OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS:
				return getUserInputs();
			case OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS:
				return getPreconditions();
			case OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS:
				return getPostconditions();
			case OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS:
				return getNegativeApplicationConditions();
			case OperationsPackage.OPERATION_SPECIFICATION__STATE:
				return getState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperationsPackage.OPERATION_SPECIFICATION__NAME:
				setName((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__TITLE_TEMPLATE:
				setTitleTemplate((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__VERSION:
				setVersion((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__IS_REFACTORING:
				setIsRefactoring((Boolean)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__MODELING_LANGUAGE:
				setModelingLanguage((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION:
				setModelFileExtension((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION:
				setDiagramFileExtension((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__EDITOR_ID:
				setEditorId((String)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT:
				setWorkingModelRoot((EObject)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM:
				getWorkingDiagram().clear();
				getWorkingDiagram().addAll((Collection<? extends EObject>)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT:
				setOriginModelRoot((EObject)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM:
				getOriginDiagram().clear();
				getOriginDiagram().addAll((Collection<? extends EObject>)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL:
				setDifferenceModel((ComparisonResourceSnapshot)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS:
				getIterations().clear();
				getIterations().addAll((Collection<? extends Iteration>)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS:
				getUserInputs().clear();
				getUserInputs().addAll((Collection<? extends UserInput>)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS:
				setPreconditions((ConditionsModel)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS:
				setPostconditions((ConditionsModel)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS:
				getNegativeApplicationConditions().clear();
				getNegativeApplicationConditions().addAll((Collection<? extends NegativeApplicationCondition>)newValue);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__STATE:
				setState((SpecificationState)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperationsPackage.OPERATION_SPECIFICATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__TITLE_TEMPLATE:
				setTitleTemplate(TITLE_TEMPLATE_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__IS_REFACTORING:
				setIsRefactoring(IS_REFACTORING_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__MODELING_LANGUAGE:
				setModelingLanguage(MODELING_LANGUAGE_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION:
				setModelFileExtension(MODEL_FILE_EXTENSION_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION:
				setDiagramFileExtension(DIAGRAM_FILE_EXTENSION_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__EDITOR_ID:
				setEditorId(EDITOR_ID_EDEFAULT);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT:
				setWorkingModelRoot((EObject)null);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM:
				getWorkingDiagram().clear();
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT:
				setOriginModelRoot((EObject)null);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM:
				getOriginDiagram().clear();
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL:
				setDifferenceModel((ComparisonResourceSnapshot)null);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS:
				getIterations().clear();
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS:
				getUserInputs().clear();
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS:
				setPreconditions((ConditionsModel)null);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS:
				setPostconditions((ConditionsModel)null);
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS:
				getNegativeApplicationConditions().clear();
				return;
			case OperationsPackage.OPERATION_SPECIFICATION__STATE:
				setState(STATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperationsPackage.OPERATION_SPECIFICATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case OperationsPackage.OPERATION_SPECIFICATION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case OperationsPackage.OPERATION_SPECIFICATION__TITLE_TEMPLATE:
				return TITLE_TEMPLATE_EDEFAULT == null ? titleTemplate != null : !TITLE_TEMPLATE_EDEFAULT.equals(titleTemplate);
			case OperationsPackage.OPERATION_SPECIFICATION__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case OperationsPackage.OPERATION_SPECIFICATION__IS_REFACTORING:
				return isRefactoring != IS_REFACTORING_EDEFAULT;
			case OperationsPackage.OPERATION_SPECIFICATION__MODELING_LANGUAGE:
				return MODELING_LANGUAGE_EDEFAULT == null ? modelingLanguage != null : !MODELING_LANGUAGE_EDEFAULT.equals(modelingLanguage);
			case OperationsPackage.OPERATION_SPECIFICATION__MODEL_FILE_EXTENSION:
				return MODEL_FILE_EXTENSION_EDEFAULT == null ? modelFileExtension != null : !MODEL_FILE_EXTENSION_EDEFAULT.equals(modelFileExtension);
			case OperationsPackage.OPERATION_SPECIFICATION__DIAGRAM_FILE_EXTENSION:
				return DIAGRAM_FILE_EXTENSION_EDEFAULT == null ? diagramFileExtension != null : !DIAGRAM_FILE_EXTENSION_EDEFAULT.equals(diagramFileExtension);
			case OperationsPackage.OPERATION_SPECIFICATION__EDITOR_ID:
				return EDITOR_ID_EDEFAULT == null ? editorId != null : !EDITOR_ID_EDEFAULT.equals(editorId);
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_MODEL_ROOT:
				return workingModelRoot != null;
			case OperationsPackage.OPERATION_SPECIFICATION__WORKING_DIAGRAM:
				return workingDiagram != null && !workingDiagram.isEmpty();
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_MODEL_ROOT:
				return originModelRoot != null;
			case OperationsPackage.OPERATION_SPECIFICATION__ORIGIN_DIAGRAM:
				return originDiagram != null && !originDiagram.isEmpty();
			case OperationsPackage.OPERATION_SPECIFICATION__DIFFERENCE_MODEL:
				return differenceModel != null;
			case OperationsPackage.OPERATION_SPECIFICATION__ITERATIONS:
				return iterations != null && !iterations.isEmpty();
			case OperationsPackage.OPERATION_SPECIFICATION__USER_INPUTS:
				return userInputs != null && !userInputs.isEmpty();
			case OperationsPackage.OPERATION_SPECIFICATION__PRECONDITIONS:
				return preconditions != null;
			case OperationsPackage.OPERATION_SPECIFICATION__POSTCONDITIONS:
				return postconditions != null;
			case OperationsPackage.OPERATION_SPECIFICATION__NEGATIVE_APPLICATION_CONDITIONS:
				return negativeApplicationConditions != null && !negativeApplicationConditions.isEmpty();
			case OperationsPackage.OPERATION_SPECIFICATION__STATE:
				return state != STATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", titleTemplate: ");
		result.append(titleTemplate);
		result.append(", version: ");
		result.append(version);
		result.append(", isRefactoring: ");
		result.append(isRefactoring);
		result.append(", modelingLanguage: ");
		result.append(modelingLanguage);
		result.append(", modelFileExtension: ");
		result.append(modelFileExtension);
		result.append(", diagramFileExtension: ");
		result.append(diagramFileExtension);
		result.append(", editorId: ");
		result.append(editorId);
		result.append(", state: ");
		result.append(state);
		result.append(')');
		return result.toString();
	}

} //OperationSpecificationImpl
