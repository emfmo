/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.diff.util.DiffUtil;
import org.modelversioning.core.match.util.MatchUtil;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.OperationSpecification;

/**
 * Utilities for {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationsUtil {

	/**
	 * Prefix for initial model templates to use in conditions (like
	 * #{initial:EClass_1}).
	 */
	public static String INITIAL_PREFIX = "initial";

	/**
	 * Prefix for revised model templates to use in conditions (like
	 * #{revised:EClass_1}).
	 */
	public static String REVISED_PREFIX = "revised";

	/**
	 * Returns <code>true</code> if there is an iteration in the specified
	 * <code>operationSpecification</code> for the specified
	 * <code>template</code>.
	 * 
	 * @param template
	 *            template to check for an iteration.
	 * @param operationSpecification
	 *            {@link OperationSpecification} to search in.
	 * @return <code>true</code> if there is an iteration. <code>false</code>
	 *         otherwise.
	 */
	public static boolean isIterated(Template template,
			OperationSpecification operationSpecification) {
		return (getIteration(template, operationSpecification) != null);
	}

	/**
	 * Returns the {@link Iteration} which is defined for <code>template</code>.
	 * If there is no iteration for the template, <code>null</code> is returned.
	 * 
	 * @param template
	 *            to get iteration for.
	 * @param operationSpecification
	 *            {@link OperationSpecification} to search in.
	 * @return the {@link Iteration} for <code>template</code> if there is one.
	 *         If there is none, <code>null</code> is returned.
	 */
	public static Iteration getIteration(Template template,
			OperationSpecification operationSpecification) {
		for (Iteration iteration : operationSpecification.getIterations()) {
			if (template.equals(iteration.getTemplate())) {
				return iteration;
			}
		}
		return null;
	}

	/**
	 * Specifies whether a parent of <code>template</code> is iterated (see
	 * {@link #isIterated(Template, OperationSpecification)}).
	 * 
	 * @param template
	 *            to check parents for {@link Iteration}s.
	 * @param operationSpecification
	 *            {@link OperationSpecification} to search in.
	 * @return <code>true</code> if there is an iterated parent.
	 *         <code>false</code> otherwise.
	 */
	public static boolean isParentIterated(Template template,
			OperationSpecification operationSpecification) {
		List<Template> parents = ConditionsUtil.createParentList(template);
		for (Template parent : parents) {
			if (isIterated(parent, operationSpecification)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the precondition template representing the left element of the
	 * specified <code>diffElement</code> contained by the specified
	 * <code>operationSpecification</code>.
	 * 
	 * Asserts that the precondition side is <em>left</em> in the diff model.
	 * 
	 * @param diffElement
	 *            to get precondition template for.
	 * @param operationSpecification
	 *            containing the precondition template.
	 * @return the template, or <code>null</code> if it could not be found.
	 */
	public static Template getPreconditionTemplateByDiffElement(
			DiffElement diffElement,
			OperationSpecification operationSpecification) {
		EObject rightElement = DiffUtil.getRightElement(diffElement);
		if (rightElement != null) {
			return ConditionsUtil.getTemplateByRepresentative(rightElement,
					operationSpecification.getPreconditions());
		}
		return null;
	}

	/**
	 * Returns the postcondition template representing the right element of the
	 * specified <code>diffElement</code> contained by the specified
	 * <code>operationSpecification</code>.
	 * 
	 * Asserts that the postcondition side is <em>right</em> in the diff model.
	 * 
	 * @param diffElement
	 *            to get precondition template for.
	 * @param operationSpecification
	 *            containing the precondition template.
	 * @return the template, or <code>null</code> if it could not be found.
	 */
	public static Template getPostconditionTemplateByDiffElement(
			DiffElement diffElement,
			OperationSpecification operationSpecification) {
		EObject leftElement = DiffUtil.getLeftElement(diffElement);
		if (leftElement != null) {
			return ConditionsUtil.getTemplateByRepresentative(leftElement,
					operationSpecification.getPostconditions());
		}
		return null;
	}

	/**
	 * Returns <code>true</code> if the specified <code>templateBindings</code>
	 * is unique for the specified <code>operationSpecification</code>, i.e.,
	 * for each active and not iterated Template there is only one single object
	 * bound to it.
	 * 
	 * @param templateBindings
	 *            to check for uniqueness.
	 * @param operationSpecification
	 *            to check <code>templateBindings</code> against.
	 * 
	 * @return <code>true</code> if unique, <code>false</code> otherwise.
	 */
	public static boolean isUnique(ITemplateBindings templateBindings,
			OperationSpecification operationSpecification) {
		boolean isUnique = true;
		EList<Template> allTemplates = ConditionsUtil
				.getAllTemplates(operationSpecification.getPreconditions());
		for (Template template : allTemplates) {
			if (!isUnique(template, templateBindings.getBoundObjects(template),
					operationSpecification)) {
				isUnique = false;
				break;
			}
		}
		return isUnique;
	}

	/**
	 * Returns <code>true</code> if the bound <code>objects</code> are unique
	 * for the specified <code>template</code> when using the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param template
	 *            {@link Template} to check.
	 * @param objects
	 *            Objects to check for.
	 * @param operationSpecification
	 *            to check <code>templateBindings</code> against.
	 * @return <code>true</code> if unique. <code>false</code> otherwise.
	 */
	public static boolean isUnique(Template template, Set<EObject> objects,
			OperationSpecification operationSpecification) {
		boolean isUnique = true;
		if (objects.size() > 1
				&& !allowsMultipleBinding(template, operationSpecification)) {
			isUnique = false;
		}
		return isUnique;
	}

	/**
	 * Returns <code>true</code> if there may be more than one object bound to
	 * the specified <code>template</code> in the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param template
	 *            {@link Template} to check.
	 * @param operationSpecification
	 *            to check against.
	 * @return <code>true</code> if there may be more than one object.
	 *         <code>false</code> otherwise.
	 */
	public static boolean allowsMultipleBinding(Template template,
			OperationSpecification operationSpecification) {
		if (isIterated(template, operationSpecification)
				|| isParentIterated(template, operationSpecification)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Splits not unique <code>templateBindings</code> to a {@link Set} of
	 * unique {@link ITemplateBindings}.
	 * 
	 * @param templateBindings
	 *            to split.
	 * @param operationSpecification
	 *            to check for uniqueness.
	 * @return a set of unique template bindings.
	 */
	public static Set<ITemplateBindings> splitToUniqueBindings(
			ITemplateBindings templateBindings,
			OperationSpecification operationSpecification) {

		Set<ITemplateBindings> splitTemplateBindings = new HashSet<ITemplateBindings>();

		boolean hadToSplit = false;
		EList<Template> allTemplates = ConditionsUtil
				.getAllTemplates(operationSpecification.getPreconditions());
		for (Template template : allTemplates) {
			Set<EObject> boundObjects = templateBindings
					.getBoundObjects(template);
			// if there is an ambiguous binding split and recurse
			if (!isUnique(template, boundObjects, operationSpecification)) {
				hadToSplit = true;
				for (EObject boundObject : boundObjects) {
					// extract binding and remove them from main binding (true)
					ITemplateBindings extractedBinding = templateBindings
							.extractSubBindings(template, boundObject, true);
					// recurse for split binding and main binding (there might
					// be more ambiguous bindnigs)
					// add the results of both recursions
					splitTemplateBindings.addAll(splitToUniqueBindings(
							extractedBinding, operationSpecification));
				}
			}
			if (hadToSplit) {
				break;
			}
		}

		// if no split was necessary add main bindings
		if (!hadToSplit) {
			splitTemplateBindings.add(templateBindings);
		}

		return splitTemplateBindings;
	}

	/**
	 * Creates an id for the specified operation specification.
	 * 
	 * @param operationSpecification
	 *            to create id for.
	 * @return the id.
	 */
	public static String getId(OperationSpecification operationSpecification) {
		return operationSpecification.getModelingLanguage() + "/" //$NON-NLS-1$
				+ operationSpecification.getVersion() + "/" //$NON-NLS-1$
				+ operationSpecification.getName();
	}

	/**
	 * Returns the corresponding post condition template of which the
	 * representative is matched to the representative of the specified
	 * <code>preConditoinTemplate</code>.
	 * 
	 * @param preConditionTemplate
	 *            of which the corresponding post condition template is
	 *            requested.
	 * @param operationSpecification
	 *            containing the post condition model and the match model to
	 *            use.
	 * @return the corresponding post condition template.
	 */
	public static Template getCorrespondingPostConditionTemplate(
			Template preConditionTemplate,
			OperationSpecification operationSpecification) {
		MatchModel matchModel = operationSpecification.getDifferenceModel()
				.getMatch();
		EObject preConditionRepresentative = preConditionTemplate
				.getRepresentative();
		EObject postConditionRepresentative = MatchUtil.getMatchingObject(
				preConditionRepresentative, matchModel);
		Template postConditionTemplate = ConditionsUtil
				.getTemplateByRepresentative(postConditionRepresentative,
						operationSpecification.getPostconditions());
		return postConditionTemplate;
	}

	/**
	 * Derives the post condition template binding from the specified
	 * <code>preConditionTemplateBindings</code> using the specified
	 * <code>matchModel</code>.
	 * 
	 * @param preConditionTemplateBindings
	 *            to derive post condition template binding from.
	 * @param matchModel
	 *            to use for derivation.
	 * @param operationSpecification
	 *            operation specification to get templates from.
	 * @return the derived post condition template binding.
	 */
	public static ITemplateBinding derivePostConditionTemplateBinding(
			ITemplateBindings preConditionTemplateBindings,
			MatchModel matchModel, OperationSpecification operationSpecification) {
		ITemplateBinding templateBinding = new TemplateBindingImpl();

		for (Template boundPreTemplate : preConditionTemplateBindings
				.getTemplates()) {
			Set<EObject> boundPreObjects = preConditionTemplateBindings
					.getBoundObjects(boundPreTemplate);
			Template postConditionTemplate = getCorrespondingPostConditionTemplate(
					boundPreTemplate, operationSpecification);
			for (EObject boundPreObject : boundPreObjects) {
				templateBinding
						.add(postConditionTemplate, MatchUtil
								.getMatchingObject(boundPreObject, matchModel));
			}
		}

		return templateBinding;
	}
}
