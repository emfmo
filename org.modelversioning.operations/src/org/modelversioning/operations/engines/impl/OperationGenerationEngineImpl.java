/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.engines.impl;

import java.util.Collection;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.match.metamodel.Match2Elements;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.engines.IConditionGenerationEngine;
import org.modelversioning.core.conditions.engines.impl.ConditionsGenerationEngineImpl;
import org.modelversioning.core.diff.service.DiffService;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.SpecificationState;
import org.modelversioning.operations.engines.IOperationGenerationEngine;

/**
 * Implements the {@link IOperationGenerationEngine} using the
 * {@link DiffService} and the {@link ConditionsGenerationEngineImpl} by
 * default.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationGenerationEngineImpl implements
		IOperationGenerationEngine {

	private static final String NAC_DEFAULT_NAME = "New NAC";

	/**
	 * The currently used {@link IConditionGenerationEngine}.
	 */
	private IConditionGenerationEngine conditionGenerationEngine = new ConditionsGenerationEngineImpl();

	/**
	 * Default constructor.
	 */
	public OperationGenerationEngineImpl() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IConditionGenerationEngine getConditionGenerationEngine() {
		return conditionGenerationEngine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setConditionGenerationEngine(
			IConditionGenerationEngine conditionGenerationEngine) {
		this.conditionGenerationEngine = conditionGenerationEngine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OperationSpecification generateOperationSpecification(
			MatchModel matchModel) {
		// create model input snapshot out of match model
		ComparisonResourceSnapshot comparisonResourceSnapshot = new DiffService()
				.generateComparisonResourceSnapshot(matchModel);

		EObject originRoot = ((Match2Elements) matchModel.getMatchedElements()
				.get(0)).getRightElement(); // changed from left to right
		EObject workingRoot = ((Match2Elements) matchModel.getMatchedElements()
				.get(0)).getLeftElement(); // changed from right to left
		ConditionsModel originConditionsModel = conditionGenerationEngine
				.generateConditionsModel(originRoot);
		ConditionsModel workingConditionsModel = conditionGenerationEngine
				.generateConditionsModel(workingRoot);

		// create operation specification setting created objects
		OperationSpecification operationSpecification = OperationsFactory.eINSTANCE
				.createOperationSpecification();
		operationSpecification.setOriginModelRoot(originRoot);
		operationSpecification.setModelingLanguage(originRoot.eClass()
				.getEPackage().getNsURI());
		operationSpecification.setWorkingModelRoot(workingRoot);
		operationSpecification.setDifferenceModel(comparisonResourceSnapshot);
		operationSpecification.setPreconditions(originConditionsModel);
		operationSpecification.setPostconditions(workingConditionsModel);
		operationSpecification.setState(SpecificationState.CONFIGURATION);

		// return created operation definition
		return operationSpecification;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OperationSpecification generateOperationSpecification(
			Resource origin, Resource working) throws MatchException {
		return generateOperationSpecification(new MatchService()
				.generateMatchModel(origin, working));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OperationSpecification generateInitialOperationSpecification(
			Resource origin, Resource originDiagram) {
		// get origin root object and generate origin condition model
		Copier copier = new Copier();
		EObject originRoot = copier.copy(origin.getContents().get(0));
		// optionally set diagram
		Collection<EObject> diagramRoots = null;
		if (originDiagram != null) {
			diagramRoots = copier.copyAll(originDiagram.getContents());

		}
		copier.copyReferences();

		ConditionsModel originConditionsModel = conditionGenerationEngine
				.generateConditionsModel(originRoot);

		// create initial operation specification and return it
		OperationSpecification operationSpecification = OperationsFactory.eINSTANCE
				.createOperationSpecification();
		operationSpecification.setOriginModelRoot(originRoot);
		if (diagramRoots != null) {
			operationSpecification.getOriginDiagram().clear();
			operationSpecification.getOriginDiagram().addAll(diagramRoots);
		}
		operationSpecification.setModelingLanguage(originRoot.eClass()
				.getEPackage().getNsURI());
		operationSpecification.setPreconditions(originConditionsModel);
		operationSpecification.setState(SpecificationState.INITIAL);
		return operationSpecification;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putRevisedModel(MatchModel matchModel,
			Resource revisedDiagramResource,
			OperationSpecification operationSpecification) {
		Copier copier = new Copier();
		// create model input snapshot out of match model
		ComparisonResourceSnapshot comparisonResourceSnapshot = new DiffService()
				.generateComparisonResourceSnapshot(matchModel);

		EObject workingRoot = matchModel.getRightRoots().get(0);

		// optionally set diagram
		Collection<EObject> diagramRoots = null;
		if (revisedDiagramResource != null) {
			diagramRoots = copier.copyAll(revisedDiagramResource.getContents());
		}
		copier.copyReferences();

		// generate working condition model
		ConditionsModel workingConditionsModel = conditionGenerationEngine
				.generateConditionsModel(workingRoot);

		// set new values and update state
		if (diagramRoots != null) {
			operationSpecification.getWorkingDiagram().clear();
			operationSpecification.getWorkingDiagram().addAll(diagramRoots);
		}
		operationSpecification.setWorkingModelRoot(workingRoot);
		operationSpecification.setDifferenceModel(comparisonResourceSnapshot);
		operationSpecification.setPostconditions(workingConditionsModel);
		operationSpecification.setState(SpecificationState.CONFIGURATION);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NegativeApplicationCondition createNegativeApplicationCondition(
			MatchModel matchModel, Resource diagramResource,
			OperationSpecification operationSpecification) {
		// create model input snapshot out of match model
		ComparisonResourceSnapshot comparisonResourceSnapshot = new DiffService()
				.generateComparisonResourceSnapshot(matchModel);

		EObject nacRoot = matchModel.getRightRoots().get(0);

		// create change based refinement condition model
		ConditionsModel conditionsModel = conditionGenerationEngine
				.generateRefinedConditionsModel(nacRoot,
						operationSpecification.getPreconditions(),
						comparisonResourceSnapshot);

		NegativeApplicationCondition newNAC = OperationsFactory.eINSTANCE
				.createNegativeApplicationCondition();
		newNAC.setName(NAC_DEFAULT_NAME);
		newNAC.setDifferenceModel(comparisonResourceSnapshot);
		newNAC.setModelRoot(nacRoot);
		newNAC.setConditions(conditionsModel);

		// optionally set diagram
		if (diagramResource != null) {
			Copier copier = new Copier();
			Collection<EObject> diagramRoots = copier.copyAll(diagramResource
					.getContents());
			newNAC.getDiagram().clear();
			newNAC.getDiagram().addAll(diagramRoots);
			copier.copyReferences();
		}

		// set to operation specification
		operationSpecification.getNegativeApplicationConditions().add(newNAC);

		return newNAC;
	}

}
