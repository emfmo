/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations;

import java.io.PrintStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.diff.DiffSignature;
import org.modelversioning.core.diff.util.DiffUtil;
import org.modelversioning.core.match.util.MatchUtil;
import org.modelversioning.operations.TemplateAffection.AffectionKind;

/**
 * A {@link DiffSignature} for {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationSignature extends DiffSignature {

	/**
	 * Optionally the according operation specification.
	 */
	private OperationSpecification operationSpecification = null;

	/**
	 * A map of precondition {@link Template}s and template affections.
	 */
	private Map<Template, Set<TemplateAffection>> preConditionAffectionMap = new HashMap<Template, Set<TemplateAffection>>();

	/**
	 * A map of postcondition {@link Template}s and template affections.
	 */
	private Map<Template, Set<TemplateAffection>> postConditionAffectionMap = new HashMap<Template, Set<TemplateAffection>>();

	/**
	 * A map from pre condition representatives to templates.
	 */
	private Map<EObject, Template> preConditionRepToTemplateMap = null;

	/**
	 * A map from post condition representatives to templates.
	 */
	private Map<EObject, Template> postConditionRepToTemplateMap = null;

	/**
	 * Creates a new {@link DiffSignature} for the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            to create operation signature for.
	 */
	public OperationSignature(OperationSpecification operationSpecification) {
		super(operationSpecification.getDifferenceModel().getDiff());
		Assert.isNotNull(operationSpecification);
		this.operationSpecification = operationSpecification;
		initializePreconditionRepToTemplateMap();
		initializePostconditionRepToTemplateMap();
	}

	/**
	 * Initializes the pre condition representative to precondition template
	 * map.
	 */
	private void initializePreconditionRepToTemplateMap() {
		// create precondition representative to template map
		this.preConditionRepToTemplateMap = ConditionsUtil
				.getRepresentativeToTemplateMap(operationSpecification
						.getPreconditions());

		// get all representatives of the precondition model
		Set<EObject> representatives = preConditionRepToTemplateMap.keySet();

		// map diff elements to the precondition templates they affect
		for (DiffElement diffElement : super.getSignatureElements()) {

			// get affected precondition representatives
			EObject element = DiffUtil.getRightElement(diffElement);
			EObject parent = DiffUtil.getRightParent(diffElement);
			EObject target = DiffUtil.getRightTarget(diffElement);

			// in some cases only the opposite target is set
			// try to find matching object and use it as target
			EObject leftTarget = DiffUtil.getLeftTarget(diffElement);
			if (leftTarget != null) {
				EObject matchingObject = MatchUtil.getMatchingObject(
						leftTarget, operationSpecification.getDifferenceModel()
								.getMatch());
				leftTarget = matchingObject;
			}

			if (element != null && representatives.contains(element)) {
				Template template = preConditionRepToTemplateMap.get(element);
				addToPreCondAffectionMap(template, diffElement,
						AffectionKind.ELEMENT);
			}

			if (parent != null && representatives.contains(parent)) {
				Template template = preConditionRepToTemplateMap.get(parent);
				addToPreCondAffectionMap(template, diffElement,
						AffectionKind.PARENT);
			}

			if (target != null && representatives.contains(target)) {
				Template template = preConditionRepToTemplateMap.get(target);
				addToPreCondAffectionMap(template, diffElement,
						AffectionKind.TARGET);
			}

			if (leftTarget != null && representatives.contains(leftTarget)) {
				Template template = preConditionRepToTemplateMap
						.get(leftTarget);
				addToPreCondAffectionMap(template, diffElement,
						AffectionKind.OPPOSITE_TARGET);
			}
		}
	}

	/**
	 * Initializes the postcondition representative to precondition template
	 * map.
	 */
	private void initializePostconditionRepToTemplateMap() {
		// create precondition representative to template map
		this.postConditionRepToTemplateMap = ConditionsUtil
				.getRepresentativeToTemplateMap(operationSpecification
						.getPostconditions());

		// get all representatives of the precondition model
		Set<EObject> representatives = postConditionRepToTemplateMap.keySet();

		// map diff elements to the precondition templates they affect
		for (DiffElement diffElement : super.getSignatureElements()) {

			// get affected precondition representatives
			EObject element = DiffUtil.getLeftElement(diffElement);
			EObject parent = DiffUtil.getLeftParent(diffElement);
			EObject target = DiffUtil.getLeftTarget(diffElement);

			// in some cases only the opposite target is set
			// try to find matching object and use it as target
			EObject rightTarget = DiffUtil.getRightTarget(diffElement);
			if (rightTarget != null) {
				EObject matchingObject = MatchUtil.getMatchingObject(
						rightTarget, operationSpecification
								.getDifferenceModel().getMatch());
				rightTarget = matchingObject;
			}

			if (element != null && representatives.contains(element)) {
				Template template = postConditionRepToTemplateMap.get(element);
				addToPostCondAffectionMap(template, diffElement,
						AffectionKind.ELEMENT);
			}

			if (parent != null && representatives.contains(parent)) {
				Template template = postConditionRepToTemplateMap.get(parent);
				addToPostCondAffectionMap(template, diffElement,
						AffectionKind.PARENT);
			}

			if (target != null && representatives.contains(target)) {
				Template template = postConditionRepToTemplateMap.get(target);
				addToPostCondAffectionMap(template, diffElement,
						AffectionKind.TARGET);
			}

			if (rightTarget != null && representatives.contains(rightTarget)) {
				Template template = postConditionRepToTemplateMap
						.get(rightTarget);
				addToPostCondAffectionMap(template, diffElement,
						AffectionKind.OPPOSITE_TARGET);
			}
		}
	}

	/**
	 * Adds the <code>change</code> to the precondition affection map for the
	 * specified <code>template</code> and <code>affectionKind</code>.
	 * 
	 * @param template
	 *            template affected by <code>change</code>.
	 * @param change
	 *            to add.
	 * @param affectionKind
	 *            kind of affection.
	 */
	private void addToPreCondAffectionMap(Template template,
			DiffElement change, AffectionKind affectionKind) {
		Set<TemplateAffection> affectionKindSet = null;
		if (!this.preConditionAffectionMap.containsKey(template)) {
			affectionKindSet = new HashSet<TemplateAffection>();
			preConditionAffectionMap.put(template, affectionKindSet);
		} else {
			affectionKindSet = preConditionAffectionMap.get(template);
		}
		affectionKindSet.add(new TemplateAffection(affectionKind, template,
				change));
	}

	/**
	 * Adds the <code>change</code> to the postcondition affection map for the
	 * specified <code>template</code> and <code>affectionKind</code>.
	 * 
	 * @param template
	 *            template affected by <code>change</code>.
	 * @param change
	 *            to add.
	 * @param affectionKind
	 *            kind of affection.
	 */
	private void addToPostCondAffectionMap(Template template,
			DiffElement change, AffectionKind affectionKind) {
		Set<TemplateAffection> affectionKindSet = null;
		if (!this.postConditionAffectionMap.containsKey(template)) {
			affectionKindSet = new HashSet<TemplateAffection>();
			postConditionAffectionMap.put(template, affectionKindSet);
		} else {
			affectionKindSet = postConditionAffectionMap.get(template);
		}
		affectionKindSet.add(new TemplateAffection(affectionKind, template,
				change));
	}

	/**
	 * @return the operationSpecification
	 */
	public OperationSpecification getOperationSpecification() {
		return operationSpecification;
	}

	/**
	 * Returns the essential precondition {@link Template}s, i.e., the templates
	 * that are effectively changed by the composite operation.
	 * 
	 * @return the essential precondition {@link Template}s
	 */
	public Set<Template> getAffectedPreconditionTemplates() {
		return Collections.unmodifiableSet(this.preConditionAffectionMap
				.keySet());
	}

	/**
	 * Returns the essential postcondition {@link Template}s, i.e., the
	 * templates that are effectively changed by the composite operation.
	 * 
	 * @return the essential postcondition {@link Template}s
	 */
	public Set<Template> getAffectedPostconditionTemplates() {
		return Collections.unmodifiableSet(this.postConditionAffectionMap
				.keySet());
	}

	/**
	 * Returns a {@link Set} of {@link TemplateAffection} for the specified
	 * <code>template</code>.
	 * 
	 * @param template
	 *            the template.
	 * @return the {@link Set} of {@link TemplateAffection}s for the specified
	 *         <code>template</code>.
	 */
	public Set<TemplateAffection> getAffectingChanges(Template template) {
		if (this.preConditionAffectionMap.containsKey(template)) {
			return Collections.unmodifiableSet(this.preConditionAffectionMap
					.get(template));
		} else if (this.postConditionAffectionMap.containsKey(template)) {
			return Collections.unmodifiableSet(this.postConditionAffectionMap
					.get(template));
		} else {
			return Collections.emptySet();
		}
	}

	/**
	 * Returns a {@link Set} of {@link TemplateAffection} for the specified
	 * <code>template</code> that are of the specified <code>kind</code>.
	 * 
	 * @param template
	 *            the template.
	 * @param kind
	 *            requested template affection kind.
	 * @return the {@link Set} of {@link TemplateAffection}s for the specified
	 *         <code>template</code>.
	 */
	public Set<TemplateAffection> getAffectingChanges(Template template,
			AffectionKind kind) {
		Set<TemplateAffection> affectingChanges = new HashSet<TemplateAffection>();
		for (TemplateAffection affection : getAffectingChanges(template)) {
			if (kind.equals(affection.getAffectionKind())) {
				affectingChanges.add(affection);
			}
		}
		return affectingChanges;
	}

	/**
	 * Debug prints this signature to the specified <code>out</code> put stream.
	 * 
	 * @param out
	 *            output stream to print signature to.
	 */
	@Override
	public void debugPring(PrintStream out) {
		out.println(this.operationSpecification.getName() + ":"); //$NON-NLS-1$
		super.debugPring(out);
	}

}
