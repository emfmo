/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations;

import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.modelversioning.core.conditions.Template;

/**
 * Represents the affection of a {@link Template} by a {@link DiffElement}. For
 * each {@link DiffElement} the AffectionKind saves in which way the template is
 * affected.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class TemplateAffection {

	/**
	 * The affection kind specifies in which way a {@link DiffElement} affects a
	 * template. It specifies whether the template acted as <code>ELEMENT</code>
	 * directly changed by the {@link DiffElement}, <code>PARENT</code> of the
	 * element directly changed, or <code>TARGET</code> of a change.
	 * <code>OPPOSITE_TARGET</code> is the target in the opposite side which has
	 * a match to the current side.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	public enum AffectionKind {
		ELEMENT, PARENT, TARGET, OPPOSITE_TARGET
	}

	/**
	 * The affection kind.
	 */
	private AffectionKind affectionKind;
	/**
	 * The template.
	 */
	private Template template;
	/**
	 * The diff element affecting the template.
	 */
	private DiffElement diffElement;

	/**
	 * Empty default constructor.
	 */
	public TemplateAffection() {

	}

	/**
	 * Creates an instance providing all essential values.
	 * 
	 * @param affectionKind
	 *            {@link AffectionKind} of this template affection.
	 * @param template
	 *            template that is affected.
	 * @param diffElement
	 *            diff element affecting the template.
	 */
	public TemplateAffection(AffectionKind affectionKind, Template template,
			DiffElement diffElement) {
		super();
		this.affectionKind = affectionKind;
		this.template = template;
		this.diffElement = diffElement;
	}

	/**
	 * @return the affectionKind
	 */
	public AffectionKind getAffectionKind() {
		return affectionKind;
	}

	/**
	 * @param affectionKind
	 *            the AffectionKind to set
	 */
	public void setAffectionKind(AffectionKind affectionKind) {
		this.affectionKind = affectionKind;
	}

	/**
	 * @return the template
	 */
	public Template getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * @return the diffElement
	 */
	public DiffElement getDiffElement() {
		return diffElement;
	}

	/**
	 * @param diffElement
	 *            the DiffElement to set
	 */
	public void setDiffElement(DiffElement diffElement) {
		this.diffElement = diffElement;
	}

}
