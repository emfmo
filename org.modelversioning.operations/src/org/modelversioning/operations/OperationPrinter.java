package org.modelversioning.operations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;

public class OperationPrinter {
	private File file;
	private FileWriter fw;
	private OperationSpecification operation;

	public OperationPrinter(OperationSpecification operation, String path,
			String fileName) {
		this.operation = operation;

		try {
			file = new File(path + "\\" + fileName);
			fw = new FileWriter(file);

			writeHeader();
			writeln("----------------Differences----------------");
			writeDifferences(operation.getDifferenceModel().getDiff());
			writeln("----------------Preconditions----------------");
			writeConditions(operation.getPreconditions());
			writeln("----------------PostConditions");
			writeConditions(operation.getPostconditions());

			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeConditions(ConditionsModel model) throws IOException {
		writeTemplate(model.getRootTemplate());
	}

	private void writeTemplate(Template template) throws IOException {
		writeln("Symbol: " + template.getName());
		for (Condition c : template.getSpecifications()) {
			writeCondtion(c);
		}

		for (Template s : template.getSubTemplates()) {
			writeTemplate(s);
		}
	}

	private void writeCondtion(Condition c) throws IOException {
		if (c instanceof FeatureCondition) {
			FeatureCondition fc = (FeatureCondition) c;
			if (fc.isActive())
				writeln("(X) " + fc.getFeature().toString());
			else
				writeln("( ) " + fc.getFeature().getName());
				writeln(fc.getExpression());
		}
	}

	private void writeln(String text) throws IOException {
		fw.write(text + "\n");
	}

	private void writeHeader() throws IOException {

		writeln("Operation: " + operation.getName());
		writeln("Description: " + operation.getDescription());
		writeln("Modeling Language: " + operation.getModelingLanguage());
		writeln("Title Template: " + operation.getTitleTemplate());
		writeln("Version: " + operation.getVersion());

	}

	private void writeDifferences(EObject element) throws IOException {
		boolean set = false;

		// if (element instanceof AddModelElement) {
		//			
		// AddModelElement ame = (AddModelElement)element;
		// if (ame.getLeftParent() != null ) {
		// writeln(ame.getKind().toString() + " of " +
		// ame.getRightElement().eClass().getName() + " " +
		// ((ENamedElement)ame.getRightElement()).getName() + " to " +
		// ame.getLeftParent().eClass().getName() + " "
		// +((ENamedElement)ame.getLeftParent()).getName());
		// } else {
		// writeln(ame.getKind().toString() + " of " +
		// ame.getRightElement().eClass().getName() + " " +
		// ((ENamedElement)ame.getRightElement()).getName());
		// }
		// set = true;
		// }
		//		
		//		
		// if (element instanceof RemoveModelElement) {
		// RemoveModelElement rme = (RemoveModelElement)element;
		// if (rme.getRightParent() != null ) {
		// writeln(rme.getKind() + " of " +
		// rme.getLeftElement().eClass().getName() + " " +
		// ((ENamedElement)rme.getLeftElement()).getName() + " from " +
		// rme.getRightParent().eClass().getName() + " " +
		// ((ENamedElement)rme.getRightParent()).getName());
		// } else {
		// writeln(rme.getKind() + " of " +
		// rme.getLeftElement().eClass().getName() + " " +
		// ((ENamedElement)rme.getLeftElement()).getName());
		// }
		// set = true;
		// }
		//		
		//		
		// if (element instanceof DiffGroup) {
		// DiffGroup dg = (DiffGroup)element;
		//
		// if (dg.getRightParent() != null) {
		// writeln(dg.getSubchanges() + " " + dg.getKind().toString() + " in " +
		// dg.getRightParent().eClass().getName() + " " +
		// ((ENamedElement)dg.getRightParent()).getName());
		// }
		//			
		// if (dg.getSubchanges() != 0) {
		// writeln(dg.getSubchanges() + " " + dg.getKind().toString());
		// }
		//			
		//			
		// }

		if (element instanceof DiffModel) {

			DiffModel dm = (DiffModel) element;
			writeln(dm.getLeftRoots() + " " + dm.getRightRoots());
		}

		if (!set && (element instanceof DiffElement)) {
			writeln(element.toString());
		}

		if (element instanceof DiffElement) {
			for (DiffElement de : ((DiffElement) element).getSubDiffElements()) {
				writeDifferences(de);
			}
			return;
		}

		if (element instanceof DiffModel) {
			for (DiffElement de : ((DiffModel) element).getOwnedElements()) {
				writeDifferences(de);
			}
			return;
		}
	}

}
