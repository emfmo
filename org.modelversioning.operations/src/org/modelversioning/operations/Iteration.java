/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.modelversioning.operations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iteration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Represents an iteration over all occurrances of the referenced Symbol.
 * 
 * <p>
 * For each or for some (depends on iterationType) identified instance object of the referenced symbol the referenced DiffElements will be repeated. An iteration may have sub iterations. The symbols referenced by the sub iterations have to be sub symbols of the symbol which is referenced by this iteration.
 * </p>
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.modelversioning.operations.Iteration#getIterationType <em>Iteration Type</em>}</li>
 *   <li>{@link org.modelversioning.operations.Iteration#getDiffElements <em>Diff Elements</em>}</li>
 *   <li>{@link org.modelversioning.operations.Iteration#getSubIterations <em>Sub Iterations</em>}</li>
 *   <li>{@link org.modelversioning.operations.Iteration#getTemplate <em>Template</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.modelversioning.operations.OperationsPackage#getIteration()
 * @model
 * @generated
 */
public interface Iteration extends EObject {
	/**
	 * Returns the value of the '<em><b>Iteration Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.modelversioning.operations.IterationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iteration Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iteration Type</em>' attribute.
	 * @see org.modelversioning.operations.IterationType
	 * @see #setIterationType(IterationType)
	 * @see org.modelversioning.operations.OperationsPackage#getIteration_IterationType()
	 * @model
	 * @generated
	 */
	IterationType getIterationType();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.Iteration#getIterationType <em>Iteration Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iteration Type</em>' attribute.
	 * @see org.modelversioning.operations.IterationType
	 * @see #getIterationType()
	 * @generated
	 */
	void setIterationType(IterationType value);

	/**
	 * Returns the value of the '<em><b>Diff Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.compare.diff.metamodel.DiffElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diff Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diff Elements</em>' reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getIteration_DiffElements()
	 * @model required="true"
	 * @generated
	 */
	EList<DiffElement> getDiffElements();

	/**
	 * Returns the value of the '<em><b>Sub Iterations</b></em>' containment reference list.
	 * The list contents are of type {@link org.modelversioning.operations.Iteration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Iterations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Iterations</em>' containment reference list.
	 * @see org.modelversioning.operations.OperationsPackage#getIteration_SubIterations()
	 * @model containment="true"
	 * @generated
	 */
	EList<Iteration> getSubIterations();

	/**
	 * Returns the value of the '<em><b>Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Template</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Template</em>' reference.
	 * @see #setTemplate(Template)
	 * @see org.modelversioning.operations.OperationsPackage#getIteration_Template()
	 * @model required="true"
	 * @generated
	 */
	Template getTemplate();

	/**
	 * Sets the value of the '{@link org.modelversioning.operations.Iteration#getTemplate <em>Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Template</em>' reference.
	 * @see #getTemplate()
	 * @generated
	 */
	void setTemplate(Template value);

} // Iteration
