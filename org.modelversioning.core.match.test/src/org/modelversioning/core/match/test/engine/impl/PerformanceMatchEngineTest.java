/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.test.engine.impl;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.engine.impl.EMFCompareMatchEngine;
import org.modelversioning.core.match.engine.impl.UUIDMatchEngine;

/**
 * Tests the performance of {@link EMFCompareMatchEngine} versus the
 * {@link UUIDMatchEngine}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class PerformanceMatchEngineTest extends TestCase {

	private Resource origin = null;
	private Resource workingCopy = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		// load resources
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		URI fileURI = URI.createFileURI("models/performance-test_origin.ecore");
		origin = resourceSet.getResource(fileURI, true);
		URI fileURI1 = URI
				.createFileURI("models/performance-test_revised.ecore");
		workingCopy = resourceSet.getResource(fileURI1, true);
		super.setUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		this.origin.unload();
		this.workingCopy.unload();
		super.tearDown();
	}

	public void testPerformanceUUIDMatcher() {
		long duration = 0;
		int runs = 10;
		UUIDMatchEngine matchEngine = new UUIDMatchEngine();
		System.out.println("UUID Matcher");
		try {
			for (int i = 0; i < runs; i++) {
				long beforeCompare = System.currentTimeMillis();
				matchEngine.generateMatchModel(origin, workingCopy);
				long afterCompare = System.currentTimeMillis();
				long singleDuration = afterCompare - beforeCompare;
				System.out.println(singleDuration);
				duration += singleDuration;
			}
			System.out.println("Average Duration: " + duration / runs);
		} catch (MatchException e) {
			e.printStackTrace();
		}
	}
	
	public void testPerformanceEMFCompare() {
		long duration = 0;
		int runs = 10;
		EMFCompareMatchEngine matchEngine = new EMFCompareMatchEngine();
		System.out.println("EMF Compare");
		try {
			for (int i = 0; i < runs; i++) {
				long beforeCompare = System.currentTimeMillis();
				matchEngine.generateMatchModel(origin, workingCopy);
				long afterCompare = System.currentTimeMillis();
				long singleDuration = afterCompare - beforeCompare;
				System.out.println(singleDuration);
				duration += singleDuration;
			}
			System.out.println("Average Duration: " + duration / runs);
		} catch (MatchException e) {
			e.printStackTrace();
		}
	}

}
