/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.match.test.engine.impl;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.match.metamodel.Match2Elements;
import org.eclipse.emf.compare.match.metamodel.Match3Elements;
import org.eclipse.emf.compare.match.metamodel.MatchElement;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.engine.IMatchEngine;
import org.modelversioning.core.match.engine.impl.EMFCompareMatchEngine;
import org.modelversioning.core.match.engine.impl.UUIDMatchEngine;
import org.modelversioning.core.util.UUIDUtil;

/**
 * Tests the {@link UUIDMatchEngine}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UUIDMatchEngineTest extends TestCase {

	private Resource origin = null;
	private Resource workingCopy1 = null;
	private Resource workingCopy2 = null;

	private IMatchEngine matchEngine;
	private Resource origin_test1;
	private Resource revised_test1;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		// load resources
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		URI fileURI = URI.createFileURI("models/origin_ids.ecore");
		origin = resourceSet.getResource(fileURI, true);
		URI fileURI1 = URI.createFileURI("models/working_copy_1_ids.ecore");
		workingCopy1 = resourceSet.getResource(fileURI1, true);
		URI fileURI2 = URI.createFileURI("models/working_copy_2_ids.ecore");
		workingCopy2 = resourceSet.getResource(fileURI2, true);

		fileURI = URI.createFileURI("models/uuid_test1_origin.ecore");
		origin_test1 = resourceSet.getResource(fileURI, true);
		fileURI1 = URI.createFileURI("models/uuid_test1_revised.ecore");
		revised_test1 = resourceSet.getResource(fileURI1, true);

		matchEngine = new UUIDMatchEngine();
		super.setUp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	@Override
	protected void tearDown() throws Exception {
		this.origin.unload();
		this.workingCopy1.unload();
		this.workingCopy2.unload();
		this.origin_test1.unload();
		this.revised_test1.unload();
		super.tearDown();
	}

	/**
	 * Tests method
	 * {@link EMFCompareMatchEngine#generateMatchModel(Resource, Resource)}
	 */
	public void testGenerateMatchModelResourceResource() throws MatchException {
		assertNotNull(workingCopy1);
		assertNotNull(workingCopy2);

		// Testing if the names of the first two levels in the match models the
		// names of the matched elements are equal.
		MatchModel mmodel = matchEngine
				.generateMatchModel(origin, workingCopy1);
		boolean hadMovedToNewMatch = false;
		for (MatchElement melement : mmodel.getMatchedElements()) {
			if (melement instanceof Match2Elements) {
				Match2Elements m2elements = (Match2Elements) melement;
				if (m2elements.getLeftElement() instanceof ENamedElement) {
					ENamedElement modelElemLeft = (ENamedElement) m2elements
							.getLeftElement();
					ENamedElement modelElemRight = (ENamedElement) m2elements
							.getRightElement();
					assertEquals(modelElemLeft.getName(),
							modelElemRight.getName());
				}
			}
			if (melement.getSubMatchElements().size() > 0) {
				for (MatchElement melementSub : melement.getSubMatchElements()) {
					if (melementSub instanceof Match2Elements) {
						Match2Elements m2elements = (Match2Elements) melementSub;
						if (m2elements.getLeftElement() instanceof ENamedElement) {
							ENamedElement modelElemLeft = (ENamedElement) m2elements
									.getLeftElement();
							ENamedElement modelElemRight = (ENamedElement) m2elements
									.getRightElement();
							assertEquals(modelElemLeft.getName(),
									modelElemRight.getName());
							if (UUIDUtil.getUUID(modelElemLeft).equals(
									"_Li8VIhRqEd6Lv50hVsPUvg")) {
								hadMovedToNewMatch = true;
							}
						}
					}
				}
			}
		}
		assertTrue(hadMovedToNewMatch);
	}

	/**
	 * Tests method
	 * {@link EMFCompareMatchEngine#generateMatchModel(Resource, Resource, Resource)}
	 */
	public void testGenerateMatchModelResourceResourceResource()
			throws MatchException {
		assertNotNull(origin);
		assertNotNull(workingCopy1);
		assertNotNull(workingCopy2);

		// Testing if the names of the level in the match models the names of
		// the matched elements are equal.
		MatchModel mmodel = matchEngine.generateMatchModel(origin,
				workingCopy1, workingCopy2);
		for (MatchElement melement : mmodel.getMatchedElements()) {
			if (melement instanceof Match3Elements) {
				Match3Elements m3element = (Match3Elements) melement;
				ENamedElement modelElemLeft = (ENamedElement) m3element
						.getLeftElement();
				ENamedElement modelElemRight = (ENamedElement) m3element
						.getRightElement();
				ENamedElement modelElemOrigin = (ENamedElement) m3element
						.getOriginElement();
				assertEquals(modelElemLeft.getName(), modelElemRight.getName(),
						modelElemOrigin.getName());
			}
			if (melement.getSubMatchElements().size() > 0) {
				for (MatchElement melementSub : melement.getSubMatchElements()) {
					if (melementSub instanceof Match3Elements) {
						Match3Elements m3elements = (Match3Elements) melementSub;
						if (m3elements.getLeftElement() instanceof ENamedElement) {
							ENamedElement modelElemLeft = (ENamedElement) m3elements
									.getLeftElement();
							ENamedElement modelElemRight = (ENamedElement) m3elements
									.getRightElement();
							ENamedElement modelElementOrigin = (ENamedElement) m3elements
									.getOriginElement();
							assertEquals(modelElemLeft.getName(),
									modelElemRight.getName(),
									modelElementOrigin.getName());
						}
					}
				}
			}
		}
	}

	public void testGenerateMatchModelEObjectEObject() throws MatchException {
		EObject eObject1 = origin_test1.getContents().get(0);
		EObject eObject2 = revised_test1.getContents().get(0);
		MatchModel matchModel = matchEngine.generateMatchModel(eObject1,
				eObject2);
		assertNotNull(matchModel);
	}

}
