/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.operations.execution;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.NegativeApplicationConditionResult;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.UserInput;

/**
 * Binds {@link Template} of an {@link OperationSpecification} to a specific
 * modeling scenario.
 * 
 * TODO add NAC evaluations
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IOperationBinding {

	/**
	 * Indicating an invalid binding.
	 */
	int INVALID_BINDING_CODE = 1;
	/**
	 * Indicating, that the binding is not unique and has to be refined.
	 */
	int NOT_UNIQUE_CODE = 2;
	/**
	 * Indicating a missing user input value.
	 */
	int MISSING_USER_INPUT_VALUES_CODE = 3;
	/**
	 * Indicating multiple errors.
	 */
	int MULTIPLE_ERROR = 100;
	/**
	 * Indicating invalid negative application condition.
	 */
	int INVALID_NAC = 4;

	/**
	 * @return the operationSpecification
	 */
	public abstract OperationSpecification getOperationSpecification();

	/**
	 * @return the workingModelRoot
	 */
	public abstract EObject getWorkingModelRoot();

	/**
	 * @return the actual template binding
	 */
	public abstract ITemplateBindings getTemplateBinding();

	/**
	 * Returns all results from the evaluation of the negative application
	 * conditions.
	 * 
	 * @return all results from the evaluation of the negative application
	 *         conditions.
	 */
	public abstract Collection<NegativeApplicationConditionResult> getNegativeApplicationConditionResults();

	/**
	 * Returns the result from the specified
	 * {@link NegativeApplicationCondition}.
	 * 
	 * @param nac
	 *            the {@link NegativeApplicationCondition} to get result for.
	 * @return the result.
	 */
	public abstract NegativeApplicationConditionResult getNegativeApplicationConditionResult(
			NegativeApplicationCondition nac);

	/**
	 * Sets <code>value</code> as value for the specified <code>userInput</code>
	 * .
	 * 
	 * @param userInput
	 *            {@link UserInput} to set value for.
	 * @param value
	 *            value for the <code>userInput</code>.
	 */
	public abstract void setUserInputValue(UserInput userInput, Object value);

	/**
	 * Returns all currently set values for the {@link UserInput}s.
	 * 
	 * @return currently set user input values.
	 */
	public abstract Map<UserInput, Object> getUserInputValues();

	/**
	 * Returns a {@link Set} of all {@link UserInput} for which a value is still
	 * missing.
	 * 
	 * @return {@link UserInput}s for which values are missing.
	 */
	public abstract Set<UserInput> getMissingUserInputs();

	/**
	 * Returns <code>true</code> if an execution of this operation binding is
	 * unique, i.e., for each active and not iterated Template there is only one
	 * single object bound to it.
	 * 
	 * @return <code>true</code> if unique, <code>false</code> otherwise.
	 */
	public abstract boolean isUnique();

	/**
	 * Validates this binding for the use of the OperationSpecification (
	 * {@link #getOperationSpecification()}).
	 * 
	 * @return validation result.
	 */
	public abstract IStatus validate();

}