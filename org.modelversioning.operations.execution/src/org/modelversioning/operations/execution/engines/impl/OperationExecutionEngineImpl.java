/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.FactoryException;
import org.eclipse.emf.compare.diff.merge.service.MergeService;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.util.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.diff.propagation.IDiffPropagationEngine;
import org.modelversioning.core.diff.propagation.IPropagationMappingProvider;
import org.modelversioning.core.diff.propagation.impl.DiffPropagationEngine;
import org.modelversioning.core.match.util.MatchUtil;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.OperationsExecutionPlugin;
import org.modelversioning.operations.execution.engines.IOperationExecutionEngine;
import org.modelversioning.operations.util.OperationsUtil;

/**
 * Implements the {@link IOperationExecutionEngine} using the
 * {@link MergeService}.
 * 
 * This class executes {@link IOperationBinding}s by re-wiring the contained
 * {@link DiffModel} to the elements of the model on which the composite
 * operation has to be applied. The re-wired {@link DiffModel} is then handed on
 * to the {@link MergeService} which actually performs the changes.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationExecutionEngineImpl implements IOperationExecutionEngine,
		IPropagationMappingProvider {

	/**
	 * Error message if we could not assign user input value.
	 */
	private static final String USER_INPUT_ERROR = "Error assigning user input value";

	/**
	 * Error message if we could not assign a value.
	 */
	private static final String CANNOT_ASSING_VALUE = "Cannot assign value";

	/**
	 * A map of representatives to templates.
	 */
	private Map<EObject, Template> representativeToTemplateMap = new HashMap<EObject, Template>();

	/**
	 * A map of diff elements (parents) to diff elements which has to be added
	 * to the parent diff element.
	 */
	private Set<DiffElement> diffElementsToAdd = new HashSet<DiffElement>();

	/**
	 * The operation binding to execute;
	 */
	private IOperationBinding operationBinding = null;

	/**
	 * The propagation engine.
	 */
	private IDiffPropagationEngine propagationEngine;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EObject execute(IOperationBinding operationBinding) {
		// initialize execution engine
		initialize(operationBinding);

		propagationEngine = new DiffPropagationEngine();
		propagationEngine.propagate(operationBinding
				.getOperationSpecification().getDifferenceModel().getDiff(),
				operationBinding.getOperationSpecification()
						.getDifferenceModel().getMatch(), this,
				new NullProgressMonitor());

		// assign user input values
		assignUserInputs();

		// assign implicit values
		assignImplicitValues();

		return operationBinding.getWorkingModelRoot();
	}

	/**
	 * Initializes this execution engine.
	 */
	private void initialize(IOperationBinding operationBinding) {
		this.operationBinding = operationBinding;
		representativeToTemplateMap.clear();
		fillRepresentativeToTemplateMap(operationBinding
				.getOperationSpecification().getPreconditions());
		diffElementsToAdd.clear();
	}

	/**
	 * Returns <code>true</code> if the specified <code>object</code> is
	 * contained by the origin model.
	 * 
	 * @param object
	 *            object to check.
	 * @return <code>true</code> if contained by the origin model.
	 *         <code>false</code> otherwise.
	 */
	@Override
	public boolean isInOriginModel(EObject object) {
		EObject originRoot = this.operationBinding.getOperationSpecification()
				.getOriginModelRoot();
		if (org.modelversioning.core.util.EcoreUtil.createParentList(object)
				.contains(originRoot)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fills representative to template map of this instance with the templates
	 * of the specified <code>conditionsModel</code>.
	 * 
	 * @param conditionsModel
	 *            conditions model containing the templates ot fill the map
	 *            with.
	 */
	private void fillRepresentativeToTemplateMap(ConditionsModel conditionsModel) {
		representativeToTemplateMap = ConditionsUtil
				.getRepresentativeToTemplateMap(conditionsModel);
	}

	/**
	 * Returns the template in the postconditions with the specified
	 * <code>representative</code>.
	 * 
	 * @param representative
	 *            representative of the template in question.
	 * @return the template which has the specified <code>representative</code>.
	 */
	private Template getPostTemplateByRepresentative(EObject representative) {
		return ConditionsUtil.getTemplateByRepresentative(representative,
				this.operationBinding.getOperationSpecification()
						.getPostconditions());
	}

	/**
	 * Returns the template with the specified <code>representative</code>.
	 * 
	 * @param representative
	 *            representative of the template in question.
	 * @return the template which has the specified <code>representative</code>.
	 */
	private Template getTemplateByRepresentative(EObject representative) {
		return representativeToTemplateMap.get(representative);
	}

	/**
	 * Returns the counterpart objects contained by the model to change of the
	 * specified <code>eObject</code> contained by the by the example origin
	 * model. The counterpart objects are the objects bound to the template
	 * which is represented by <code>eObject</code>.
	 * 
	 * @param eObject
	 *            object contained by the example origin model of which the
	 *            counterpart is requested.
	 * @return the set of counterpart objects.
	 */
	@Override
	public Set<EObject> getCounterpartObjects(EObject eObject) {
		Template template = getTemplateByRepresentative(eObject);
		Set<EObject> boundObjects = operationBinding.getTemplateBinding()
				.getBoundObjects(template);
		if (boundObjects == null) {
			boundObjects = Collections.emptySet();
		}
		return boundObjects;
	}

	/**
	 * Returns the counterpart object contained by the model to change of the
	 * specified <code>eObject</code> contained by the by the example origin
	 * model. The counterpart object is the objects bound to the template which
	 * is represented by <code>eObject</code>.
	 * 
	 * If there are more than one objects bound to the template, this method
	 * should use <code>diffElement</code> and <code>context</code> to decide
	 * which object to return. This is currently not yet implemented.
	 * 
	 * @param eObject
	 *            object contained by the example origin model of which the
	 *            counterpart is requested.
	 * @param diffElement
	 *            context diff element.
	 * @param context
	 *            context object.
	 * @return the counterpart object.
	 */
	@Override
	public EObject getCounterpartObject(EObject eObject,
			DiffElement diffElement, EObject context) {
		Set<EObject> counterpartObjects = getCounterpartObjects(eObject);
		// if (counterpartObjects.size() > 1) {
		// TODO use context and diff element to find out which one to choose
		// }
		if (counterpartObjects.size() > 0) {
			return counterpartObjects.iterator().next();
		} else {
			return null;
		}
	}

	/**
	 * Assigns the specified values for the {@link UserInput}s of the
	 * {@link #operationBinding}.
	 */
	private void assignUserInputs() {
		for (UserInput userInput : this.operationBinding.getUserInputValues()
				.keySet()) {
			try {
				EObject match = null;
				if ((match = getMatchingObject(userInput.getTemplate()
						.getRepresentative())) != null) {
					// match in origin model
					// so get template of matching object and assign value
					// to bound objects
					Set<EObject> objects = this.operationBinding
							.getTemplateBinding().getBoundObjects(
									getTemplateByRepresentative(match));
					for (EObject eObject : objects) {
						EFactory.eSet(
								eObject,
								userInput.getFeature().getName(),
								this.operationBinding.getUserInputValues().get(
										userInput));
					}
				} else if (propagationEngine.getCopies(
						userInput.getTemplate().getRepresentative()).size() > 0) {
					// no match in origin model
					// assign value to copies
					EList<EObject> copies = propagationEngine
							.getCopies(userInput.getTemplate()
									.getRepresentative());
					for (EObject copy : copies) {
						EFactory.eSet(
								copy,
								userInput.getFeature().getName(),
								this.operationBinding.getUserInputValues().get(
										userInput));
					}
				}
			} catch (FactoryException e) {
				OperationsExecutionPlugin
						.getDefault()
						.getLog()
						.log(new Status(IStatus.WARNING,
								OperationsExecutionPlugin.PLUGIN_ID,
								USER_INPUT_ERROR, e));
			}
		}
	}

	/**
	 * Assigns the values implicitly specified using the postconditions to the
	 * changed model.
	 */
	private void assignImplicitValues() {
		try {
			IConditionEvaluationEngine postEvaluationEngine = new ConditionsEvaluationEngineImpl();
			postEvaluationEngine.setConditionsModel(this.operationBinding
					.getOperationSpecification().getPostconditions());

			// set initial model binding as related template binding
			postEvaluationEngine.registerRelatedTemplateBinding(
					OperationsUtil.INITIAL_PREFIX,
					operationBinding.getTemplateBinding());

			// derive candidate map of execution using the mapping
			ITemplateBinding executedBinding = deriveExecutedBinding();

			// iterate through all templates and its conditions
			for (Template postTemplate : executedBinding.getTemplates()) {
				for (EObject boundObject : executedBinding
						.getBoundObjects(postTemplate)) {
					// check each condition with each bound object
					for (Condition condition : postTemplate.getSpecifications()) {
						// if condition fail try to set suitable value
						if (condition instanceof FeatureCondition
								&& condition.isActive()
								&& !postEvaluationEngine.evaluate(condition,
										boundObject, executedBinding).isOK()) {
							Object value = null;
							try {
								value = postEvaluationEngine.getValidValue(
										(FeatureCondition) condition,
										boundObject, executedBinding);
								if (value != null) {
									// set value to the feature of feature
									// condition in boundObject
									boundObject.eSet(
											((FeatureCondition) condition)
													.getFeature(), value);
								}
							} catch (Exception e) {
								OperationsExecutionPlugin
										.getDefault()
										.getLog()
										.log(new Status(
												IStatus.WARNING,
												OperationsExecutionPlugin.PLUGIN_ID,
												CANNOT_ASSING_VALUE, e));
							}
						}
					}
				}
			}
		} catch (UnsupportedConditionLanguage e) {
			e.printStackTrace();
		}
	}

	/**
	 * Derives the post condition template to object binding for the changed
	 * model.
	 * 
	 * TODO performance of this method might be increased (has to check the
	 * whole working model)
	 * 
	 * @return the post condition template to object binding for the changed
	 *         model.
	 */
	private ITemplateBinding deriveExecutedBinding() {
		ITemplateBinding executedBinding = new TemplateBindingImpl();
		// bind root
		Template postRootTemplate = this.operationBinding
				.getOperationSpecification().getPostconditions()
				.getRootTemplate();
		executedBinding.add(postRootTemplate,
				this.operationBinding.getWorkingModelRoot());

		// bind children
		TreeIterator<EObject> iterator = this.operationBinding
				.getWorkingModelRoot().eAllContents();
		while (iterator.hasNext()) {
			EObject currentObject = iterator.next();
			Template postTemplate = null;

			// check if current object is a copy
			EObject original = propagationEngine
					.getOriginalEObjectFromCopy(currentObject);
			if (original != null) {
				postTemplate = getPostTemplateByRepresentative(original);
			} else {
				// try to derive using the match
				Template preTemplate = getBoundTemplate(currentObject);
				if (preTemplate != null) {
					// we got a precondition template, so we can find
					// postcondition template browsing to representative, then
					// finding its match in match model which finally is the
					// representative of the
					// postcondition template
					EObject postRepresentative = getMatchingObject(preTemplate
							.getRepresentative());
					postTemplate = getPostTemplateByRepresentative(postRepresentative);
				}
			}

			if (postTemplate != null) {
				executedBinding.add(postTemplate, currentObject);
			}
		}
		return executedBinding;
	}

	/**
	 * Returns the first {@link Template} bound to the specified
	 * <code>eObject</code> in the current operation binding.
	 * 
	 * @param eObject
	 *            to find template to which it is bound.
	 * @return bound {@link Template} or <code>null</code>.
	 */
	private Template getBoundTemplate(EObject eObject) {
		Set<Template> templates = getBoundTemplates(eObject);
		if (templates.size() < 1) {
			return null;
		} else {
			return templates.iterator().next();
		}
	}

	/**
	 * Returns the {@link Template}s bound to the specified <code>eObject</code>
	 * in the current operation binding.
	 * 
	 * @param eObject
	 *            to find template to which it is bound.
	 * @return bound {@link Template}s or an empty {@link Set}.
	 */
	private Set<Template> getBoundTemplates(EObject eObject) {
		return this.operationBinding.getTemplateBinding().getBoundTemplates(
				eObject);
	}

	/**
	 * Returns the matching object in the match model of the specified
	 * <code>eObject</code>. If there is no matching object this method returns
	 * <code>null</code>.
	 * 
	 * @param eObject
	 *            to find match of.
	 * @return the matching object or <code>null</code> if there is none.
	 */
	private EObject getMatchingObject(EObject eObject) {
		return MatchUtil.getMatchingObject(eObject, this.operationBinding
				.getOperationSpecification().getDifferenceModel().getMatch());
	}

}
