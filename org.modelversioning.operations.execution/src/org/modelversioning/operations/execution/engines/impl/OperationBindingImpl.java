/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.BindingException;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.NegativeApplicationConditionResult;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.OperationsExecutionPlugin;
import org.modelversioning.operations.util.OperationsUtil;

/**
 * Binds {@link Template} of an {@link OperationSpecification} to a specific
 * modeling scenario.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationBindingImpl implements IOperationBinding {

	private static final String OK = "Binding is valid";

	private static final String INVALID_BINDING = "Binding is not valid";

	private static final String FIRED_NAC = "Negative Application Condition is true";

	private static final String NOT_UNIQUE = "Binding is not unique";

	/**
	 * The operation specification.
	 */
	private OperationSpecification operationSpecification;

	/**
	 * The actual binding.
	 */
	private ITemplateBindings binding;

	/**
	 * The {@link NegativeApplicationCondition} result map.
	 */
	private Map<NegativeApplicationCondition, NegativeApplicationConditionResult> nacResults = new HashMap<NegativeApplicationCondition, NegativeApplicationConditionResult>();

	/**
	 * The currently set values for the user inputs of the operation
	 * specification.
	 */
	private Map<UserInput, Object> userInputValues = new HashMap<UserInput, Object>();

	/**
	 * Instantiates an empty operation binding.
	 */
	protected OperationBindingImpl() {
		super();
	}

	/**
	 * Instantiates an operation binding specifying the operation specification.
	 * 
	 * @param operationSpecification
	 *            the operation specification.
	 */
	protected OperationBindingImpl(OperationSpecification operationSpecification) {
		super();
		this.operationSpecification = operationSpecification;
	}

	/**
	 * Instantiates an operation binding specifying the operation specification
	 * as well as the template binding.
	 * 
	 * @param operationSpecification
	 *            operation specification of this operation binding.
	 * @param binding
	 *            {@link ITemplateBindings} of this operation binding.
	 */
	protected OperationBindingImpl(
			OperationSpecification operationSpecification,
			ITemplateBindings binding) {
		super();
		this.operationSpecification = operationSpecification;
		this.binding = binding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OperationSpecification getOperationSpecification() {
		return operationSpecification;
	}

	/**
	 * Sets the operation specification.
	 * 
	 * @param operationSpecification
	 *            to set.
	 */
	protected void setOperationSpecification(
			OperationSpecification operationSpecification) {
		this.operationSpecification = operationSpecification;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EObject getWorkingModelRoot() {
		// guard unset operation specification
		if (this.operationSpecification == null
				|| this.operationSpecification.getPreconditions() == null) {
			return null;
		}
		// guard unset binding
		if (this.binding == null) {
			return null;
		}
		// get bound object for root template
		Set<EObject> boundRootObjects = this.binding
				.getBoundObjects(this.operationSpecification.getPreconditions()
						.getRootTemplate());
		if (boundRootObjects.size() > 0) {
			return boundRootObjects.iterator().next();
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITemplateBindings getTemplateBinding() {
		return binding;
	}

	/**
	 * Sets the template binding.
	 * 
	 * @param templateBinding
	 *            template binding to set.
	 */
	protected void setTemplateBinding(ITemplateBindings templateBinding) {
		this.binding = templateBinding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IStatus validate() {
		Status status = new Status(IStatus.OK,
				OperationsExecutionPlugin.PLUGIN_ID, OK);

		// check if template binding is valid
		if (this.binding == null || !this.binding.validate().isOK()) {
			status = new Status(IStatus.ERROR,
					OperationsExecutionPlugin.PLUGIN_ID,
					IOperationBinding.INVALID_BINDING_CODE, INVALID_BINDING,
					new BindingException(INVALID_BINDING, null));
		}

		// check for valid NACs
		for (NegativeApplicationConditionResult result : this.nacResults
				.values()) {
			if (!result.isOK()) {
				status = new Status(IStatus.ERROR,
						OperationsExecutionPlugin.PLUGIN_ID,
						IOperationBinding.INVALID_NAC, result.getMessage(),
						new BindingException(FIRED_NAC, null));
				continue;
			}
		}

		// check if template binding corresponds to iterations of operation
		// specification, i.e., is unique
		if (!this.isUnique()) {
			status = new Status(IStatus.WARNING,
					OperationsExecutionPlugin.PLUGIN_ID,
					IOperationBinding.NOT_UNIQUE_CODE, NOT_UNIQUE,
					new BindingException(NOT_UNIQUE, null));
		}

		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUnique() {
		return OperationsUtil.isUnique(this.binding,
				this.operationSpecification);
	}

	/**
	 * Returns <code>true</code> if the bound <code>objects</code> are unique
	 * for the specified <code>template</code>.
	 * 
	 * @param template
	 *            {@link Template} to check.
	 * @param objects
	 *            Objects to check for.
	 * @return <code>true</code> if unique. <code>false</code> otherwise.
	 */
	public boolean isUnique(Template template, Set<EObject> objects) {
		return OperationsUtil.isUnique(template, objects,
				this.operationSpecification);
	}

	/**
	 * Returns <code>true</code> if there may be more than one object bound to
	 * the specified <code>template</code>.
	 * 
	 * @param template
	 *            {@link Template} to check.
	 * @return <code>true</code> if there may be more than one object.
	 *         <code>false</code> otherwise.
	 */
	public boolean allowsMultipleBinding(Template template) {
		return OperationsUtil.allowsMultipleBinding(template,
				this.operationSpecification);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<UserInput, Object> getUserInputValues() {
		return this.userInputValues;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<UserInput> getMissingUserInputs() {
		Set<UserInput> missingInputs = new HashSet<UserInput>();
		missingInputs.addAll(this.operationSpecification.getUserInputs());
		missingInputs.removeAll(this.userInputValues.keySet());
		return missingInputs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUserInputValue(UserInput userInput, Object value) {
		this.userInputValues.put(userInput, value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<NegativeApplicationConditionResult> getNegativeApplicationConditionResults() {
		return Collections.unmodifiableCollection(this.nacResults.values());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NegativeApplicationConditionResult getNegativeApplicationConditionResult(
			NegativeApplicationCondition nac) {
		return this.nacResults.get(nac);
	}

	/**
	 * Adds a {@link NegativeApplicationConditionResult} for the specified
	 * {@link NegativeApplicationConditionResult}.
	 * 
	 * @param nac
	 *            {@link NegativeApplicationCondition}.
	 * @param nacResult
	 *            {@link NegativeApplicationConditionResult} to set.
	 */
	public void addNegativeApplicationConditionResult(
			NegativeApplicationCondition nac,
			NegativeApplicationConditionResult nacResult) {
		this.nacResults.put(nac, nacResult);
	}

}
