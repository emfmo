/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.IOperationBinding;

/**
 * Executes {@link OperationSpecification}s on models on the basis of an
 * {@link IOperationBinding}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IOperationExecutionEngine {

	/**
	 * Executes the {@link OperationSpecification} linked in the
	 * <code>operationBinding</code> on the linked model.
	 * 
	 * @param operationBinding
	 *            binding of operation to model.
	 * @return modified model's root object.
	 */
	public EObject execute(IOperationBinding operationBinding);

}
