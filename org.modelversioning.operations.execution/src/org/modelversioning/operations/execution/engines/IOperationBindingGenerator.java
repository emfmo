/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.engines;

import org.eclipse.emf.ecore.EObject;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.IOperationBinding;

/**
 * Generates {@link IOperationBinding}s for {@link OperationSpecification}s and
 * arbitrary models.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IOperationBindingGenerator {

	/**
	 * Returns currently used evaluation engine.
	 * 
	 * @return currently used evaluation engine.
	 */
	public IConditionEvaluationEngine getEvaluationEngine();

	/**
	 * Sets evaluation engine to use.
	 * 
	 * @param evaluationEngine
	 *            evaluation engine to use.
	 */
	public void setEvaluationEngine(IConditionEvaluationEngine evaluationEngine);

	/**
	 * Generates a {@link IOperationBinding} for the specified
	 * <code>operationSpecification</code> and the specified
	 * <code>modelRoot</code>.
	 * 
	 * @param operationSpecification
	 *            specification of the operation to be applied on the
	 *            <code>modelRoot</code>.
	 * @param modelRoot
	 *            root object of the model on which the specified
	 *            <code>operationSpecification</code> shall be applied.
	 * @return generated operation binding.
	 * @throws UnsupportedConditionLanguage
	 *             if no evaluation engine for the condition language of
	 *             specified <code>operationSpecification</code> is available.
	 */
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification, EObject modelRoot)
			throws UnsupportedConditionLanguage;

	/**
	 * Generates an {@link IOperationBinding} using the specified
	 * {@link ITemplateBindings} for the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            specification of the operation to be applied.
	 * @param templateBindings
	 *            to build {@link IOperationBinding} for.
	 * @return generated operation binding.
	 * @throws UnsupportedConditionLanguage
	 *             if no evaluation engine for the condition language of
	 *             specified <code>operationSpecification</code> is available.
	 */
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification,
			ITemplateBindings templateBindings)
			throws UnsupportedConditionLanguage;

	/**
	 * Generates an {@link IOperationBinding} for the specified
	 * <code>operationSpecification</code> and the specified
	 * <code>preBinding</code> in which one or more templates are manually
	 * binded in advance.
	 * 
	 * @param operationSpecification
	 *            specification of the operation to be applied on the model
	 *            elements in the specified <code>preBinding</code>.
	 * @param preBinding
	 *            pre-binding binds model elements to templates.
	 * @return generated operation binding.
	 * @throws UnsupportedConditionLanguage
	 *             if no evaluation engine for the condition language of
	 *             specified <code>operationSpecification</code> is available.
	 */
	public IOperationBinding generateOperationBinding(
			OperationSpecification operationSpecification,
			ITemplateBinding preBinding) throws UnsupportedConditionLanguage;

}
