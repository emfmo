/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.editors;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.progress.UIJob;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;
import org.modelversioning.core.util.IDCopier;
import org.modelversioning.core.util.UUIDUtil;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.SpecificationState;
import org.modelversioning.operations.engines.IOperationGenerationEngine;
import org.modelversioning.operations.engines.impl.OperationGenerationEngineImpl;
import org.modelversioning.operations.repository.ModelOperationRepositoryPlugin;
import org.modelversioning.operations.ui.OperationsUIPlugin;
import org.modelversioning.operations.ui.pages.GeneralConfigPage;
import org.modelversioning.operations.ui.pages.NegativeApplicationConditionsPage;
import org.modelversioning.operations.ui.pages.OperationConfigurationPage;
import org.modelversioning.operations.ui.views.ConditionView;
import org.modelversioning.operations.util.OperationsUtil;
import org.modelversioning.ui.commons.CommonUIUtil;

/**
 * Editor for creating and editing {@link OperationSpecification}s.
 * 
 * TODO enable re-demonstration
 * 
 * @author Martina Seidl
 * @author Felix Rinker
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class OperationRecorderEditor extends FormEditor implements
		ChangeListener {

	private static final String NAC_PREFIX = "nac_"; //$NON-NLS-1$
	private static final String CONFIGURATION_PAGE_ID = "configuration"; //$NON-NLS-1$
	private static final String NAC_PAGE_ID = "nac"; //$NON-NLS-1$
	private static final String POSTCONDITION_VIEW_ID = "working"; //$NON-NLS-1$
	private static final String PRECONDITION_VIEW_ID = "origin"; //$NON-NLS-1$
	private static final String NAC_CONDITION_VIEW_ID = "nac"; //$NON-NLS-1$
	private static final String DIR_SEP = "/"; //$NON-NLS-1$
	private static final String DOT = "."; //$NON-NLS-1$
	protected static final String START_REVISION_TASK = "Starting revision";
	protected static final String COMMIT_TASK = "Committing revision";

	/**
	 * The resource set for the specification persistence to use.
	 */
	private final ResourceSet resourceSet = new ResourceSetImpl();
	/**
	 * The operation specification to edit.
	 */
	private OperationSpecification operationSpecification;
	/**
	 * The file to save operation specification to.
	 */
	private IFile operationSpecificationFile;
	/**
	 * The resource containing the operation specification.
	 */
	private Resource operationSpecificationResource;
	/**
	 * Specifies whether this editor is dirty (needs save).
	 */
	private boolean dirty = false;
	/**
	 * Precondition view.
	 */
	private ConditionView preconditionView;
	/**
	 * Postcondition view.
	 */
	private ConditionView postconditionView;
	/**
	 * Condition view for current NAC.
	 */
	private ConditionView nacConditionView;
	/**
	 * Saves the names of the active associated views.
	 */
	private final Collection<String> activeViewNames = new ArrayList<String>();
	/**
	 * The first page of this editor.
	 */
	private GeneralConfigPage generalConfigPage;
	/**
	 * The second page of this editor.
	 */
	private OperationConfigurationPage operationDefPage;
	/**
	 * The second page of this editor.
	 */
	private NegativeApplicationConditionsPage nacPage;
	/**
	 * The file handle for the diagram of the working copy.
	 */
	private IFile workingDiagramFile;
	/**
	 * Resource for the working diagram.
	 */
	private Resource workingDiagramResource;
	/**
	 * The file handle for the model of the working copy.
	 */
	private IFile workingModelFile;
	/**
	 * Working model resource.
	 */
	private Resource workingModelResource;
	/**
	 * The editor to edit the working copy.
	 */
	private IEditorPart workingEditor;
	/**
	 * Current editor for editing the NAC model.
	 */
	private IEditorPart currentNACEditor;
	/**
	 * Current NAC model file for demonstrating the current NAC.
	 */
	private IFile currentNACModelFile;
	/**
	 * Current NAC diagram file for demonstrating the current NAC.
	 */
	private IFile currentNACDiagramFile;
	/**
	 * Current NAC model resource.
	 */
	private Resource currentNACModelResource;
	/**
	 * Current NAC diagram resource.
	 */
	private Resource currentNACDiagramResource;
	/**
	 * The operation generator to use.
	 */
	private IOperationGenerationEngine operationGenerationEngine = new OperationGenerationEngineImpl();
	/**
	 * The condition evaluation engine for preconditions to use.
	 */
	private IConditionEvaluationEngine preConditionEvaluationEngine = new ConditionsEvaluationEngineImpl();
	/**
	 * The condition evaluation engine for postconditions to use.
	 */
	private IConditionEvaluationEngine postConditionEvaluationEngine = new ConditionsEvaluationEngineImpl();
	/**
	 * The conditon evaluation engine for NAC condition view.
	 */
	private IConditionEvaluationEngine nacConditionEvaluationEngine = new ConditionsEvaluationEngineImpl();
	/**
	 * Specifies whether the user currently may commit a demonstration.
	 */
	private boolean mayCommit = false;
	/**
	 * The current NAC specification state.
	 */
	private NACSpecificationState nacState = NACSpecificationState.CONFIGURATION;

	/**
	 * The possible states of the {@link NegativeApplicationCondition}
	 * specification.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	public enum NACSpecificationState {
		DEMONSTRATION, CONFIGURATION
	}

	/**
	 * Constructs the OperationRecorderEditor
	 */
	public OperationRecorderEditor() {
		super();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl()); //$NON-NLS-1$
		resourceSet
				.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(OperationsUIPlugin.EXTENSION,
						new EcoreResourceFactoryImpl());
	}

	/**
	 * Returns the current {@link OperationSpecification}.
	 * 
	 * @return the operation specification.
	 */
	public OperationSpecification getOperationSpecification() {
		return operationSpecification;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Tries to load the Operation Specification from the provided
	 * <code>input</code>.
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {

		// guard wrong input
		if (!(input instanceof IFileEditorInput)) {
			throw new PartInitException(
					"Invalid Input: Must be IFileEditorInput");
		}

		// Cast the editor input to IFile and set it
		operationSpecificationFile = ((IFileEditorInput) input).getFile();
		// set title
		this.setPartName(((IFileEditorInput) input).getFile().getName());

		// try to load file
		operationSpecificationResource = resourceSet.getResource(URI
				.createFileURI(operationSpecificationFile.getLocation()
						.toString()), true);
		if (operationSpecificationResource != null
				&& operationSpecificationResource.getContents() != null
				&& operationSpecificationResource.getContents().size() > 0) {
			EObject eObject = operationSpecificationResource.getContents().get(
					0);
			if (eObject instanceof OperationSpecification) {
				operationSpecification = (OperationSpecification) eObject;
			}
		}

		// Must not be null!
		if (operationSpecification == null) {
			PartInitException e = new PartInitException(
					"Could not load Operation Specification");
			showError(e);
			throw e;
		}

		// set site and input
		this.setSite(site);
		this.setInput(input);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * Sets the dirty state.
	 * 
	 * @param dirty
	 *            <code>true</code> if dirty, <code>false</code> otherwise.
	 */
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		firePropertyChange(PROP_DIRTY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addPages() {
		try {
			// show first page
			this.createGeneralConfigPage();
			// show second page only if operation specification is in
			// configuration state
			if (SpecificationState.CONFIGURATION.equals(operationSpecification
					.getState())) {
				this.createOperationDefinitionPage();
				this.createNACPage();
			}
		} catch (PartInitException e) {
			showError(e);
		}
	}

	/**
	 * Creates a new general configuration page. And add them to the multi part
	 * editor.
	 * 
	 * @throws PartInitException
	 * 
	 * @see {@link GeneralConfigPage}
	 */
	private void createGeneralConfigPage() throws PartInitException {
		this.generalConfigPage = new GeneralConfigPage(this, "general",
				"General");
		int index = addPage(this.generalConfigPage);
		this.setActivePage(index);
		this.pageChange(index);
	}

	/**
	 * Create page for creating negative application conditions.
	 * 
	 * @throws PartInitException
	 */
	private void createNACPage() throws PartInitException {
		if (this.nacPage != null) {
			removePage(this.pages.indexOf(nacPage));
		}
		this.nacPage = new NegativeApplicationConditionsPage(this, NAC_PAGE_ID,
				"Negative Application Conditions");
		int index = addPage(this.nacPage);
		this.setPageText(index, "Negative Application Conditions");
		this.pageChange(index);
	}

	/**
	 * 
	 * Open condition views.
	 */
	public void openConditionViews() {
		this.openPreconditionView();
		this.openPostconditionsView();
	}

	/**
	 * Opens the origin condition view.
	 */
	public void openPreconditionView() {
		try {
			this.preconditionView = (ConditionView) PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView(
							"org.modelversioning.operations.ui.conditionview", //$NON-NLS-1$
							PRECONDITION_VIEW_ID,
							org.eclipse.ui.IWorkbenchPage.VIEW_VISIBLE);
			preConditionEvaluationEngine
					.setConditionsModel(operationSpecification
							.getPreconditions());
			this.preconditionView
					.setConditionEvaluationEngine(preConditionEvaluationEngine);
			this.preconditionView.show("Preconditions of "
					+ operationSpecification.getName(), OperationsUIPlugin
					.getImage(OperationsUIPlugin.IMG_PRECONDITIONS),
					operationSpecification.getPreconditions(), ConditionsUtil
							.getActiveConditionArray(operationSpecification
									.getPreconditions()));
			this.preconditionView.expandAll();
			this.preconditionView.addChangeListener(this);
			this.activeViewNames.add(this.preconditionView.getPartName());
		} catch (PartInitException e) {
			showError(e);
		} catch (UnsupportedConditionLanguage e) {
			showError(e);
		}
	}

	/**
	 * Opens the post condition view.
	 */
	public void openPostconditionsView() {
		try {

			this.postconditionView = (ConditionView) PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView(
							"org.modelversioning.operations.ui.conditionview", //$NON-NLS-1$
							POSTCONDITION_VIEW_ID,
							org.eclipse.ui.IWorkbenchPage.VIEW_VISIBLE);
			postConditionEvaluationEngine
					.setConditionsModel(operationSpecification
							.getPostconditions());
			postConditionEvaluationEngine
					.registerRelatedTemplateBinding(
							OperationsUtil.INITIAL_PREFIX,
							ConditionsUtil.createTemplateBindings(ConditionsUtil
									.getTemplateToRepresentativeMap(operationSpecification
											.getPreconditions())));
			this.postconditionView
					.setConditionEvaluationEngine(postConditionEvaluationEngine);
			this.postconditionView.show("Postconditions of "
					+ operationSpecification.getName(), OperationsUIPlugin
					.getImage(OperationsUIPlugin.IMG_POSTCONDITIONS),
					operationSpecification.getPostconditions(), ConditionsUtil
							.getActiveConditionArray(operationSpecification
									.getPostconditions()));
			this.postconditionView.expandAll();
			this.postconditionView.addChangeListener(this);
			this.activeViewNames.add(this.postconditionView.getPartName());
		} catch (PartInitException e) {
			showError(e);
		} catch (UnsupportedConditionLanguage e) {
			showError(e);
		}
	}

	/**
	 * Displays an error dialog for the specified exception.
	 * 
	 * @param e
	 *            exception to display.
	 */
	public void showError(Exception e) {
		IStatus status = new Status(IStatus.ERROR,
				OperationsUIPlugin.PLUGIN_ID, e.getLocalizedMessage(), e);
		// log error
		OperationsUIPlugin.getDefault().getLog().log(status);
		// show error
		ErrorDialog
				.openError(getSite().getShell(),
						"Unhandled Exception Occurred",
						e.getLocalizedMessage(), status);
	}

	/**
	 * Displays an error dialog for the specified message.
	 * 
	 * @param msg
	 *            message to display.
	 */
	public void showError(String msg) {
		IStatus status = new Status(IStatus.ERROR,
				OperationsUIPlugin.PLUGIN_ID, msg);
		// log error
		OperationsUIPlugin.getDefault().getLog().log(status);
		// show error
		ErrorDialog.openError(getSite().getShell(), "Error occurred", msg,
				status);
	}

	/**
	 * Starts the revision creation to illustrate the operation.
	 */
	public void startRevisingOriginModel() {
		boolean doRevision = true;
		if (SpecificationState.CONFIGURATION.equals(operationSpecification
				.getState())) {
			// show question
			// doRevision = MessageDialog
			// .openQuestion(
			// getSite().getShell(),
			// "Confirm overwriting the revised model",
			// "In this operation, the revised "
			// +
			// "model has already been specified. Do you really want to overwrite "
			// + "the currently specified revised model and consequently all "
			// + "postconditions?");
			MessageDialog.openInformation(getSite().getShell(),
					"Not yet supported",
					"Re-revision of the working copy is not implemented yet.");
			return;
		}
		if (doRevision) {
			try {
				OperationsUIPlugin.getDefault().getWorkbench()
						.getProgressService()
						.run(false, false, new IRunnableWithProgress() {
							@Override
							public void run(IProgressMonitor monitor)
									throws InvocationTargetException,
									InterruptedException {
								monitor.beginTask(START_REVISION_TASK, 2);
								operationSpecification
										.setState(SpecificationState.INITIAL);
								operationSpecification
										.setWorkingModelRoot(null);
								operationSpecification.getWorkingDiagram()
										.clear();
								// clear user inputs and iterations
								operationSpecification.getIterations().clear();
								operationSpecification.getUserInputs().clear();
								// create working copy
								createWorkingCopy(monitor);
								monitor.worked(1);
								// open working copy
								openWorkingCopy(monitor);
								monitor.worked(1);
								mayCommit = true;
								monitor.done();
								setDirty(true);
							}
						});
			} catch (InvocationTargetException e) {
				showError(e);
			} catch (InterruptedException e) {
				showError(e);
			}
		}
	}

	/**
	 * Creates a working copy of the origin model (and diagram if applicable)
	 * and sets the created files to the respective fields.
	 * 
	 * @param monitor
	 *            to report progress to.
	 */
	private void createWorkingCopy(IProgressMonitor monitor) {
		Assert.isNotNull(operationSpecification.getOriginModelRoot());

		// prepare for uuid persistence
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(operationSpecification.getModelFileExtension(), //$NON-NLS-1$
						new UUIDResourceFactoryImpl());

		// create copier and add UUIDs to origin model
		Copier copier = new Copier();
		addUUIDsToOrigin();

		// fetch origin model root and diagram root objects
		EObject originModelRoot = operationSpecification.getOriginModelRoot();
		EList<EObject> diagramRootObjects = operationSpecification
				.getOriginDiagram();

		// make a copy of originModelRoot and originDiagram (if not null)
		monitor.subTask("Copying model and optionally the diagram...");
		EObject copyModelRootObject = copier.copy(originModelRoot);
		Collection<EObject> copyDiagramRootObjects = null;
		boolean haveDiagram = false;
		if (diagramRootObjects != null && diagramRootObjects.size() > 0) {
			copyDiagramRootObjects = copier.copyAll(diagramRootObjects);
			haveDiagram = true;
		}
		copier.copyReferences();

		/*
		 * Create copies
		 */
		monitor.subTask("Saving model to temporary file...");
		try {
			// prepare general file name variables
			String uuid = EcoreUtil.generateUUID();
			String modelFileExtension = operationSpecification
					.getModelFileExtension();
			String diagramFileExtension = operationSpecification
					.getDiagramFileExtension();

			// prepare origin variables
			String workingFileName = "revision" + uuid; //$NON-NLS-1$
			IPath workingPath = getTempModelPath();

			// create working model file
			URI modelURI = URI.createPlatformResourceURI(workingPath.toString()
					+ DIR_SEP + workingFileName + DOT + modelFileExtension,
					true);
			workingModelResource = resourceSet.createResource(modelURI);
			workingModelResource.getContents().clear();
			workingModelResource.getContents().add(copyModelRootObject);
			UUIDUtil.copyUUIDs(originModelRoot, copier);
			workingModelResource.save(Collections.emptyMap());
			workingModelFile = CommonUIUtil.getIFile(workingModelResource);

			// create diagram file if available
			if (haveDiagram) {
				monitor.subTask("Saving diagram to temporary file...");
				URI diagramURI = URI.createPlatformResourceURI(
						workingPath.toString() + DIR_SEP + workingFileName
								+ DOT + diagramFileExtension, true);
				workingDiagramResource = resourceSet.createResource(diagramURI);
				workingDiagramResource.getContents().clear();
				workingDiagramResource.getContents().addAll(
						copyDiagramRootObjects);
				workingDiagramResource.save(Collections.emptyMap());
				workingDiagramFile = CommonUIUtil
						.getIFile(workingDiagramResource);
			} else {
				workingDiagramFile = null;
			}
		} catch (IOException e) {
			showError(e);
		}
	}

	/**
	 * Returns the path for the temporary models.
	 * 
	 * @return the path for the temporary models.
	 */
	private IPath getTempModelPath() {
		return this.operationSpecificationFile.getParent().getFullPath();
	}

	/**
	 * Opens the currently set working copy file in the preferred editor.
	 * 
	 * @param monitor
	 *            to report progress to.
	 */
	private void openWorkingCopy(IProgressMonitor monitor) {
		try {
			monitor.subTask("Opening working copy editor...");
			// Get current working page
			IWorkbenchPage page = PlatformUI.getWorkbench()
					.getActiveWorkbenchWindow().getActivePage();

			IFile fileToOpen = workingDiagramFile != null ? workingDiagramFile
					: workingModelFile;

			// open the net editor with the descriptor
			workingEditor = IDE.openEditor(page, fileToOpen,
					operationSpecification.getEditorId(), true);

		} catch (PartInitException e) {
			showError(e);
		}
	}

	/**
	 * Commits the currently created working copy
	 */
	public void commitWorkingCopy() {
		// guard wrong state for committing a new rule
		if (!SpecificationState.INITIAL.equals(operationSpecification
				.getState())) {
			showError("Cannot commit a revision when not in initial state.");
			return;
		}

		try {
			OperationsUIPlugin.getDefault().getWorkbench().getProgressService()
					.run(false, false, new IRunnableWithProgress() {
						@Override
						public void run(IProgressMonitor monitor)
								throws InvocationTargetException,
								InterruptedException {
							doCommitWorkingCopy(monitor);
						}
					});
		} catch (InvocationTargetException e) {
			showError(e);
		} catch (InterruptedException e) {
			showError(e);
		}
	}

	/**
	 * Finally executes the commit working copy action.
	 * 
	 * @param monitor
	 *            to report progress to.
	 */
	private void doCommitWorkingCopy(IProgressMonitor monitor) {
		monitor.beginTask(COMMIT_TASK, 5);
		monitor.subTask("Saving editor...");
		// save editor
		if (workingEditor != null) {
			workingEditor.doSave(monitor);
		}
		monitor.worked(1);

		try {
			// update resources
			monitor.subTask("Reading model...");
			workingModelResource.unload();
			workingModelResource.load(Collections.emptyMap());
			if (workingDiagramResource != null) {
				workingDiagramResource.unload();
				workingDiagramResource.load(Collections.emptyMap());
			}
			monitor.worked(1);

			// put revised model to operation specification
			monitor.subTask("Computing changes");
			MatchModel matchModel = new MatchService().generateMatchModel(
					operationSpecification.getOriginModelRoot(),
					workingModelResource.getContents().get(0));
			monitor.worked(1);

			monitor.subTask("Generating conditions...");
			getOperationGenerationEngine().putRevisedModel(matchModel,
					workingDiagramResource, operationSpecification);
			monitor.worked(1);

			// remove temp files
			monitor.subTask("Removing temporary files...");
			CommonUIUtil.deleteFile(workingModelFile, monitor);
			workingModelFile = null;
			if (workingDiagramFile != null) {
				CommonUIUtil.deleteFile(workingDiagramFile, monitor);
				workingDiagramFile = null;
			}
			monitor.worked(1);

			mayCommit = false;

			createOperationDefinitionPage();
			createNACPage();
			this.setActivePage(pages.indexOf(this.operationDefPage));

			// refresh the file container
			operationSpecificationFile.getParent().refreshLocal(1, monitor);

			// refresh view
			refresh();

			// set dirty
			setDirty(true);

		} catch (IOException e) {
			showError(e);
		} catch (MatchException e) {
			showError(e);
		} catch (PartInitException e) {
			showError(e);
		} catch (CoreException e) {
			showError(e);
		} finally {
			// reset everything and end revision
			workingDiagramFile = null;
			workingModelFile = null;
			if (workingDiagramResource != null) {
				workingDiagramResource.unload();
			}
			workingDiagramResource = null;
			workingModelResource.unload();
			workingModelResource = null;
			operationSpecification.setState(SpecificationState.CONFIGURATION);
			monitor.done();
		}
	}

	/**
	 * Refreshes this editor.
	 */
	private void refresh() {
		this.generalConfigPage.refresh();
		if (this.operationDefPage != null) {
			this.operationDefPage.refresh();
		}
	}

	/**
	 * Creates a new operation definition page. And add them to the multi part
	 * editor.
	 * 
	 * @throws PartInitException
	 * 
	 * @see {@link OperationConfigurationPage}
	 */
	private void createOperationDefinitionPage() throws PartInitException {
		if (this.operationDefPage != null) {
			removePage(this.pages.indexOf(operationDefPage));
		}
		this.operationDefPage = new OperationConfigurationPage(this,
				CONFIGURATION_PAGE_ID, "Configuration");
		int index = addPage(this.operationDefPage);
		this.setPageText(index, "Configuration");
		this.pageChange(index);
	}

	/**
	 * Specifies whether the user may start demonstration in the current state.
	 * 
	 * @return <code>true</code> if the user may, <code>false</code> if not.
	 */
	public boolean mayDemonstrate() {
		return SpecificationState.INITIAL.equals(operationSpecification
				.getState()) && !mayCommit();
	}

	/**
	 * Specifies whether the user may start re-demonstration in the current
	 * state.
	 * 
	 * @return <code>true</code> if the user may, <code>false</code> if not.
	 */
	public boolean mayReDemonstrate() {
		// return SpecificationState.CONFIGURATION.equals(operationSpecification
		// .getState());
		// Currently not implemented.
		return false;
	}

	/**
	 * Specifies whether the user may commit the current demonstration.
	 * 
	 * @return <code>true</code> if the user may, <code>false</code> if not.
	 */
	public boolean mayCommit() {
		return mayCommit;
	}

	/**
	 * Specifies whether the user may do some post processing now.
	 * 
	 * @return <code>true</code> if the user may, <code>false</code> if not.
	 */
	public boolean mayPostProcess() {
		return SpecificationState.CONFIGURATION.equals(operationSpecification
				.getState()) && !mayCommit();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		UIJob job = new UIJob("Closing related view") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				if (PlatformUI.getWorkbench().getActiveWorkbenchWindow() != null
						&& PlatformUI.getWorkbench().getActiveWorkbenchWindow()
								.getActivePage() != null) {
					IWorkbenchPage activePage = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();
					// Get the active views
					IViewReference[] viewRef = activePage.getViewReferences();
					// hide all active views
					for (IViewReference view : viewRef) {
						if (activeViewNames.contains(view.getPartName()))
							activePage.hideView(view);
					}
					CommonUIUtil.deleteFile(workingDiagramFile, monitor);
				}
				return new Status(IStatus.OK, OperationsUIPlugin.PLUGIN_ID, ""); //$NON-NLS-1$
			}
		};
		job.setUser(true);
		job.schedule();
		super.dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Saving transformation to file", 1);
			UUIDUtil.removeUUIDs(operationSpecification);
			operationSpecificationResource.save(Collections.emptyMap());
			setDirty(false);
			monitor.worked(1);
			monitor.done();
		} catch (IOException e) {
			OperationsUIPlugin
					.getDefault()
					.getLog()
					.log(new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							"Could not save to file", e));
			showError(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void doSaveAs() {
		SaveAsDialog saveAsDialog = new SaveAsDialog(getSite().getShell());
		saveAsDialog.setOriginalFile(operationSpecificationFile);
		saveAsDialog.open();
		IPath path = saveAsDialog.getResult();
		if (path != null) {
			IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
			if (file != null) {
				operationSpecificationFile = file;
				operationSpecificationResource = resourceSet.createResource(URI
						.createPlatformResourceURI(operationSpecificationFile
								.getFullPath().toString(), true));
				operationSpecificationResource.getContents().add(
						operationSpecification);
				try {
					operationSpecificationResource.save(Collections.emptyMap());
					this.setPartName(operationSpecificationFile.getName());
				} catch (IOException e) {
					showError(e);
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSaveAsAllowed() {
		if (mayCommit) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Notified when condition view is changed.
	 */
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof Template
				|| e.getSource() instanceof Condition) {
			this.setDirty(true);
		}
	}

	/**
	 * Registers the currently set {@link #operationSpecification}.
	 */
	public void registerOperationSpecification() {
		ModelOperationRepositoryPlugin.getDefault().getOperationRepository()
				.register(operationSpecification);
		UIJob job = new UIJob("Open Model Operations View") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					PlatformUI
							.getWorkbench()
							.getActiveWorkbenchWindow()
							.getActivePage()
							.showView(
									"org.modelversioning.operations.execution.ui"
											+ ".views.ModelOperationsView");
					return new Status(IStatus.OK, OperationsUIPlugin.PLUGIN_ID,
							""); //$NON-NLS-1$
				} catch (PartInitException e) {
					return new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							e.getLocalizedMessage(), e);
				}
			}
		};
		job.setUser(true);
		job.schedule();
	}

	/**
	 * Exports and opens the initial model of the currently set
	 * {@link #operationSpecification}.
	 */
	public void showInitialModel() {
		UIJob job = new UIJob("Export and open the initial model") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					exportAndOpenModelDiagram(monitor, true);
					return new Status(IStatus.OK, OperationsUIPlugin.PLUGIN_ID,
							""); //$NON-NLS-1$
				} catch (PartInitException e) {
					return new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							e.getLocalizedMessage(), e);
				} catch (IOException e) {
					return new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							e.getLocalizedMessage(), e);
				} finally {
					monitor.done();
				}
			}
		};
		job.setUser(true);
		job.schedule();
	}

	/**
	 * Exports and opens the revised model of the currently set
	 * {@link #operationSpecification}.
	 */
	public void showRevisedModel() {
		UIJob job = new UIJob("Export and open the revised model") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					exportAndOpenModelDiagram(monitor, false);
					return new Status(IStatus.OK, OperationsUIPlugin.PLUGIN_ID,
							""); //$NON-NLS-1$
				} catch (PartInitException e) {
					return new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							e.getLocalizedMessage(), e);
				} catch (IOException e) {
					return new Status(IStatus.ERROR,
							OperationsUIPlugin.PLUGIN_ID,
							e.getLocalizedMessage(), e);
				} finally {
					monitor.done();
				}
			}
		};
		job.setUser(true);
		job.schedule();
	}

	/**
	 * Exports and opens the model and, if available the diagram.
	 * 
	 * @param monitor
	 *            to report progress to.
	 * @param initial
	 *            specifies whether to export and open the inital or the revised
	 *            model.
	 * @throws IOException
	 *             if writing the file fails.
	 * @throws PartInitException
	 *             if opening the editor fails.
	 */
	private void exportAndOpenModelDiagram(IProgressMonitor monitor,
			boolean initial) throws IOException, PartInitException {
		monitor.beginTask("Exporting and opening the model", 2);
		// fetching objects
		EObject rootObject = initial ? operationSpecification
				.getOriginModelRoot() : operationSpecification
				.getWorkingModelRoot();
		Collection<EObject> diagramObjects = initial ? operationSpecification
				.getOriginDiagram() : operationSpecification
				.getWorkingDiagram();
		boolean haveDiagram = diagramObjects.size() > 0;

		// deriving file names
		String originOrRevised = initial ? "initial" : "revised"; //$NON-NLS-1$ $NON-NLS-2$
		String modelFileString = operationSpecificationFile.getParent()
				.getFullPath().toString()
				+ DIR_SEP
				+ operationSpecificationFile.getName().replace(
						DOT + operationSpecificationFile.getFileExtension(),
						DOT)
				+ originOrRevised
				+ DOT
				+ operationSpecification.getModelFileExtension();
		URI modelFileURI = URI.createPlatformResourceURI(modelFileString, true);
		String diagramFileString = null;
		URI diagramFileURI = null;
		if (haveDiagram) {
			diagramFileString = operationSpecificationFile.getParent()
					.getFullPath().toString()
					+ DIR_SEP
					+ operationSpecificationFile
							.getName()
							.replace(
									DOT
											+ operationSpecificationFile
													.getFileExtension(),
									DOT)
					+ originOrRevised
					+ DOT
					+ operationSpecification.getDiagramFileExtension();
			diagramFileURI = URI.createPlatformResourceURI(diagramFileString,
					true);
		}

		// creating model file
		monitor.subTask("Exporting files");
		Copier copier = new IDCopier();
		Resource modelResource = resourceSet.createResource(modelFileURI);
		modelResource.getContents().clear();
		modelResource.getContents().add(copier.copy(rootObject));

		// create diagram file
		Resource diagramResource = null;
		if (haveDiagram) {
			diagramResource = resourceSet.createResource(diagramFileURI);
			diagramResource.getContents().clear();
			diagramResource.getContents()
					.addAll(copier.copyAll(diagramObjects));
			copier.copyReferences();
			diagramResource.save(Collections.emptyMap());
		} else {
			copier.copyReferences();
		}
		modelResource.save(Collections.emptyMap());
		monitor.worked(1);

		// open right editor
		monitor.subTask("Opening editor");
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		IFile fileToOpen = null;
		if (haveDiagram) {
			fileToOpen = CommonUIUtil.getIFile(diagramResource);
		} else {
			fileToOpen = CommonUIUtil.getIFile(modelResource);
		}
		IDE.openEditor(page, fileToOpen, operationSpecification.getEditorId(),
				true);
		monitor.worked(1);
	}

	/**
	 * Returns the {@link IOperationGenerationEngine} to use.
	 * 
	 * @return the {@link IOperationGenerationEngine} to use.
	 */
	private IOperationGenerationEngine getOperationGenerationEngine() {
		if (this.operationGenerationEngine == null) {
			this.operationGenerationEngine = new OperationGenerationEngineImpl();
		}
		return this.operationGenerationEngine;
	}

	/**
	 * Opens the configuration tab, if already existing.
	 */
	public void openConfigurationTab() {
		if (this.operationDefPage != null) {
			pageChange(this.pages.indexOf(operationDefPage));
		} else {
			MessageDialog.openInformation(getSite().getShell(),
					"No configuration yet", "Please demonstrate the "
							+ "transformation before proceeding "
							+ "to the configuration.");
		}
	}

	/**
	 * Adds UUIDs to the {@link OperationSpecification#getOriginModelRoot()
	 * origin model} of the current {@link OperationSpecification}.
	 */
	private void addUUIDsToOrigin() {
		UUIDUtil.addUUIDs(operationSpecification.getOriginModelRoot());
	}

	/**
	 * Removes the specified {@link NegativeApplicationCondition}.
	 * 
	 * @param nac
	 *            the {@link NegativeApplicationCondition} to remove.
	 */
	public void removeNAC(NegativeApplicationCondition nac) {
		boolean question = MessageDialog.openQuestion(getSite().getShell(),
				"Confirm to delete NAC", "Are you sure you want to delete \""
						+ nac.getName() + "\"?");

		if (question) {
			EcoreUtil.delete(nac, true);
			setDirty(true);
			this.nacPage.refresh();
		}
	}

	/**
	 * Starts creating a new {@link NegativeApplicationCondition}.
	 */
	public void addNewNAC() {
		// guard wrong state for adding a new rule
		if (NACSpecificationState.DEMONSTRATION.equals(this.nacState)) {
			showError("Cannot add Negative Application Condition when there is an uncommited one.");
			return;
		}

		// create source copy
		createNACModelDiagramFileCopy(
				operationSpecification.getOriginModelRoot(),
				operationSpecification.getOriginDiagram());

		// open editors
		try {
			IFile sourceInput = this.currentNACDiagramFile != null ? this.currentNACDiagramFile
					: this.currentNACModelFile;
			currentNACEditor = IDE.openEditor(this.getSite().getPage(),
					sourceInput, this.operationSpecification.getEditorId(),
					true);

			// update state
			this.nacState = NACSpecificationState.DEMONSTRATION;

			// set state in rules page
			this.nacPage.setState(this.nacState);

			// set dirty
			setDirty(true);

		} catch (PartInitException e) {
			showError(e);
		}
	}

	/**
	 * Creates a new temporary file for the containing a deep copy of the
	 * specified <code>modelRootObject</code> and if not <code>null</code> a
	 * diagram file.
	 * 
	 * @param modelRootObject
	 *            the model root.
	 * @param diagramRootObjects
	 *            the diagram root objects (may be <code>null</code> if no
	 *            diagram is selected).
	 */
	private void createNACModelDiagramFileCopy(EObject originModelRoot,
			EList<EObject> originDiagram) {
		Assert.isNotNull(originModelRoot);
		addUUIDsToOrigin();

		// make a copy of originModelRoot and originDiagram (if not null)
		Copier copier = new Copier();
		EObject copyModelRootObject = copier.copy(originModelRoot);
		Collection<EObject> copyDiagramRootObjects = null;
		boolean haveDiagram = false;
		if (originDiagram != null && originDiagram.size() > 0) {
			copyDiagramRootObjects = copier.copyAll(originDiagram);
			haveDiagram = true;
		}
		copier.copyReferences();

		// generate file name
		String fileName = EcoreUtil.generateUUID();

		try {
			// create model file
			String fileModelExtension = operationSpecification
					.getModelFileExtension();
			fileName = this.operationSpecificationFile.getName() + DOT
					+ NAC_PREFIX + fileName;
			IPath path = getTempModelPath();
			URI modelURI = URI.createPlatformResourceURI(path.toString()
					+ DIR_SEP + fileName + DOT + fileModelExtension, true);
			Resource modelResource = resourceSet.createResource(modelURI);
			modelResource.getContents().add(copyModelRootObject);
			UUIDUtil.copyUUIDs(originModelRoot, copier);
			modelResource.save(Collections.emptyMap());

			// save current diagram resource and file to fields
			currentNACModelFile = CommonUIUtil.getIFile(modelResource);
			currentNACModelResource = modelResource;

			// create diagram file if available
			if (haveDiagram) {
				fileModelExtension = operationSpecification
						.getDiagramFileExtension();
				URI diagramURI = URI.createPlatformResourceURI(path.toString()
						+ DIR_SEP + fileName + DOT + fileModelExtension, true);
				Resource diagramResource = resourceSet
						.createResource(diagramURI);
				diagramResource.getContents().addAll(copyDiagramRootObjects);
				diagramResource.save(Collections.emptyMap());

				// save current diagram resource and file to fields
				currentNACDiagramFile = CommonUIUtil.getIFile(diagramResource);
				currentNACDiagramResource = diagramResource;

			} else {
				// unset current diagram fields
				this.currentNACDiagramFile = null;
				this.currentNACDiagramResource = null;
			}

		} catch (IOException e) {
			showError(e);
		}
	}

	/**
	 * Commits the currently demonstrated {@link NegativeApplicationCondition}.
	 */
	public void commitNAC() {
		// guard wrong state for committing a new rule
		if (!NACSpecificationState.DEMONSTRATION.equals(this.nacState)) {
			showError("Cannot commit a new NAC demonstration when no new NAC is pending.");
			return;
		}

		// save editor
		if (this.currentNACEditor != null) {
			this.currentNACEditor.doSave(new NullProgressMonitor());
		}

		// create new NAC
		try {
			// update resources
			currentNACModelResource.unload();
			currentNACModelResource.load(Collections.emptyMap());
			if (currentNACDiagramResource != null) {
				currentNACDiagramResource.unload();
				currentNACDiagramResource.load(Collections.emptyMap());
			}
			EObject originModelRoot = operationSpecification
					.getOriginModelRoot();
			EObject nacModelRoot = currentNACModelResource.getContents().get(0);

			// build match model from original model to NAC demo model
			MatchModel matchModel = new MatchService().generateMatchModel(
					originModelRoot, nacModelRoot);

			// call operation specification builder to build NAC
			NegativeApplicationCondition newNAC = operationGenerationEngine
					.createNegativeApplicationCondition(matchModel,
							currentNACDiagramResource, operationSpecification);

			// remove temp files
			CommonUIUtil.deleteFile(currentNACModelFile,
					new NullProgressMonitor());
			currentNACModelFile = null;
			if (currentNACDiagramFile != null) {
				CommonUIUtil.deleteFile(currentNACDiagramFile,
						new NullProgressMonitor());
				currentNACDiagramFile = null;
			}

			// unset fields
			currentNACModelResource.unload();
			if (currentNACDiagramResource != null) {
				currentNACDiagramResource.unload();
			}
			currentNACDiagramResource = null;
			currentNACEditor = null;
			currentNACModelResource = null;

			// refresh NAC view
			this.nacPage.refresh();
			this.nacPage.selectNAC(newNAC);

			// update state
			this.nacState = NACSpecificationState.CONFIGURATION;
			this.nacPage.setState(this.nacState);

			// set dirty
			setDirty(true);

		} catch (IOException e) {
			showError(e);
		} catch (MatchException e) {
			showError(e);
		}
	}

	/**
	 * Opens the conditions model contained by the specified
	 * {@link NegativeApplicationCondition}.
	 * 
	 * @param nac
	 *            to open conditions model for.
	 */
	public void showNegativeApplicationConditionView(
			NegativeApplicationCondition nac) {
		try {
			this.nacConditionView = (ConditionView) PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView(
							"org.modelversioning.operations.ui.conditionview", //$NON-NLS-1$
							NAC_CONDITION_VIEW_ID,
							org.eclipse.ui.IWorkbenchPage.VIEW_VISIBLE);
			nacConditionEvaluationEngine
					.setConditionsModel(nac.getConditions());
			nacConditionEvaluationEngine
					.registerRelatedTemplateBinding(
							OperationsUtil.INITIAL_PREFIX,
							ConditionsUtil.createTemplateBindings(ConditionsUtil
									.getTemplateToRepresentativeMap(operationSpecification
											.getPreconditions())));
			this.nacConditionView
					.setConditionEvaluationEngine(nacConditionEvaluationEngine);
			this.nacConditionView
					.show("NAC: " + nac.getName(), OperationsUIPlugin
							.getImage(OperationsUIPlugin.IMG_NAC), nac
							.getConditions(), ConditionsUtil
							.getActiveConditionArray(nac.getConditions()));
			this.nacConditionView.expandAll();
			this.nacConditionView.addChangeListener(this);
			this.activeViewNames.add(this.nacConditionView.getPartName());
		} catch (PartInitException e) {
			showError(e);
		} catch (UnsupportedConditionLanguage e) {
			showError(e);
		}
	}

}
