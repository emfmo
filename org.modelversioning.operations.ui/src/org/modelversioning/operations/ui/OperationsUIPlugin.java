package org.modelversioning.operations.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class OperationsUIPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.modelversioning.operations.ui"; //$NON-NLS-1$

	// The shared instance
	private static OperationsUIPlugin plugin;
	
	/**
	 * The extension.
	 */
	public static final String EXTENSION = "operation"; //$NON-NLS-1$
	
	/**
	 * The icons path.
	 */
	private static final String iconPath = "icons/"; //$NON-NLS-1$

	/**
	 * The image for demonstrate action.
	 */
	public static final String IMG_RECORD = "record"; //$NON-NLS-1$
	
	/**
	 * The image for re-demonstrate action.
	 */
	public static final String IMG_RERECORD = "re-record"; //$NON-NLS-1$
	
	/**
	 * The image for re-demonstrate action.
	 */
	public static final String IMG_COMMIT = "commit"; //$NON-NLS-1$
	
	/**
	 * The image for initial model.
	 */
	public static final String IMG_INTIALMODEL = "initialModel"; //$NON-NLS-1$
	
	/**
	 * The image for initial model.
	 */
	public static final String IMG_REVISEDMODEL = "revisedModel"; //$NON-NLS-1$
	
	/**
	 * The image for the post conditions.
	 */
	public static final String IMG_POSTCONDITIONS = "postconditions"; //$NON-NLS-1$
	
	/**
	 * The image for the post conditions.
	 */
	public static final String IMG_PRECONDITIONS = "preconditions"; //$NON-NLS-1$
	
	/**
	 * The image for the negative application conditions.
	 */
	public static final String IMG_NAC = "nac"; //$NON-NLS-1$

	/**
	 * The constructor
	 */
	public OperationsUIPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext
	 * )
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static OperationsUIPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry registry) {
		registerImage(registry, IMG_RECORD, "record_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_RERECORD, "re-record.png"); //$NON-NLS-1$
		registerImage(registry, IMG_COMMIT, "commit.png"); //$NON-NLS-1$
		registerImage(registry, IMG_INTIALMODEL, "initialModel_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_REVISEDMODEL, "revisedModel_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_PRECONDITIONS, "preconditions_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_POSTCONDITIONS, "postconditions_16.png"); //$NON-NLS-1$
		registerImage(registry, IMG_NAC, "nac_16.png"); //$NON-NLS-1$
	}
	
	/**
	 * Registers the specified fileName under the specified key to the specified
	 * registry.
	 * 
	 * @param registry
	 *            registry to register image.
	 * @param key
	 *            key of image.
	 * @param fileName
	 *            file name.
	 */
	private void registerImage(ImageRegistry registry, String key,
			String fileName) {
		try {
			IPath path = new Path(iconPath + fileName);
			URL url = FileLocator.find(getBundle(), path, null);
			if (url != null) {
				ImageDescriptor desc = ImageDescriptor.createFromURL(url);
				registry.put(key, desc);
			}
		} catch (Exception e) {
		}
	}
	
	/**
	 * Returns the image for the specified image name.
	 * 
	 * @param img
	 *            image name.
	 * @return the image.
	 */
	public static Image getImage(String key) {
		return getDefault().getImageRegistry().get(key);
	}

	/**
	 * Returns an image descriptor for the image file identified by the given
	 * key.
	 * 
	 * @param key
	 *            the key
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String key) {
		return getDefault().getImageRegistry().getDescriptor(key);
	}

}
