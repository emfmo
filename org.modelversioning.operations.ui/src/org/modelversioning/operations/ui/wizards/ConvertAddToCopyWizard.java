/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.wizards;

import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.diff.copydiff.CopyDiffFactory;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;
import org.modelversioning.operations.ui.commons.wizards.ITemplateSelectionValidator;
import org.modelversioning.operations.ui.commons.wizards.TemplateSelectionPage;

/**
 * Wizard to select a template for any purpose.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConvertAddToCopyWizard extends Wizard implements
		ITemplateSelectionValidator {

	/**
	 * {@link ConditionsModel} to select template from.
	 */
	private ConditionsModel conditionsModel;

	/**
	 * The selection provided from outside.
	 */
	private IStructuredSelection selection = null;
	/**
	 * The template selection page.
	 */
	private TemplateSelectionPage templateSelectionPage;
	/**
	 * Diff Element to be replaced.
	 */
	private ModelElementChangeLeftTarget addDiffElement;
	/**
	 * The page name.
	 */
	private static final String TEMPLATE_SELECTION_PAGE_NAME = "templateSelectionPage";
	/**
	 * The title to use.
	 */
	private static final String WINDOW_TITLE = "Select origin template";
	/**
	 * The title of the selection page.
	 */
	private String PAGE_TITLE;
	/**
	 * The message if the template may be selected.
	 */
	private static final String MESSAGE_OK = "Select the origin template to be copied.";
	/**
	 * The message if the template must not be selected.
	 */
	private static final String MESSAGE_ERROR = "The selected template cannot be used as copy source.";

	/**
	 * Constructing a new wizard for the specified <code>conditionsModel</code>.
	 * 
	 * @param conditionsModel
	 *            to which a new condition has to be added.
	 * @param addDiffElement
	 *            {@link ModelElementChangeLeftTarget} to be replaced.
	 */
	public ConvertAddToCopyWizard(ConditionsModel conditionsModel,
			ModelElementChangeLeftTarget addDiffElement) {
		super();
		this.conditionsModel = conditionsModel;
		this.addDiffElement = addDiffElement;
		this.setWindowTitle(WINDOW_TITLE);
	}

	/**
	 * Sets the user's selection.
	 * 
	 * @param selection
	 *            selection to set.
	 */
	public void setSelection(IStructuredSelection selection) {
		this.selection = selection;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Adds the pages necessary to collect all information to create a
	 * condition.
	 */
	@Override
	public void addPages() {
		templateSelectionPage = new TemplateSelectionPage(
				TEMPLATE_SELECTION_PAGE_NAME, this, conditionsModel, selection);
		templateSelectionPage.setTitle(PAGE_TITLE);
		templateSelectionPage.setTitle(MESSAGE_OK);
		addPage(templateSelectionPage);
	}

	/**
	 * Returns the currently selected template.
	 * 
	 * @return the currently selected template.
	 */
	public Template getSelectedTemplate() {
		return templateSelectionPage.getSelectedTemplate();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * A valid template represents an object which can be set to the feature to
	 * which the added object has been set.
	 */
	@Override
	public boolean isValidSelection(Template template) {
		if (template == null) {
			return false;
		}
		if (this.addDiffElement == null) {
			return true;
		}
		EClassifier eTargetType = addDiffElement.getLeftElement()
				.eContainingFeature().getEType();
		EClass actualType = template.getRepresentative().eClass();
		if (actualType.equals(eTargetType)
				|| actualType.getEAllSuperTypes().contains(eTargetType)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMessage(Template template) {
		if (isValidSelection(template)) {
			return MESSAGE_OK;
		} else {
			return MESSAGE_ERROR;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canFinish() {
		return this.templateSelectionPage.isPageComplete();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performFinish() {
		final Template template = this.getSelectedTemplate();
		if (isValidSelection(template)) {
			// create new Copy Diff Element
			CopyElementLeftTarget copyElementLT = CopyDiffFactory.eINSTANCE
					.createCopyElementLeftTarget();
			copyElementLT.setLeftElement(addDiffElement.getLeftElement());
			copyElementLT.setRightParent(addDiffElement.getRightParent());
			copyElementLT.setRemote(addDiffElement.isRemote());
			copyElementLT.setCopiedRightElement(template.getRepresentative());
			// replace addDiffElement
			// DiffElement parentDiffElement = (DiffElement)
			// addDiffElement
			// .eContainer();
			// parentDiffElement.getSubDiffElements().set(
			// parentDiffElement.getSubDiffElements().indexOf(
			// addDiffElement), copyElementLT);
			EcoreUtil.replace(addDiffElement, copyElementLT);
		}
		return true;
	}

}
