/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.wizards;

import org.eclipse.jface.wizard.Wizard;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.IterationType;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.ui.commons.wizards.TemplateSelectionPage;

/**
 * Wizard for creating a new {@link Iteration}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NewIterationWizard extends Wizard {

	/** {@link OperationSpecification} to which a new iteration has to be added. */
	private OperationSpecification operationSpecification;

	/** The template selection page. */
	private TemplateSelectionPage templateSelectionPage;

	/** The template selection page ID. */
	private static final String TEMPLATE_SELECTION_PAGE_NAME = "templateSelectionPage";

	/**
	 * Constructing a new wizard for the specified
	 * <code>operationSpecification</code>.
	 * 
	 * @param operationSpecification
	 *            to attach the iteration to.
	 */
	public NewIterationWizard(OperationSpecification operationSpecification) {
		super();
		this.operationSpecification = operationSpecification;
		setWizardProperties();
	}

	/**
	 * Sets the wizard properties.
	 */
	private void setWizardProperties() {
		super.setWindowTitle("New Iteration");
		setHelpAvailable(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Adds the pages necessary to collect all information to create a
	 * condition.
	 */
	@Override
	public void addPages() {
		templateSelectionPage = new TemplateSelectionPage(
				TEMPLATE_SELECTION_PAGE_NAME,
				operationSpecification.getPreconditions(), null);
		templateSelectionPage.setTitle("Select Template for new Iteration");
		templateSelectionPage.setDescription("Select precondition template to "
				+ "attach iteration to.");
		addPage(templateSelectionPage);
	}

	/**
	 * Adds the created iteration to the specified
	 * {@link #operationSpecification}.
	 */
	@Override
	public boolean performFinish() {
		Iteration i = OperationsFactory.eINSTANCE.createIteration();
		i.setTemplate(templateSelectionPage.getSelectedTemplate());
		i.setIterationType(IterationType.FOR_ALL);
		operationSpecification.getIterations().add(i);
		return true;
	}

}
