package org.modelversioning.operations.ui.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.modelversioning.core.util.ModelDiagramUtil;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.engines.IOperationGenerationEngine;
import org.modelversioning.operations.engines.impl.OperationGenerationEngineImpl;
import org.modelversioning.operations.ui.OperationsUIPlugin;
import org.modelversioning.ui.commons.wizards.ModelDiagramSelectionPage;

/**
 * The &quot;New In-place Model Transformation&quot wizard.
 * 
 * @author Martina Seidl
 * @author Felix Rinker
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class WizardNewOperation extends Wizard implements INewWizard {

	private WizardNewOperationFilePage createFilePage;
	private ModelDiagramSelectionPage selectModelPage;
	private IStructuredSelection initialSelection;
	private IWorkbench workbench;

	/**
	 * Constructor of ORNewWizard.
	 */
	public WizardNewOperation() {
		super();
		this.setWindowTitle("In-place Model Transformation By Demonstration");
		this.setHelpAvailable(false);
		setNeedsProgressMonitor(true);
	}

	/**
	 * Adds two pages to the wizard. The first page provides a dialog for
	 * selecting the container and setting the filename. The second page
	 * contains a dialog for selecting the model file and setting the favorite
	 * editor for opening.
	 */
	public void addPages() {
		// Create and add the first page.
		createFilePage = new WizardNewOperationFilePage("Select File",
				initialSelection);
		createFilePage.setDescription("Select the location and file name.");
		createFilePage.setFileName("new-model-operation.operation"); //$NON-NLS-1$
		this.addPage(createFilePage);

		// Create and add the second page.
		selectModelPage = new ModelDiagramSelectionPage(
				"Select initial model or diagram", initialSelection, workbench);
		selectModelPage.setTitle("Select initial model or diagram");
		this.addPage(selectModelPage);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	public boolean performFinish() {

		final IFile operationFile = createFilePage.getModelFile();
		final IFile editorFile = selectModelPage.getModelFile();
		final IFile modelFile = selectModelPage.getDiagramFile();
		final IEditorDescriptor editorFileDescriptor = selectModelPage
				.getEditorFileDescriptor();

		IRunnableWithProgress op = new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {

				try {
					doFinish(operationFile, editorFile, modelFile,
							editorFileDescriptor, monitor);

				} catch (CoreException e) {
					e.printStackTrace();
					throw new InvocationTargetException(e);

				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);

		} catch (InterruptedException e) {
			MessageDialog.openError(getShell(), "Error", e.getMessage());

			return false;

		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error",
					realException.getMessage());

			return false;
		}
		return true;
	}

	/**
	 * This method will finally create an initial operation specification and
	 * opens the operation specification editor.
	 * 
	 * @param operationFile
	 *            to save operation specification to.
	 * @param modelFile
	 *            selected model file.
	 * @param diagramFile
	 *            selected diagram file (might be null if model only).
	 * @param editorFileDescriptor
	 *            selected editor file descriptor.
	 * @param monitor
	 *            to report progress to.
	 * @throws CoreException
	 *             if file handles fail.
	 */
	private void doFinish(final IFile operationFile, final IFile modelFile,
			final IFile diagramFile,
			final IEditorDescriptor editorFileDescriptor,
			IProgressMonitor monitor) throws CoreException {

		// create empty operation specification
		monitor.beginTask("Creating operation specification", 3);

		try {

			// create resource set
			ResourceSet resourceSet = new ResourceSetImpl();
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
					.put("operation", new EcoreResourceFactoryImpl()); //$NON-NLS-1$
			// load initial model
			monitor.setTaskName("Loading specified initial model...");
			Resource modelResource = resourceSet.getResource(URI
					.createPlatformResourceURI(
							ModelDiagramUtil.getPlatformResourceURI(modelFile),
							true), true);
			// load initial diagram
			Resource diagramResource = null;
			if (diagramFile != null) {
				diagramResource = resourceSet.getResource(URI
						.createPlatformResourceURI(ModelDiagramUtil
								.getPlatformResourceURI(diagramFile), true),
						true);
			}
			monitor.worked(1);

			// generate initial operation specification
			monitor.setTaskName("Creating initial operation specification for initial model...");
			IOperationGenerationEngine operationGenerationEngine = new OperationGenerationEngineImpl();
			OperationSpecification operationSpecification = operationGenerationEngine
					.generateInitialOperationSpecification(modelResource,
							diagramResource);
			operationSpecification.setEditorId(editorFileDescriptor.getId());
			operationSpecification.setModelFileExtension(modelFile
					.getFileExtension());
			// set diagram specific properties
			if (diagramFile != null) {
				operationSpecification.setDiagramFileExtension(diagramFile
						.getFileExtension());
			}
			monitor.worked(1);

			// save initial operation specification to file
			monitor.setTaskName("Persisting initial operation specification...");
			Resource resource = resourceSet.createResource(URI
					.createPlatformResourceURI(operationFile.getFullPath()
							.toString(), true));
			resource.getContents().add(operationSpecification);
			resource.save(Collections.emptyMap());
			monitor.worked(1);

			// open editor
			getShell().getDisplay().asyncExec(new Runnable() {
				public void run() {
					IWorkbenchPage page = PlatformUI.getWorkbench()
							.getActiveWorkbenchWindow().getActivePage();
					try {
						IDE.openEditor(page, operationFile, true);
					} catch (PartInitException e) {
						IStatus status = new Status(IStatus.ERROR,
								OperationsUIPlugin.PLUGIN_ID, e
										.getLocalizedMessage(), e);
						OperationsUIPlugin.getDefault().getLog().log(status);
					}
				}
			});

		} catch (IOException e) {
			IStatus status = new Status(IStatus.ERROR,
					OperationsUIPlugin.PLUGIN_ID, e.getLocalizedMessage(), e);
			// log error
			OperationsUIPlugin.getDefault().getLog().log(status);
			throw new CoreException(new Status(IStatus.ERROR,
					OperationsUIPlugin.PLUGIN_ID, "Could save to file: "
							+ operationFile.getName(), e));
		} finally {
			monitor.done();
		}
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.initialSelection = selection;
		this.workbench = workbench;
	}
}