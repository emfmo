package org.modelversioning.operations.ui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.dialogs.WizardNewFileCreationPage;
import org.modelversioning.operations.OperationPlugin;

/**
 * Wizard page for selecting the file to save operation to.
 * 
 * @author Martina Seidl
 * @author Felix Rinker
 */

public class WizardNewOperationFilePage extends WizardNewFileCreationPage {

	/**
	 * Constructs for the {@link WizardNewOperationFilePage}.
	 * 
	 * @param pageName
	 *            the page name
	 * 
	 * @param selection
	 *            the selection
	 */
	public WizardNewOperationFilePage(String pageName,
			IStructuredSelection selection) {
		super(pageName, selection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean validatePage() {
		if (super.validatePage()) {
			String extension = new Path(getFileName()).getFileExtension();
			if (extension == null || !OperationPlugin.EXTENSION.equals(extension)) {
				setErrorMessage("The extension must be " + OperationPlugin.EXTENSION);
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns the selected file.
	 * 
	 * @return the selected file.
	 */
	public IFile getModelFile() {
		return ResourcesPlugin.getWorkspace().getRoot().getFile(
				getContainerFullPath().append(getFileName()));
	}
}