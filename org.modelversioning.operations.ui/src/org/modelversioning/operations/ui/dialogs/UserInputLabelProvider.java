package org.modelversioning.operations.ui.dialogs;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.modelversioning.operations.UserInput;

public class UserInputLabelProvider implements ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {

		if (element instanceof UserInput) {
			UserInput ui = (UserInput) element;

			switch (columnIndex) {
			case 0:
				return ui.getName();

			case 1:
				return ui.getTemplate().getTitle();

			case 2:
				return ui.getFeature().getName();

			}
		}

		return null;
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

}
