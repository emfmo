package org.modelversioning.operations.ui.dialogs;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.modelversioning.operations.Iteration;

public class IterationsLabelProvider implements ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {

		if (element instanceof Iteration) {
			Iteration iteration = (Iteration) element;

			switch (columnIndex) {
			case 0:
				return iteration.getTemplate().getTitle();

			case 1:
				return iteration.getIterationType().toString();

			}
		}

		return null;
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		// nothing todo
	}

	@Override
	public void dispose() {
		// nothing todo
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// nothing todo
	}

}
