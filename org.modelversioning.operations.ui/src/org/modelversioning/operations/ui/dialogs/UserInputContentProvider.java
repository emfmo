package org.modelversioning.operations.ui.dialogs;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.operations.OperationSpecification;

public class UserInputContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// nothing to do
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// nothing to do
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof OperationSpecification) {
			return ((OperationSpecification) parentElement).getUserInputs()
					.toArray();
		}
		return null;
	}

	@Override
	public Object getParent(Object element) {
		// no parents
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof OperationSpecification) {
			return (((OperationSpecification) element).getUserInputs().size() != 0);
		}
		return false;

	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

}
