/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.compare.util.AdapterUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;
import org.eclipse.jface.fieldassist.IContentProposalListener2;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.ocl.helper.Choice;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.EvaluationStatus;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.util.ConditionsUtil;

/**
 * Dialog for editing {@link Condition Conditions}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionEditingDialog extends Dialog {

	private static final int RESULT_COLOR_OK = SWT.COLOR_GREEN;
	private static final int RESULT_COLOR_ERROR = SWT.COLOR_RED;
	private static final int RESULT_COLOR_WARNING = SWT.COLOR_YELLOW;
	private static final String MSG_OK = "OK";
	private static final String MSG_UNSATISFIED = "Condition does not match with example";

	/**
	 * The condition to edit.
	 */
	private Condition condition;
	/**
	 * The condition text area.
	 */
	private Text txtCondition = null;
	/**
	 * The condition text.
	 */
	private String conditionText = "";
	/**
	 * Text area containing the result
	 */
	private Text txtResult = null;
	/**
	 * The condition prefix.
	 */
	private String conditionPrefix = "";
	/**
	 * The condition evaluation engine to use.
	 */
	private IConditionEvaluationEngine evaluationEngine = null;
	/**
	 * The proposal provider.
	 */
	private IContentProposalProvider contentProposalProvider = null;
	/**
	 * The binding for evaluating conditions.
	 */
	private ITemplateBinding evaluationBinding;

	/**
	 * Constructs a new {@link ConditionEditingDialog} for the specified
	 * <code>condition</code>.
	 * 
	 * @param parentShell
	 *            the parent shell.
	 * @param condition
	 *            the condition to edit.
	 */
	public ConditionEditingDialog(Shell parentShell, Condition condition,
			IConditionEvaluationEngine evaluationEngine) {
		super(parentShell);
		this.condition = condition;
		this.evaluationEngine = evaluationEngine;
		this.conditionPrefix = evaluationEngine.getConditionPrefix(condition);
		this.evaluationBinding = ConditionsUtil
				.createTemplateBinding(ConditionsUtil
						.getTemplateToRepresentativeMap(ConditionsUtil
								.getContainingConditionsModel(condition
										.getTemplate())));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Sets the window title.
	 */
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Edit Condition of " + condition.getTemplate().getTitle());
	}

	/**
	 * {@inheritDoc}
	 */
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);
		GridLayout layout = (GridLayout) comp.getLayout();

		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;

		// template name
		final Label labTemplateName = new Label(comp, 0);
		labTemplateName.setLayoutData(new GridData());
		labTemplateName.setText("Base Template Name:");
		final Text txtBaseTemplateName = new Text(comp, SWT.BORDER);
		txtBaseTemplateName
				.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		txtBaseTemplateName.setEditable(false);
		txtBaseTemplateName.setText(condition.getTemplate().getName());

		// base type
		final Label labBaseType = new Label(comp, 0);
		labBaseType.setLayoutData(new GridData());
		labBaseType.setText("Base Type:");
		final Text txtBaseType = new Text(comp, SWT.BORDER);
		txtBaseType.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		txtBaseType.setEditable(false);
		txtBaseType.setText(AdapterUtils.getItemProviderText(condition
				.getTemplate().getRepresentative().eClass()));

		// condition text
		txtCondition = new Text(comp, SWT.MULTI | SWT.V_SCROLL | SWT.BORDER);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 2;
		gd.heightHint = 70;
		gd.widthHint = 400;
		txtCondition.setLayoutData(gd);
		// TODO somehow style condition prefix
		txtCondition.setText(conditionPrefix
				+ ConditionsUtil.getExpression(condition));
		conditionText = txtCondition.getText()
				.replaceFirst(conditionPrefix, ""); //$NON-NLS-1$
		txtCondition.setFocus();
		txtCondition.setSelection(txtCondition.getText().length());
		// modify listener for status update
		txtCondition.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (txtCondition.getText().startsWith(conditionPrefix)) {
					conditionText = txtCondition.getText().replaceFirst(
							conditionPrefix, ""); //$NON-NLS-1$
				} else {
					txtCondition.setText(conditionPrefix + conditionText);
				}

				updateResultStatus();
			}
		});

		initializeContentAssist();

		// current result
		final Label labName = new Label(comp, 0);
		labName.setLayoutData(new GridData());
		labName.setText("Result:");
		txtResult = new Text(comp, SWT.BORDER);
		txtResult.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		txtResult.setEditable(false);

		updateResultStatus();

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void okPressed() {
		ConditionsUtil.setExpression(condition, conditionText);
		super.okPressed();
	}

	private void updateResultStatus() {
		// set new value to expression
		String oldExpression = ConditionsUtil.getExpression(condition);
		ConditionsUtil.setExpression(condition, conditionText);
		boolean isActive = condition.isActive();
		condition.setActive(true);

		// evaluate first syntactic correctness, then semantic correctness
		EvaluationResult syntaxResult = evaluationEngine.validate(condition);
		if (syntaxResult.isOK()) {
			EvaluationResult evalResult = evaluationEngine.evaluate(condition,
					condition.getTemplate().getRepresentative(),
					evaluationBinding);
			updateResult(evalResult);
		} else {
			updateResult(syntaxResult);
		}

		// set to old value again
		condition.setActive(isActive);
		ConditionsUtil.setExpression(condition, oldExpression);
	}

	private void updateResult(EvaluationResult result) {
		if (result.isOK()) {
			txtResult.setText(MSG_OK);
			updateResultColor(RESULT_COLOR_OK);
			if (this.getButton(OK) != null) {
				this.getButton(OK).setEnabled(true);
			}
		} else if (EvaluationStatus.UNSATISFIED.equals(result.getStatus())) {
			txtResult.setText(MSG_UNSATISFIED);
			updateResultColor(RESULT_COLOR_WARNING);
			if (this.getButton(OK) != null) {
				this.getButton(OK).setEnabled(true);
			}
		} else if (!result.isOK()) {
			txtResult.setText(result.getMessage());
			updateResultColor(RESULT_COLOR_ERROR);
			if (this.getButton(OK) != null) {
				this.getButton(OK).setEnabled(false);
			}
		}
	}

	private void updateResultColor(int color) {
		if (txtResult != null) {
			txtResult.setBackground(txtResult.getShell().getDisplay()
					.getSystemColor(color));
		}
	}

	private void initializeContentAssist() {
		char[] autoActivationCharacters = new char[] { '.', '#', ':', '>', '^' };
		try {
			contentProposalProvider = new IContentProposalProvider() {
				@Override
				public IContentProposal[] getProposals(String contents,
						final int position) {
					Choice[] choices = evaluationEngine.getContentProposals(
							contents, position, condition);
					List<IContentProposal> proposals = new ArrayList<IContentProposal>();
					for (final Choice choice : choices) {
						proposals.add(new IContentProposal() {

							@Override
							public String getLabel() {
								return choice.getName();
							}

							@Override
							public String getDescription() {
								return choice.getDescription();
							}

							@Override
							public int getCursorPosition() {
								return position;
							}

							@Override
							public String getContent() {
								return choice.getName();
							}
						});
					}
					return proposals.toArray(new IContentProposal[proposals
							.size()]);
				}
			};
			ContentProposalAdapter adapter = new ContentProposalAdapter(
					txtCondition, new TextContentAdapter(),
					contentProposalProvider, null, autoActivationCharacters);
			adapter.setProposalAcceptanceStyle(ContentProposalAdapter.PROPOSAL_IGNORE);
			// adapter.setFilterStyle(ContentProposalAdapter.FILTER_NONE);
			CleverInsertProposalListener listener = new CleverInsertProposalListener(
					adapter);
			adapter.addContentProposalListener((IContentProposalListener) listener);
			adapter.addContentProposalListener((IContentProposalListener2) listener);
			adapter.setPopupSize(new Point(300, 150));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the currently set condition text.
	 * 
	 * @return the currently set condition text.
	 */
	public String getConditionText() {
		return conditionText;
	}

	class CleverInsertProposalListener implements IContentProposalListener,
			IContentProposalListener2 {

		private final ContentProposalAdapter adapter;

		private int startCaretPosition;

		private int endCaretPosition;

		public CleverInsertProposalListener(ContentProposalAdapter adapter) {
			this.adapter = adapter;
		}

		/**
		 * {@inheritDoc}
		 */
		public void proposalAccepted(IContentProposal proposal) {
			IControlContentAdapter contentAdapter = adapter
					.getControlContentAdapter();
			Control control = adapter.getControl();
			StringBuilder sb = new StringBuilder(
					contentAdapter.getControlContents(control));
			sb.insert(
					contentAdapter.getCursorPosition(control),
					proposal.getContent().substring(
							endCaretPosition - startCaretPosition));
			contentAdapter.setControlContents(control, sb.toString(),
					startCaretPosition + proposal.getCursorPosition());
		}

		/**
		 * {@inheritDoc}
		 */
		public void proposalPopupOpened(ContentProposalAdapter adapter) {
			startCaretPosition = adapter.getControlContentAdapter()
					.getCursorPosition(adapter.getControl());
		}

		/**
		 * {@inheritDoc}
		 */
		public void proposalPopupClosed(ContentProposalAdapter adapter) {
			endCaretPosition = adapter.getControlContentAdapter()
					.getCursorPosition(adapter.getControl());
		}

	}

}
