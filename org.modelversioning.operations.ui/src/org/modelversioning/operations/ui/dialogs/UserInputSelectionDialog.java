package org.modelversioning.operations.ui.dialogs;

import java.util.Hashtable;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.modelversioning.core.conditions.Template;

public class UserInputSelectionDialog extends Dialog {
	private Combo templateCombo;
	private Combo featuresCombo;
	private List<Template> templates;
	private Text txtName;
	private String name = "";
	private String selectedTemplate, selectedFeature;
	private Hashtable<String, Integer> templateNames;

	public UserInputSelectionDialog(Shell parentShell, List<Template> list) {
		super(parentShell);
		this.templates = list;
		templateNames = new Hashtable<String, Integer>();
	}

	public Template getSelectedTemplate() {
		return templates.get(templateNames.get(selectedTemplate));
	}

	public String getUserInputName() {
		return name;
	}

	public EStructuralFeature getFeature() {
		Template t = templates.get(templateNames.get(selectedTemplate));
		EList<EStructuralFeature> esf = t.getRepresentative().eClass()
				.getEAllStructuralFeatures();

		for (EStructuralFeature f : esf) {
			if (f.getName().equals(selectedFeature)) {
				return f;
			}
		}
		return null;
	}

	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("New User Input");
	}

	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite) super.createDialogArea(parent);
		GridLayout layout = (GridLayout) comp.getLayout();

		layout.numColumns = 3;

		final Label labName = new Label(comp, 0);
		labName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		labName.setText("Name:");

		final Label labTemplate = new Label(comp, 0);
		labTemplate.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		labTemplate.setText("Template:");

		final Label labFeature = new Label(comp, 0);
		labFeature.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		labFeature.setText("Feature:");

		txtName = new Text(comp, 0);
		txtName.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		int i = 0;
		templateCombo = new Combo(comp, SWT.READ_ONLY);
		for (Template s : templates) {
			templateCombo.add(s.getTitle());
			templateNames.put(s.getTitle(), Integer.valueOf(i));
			i++;
		}

		featuresCombo = new Combo(comp, SWT.READ_ONLY);

		featuresCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		templateCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		templateCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Template t = templates.get(templateNames.get(templateCombo
						.getText()));
				selectedTemplate = templateCombo.getText();

				EList<EStructuralFeature> esf = t.getRepresentative().eClass()
						.getEAllStructuralFeatures();

				for (EStructuralFeature f : esf) {
					featuresCombo.add(f.getName());
				}
			}
		});

		featuresCombo.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				selectedFeature = featuresCombo.getText();
				name = txtName.getText();
			}
		});

		/*
		 * featuresCombo.addSelectionListener(new SelectionAdapter() { public
		 * void widgetSelected(SelectionEvent e) { if
		 * (featuresCombo.getText().equals("FOR ALL")) type =
		 * IterationType.FOR_ALL; else type = IterationType.FOR_SOME; } });
		 */
		return null;
	}

	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
	}

	protected void buttonPressed(int buttonId) {
		name = txtName.getText();
		super.buttonPressed(buttonId);
	}

}
