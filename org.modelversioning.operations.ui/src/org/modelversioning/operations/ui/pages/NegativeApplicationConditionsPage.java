package org.modelversioning.operations.ui.pages;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor.NACSpecificationState;

/**
 * Page for creating Negative Application Conditions in
 * {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NegativeApplicationConditionsPage extends FormPage {

	/**
	 * The block.
	 */
	private NegativeApplicationConditionsBlock nacBlock;

	/**
	 * Constructs the NAC page.
	 * 
	 * @param editor
	 *            the containing operation recorder editor
	 * @param id
	 *            the id of this page
	 * @param title
	 *            the title of the page
	 * 
	 */
	public NegativeApplicationConditionsPage(OperationRecorderEditor editor,
			String id, String title) {
		super(editor, id, title);
		this.nacBlock = new NegativeApplicationConditionsBlock(editor);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit tk = new FormToolkit(Display.getCurrent());
		ScrolledForm form = managedForm.getForm();

		// Header
		form.setText("Negative Application Conditions");
		tk.paintBordersFor(form);
		tk.decorateFormHeading(form.getForm());

		// Create rules block
		this.nacBlock.createContent(managedForm);

	}
	
	/**
	 * Sets the state of this page.
	 * 
	 * @param state
	 *            state to set.
	 */
	public void setState(NACSpecificationState state) {
		this.nacBlock.setState(state);
	}

	/**
	 * Refreshes this page.
	 */
	public void refresh() {
		this.nacBlock.refresh();
	}

	/**
	 * Selects the specified <code>nac</code>.
	 * 
	 * @param nac
	 *            to select
	 */
	public void selectNAC(NegativeApplicationCondition nac) {
		this.nacBlock.selectNAC(nac);
	}

}
