package org.modelversioning.operations.ui.pages;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIPlugin;
import org.modelversioning.operations.ui.OperationsUIPlugin;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor;

/**
 * This page contains the entry information in the editor. Its the ability to
 * set the name, version and description. A button is presented to start the
 * operation demonstration.
 * 
 * @author Martina Seidl
 * @author Felix Rinker
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class GeneralConfigPage extends FormPage implements ModifyListener {

	/**
	 * The containing editor.
	 */
	private OperationRecorderEditor editor;
	/**
	 * Text control for the name.
	 */
	private Text txtName;
	/**
	 * Text control for the version.
	 */
	private Text txtVersion;
	/**
	 * Text control for the name template.
	 */
	private Text txtTitleTemplate;
	/**
	 * Text control for the modeling language.
	 */
	private Text txtModelingLanguage;
	/**
	 * Text control for the description.
	 */
	private Text txtDescription;
	private IManagedForm managedForm;

	/**
	 * Constructor.
	 * 
	 * @param editor
	 *            containing editor of this page.
	 * @param id
	 *            page id.
	 * @param title
	 *            page title.
	 */
	public GeneralConfigPage(OperationRecorderEditor editor, String id,
			String title) {
		super(editor, id, title);
		this.editor = editor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		this.managedForm = managedForm;
		FormToolkit tk = new FormToolkit(Display.getCurrent());
		ScrolledForm form = managedForm.getForm();

		form.setText("General Information");
		tk.paintBordersFor(form);
		tk.decorateFormHeading(form.getForm());

		TableWrapLayout layout = new TableWrapLayout();
		layout.numColumns = 2;
		form.getBody().setLayout(layout);

		createLeftSection(tk, form);
		createRightSection(tk, form);

		// Add modify listener
		this.txtName.addModifyListener(this);
		this.txtVersion.addModifyListener(this);
		this.txtTitleTemplate.addModifyListener(this);
		this.txtDescription.addModifyListener(this);

		conifgureToolBarActions();
		this.setFocus();
	}

	/**
	 * Adds the toolbar actions.
	 */
	protected void conifgureToolBarActions() {
		final ScrolledForm form = managedForm.getForm();
		// clear toolbar
		form.getToolBarManager().removeAll();

		// add demonstrate button if we may demonstrate
		if (editor.mayDemonstrate()) {
			Action recordAction = new Action(
					"demonstrate", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.startRevisingOriginModel();
					refresh();
				}
			};
			recordAction
					.setToolTipText("Start demonstrating the model operation");
			recordAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_RECORD));
			form.getToolBarManager().add(recordAction);
		}

		// add re-demonstrate button if we may re-demonstrate
		if (editor.mayReDemonstrate()) {
			Action rerecordAction = new Action(
					"redemonstrate", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.startRevisingOriginModel();
					refresh();
				}
			};
			rerecordAction.setToolTipText("Re-demonstrate the model operation");
			rerecordAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_RERECORD));
			form.getToolBarManager().add(rerecordAction);
		}

		// add commit button if we may commit a demonstration
		if (editor.mayCommit()) {
			Action commitAction = new Action("commit", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.commitWorkingCopy();
					refresh();
				}
			};
			commitAction
					.setToolTipText("Commit the demonstrated model operation");
			commitAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_COMMIT));
			form.getToolBarManager().add(commitAction);
		}

		// add post processing buttons
		if (editor.mayPostProcess()) {
			// add show model buttons
			Action showInitialModelAction = new Action(
					"showInitial", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.showInitialModel();
				}
			};
			showInitialModelAction
					.setToolTipText("Export and open initial model");
			showInitialModelAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_INTIALMODEL));
			form.getToolBarManager().add(showInitialModelAction);

			Action showRevisedModelAction = new Action(
					"showRevised", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.showRevisedModel();
				}
			};
			showRevisedModelAction
					.setToolTipText("Export and open revised model");
			showRevisedModelAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_REVISEDMODEL));
			form.getToolBarManager().add(showRevisedModelAction);

			form.getToolBarManager().add(
					new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

			// add show conditions buttons
			Action showPreconditionsAction = new Action(
					"showPreconditions", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.openPreconditionView();
				}
			};
			showPreconditionsAction.setToolTipText("Show preconditions");
			showPreconditionsAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_PRECONDITIONS));
			form.getToolBarManager().add(showPreconditionsAction);

			Action showPostconditionsAction = new Action(
					"showPostconditions", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.openPostconditionsView();
				}
			};
			showPostconditionsAction.setToolTipText("Show postconditions");
			showPostconditionsAction.setImageDescriptor(OperationsUIPlugin
					.getImageDescriptor(OperationsUIPlugin.IMG_POSTCONDITIONS));
			form.getToolBarManager().add(showPostconditionsAction);

			form.getToolBarManager().add(
					new Separator(IWorkbenchActionConstants.MB_ADDITIONS));

			form.getToolBarManager().add(
					new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
			// add register button
			Action registerAction = new Action(
					"register", Action.AS_PUSH_BUTTON) { //$NON-NLS-1$
				public void run() {
					editor.registerOperationSpecification();
				}
			};
			registerAction.setToolTipText("Register the model operation");
			registerAction
					.setImageDescriptor(OperationsExecutionUIPlugin
							.getImageDescriptor(OperationsExecutionUIPlugin.IMG_REGISTER));
			form.getToolBarManager().add(registerAction);
		}

		form.getToolBarManager().update(true);
	}

	/**
	 * Creates all elements in the left section
	 * 
	 * @param tk
	 *            the formToolkit object
	 * @param form
	 *            the ScrolledForm object
	 */
	private void createLeftSection(FormToolkit tk, ScrolledForm form) {

		Composite container = tk.createComposite(form.getBody());
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		container.setLayoutData(td);

		// mandatory arguments
		Section sm = tk.createSection(container, Section.DESCRIPTION
				| Section.TITLE_BAR);

		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);

		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		sm.setLayoutData(gd);

		sm.setText("Details");
		sm.setDescription("General information about the composite operation.");

		Composite sectionClient = tk.createComposite(sm);
		sectionClient.setLayout(new GridLayout(2, false));

		gd = new GridData();
		Label labName = tk.createLabel(sectionClient, "Name:");
		labName.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		this.txtName = tk.createText(sectionClient, editor
				.getOperationSpecification().getName(), SWT.BORDER);
		this.txtName.setLayoutData(gd);

		gd = new GridData();
		Label labVers = tk.createLabel(sectionClient, "Version:");
		labVers.setLayoutData(gd);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		this.txtVersion = tk.createText(sectionClient, editor
				.getOperationSpecification().getVersion(), SWT.BORDER);
		this.txtVersion.setLayoutData(gd);

		gd = new GridData();
		Label labTemp = tk.createLabel(sectionClient, "Title Template:");
		labTemp.setLayoutData(gd);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		this.txtTitleTemplate = tk.createText(sectionClient, editor
				.getOperationSpecification().getTitleTemplate(), SWT.BORDER);
		this.txtTitleTemplate.setLayoutData(gd);

		gd = new GridData();
		Label labLang = tk.createLabel(sectionClient, "Language:");
		labLang.setLayoutData(gd);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		this.txtModelingLanguage = tk.createText(sectionClient, editor
				.getOperationSpecification().getModelingLanguage(), SWT.BORDER);
		this.txtModelingLanguage.setEditable(false);
		this.txtModelingLanguage.setLayoutData(gd);

		gd = new GridData();
		Label labDescr = tk.createLabel(sectionClient, "Description:");
		labDescr.setLayoutData(gd);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.grabExcessVerticalSpace = true;
		gd.heightHint = 100;
		this.txtDescription = tk.createText(sectionClient, editor
				.getOperationSpecification().getDescription(), SWT.BORDER
				| SWT.MULTI);
		this.txtDescription.setLayoutData(gd);

		sm.setClient(sectionClient);
	}

	/**
	 * Creates the right section.
	 * 
	 * @param tk
	 *            form toolkit to use.
	 * @param form
	 *            scrolled form to add controls to.
	 */
	private void createRightSection(FormToolkit tk, ScrolledForm form) {
		Composite rightContainer = tk.createComposite(form.getBody());
		rightContainer.setLayoutData(new TableWrapData());
		rightContainer.setLayout(new TableWrapLayout());
		rightContainer.setLayoutData(new TableWrapData());

		// help section
		Section helpSection = tk.createSection(rightContainer,
				Section.TITLE_BAR);

		helpSection.setLayout(new TableWrapLayout());
		helpSection.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));
		helpSection.setText("Help");
		Composite sectionHelpComposite = tk.createComposite(helpSection);
		sectionHelpComposite.setLayout(new TableWrapLayout());
		sectionHelpComposite.setLayoutData(new TableWrapData());
		// help text
		FormText helpText = tk.createFormText(sectionHelpComposite, true);
		helpText.setLayoutData(new TableWrapData(TableWrapData.FILL));
		helpText.setWhitespaceNormalized(true);
		Image recordImage = OperationsUIPlugin
				.getImage(OperationsUIPlugin.IMG_RECORD);
		helpText.setImage("recordButton", recordImage);
		Image preconditionsImage = OperationsUIPlugin
				.getImage(OperationsUIPlugin.IMG_PRECONDITIONS);
		helpText.setImage("preconditions", preconditionsImage);
		Image postconditionsImage = OperationsUIPlugin
				.getImage(OperationsUIPlugin.IMG_POSTCONDITIONS);
		helpText.setImage("postconditions", postconditionsImage);
		Image openInitialImage = OperationsUIPlugin
				.getImage(OperationsUIPlugin.IMG_INTIALMODEL);
		helpText.setImage("initialmodel", openInitialImage);
		Image openRevisedImage = OperationsUIPlugin
				.getImage(OperationsUIPlugin.IMG_REVISEDMODEL);
		helpText.setImage("revisedmodel", openRevisedImage);
		Image registerImage = OperationsExecutionUIPlugin
				.getImage(OperationsExecutionUIPlugin.IMG_REGISTER);
		helpText.setImage("register", registerImage);
		if (editor.mayDemonstrate()) {
			helpText.setText("<form>" + "<p>"
					+ "To specify an <b>in-place transformation</b>, push the "
					+ "record button (<img href=\"recordButton\"/>) and "
					+ "demonstrate the model transformation.</p>"
					+ "<p>After that, you may specify iterations and "
					+ "user inputs in " + "<b>configuration</b> tab.</p>"
					+ "<p>For more information on <b>Model Transformation "
					+ "By Demonstration</b> please consult "
					+ "<b>http://www.modelversioning.org/"
					+ "operation-recorder</b>.</p>" + "</form>", true, false);
		} else {
			helpText.setText("<form>" + "<p>"
					+ "Now you may specify iterations and " + "user inputs in "
					+ "<b>configuration</b> tab.</p>"
					+ "<p>Push the buttons on the upper right to show "
					+ "and configure the "
					+ "preconditions (<img href=\"preconditions\"/>), "
					+ "the postconditions (<img href=\"postconditions\"/>), "
					+ "export and open "
					+ "the initial model (<img href=\"initialmodel\"/>), "
					+ "the revised model (<img href=\"revisedmodel\"/>) "
					+ "or export the model operation "
					+ "(<img href=\"register\"/>) " + "to the local model "
					+ "operation repository." + "</p>"
					+ "<p>For more information on \"Model Transformation "
					+ "By Demonstration\" please consult</p>"
					+ "<p><b>http://www.modelversioning.org/"
					+ "operation-recorder</b>.</p>" + "</form>", true, false);
		}
		helpText.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				if ("configTab".equals(e.getHref())) { //$NON-NLS-1$
					editor.openConfigurationTab();
				}
			}
		});
		helpSection.setClient(sectionHelpComposite);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFocus() {
		this.txtName.setFocus();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Writes the values to the underlying model.
	 */
	@Override
	public void modifyText(ModifyEvent e) {
		editor.setDirty(true);
		doCommit();
	}

	/**
	 * Writes the control's values to the underlying model.
	 */
	protected void doCommit() {
		editor.getOperationSpecification().setName(txtName.getText());
		editor.getOperationSpecification().setDescription(
				txtDescription.getText());
		editor.getOperationSpecification().setVersion(txtVersion.getText());
		editor.getOperationSpecification().setModelingLanguage(
				txtModelingLanguage.getText());
		editor.getOperationSpecification().setTitleTemplate(
				txtTitleTemplate.getText());
	}

	/**
	 * Refreshes this view.
	 */
	public void refresh() {
		conifgureToolBarActions();
	}

}
