package org.modelversioning.operations.ui.pages;

import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.compare.util.AdapterUtils;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.ui.dialogs.IterationsContentProvider;
import org.modelversioning.operations.ui.dialogs.IterationsLabelProvider;
import org.modelversioning.operations.ui.dialogs.UserInputContentProvider;
import org.modelversioning.operations.ui.dialogs.UserInputLabelProvider;
import org.modelversioning.operations.ui.dialogs.UserInputSelectionDialog;
import org.modelversioning.operations.ui.diff.OperationDiffContentProvider;
import org.modelversioning.operations.ui.editors.OperationRecorderEditor;
import org.modelversioning.operations.ui.views.actions.ConvertAddToCopyAction;
import org.modelversioning.operations.ui.wizards.NewIterationWizard;

/**
 * Configuration page for {@link OperationSpecification}s incorporating a
 * working copy.
 * 
 * @author Martina Seidl
 * @author Felix Rinker
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationConfigurationPage extends FormPage {

	private OperationRecorderEditor editor;
	private TreeViewer userItreeViewer;
	private TreeViewer iterTreeViewer;
	private Tree userItree;
	private Tree iterTree;

	/**
	 * Constructs the operation definition page.
	 * 
	 * @param editor
	 *            the containing operation recorder editor
	 * @param id
	 *            the id of this page
	 * @param title
	 *            the title of the page
	 * 
	 */
	public OperationConfigurationPage(OperationRecorderEditor editor,
			String id, String title) {
		super(editor, id, title);
		this.editor = editor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {

		FormToolkit tk = new FormToolkit(Display.getCurrent());
		ScrolledForm form = managedForm.getForm();

		form.setText("Advanced Configuration"); // heading
		tk.paintBordersFor(form);
		tk.decorateFormHeading(form.getForm());

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		form.getBody().setLayout(layout);

		createDifferences(tk, form);
		createConfiguration(tk, form);
	}

	/**
	 * Constructs the model differences section
	 * 
	 * @param tk
	 *            the form toolkit
	 * @param form
	 *            the scrolled form
	 * 
	 * @see {@link FormToolkit}
	 * @see {@link ScrolledForm}
	 */
	private void createDifferences(FormToolkit tk, ScrolledForm form) {

		Composite container = tk.createComposite(form.getBody());
		GridData gd = new GridData(GridData.FILL_BOTH);
		container.setLayoutData(gd);

		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);

		// mandatory arguments
		Section sm = tk.createSection(container, Section.DESCRIPTION
				| Section.TITLE_BAR);

		gd = new GridData(GridData.FILL_BOTH);
		sm.setLayoutData(gd);

		sm.setText("Differences");
		sm.setDescription("The differences between the "
				+ "original model and the revised model.");

		Composite sectionClient = tk.createComposite(sm);
		sectionClient.setLayout(new GridLayout(2, false));
		sectionClient.setLayoutData(new GridData(GridData.FILL_BOTH));

		gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 100;

		// Tree View
		final TreeViewer differenceTreeViewer = new TreeViewer(sectionClient,
				GridData.FILL_BOTH);

		Tree tree = differenceTreeViewer.getTree();
		tree.setHeaderVisible(false);
		tree.setLayoutData(gd);

		// Set content and label provider
		differenceTreeViewer
				.setContentProvider(new OperationDiffContentProvider());
		differenceTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(
				AdapterUtils.getAdapterFactory()));

		// Set the difference model
		differenceTreeViewer.setInput(editor.getOperationSpecification()
				.getDifferenceModel().getDiff());

		// add difference buttons
		Composite buttonContainer = tk.createComposite(sectionClient);
		buttonContainer.setLayoutData(new GridData());

		buttonContainer.setLayout(new GridLayout());

		final Button addToCopyButton = tk.createButton(buttonContainer,
				"Add to Copy", SWT.BUTTON1);

		final ConvertAddToCopyAction convertAddToCopyAction = new ConvertAddToCopyAction();
		convertAddToCopyAction.setConditionsModel(editor
				.getOperationSpecification().getPreconditions());
		addToCopyButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		addToCopyButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (differenceTreeViewer.getTree().getSelectionCount() == 1) {
					TreeItem item = differenceTreeViewer.getTree()
							.getSelection()[0];
					if (!(item.getData() instanceof CopyElementLeftTarget)
							&& item.getData() instanceof ModelElementChangeLeftTarget) {
						ModelElementChangeLeftTarget addDiffElement = (ModelElementChangeLeftTarget) item
								.getData();
						convertAddToCopyAction
								.setAddModelElement(addDiffElement);
						convertAddToCopyAction.run();
						editor.setDirty(true);
						differenceTreeViewer.refresh(true);
					}

				}
			}
		});
		addToCopyButton.setEnabled(false);

		final Button copyToAddButton = tk.createButton(buttonContainer,
				"Copy to Add", SWT.BUTTON1);

		copyToAddButton.setLayoutData(new GridData());
		copyToAddButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (differenceTreeViewer.getTree().getSelectionCount() == 1) {
					TreeItem item = differenceTreeViewer.getTree()
							.getSelection()[0];
					if (item.getData() instanceof CopyElementLeftTarget) {
						CopyElementLeftTarget copyElement = (CopyElementLeftTarget) item
								.getData();
						convertAddToCopyAction.setCopyModelElement(copyElement);
						convertAddToCopyAction.run();
						editor.setDirty(true);
						differenceTreeViewer.refresh(true);
					}

				}
			}
		});
		copyToAddButton.setEnabled(false);

		differenceTreeViewer.expandAll();

		// add listener for tree viewer to enable or disable "add to copy"
		// buttons
		differenceTreeViewer
				.addSelectionChangedListener(new ISelectionChangedListener() {
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						if (differenceTreeViewer.getTree().getSelectionCount() == 1) {
							TreeItem item = differenceTreeViewer.getTree()
									.getSelection()[0];
							if (!(item.getData() instanceof CopyElementLeftTarget)
									&& item.getData() instanceof ModelElementChangeLeftTarget) {
								copyToAddButton.setEnabled(false);
								addToCopyButton.setEnabled(true);
							} else if (item.getData() instanceof CopyElementLeftTarget) {
								copyToAddButton.setEnabled(true);
								addToCopyButton.setEnabled(false);
							} else {
								copyToAddButton.setEnabled(false);
								addToCopyButton.setEnabled(false);
							}
						}
					}
				});

		sm.setClient(sectionClient);
	}

	/**
	 * Constructs the configuration section. The section include areas for
	 * configure the iterations and templates. Further several buttons are
	 * included.
	 * 
	 * @param tk
	 *            the form toolkit
	 * @param form
	 *            the scrolled form
	 * 
	 * @see {@link FormToolkit}
	 * @see {@link ScrolledForm}
	 */
	private void createConfiguration(FormToolkit tk, ScrolledForm form) {

		Composite container = tk.createComposite(form.getBody());
		GridData gd = new GridData(GridData.FILL_BOTH);
		container.setLayoutData(gd);

		// mandatory arguments
		Section iterationSection = tk.createSection(container,
				Section.DESCRIPTION | Section.TITLE_BAR);

		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);

		gd = new GridData(GridData.FILL_BOTH);
		iterationSection.setLayoutData(gd);

		iterationSection.setLayout(new GridLayout());
		iterationSection.setText("Iterations");
		iterationSection
				.setDescription("Add iterations for templates to enable "
						+ "repeated transformations of multiple model "
						+ "elements at once.");

		Composite iterationComposite = tk.createComposite(iterationSection);
		iterationComposite.setLayout(new GridLayout(2, false));
		iterationComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

		// /////////////////
		// Iteration Tree
		// ////////////////
		iterTreeViewer = new TreeViewer(iterationComposite, SWT.MULTI
				| Section.EXPANDED | SWT.FULL_SELECTION);

		iterTree = iterTreeViewer.getTree();
		iterTree.setHeaderVisible(true);
		gd = new GridData(GridData.FILL_VERTICAL);
		gd.heightHint = 100;
		iterTree.setLayoutData(gd);

		// Set label and content provider
		iterTreeViewer.setLabelProvider(new IterationsLabelProvider());
		iterTreeViewer.setContentProvider(new IterationsContentProvider());

		// set the content
		iterTreeViewer.setInput(editor.getOperationSpecification());

		TreeColumn symbolColumn = new TreeColumn(iterTree, SWT.NONE);
		symbolColumn.setText("Template");
		symbolColumn.setWidth(300);

		// TreeColumn iterationColumn = new TreeColumn(iterTree, SWT.NONE);
		// iterationColumn.setText("Iteration Type");
		// iterationColumn.setWidth(150);

		Composite buttonContainer = tk.createComposite(iterationComposite);
		buttonContainer.setLayoutData(new GridData());

		buttonContainer.setLayout(new GridLayout());

		Button addButton = tk.createButton(buttonContainer, "Add", SWT.BUTTON1);

		addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		addButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				NewIterationWizard wizard = new NewIterationWizard(editor
						.getOperationSpecification());
				WizardDialog wizardDialog = new WizardDialog(getSite()
						.getShell(), wizard);
				if (wizardDialog.open() == Dialog.OK) {
					refresh();
					editor.setDirty(true);
				}
			}
		});

		Button delButton = tk.createButton(buttonContainer, "Delete",
				SWT.BUTTON1);

		delButton.setLayoutData(new GridData());
		delButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (iterTree.getSelection().length != 0) {
					removeIteration((Iteration) iterTree.getSelection()[0]
							.getData());
				}
			}
		});

		iterationSection.setClient(iterationComposite);

		Section userInputSection = tk.createSection(container,
				Section.DESCRIPTION | Section.TITLE_BAR);

		layout = new GridLayout(1, false);
		container.setLayout(layout);

		gd = new GridData(GridData.FILL_BOTH);
		userInputSection.setLayoutData(gd);

		userInputSection.setLayout(new GridLayout());
		userInputSection.setText("User Inputs");
		userInputSection
				.setDescription("Configure User Inputs here if necessary.");

		Composite userInputComposite = tk.createComposite(userInputSection);
		userInputComposite.setLayout(new GridLayout(2, false));
		userInputComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

		// /////////////////
		// User Input Tree
		// ////////////////
		userItreeViewer = new TreeViewer(userInputComposite, SWT.MULTI
				| Section.EXPANDED | SWT.FULL_SELECTION);

		userItree = userItreeViewer.getTree();
		userItree.setHeaderVisible(true);
		userItree.setLayoutData(new GridData(GridData.FILL_VERTICAL));

		// Set content and label provider
		userItreeViewer.setLabelProvider(new UserInputLabelProvider());
		userItreeViewer.setContentProvider(new UserInputContentProvider());

		// set the content
		userItreeViewer.setInput(editor.getOperationSpecification());

		TreeColumn nameUIColumn;
		nameUIColumn = new TreeColumn(userItree, SWT.NONE);
		nameUIColumn.setText("Name");
		nameUIColumn.setWidth(150);

		TreeColumn tempUIColumn;
		tempUIColumn = new TreeColumn(userItree, SWT.NONE);
		tempUIColumn.setText("Template");
		tempUIColumn.setWidth(150);

		TreeColumn featureUIColumn;
		featureUIColumn = new TreeColumn(userItree, SWT.NONE);
		featureUIColumn.setText("Feature");
		featureUIColumn.setWidth(150);

		Composite buttonContainer2 = tk.createComposite(userInputComposite);
		buttonContainer2.setLayoutData(new GridData());

		buttonContainer2.setLayout(new GridLayout());

		Button addButton2 = tk.createButton(buttonContainer2, "Add",
				SWT.BUTTON1);
		addButton2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		addButton2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				UserInputSelectionDialog dialog = new UserInputSelectionDialog(
						PlatformUI.getWorkbench().getDisplay().getActiveShell()
								.getShell(), ConditionsUtil
								.getAllTemplates(editor
										.getOperationSpecification()
										.getPostconditions()));
				int button = dialog.open();
				if (button == 0) {
					addUserInput(dialog.getUserInputName(),
							dialog.getSelectedTemplate(), dialog.getFeature());
				}
			}
		});

		Button delButton2 = tk.createButton(buttonContainer2, "Delete",
				SWT.BUTTON1);
		delButton2.setLayoutData(new GridData());
		delButton2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (userItree.getSelection().length != 0) {
					removeUserInput((UserInput) userItree.getSelection()[0]
							.getData());
				}
			}
		});

		userInputSection.setClient(userInputComposite);

		// refresh the trees
		refresh();
	}

	/**
	 * Add the user input from the general configuration site to the operation
	 * specification object.
	 * 
	 * @param name
	 *            the Name of the operation specification
	 * @param template
	 *            the set Template
	 * @param feature
	 *            the set Feature
	 */
	public void addUserInput(String name, Template template,
			EStructuralFeature feature) {
		UserInput ui = OperationsFactory.eINSTANCE.createUserInput();
		ui.setName(name);
		ui.setTemplate(template);
		ui.setFeature(feature);
		editor.getOperationSpecification().getUserInputs().add(ui);
		editor.setDirty(true);
		refresh();
	}

	/**
	 * Removes a iteration from the {@link OperationSpecification}
	 * 
	 * @param iteration
	 *            the iteration
	 * 
	 * @see {@link Iteration}
	 */
	public void removeIteration(Iteration iteration) {
		editor.getOperationSpecification().getIterations().remove(iteration);
		editor.setDirty(true);
		refresh();
	}

	/**
	 * Removes a {@link UserInput} from the {@link OperationSpecification}
	 * 
	 * @param userInput
	 *            user input to remove
	 * 
	 * @see {@link UserInput}
	 */
	public void removeUserInput(UserInput userInput) {
		editor.getOperationSpecification().getUserInputs().remove(userInput);
		editor.setDirty(true);
		refresh();
	}

	public void refresh() {
		iterTreeViewer.refresh();
		iterTree.redraw();
		userItreeViewer.refresh();
		userItree.redraw();
	}

}
