/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views.actions;

import java.util.Iterator;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * @author Felix Rinker
 * 
 */
public class ValidateConditionAction extends AbstractConditionsModelAction {

	private static final String CONDITION_OK_MSG = "The condition is valid.";
	private static final String CONDITION_OK_TITLE = "Validation Success";
	private static final String CONDITION_ERR_MSG = "The condition is invalid.";
	private static final String CONDITION_ERR_TITLE = "Validation Error";
	private static final String CONDITION_EVAL_ENGINE_ERR_MSG = "Could not create suitable condition evaluation engine.";

	private IConditionEvaluationEngine evaluationEngine = new ConditionsEvaluationEngineImpl();

	/**
	 * Action to validate the selected condition in the specified
	 * <code>treeViewer</code>.
	 * 
	 * @param treeViewer
	 *            to get selected condition from.
	 */
	public ValidateConditionAction(TreeViewer treeViewer) {
		super(treeViewer);
		super.setSelectionCountMin(1);
		super.setSelectionCountMax(1);

		this.setText("Validate Condition");
		this.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_DEC_FIELD_WARNING));
		this.setToolTipText("Syntactically validates the selected condition.");
	}

	/**
	 * Initiates validating of the selected condition.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		if (getTreeViewer().getInput() instanceof ConditionsModel) {
			IStructuredSelection selection = getSelection();
			if (selection != null) {
				try {
					evaluationEngine
							.setConditionsModel((ConditionsModel) getTreeViewer()
									.getInput());

					Iterator<Object> iterator = selection.iterator();
					while (iterator.hasNext()) {
						Object next = iterator.next();
						if (next instanceof Condition) {
							Condition condition = (Condition) next;
							EvaluationResult validate = evaluationEngine
									.validate(condition);
							if (validate.isOK()) {
								showMessage(CONDITION_OK_TITLE,
										CONDITION_OK_MSG);
							} else {
								showErrorMessage(CONDITION_ERR_TITLE,
										CONDITION_ERR_MSG);
							}
						}
					}
				} catch (UnsupportedConditionLanguage e) {
					showErrorMessage(CONDITION_ERR_TITLE,
							CONDITION_EVAL_ENGINE_ERR_MSG);
				}
			}
		}
	}

}
