/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Display;
import org.modelversioning.core.conditions.ConditionsModel;

/**
 * Abstract {@link Action} for {@link ConditionsModel}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public abstract class AbstractConditionsModelAction extends Action {

	/**
	 * The tree viewer for which this action is created.
	 */
	private TreeViewer treeViewer;

	/**
	 * The maximum of allowed selected items to execute the action.
	 */
	private int selectionCountMax = 1;

	/**
	 * The minimum of allowed selected items to execute the action.
	 */
	private int selectionCountMin = 1;

	/**
	 * @param treeViewer
	 *            the {@link TreeViewer} to set.
	 */
	protected AbstractConditionsModelAction(TreeViewer treeViewer) {
		super();
		this.treeViewer = treeViewer;
	}

	/**
	 * @return the selectionCountMax
	 */
	protected int getSelectionCountMax() {
		return selectionCountMax;
	}

	/**
	 * @param selectionCountMax
	 *            the selectionCountMax to set
	 */
	protected void setSelectionCountMax(int selectionCountMax) {
		this.selectionCountMax = selectionCountMax;
	}

	/**
	 * @return the selectionCountMin
	 */
	protected int getSelectionCountMin() {
		return selectionCountMin;
	}

	/**
	 * @param selectionCountMin
	 *            the selectionCountMin to set
	 */
	protected void setSelectionCountMin(int selectionCountMin) {
		this.selectionCountMin = selectionCountMin;
	}

	/**
	 * @return the treeViewer
	 */
	protected TreeViewer getTreeViewer() {
		return treeViewer;
	}

	/**
	 * @param treeViewer
	 *            the treeViewer to set
	 */
	protected void setTreeViewer(TreeViewer treeViewer) {
		this.treeViewer = treeViewer;
	}

	/**
	 * Synchronizes the specified <code>runnable</code>.
	 * 
	 * @param runnable
	 *            to synchronize.
	 */
	protected void syncExec(Runnable runnable) {
		if (Display.getCurrent() == null) {
			Display.getDefault().syncExec(runnable);
		} else {
			runnable.run();
		}
	}

	/**
	 * Shows a message with the specified message.
	 * 
	 * @param title
	 *            to show.
	 * @param message
	 *            to show.
	 */
	protected void showMessage(String title, String message) {
		MessageDialog.openInformation(treeViewer.getControl().getShell(),
				title, message);
	}

	/**
	 * Shows a error message with the specified message.
	 * 
	 * @param title
	 *            to show.
	 * @param message
	 *            to show.
	 */
	protected void showErrorMessage(String title, String message) {
		MessageDialog.openError(treeViewer.getControl().getShell(), title,
				message);
	}

	/**
	 * Returns the selected items in the tree viewer ({@link #getTreeViewer()}.
	 * 
	 * If the selection exceeds {@link #getSelectionCountMax()} or deceeds
	 * {@link #getSelectionCountMin()} this method shows a message to the user
	 * and returns <code>null</code>.
	 * 
	 * @return the selection.
	 */
	protected IStructuredSelection getSelection() {
		ISelection selection = treeViewer.getSelection();

		// guard no selection
		if (selectionCountMin != -1
				&& ((IStructuredSelection) selection).size() < selectionCountMin) {
			showMessage("Missing selection",
					"This action can only be executed if there is at least one item selected.");
			super.run();
			return null;
		}

		// guard multi-selection
		if (selectionCountMax != -1
				&& ((IStructuredSelection) selection).size() > selectionCountMax) {
			showMessage("Too many selections",
					"This action can only be executed if there are at most "
							+ selectionCountMax + " selected items.");
			super.run();
			return null;
		}

		return (IStructuredSelection) selection;
	}
}
