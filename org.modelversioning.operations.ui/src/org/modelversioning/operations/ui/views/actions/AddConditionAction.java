/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.views.actions;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.operations.ui.commons.OperationsUICommonsPlugin;
import org.modelversioning.operations.ui.commons.wizards.NewConditionWizard;
import org.modelversioning.operations.ui.views.ConditionView;

/**
 * Action initiating the addition of a new {@link Condition}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * @author Felix Rinker
 * 
 */
public class AddConditionAction extends AbstractConditionsModelAction {

	/** The parent condition view */
	private ConditionView conditionView;

	/**
	 * Constructing a new add condition action for the specified tree viewer.
	 * 
	 * @param treeViewer
	 *            tree viewer for which this action is made for.
	 */
	public AddConditionAction(TreeViewer treeViewer, ConditionView conditionView) {
		super(treeViewer);
		super.setSelectionCountMin(-1);
		super.setSelectionCountMax(-1);

		this.conditionView = conditionView;
		this.setText("Add Condition");
		this.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages()
				.getImageDescriptor(ISharedImages.IMG_OBJ_ADD));
	}

	/**
	 * Initiates the addition of a new condition.
	 */
	@Override
	public void run() {
		if (getTreeViewer().getInput() instanceof ConditionsModel) {
			Runnable runnable = new Runnable() {
				public void run() {
					NewConditionWizard wizard = new NewConditionWizard(
							(ConditionsModel) getTreeViewer().getInput());
					wizard.setSelection(getSelection());
					WizardDialog wizardDialog = new WizardDialog(
							OperationsUICommonsPlugin.getShell(), wizard);
					if (wizardDialog.open() == Dialog.OK) {
						conditionView.openConditionEditingDialog(wizard
								.getCreatedCondition()).open();
					}
				}
			};
			syncExec(runnable);
			conditionView.refresh();
		}
	}

}
