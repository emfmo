/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.ui.provider;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.modelversioning.operations.NegativeApplicationCondition;
import org.modelversioning.operations.ui.OperationsUIPlugin;

/**
 * Label provider for {@link NegativeApplicationCondition}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class NegativeApplicationConditionLabelProvider extends LabelProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Image getImage(Object element) {
		if (element instanceof NegativeApplicationCondition) {
			return OperationsUIPlugin.getImage(OperationsUIPlugin.IMG_NAC);
		}
		return super.getImage(element);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText(Object element) {
		if (element instanceof NegativeApplicationCondition) {
			return ((NegativeApplicationCondition) element).getName();
		}
		return super.getText(element);
	}

}
