package org.modelversioning.operations.ui.provider;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.Template;

/**
 * @author Martina Seidl content provider class for conditions view
 */
public class ConditionsViewContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getChildren(Object parentElement) {

		if (parentElement instanceof ConditionsModel) {
			Object[] tmp = new Object[1];
			tmp[0] = ((ConditionsModel) parentElement).getRootTemplate();

			return tmp;
		}

		if (parentElement instanceof Template) {
			Object[] conds = ((Template) parentElement).getSpecifications()
					.toArray();
			Object[] ssyms = ((Template) parentElement).getSubTemplates()
					.toArray();

			Object[] tmp = new Object[conds.length + ssyms.length];

			int i;

			for (i = 0; i < conds.length; i++) {
				tmp[i] = conds[i];
			}

			for (int j = 0; j < ssyms.length; j++) {
				tmp[i + j] = ssyms[j];
			}

			return tmp;

		}

		// if (parentElement instanceof FeatureCondition) {
		// return ((FeatureCondition) parentElement).getExpressions()
		// .toArray();
		// }

		return null;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof Template) {
			return ((Template) element).getParentTemplate();
		}

		if (element instanceof Condition) {
			return ((Condition) element).getTemplate();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {

		if (element instanceof Template) {
			return ((((Template) element).getSpecifications().toArray().length != 0) || (((Template) element)
					.getSubTemplates().toArray().length != 0));
		}

		// if (element instanceof FeatureCondition) {
		// return ((FeatureCondition) element).getExpressions().size() > 1;
		// }

		return false;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}
}
