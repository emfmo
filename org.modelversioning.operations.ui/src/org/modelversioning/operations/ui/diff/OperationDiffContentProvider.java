package org.modelversioning.operations.ui.diff;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.AbstractDiffExtension;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffGroup;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class OperationDiffContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getChildren(Object parentElement) {

		if (parentElement instanceof DiffGroup) {
			return filterDiffElements(
					((DiffGroup) parentElement).getSubDiffElements()).toArray();
		}

		if (parentElement instanceof DiffModel) {
			return filterDiffElements(
					((DiffModel) parentElement).getOwnedElements()).toArray();
		}

		if (parentElement instanceof AbstractDiffExtension) {
			return ((AbstractDiffExtension) parentElement).getHideElements()
					.toArray();
		}

		if (parentElement instanceof DiffElement) {
			return filterDiffElements(
					((DiffElement) parentElement).getSubDiffElements())
					.toArray();
		}

		return null;
	}

	private List<DiffElement> filterDiffElements(EList<DiffElement> diffElements) {
		List<DiffElement> filteredDiffElements = new ArrayList<DiffElement>();
		for (DiffElement diffElement : diffElements) {
			// check if diffElement is hidden
			if (diffElement.getIsHiddenBy().size() == 0) {
				// check if diffElement is a group and only contains hidden diff
				// elements
				boolean hide = false;
				if (diffElement instanceof DiffGroup) {
					if (filterDiffElements(diffElement.getSubDiffElements())
							.size() < 1) {
						hide = true;
					}
				}
				if (!hide) {
					filteredDiffElements.add(diffElement);
				}
			}
		}
		return filteredDiffElements;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
