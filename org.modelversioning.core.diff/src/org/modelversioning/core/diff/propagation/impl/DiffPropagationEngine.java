/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.propagation.impl;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.FactoryException;
import org.eclipse.emf.compare.diff.merge.EMFCompareEObjectCopier;
import org.eclipse.emf.compare.diff.merge.IMergeListener;
import org.eclipse.emf.compare.diff.merge.MergeEvent;
import org.eclipse.emf.compare.diff.merge.service.MergeService;
import org.eclipse.emf.compare.diff.metamodel.AttributeChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.AttributeChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.ConflictingDiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffFactory;
import org.eclipse.emf.compare.diff.metamodel.DiffGroup;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.MoveModelElement;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.UpdateAttribute;
import org.eclipse.emf.compare.diff.metamodel.UpdateReference;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.compare.util.EFactory;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.modelversioning.core.diff.DiffPlugin;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;
import org.modelversioning.core.diff.copydiff.CopyElementRightTarget;
import org.modelversioning.core.diff.propagation.IDiffPropagationEngine;
import org.modelversioning.core.diff.propagation.IPropagationMappingProvider;
import org.modelversioning.core.diff.util.DiffUtil;
import org.modelversioning.core.match.util.MatchUtil;
import org.modelversioning.core.util.UUIDUtil;

/**
 * Default implementation of the {@link IDiffPropagationEngine} re-wiring
 * {@link DiffElement}s and using the {@link MergeService} to execute changes.
 * 
 * TODO deactivate diff element if there is no subject or object provided for it
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffPropagationEngine implements IDiffPropagationEngine,
		IMergeListener {

	/**
	 * Error message to indicate that we cannot execute conflicting diff
	 * elements.
	 */
	private static final String CANNOT_EXECUTE_CONFLICTING_DIFF = "Cannot execute conflicting diff";

	/**
	 * The match model.
	 */
	private MatchModel matchModel;
	/**
	 * The diff model to execute.
	 */
	private DiffModel executionDiff;
	/**
	 * The mapping provider to use.
	 */
	private IPropagationMappingProvider mappingProvider;
	/**
	 * The copier instance used by the EMF compare merge service to resolve
	 * copied (created) objects.
	 */
	private EMFCompareEObjectCopier copier;
	/**
	 * Additional {@link DiffElement DiffElements} due to multiple counterparts
	 * to add for execution.
	 */
	private Set<DiffElement> diffElementsToAdd = new HashSet<DiffElement>();

	/**
	 * Set of {@link DiffElement DiffElements} to deactivate due to lacking
	 * counterparts.
	 */
	private Set<DiffElement> deactivatedDiffElements = new HashSet<DiffElement>();

	/**
	 * Saves the added (copied) objects additionally to the {@link #copier}
	 * since the copier only saves the last value.
	 */
	private Map<EObject, EList<EObject>> addedEObjects = new HashMap<EObject, EList<EObject>>();

	/**
	 * Specifies whether this engine should add the container only and omit its
	 * containments. This is only useful if you incrementally evolve the model
	 * and diff again after each step (then the omitted containments get
	 * reported as new add diff element).
	 * 
	 * Default should be <code>false</code>.
	 */
	private boolean addContainerOnly = false;

	/**
	 * If {@link #addContainerOnly} is <code>true</code>, this list keeps track
	 * of containments that should be omitted.
	 */
	private Set<EObject> containmentsToOmit = new HashSet<EObject>();

	/**
	 * Returns whether this engine should add the container only and omit its
	 * containments.
	 * 
	 * <p>
	 * This is only useful if you incrementally evolve the model and diff again
	 * after each step (then the omitted containments get reported as new add
	 * diff element)
	 * </p>
	 * 
	 * <p>
	 * Default is <code>false</code>.
	 * </p>
	 * 
	 * @return <code>true</code> if only container should be added.
	 *         <code>false</code> otherwise.
	 */
	public boolean isAddContainerOnly() {
		return addContainerOnly;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAddContainerOnly(boolean addContainerOnly) {
		this.addContainerOnly = addContainerOnly;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void propagate(DiffModel diffModel, MatchModel matchModel,
			IPropagationMappingProvider mappingProvider,
			IProgressMonitor monitor) {
		propagate(DiffUtil.getEffectiveDiffElements(diffModel), matchModel,
				mappingProvider, monitor);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * TODO report progress to monitor
	 */
	@Override
	public void propagate(Collection<DiffElement> diffElements,
			MatchModel matchModel, IPropagationMappingProvider mappingProvider,
			IProgressMonitor monitor) {
		// reset added eobject list
		addedEObjects.clear();
		containmentsToOmit.clear();
		// set local fields
		this.matchModel = matchModel;
		this.mappingProvider = mappingProvider;
		// add me as listener to enable re-wiring added objects after additions
		MergeService.addMergeListener(this);

		// get add elements
		Collection<DiffElement> addElements = getAddElements(DiffUtil
				.shallowCopy(diffElements));

		// get change/delete elements
		Collection<DiffElement> changeDeleteElements = getChangeDeleteElements(DiffUtil
				.shallowCopy(diffElements));
		// EObject modelRoot = mappingProvider.getWorkingRootEObject();

		// create new diff model
		executionDiff = DiffFactory.eINSTANCE.createDiffModel();
		DiffGroup diffGroupAdds = DiffFactory.eINSTANCE.createDiffGroup();
		// add only add diff elements
		executionDiff.getOwnedElements().add(diffGroupAdds);
		diffGroupAdds.getSubDiffElements().addAll(addElements);
		// reset diff elements to deactivate
		deactivatedDiffElements.clear();
		if (executionDiff.getOwnedElements().size() > 0) {
			// re-wire add elements
			rewireDiffElements(executionDiff.getOwnedElements());
			// add elements to add
			addDiffElementsToDiffGroup(diffGroupAdds);
			// clear deactivated diff elements
			executionDiff.getOwnedElements().get(0).getSubDiffElements()
					.removeAll(deactivatedDiffElements);
			// execute add elments first
			execute(executionDiff.getOwnedElements());
			for (EObject addedObject : addedEObjects.keySet()) {
				rewireAddedObject(addedObject);
			}
		}

		// clear execution diff
		executionDiff.getOwnedElements().clear();
		diffElementsToAdd.clear();
		DiffGroup diffGroupChanges = DiffFactory.eINSTANCE.createDiffGroup();
		// add all change/delete elements
		diffGroupChanges.getSubDiffElements().addAll(changeDeleteElements);
		executionDiff.getOwnedElements().add(diffGroupChanges);
		// reset diff elements to deactivate
		deactivatedDiffElements.clear();
		// re-wire them
		rewireDiffElements(executionDiff.getOwnedElements());
		// add elements to add
		addDiffElementsToDiffGroup(diffGroupChanges);
		// clear deactivated diff elements
		executionDiff.getOwnedElements().get(0).getSubDiffElements()
				.removeAll(deactivatedDiffElements);
		// execute remaining diff elements
		execute(executionDiff.getOwnedElements());

		// detach listener
		MergeService.removeMergeListener(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EMFCompareEObjectCopier getCopier() {
		return copier;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EList<EObject> getCopies(EObject original) {
		if (addedEObjects.get(original) != null
				&& addedEObjects.get(original).size() > 0) {
			return ECollections.unmodifiableEList(addedEObjects.get(original));
		} else {
			return ECollections.emptyEList();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EObject getOriginalEObjectFromCopy(EObject copy) {
		for (EObject currentKey : addedEObjects.keySet()) {
			if (currentKey != null && addedEObjects.get(currentKey) != null
					&& addedEObjects.get(currentKey).contains(copy)) {
				return currentKey;
			}
		}
		return null;
	}

	/**
	 * Executes the specified <code>diffElements</code> using the
	 * {@link MergeService}.
	 * 
	 * @param diffElements
	 *            to execute.
	 */
	private void execute(EList<DiffElement> diffElements) {
		int size = diffElements.size();
		for (int i = 0; i < size; i++) {
			// undo in target
			MergeService.merge(diffElements.get(i), true);
		}
	}

	/**
	 * Returns all {@link ModelElementChangeLeftTarget} elements contained in
	 * the specified list of <code>diffElements</code>.
	 * 
	 * @param diffElements
	 *            to search {@link ModelElementChangeLeftTarget} elements in.
	 * @return a collection of all found elements.
	 */
	private Collection<DiffElement> getAddElements(
			EList<DiffElement> diffElements) {
		Collection<DiffElement> addElements = new HashSet<DiffElement>();
		for (DiffElement diffElement : diffElements) {
			if (diffElement instanceof DiffGroup) {
				DiffGroup diffGroup = (DiffGroup) diffElement;
				addElements.addAll(getAddElements(diffGroup
						.getSubDiffElements()));
			} else if (diffElement instanceof ModelElementChangeLeftTarget) {
				addElements.add(diffElement);
			}
		}
		return addElements;
	}

	/**
	 * Returns all elements excluding {@link ModelElementChangeLeftTarget}
	 * contained in the specified list of <code>diffElements</code>.
	 * 
	 * @param diffElements
	 *            to search elements in.
	 * @return a collection of all found elements.
	 */
	private Collection<DiffElement> getChangeDeleteElements(
			EList<DiffElement> diffElements) {
		EList<DiffElement> changeDeleteElements = new BasicEList<DiffElement>();
		for (DiffElement diffElement : diffElements) {
			if (diffElement instanceof DiffGroup) {
				DiffGroup diffGroup = (DiffGroup) diffElement;
				changeDeleteElements.addAll(getChangeDeleteElements(diffGroup
						.getSubDiffElements()));
			} else if (!(diffElement instanceof ModelElementChangeLeftTarget)) {
				changeDeleteElements.add(diffElement);
			}
		}
		return changeDeleteElements;
	}

	/**
	 * Adds the diff elements according to {@link #diffElementsToAdd} to the
	 * respective parent diff elements.
	 */
	private void addDiffElementsToDiffGroup(DiffGroup diffGroup) {
		for (DiffElement diffElement : this.diffElementsToAdd) {
			diffGroup.getSubDiffElements().add(diffElement);
		}
		// sort change/delete list, so that deletes are at the end
		ECollections.sort(diffGroup.getSubDiffElements(),
				new Comparator<DiffElement>() {
					@Override
					public int compare(DiffElement o1, DiffElement o2) {
						if (o1 instanceof ModelElementChangeRightTarget
								&& !(o2 instanceof ModelElementChangeRightTarget)) {
							return 1;
						}
						return 0;
					}
				});
	}

	/**
	 * Adds a <code>diffElement</code> to the set of diff elements which have to
	 * be added later. See {@link #addDiffElementsToDiffModel()}.
	 * 
	 * @param diffElement
	 *            diff element to be added.
	 */
	private void addDiffElementsToAdd(DiffElement diffElement) {
		this.diffElementsToAdd.add(diffElement);
	}

	/**
	 * Checks the specified <code>diffElement</code> for deactivation with the
	 * specified collection of <code>counterpartObjects</code> for right
	 * elements. If <code>counterpartObjects</code> is empty or
	 * <code>null</code>, <code>diffElement</code> will be deactivated.
	 * 
	 * @param diffElement
	 *            to check and potentially deactivate.
	 * @param counterpartObjectsElement
	 *            to check if empty.
	 */
	private void checkForDeactivation(DiffElement diffElement,
			Collection<EObject> counterpartObjects) {
		if (counterpartObjects == null || counterpartObjects.size() < 1) {
			deactivatedDiffElements.add(diffElement);
		}
	}

	/**
	 * Checks the specified <code>diffElement</code> for deactivation with the
	 * right target of the <code>diffElement</code>. If the
	 * {@link DiffUtil#getRightTarget(DiffElement) right target} is
	 * <code>null</code>, <code>diffElement</code> will be deactivated.
	 * 
	 * @param diffElement
	 *            to check and potentially deactivate.
	 */
	private void checkForDeactivationRightTarget(DiffElement diffElement) {
		if (DiffUtil.getRightTarget(diffElement) == null) {
			deactivatedDiffElements.add(diffElement);
		}
	}

	/**
	 * Checks the specified <code>diffElement</code> for deactivation with the
	 * right target of the <code>diffElement</code>. If the
	 * {@link DiffUtil#getLeftTarget(DiffElement) left target} is
	 * <code>null</code>, <code>diffElement</code> will be deactivated.
	 * 
	 * @param diffElement
	 *            to check and potentially deactivate.
	 */
	private void checkForDeactivationLeftTarget(DiffElement diffElement) {
		if (DiffUtil.getLeftTarget(diffElement) == null) {
			deactivatedDiffElements.add(diffElement);
		}
	}

	/**
	 * Re-wires all specified <code>diffElements</code>.
	 * 
	 * @param diffElements
	 *            diff elements to re-wire.
	 */
	private void rewireDiffElements(EList<DiffElement> diffElements) {
		for (DiffElement diffElement : diffElements) {
			rewireDiffElement(diffElement);
		}
	}

	/**
	 * Re-wires the right element or right parent of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireDiffElement(DiffElement diffElement) {

		// call specific re-wire method
		if (diffElement instanceof AttributeChangeLeftTarget) {
			rewireAttributeChangeLeftTarget((AttributeChangeLeftTarget) diffElement);
		} else if (diffElement instanceof AttributeChangeRightTarget) {
			rewireAttributeChangeRightTarget((AttributeChangeRightTarget) diffElement);
		} else if (diffElement instanceof UpdateAttribute) {
			rewireUpdateAttribute((UpdateAttribute) diffElement);
		} else if (diffElement instanceof ConflictingDiffElement) {
			rewireConflictingDiffElement((ConflictingDiffElement) diffElement);
		} else if (diffElement instanceof CopyElementLeftTarget) {
			rewireCopyElementLeftTarget((CopyElementLeftTarget) diffElement);
		} else if (diffElement instanceof CopyElementRightTarget) {
			rewireCopyElementRightTarget((CopyElementRightTarget) diffElement);
		} else if (diffElement instanceof ModelElementChangeLeftTarget) {
			rewireModelElementChangeLeftTarget((ModelElementChangeLeftTarget) diffElement);
		} else if (diffElement instanceof ModelElementChangeRightTarget) {
			rewireModelElementChangeRightTarget((ModelElementChangeRightTarget) diffElement);
		} else if (diffElement instanceof MoveModelElement) {
			rewireMoveModelElement((MoveModelElement) diffElement);
		} else if (diffElement instanceof ReferenceChangeLeftTarget) {
			rewireReferenceChangeLeftTarget((ReferenceChangeLeftTarget) diffElement);
		} else if (diffElement instanceof ReferenceChangeRightTarget) {
			rewireReferenceChangeRightTarget((ReferenceChangeRightTarget) diffElement);
		} else if (diffElement instanceof UpdateReference) {
			rewireUpdateReference((UpdateReference) diffElement);
		}

		// call sub diff elements
		for (DiffElement subDiffElements : diffElement.getSubDiffElements()) {
			rewireDiffElement(subDiffElements);
		}
	}

	/**
	 * Re-wires the right element and the left target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * TODO leftTarget
	 * 
	 * Represents an addition to a multi-valued attribute.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireAttributeChangeLeftTarget(
			AttributeChangeLeftTarget diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
		}
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
			} else {
				// we already re-wired first one
				// so make a copy
				AttributeChangeLeftTarget newDiffElement = (AttributeChangeLeftTarget) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
			}
		}
	}

	/**
	 * Re-wires the right element and right target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * TODO right Target
	 * 
	 * Represents an deletion in a multi-valued attribute.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireAttributeChangeRightTarget(
			AttributeChangeRightTarget diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
		}
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
			} else {
				// we already re-wired first one
				// so make a copy
				AttributeChangeRightTarget newDiffElement = (AttributeChangeRightTarget) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
			}
		}
	}

	/**
	 * Re-wires the right element of the specified <code>diffElement</code> to
	 * the objects of the model to execute the composite operation on.
	 * 
	 * Represents a value change in a single-valued attribute.
	 * 
	 * TODO merger sets value of left element (-> rewire if this value is a
	 * model element)?
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireUpdateAttribute(UpdateAttribute diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
		}
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
			} else {
				// we already re-wired first one
				// so make a copy
				UpdateAttribute newDiffElement = (UpdateAttribute) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
			}
		}
	}

	/**
	 * Just logs a warning since we cannot execute conflicting diff elements.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireConflictingDiffElement(ConflictingDiffElement diffElement) {
		DiffPlugin
				.getDefault()
				.getLog()
				.log(new Status(IStatus.WARNING, DiffPlugin.PLUGIN_ID,
						CANNOT_EXECUTE_CONFLICTING_DIFF));
	}

	/**
	 * Re-wires the right element of the specified <code>diffElement</code> to
	 * the objects of the model to execute the composite operation on.
	 * 
	 * Represents an element deletion (i.e., the deletion of the copy).
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireCopyElementRightTarget(CopyElementRightTarget diffElement) {
		rewireModelElementChangeRightTarget(diffElement);
	}

	/**
	 * Re-wires the right parent of the specified <code>diffElement</code> to
	 * the objects of the model to execute the composite operation on. Re-wires
	 * left element (which will be copied by merger to the right) to the object
	 * bound to the copied element.
	 * 
	 * Represents the addition of the copy.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireCopyElementLeftTarget(CopyElementLeftTarget diffElement) {
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightParent());
		checkForDeactivation(diffElement, counterpartObjects);
		Collection<EObject> copyCounterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getCopiedRightElement());
		checkForDeactivation(diffElement, copyCounterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			for (EObject target : copyCounterpartObjects) {
				if (isFirst) {
					isFirst = false;
					// re-wire existing diff element
					diffElement.setRightParent(eObject);
					// re-wire left element
					diffElement.setLeftElement(target);
				} else {
					// we already re-wired first one
					// so make a copy
					ModelElementChangeLeftTarget newDiffElement = (ModelElementChangeLeftTarget) EcoreUtil
							.copy(diffElement);
					// mark diff element to be added later
					addDiffElementsToAdd(newDiffElement);
					// re-wire it to current object
					newDiffElement.setRightParent(eObject);
					// re-wire left element
					newDiffElement.setLeftElement(target);
				}
			}
		}
	}

	/**
	 * Re-wires the right parent of the specified <code>diffElement</code> to
	 * the objects of the model to execute the composite operation on.
	 * 
	 * Represents an element addition.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireModelElementChangeLeftTarget(
			ModelElementChangeLeftTarget diffElement) {
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightParent());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightParent(eObject);
			} else {
				// we already re-wired first one
				// so make a copy
				ModelElementChangeLeftTarget newDiffElement = (ModelElementChangeLeftTarget) EcoreUtil
						.copy(diffElement);
				// re-wire it to current object
				newDiffElement.setRightParent(eObject);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
			}
		}
	}

	/**
	 * Re-wires the right element of the specified <code>diffElement</code> to
	 * the objects of the model to execute the composite operation on.
	 * 
	 * Represents an element deletion.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireModelElementChangeRightTarget(
			ModelElementChangeRightTarget diffElement) {
		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {
			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
			} else {
				// we already re-wired first one
				// so make a copy
				ModelElementChangeRightTarget newDiffElement = (ModelElementChangeRightTarget) EcoreUtil
						.copy(diffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
			}
		}
	}

	/**
	 * Re-wires the right element and the right target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * Represents a move of an element.
	 * 
	 * TODO if there are more counterpart target objects, convert move to copy
	 * and iterate for each?
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireMoveModelElement(MoveModelElement diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
			EObject leftTarget = diffElement.getLeftTarget();
			EObject rightTarget = diffElement.getRightTarget();
			diffElement.setRightTarget(leftTarget);
			diffElement.setLeftTarget(rightTarget);
		}

		// get counterpart objects
		Collection<EObject> counterpartObjectsElement = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjectsElement);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;

		EObject rightTarget = diffElement.getRightTarget();
		for (EObject eObject : counterpartObjectsElement) {

			// re-wire right target
			if (copier.get(diffElement.getLeftElement().eContainer()) != null) {
				diffElement.setRightTarget(copier.get(diffElement
						.getLeftElement().eContainer()));
			} else {
				diffElement
						.setRightTarget(mappingProvider.getCounterpartObject(
								rightTarget, diffElement, eObject));
			}

			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
				checkForDeactivationRightTarget(diffElement);
			} else {
				// we already re-wired first one
				// so make a copy
				MoveModelElement newDiffElement = (MoveModelElement) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
				checkForDeactivationRightTarget(newDiffElement);
			}
		}

	}

	/**
	 * Re-wires the right element and the left target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * Represents an addition of a multi-valued reference.
	 * 
	 * TODO repeat for iterated counterpart left target objects?
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireReferenceChangeLeftTarget(
			ReferenceChangeLeftTarget diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
			EObject leftTarget = diffElement.getLeftTarget();
			EObject rightTarget = diffElement.getRightTarget();
			diffElement.setRightTarget(leftTarget);
			diffElement.setLeftTarget(rightTarget);
		}

		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {

			// re-wire left target
			if (copier.get(diffElement.getLeftTarget()) != null) {
				diffElement.setLeftTarget(copier.get(diffElement
						.getLeftTarget()));
			} else {
				diffElement.setLeftTarget(mappingProvider.getCounterpartObject(
						diffElement.getRightTarget(), diffElement, eObject));
			}
			// handle null right target
			if (diffElement.getRightTarget() == null) {
				diffElement.setRightTarget(diffElement.getLeftTarget());
			}

			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
				checkForDeactivationLeftTarget(diffElement);
			} else {
				// we already re-wired first one
				// so make a copy
				ReferenceChangeLeftTarget newDiffElement = (ReferenceChangeLeftTarget) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
				checkForDeactivationLeftTarget(newDiffElement);
			}
		}
	}

	/**
	 * Re-wires the right element and the left target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * Represents an deletion to a multi-valued reference.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireReferenceChangeRightTarget(
			ReferenceChangeRightTarget diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
			EObject leftTarget = diffElement.getLeftTarget();
			EObject rightTarget = diffElement.getRightTarget();
			diffElement.setRightTarget(leftTarget);
			diffElement.setLeftTarget(rightTarget);
		}

		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {

			// re-wire right target
			if (copier.get(diffElement.getRightTarget()) != null) {
				diffElement.setLeftTarget(copier.get(diffElement
						.getRightTarget()));
			} else {
				diffElement.setLeftTarget(mappingProvider.getCounterpartObject(
						diffElement.getRightTarget(), diffElement, eObject));
			}
			// handle null right target
			if (diffElement.getRightTarget() == null) {
				diffElement.setRightTarget(diffElement.getLeftTarget());
			}

			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
				checkForDeactivationLeftTarget(diffElement);
			} else {
				// we already re-wired first one
				// so make a copy
				ReferenceChangeRightTarget newDiffElement = (ReferenceChangeRightTarget) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
				checkForDeactivationLeftTarget(newDiffElement);
			}
		}
	}

	/**
	 * Re-wires the right element and right target of the specified
	 * <code>diffElement</code> to the objects of the model to execute the
	 * composite operation on.
	 * 
	 * Represents an update of a single-valued reference.
	 * 
	 * @param diffElement
	 *            diff element to re-wire.
	 */
	private void rewireUpdateReference(UpdateReference diffElement) {
		// change direction if necessary
		if (!mappingProvider.isInOriginModel(diffElement.getRightElement())) {
			EObject leftElement = diffElement.getLeftElement();
			EObject rightElement = diffElement.getRightElement();
			diffElement.setRightElement(leftElement);
			diffElement.setLeftElement(rightElement);
		}
		if (!mappingProvider.isInOriginModel(diffElement.getRightTarget())) {
			EObject leftTarget = diffElement.getLeftTarget();
			EObject rightTarget = diffElement.getRightTarget();
			diffElement.setRightTarget(leftTarget);
			diffElement.setLeftTarget(rightTarget);
		}

		// get counterpart objects
		Collection<EObject> counterpartObjects = mappingProvider
				.getCounterpartObjects(diffElement.getRightElement());
		checkForDeactivation(diffElement, counterpartObjects);
		// re-wire or create and re-wire the diff element
		boolean isFirst = true;
		for (EObject eObject : counterpartObjects) {

			// re-wire right target
			if (copier.get(diffElement.getRightTarget()) != null) {
				diffElement.setRightTarget(copier.get(diffElement
						.getRightTarget()));
			} else {
				diffElement.setRightTarget(mappingProvider
						.getCounterpartObject(diffElement.getRightTarget(),
								diffElement, eObject));
			}

			if (isFirst) {
				isFirst = false;
				// re-wire existing diff element
				diffElement.setRightElement(eObject);
				checkForDeactivationRightTarget(diffElement);
			} else {
				// we already re-wired first one
				// so make a copy
				UpdateReference newDiffElement = (UpdateReference) EcoreUtil
						.copy(diffElement);
				// mark diff element to be added later
				addDiffElementsToAdd(newDiffElement);
				// re-wire it to current object
				newDiffElement.setRightElement(eObject);
				checkForDeactivationRightTarget(newDiffElement);
			}
		}
	}

	/**
	 * Catches call backs of the merge events.
	 * 
	 * Re-wires and cleans up additions (ModelElementChangeLeftTarget).
	 */
	@Override
	public void mergeDiffEnd(MergeEvent event) {
		for (DiffElement diffElement : event.getDifferences()) {
			if (diffElement instanceof ModelElementChangeLeftTarget) {
				ModelElementChangeLeftTarget addition = (ModelElementChangeLeftTarget) diffElement;
				// save created copy
				EObject original = addition.getLeftElement();
				EObject copy = copier.get(original);
				if (addedEObjects.get(original) == null) {
					addedEObjects.put(original, new BasicEList<EObject>());
				}
				if (copy != null && !addedEObjects.get(original).contains(copy)) {
					addedEObjects.get(original).add(copy);
				}

				if (addContainerOnly) {
					for (EObject containment : new BasicEList<EObject>(
							copy.eContents())) {
						if (!(containment instanceof EGenericType)) {
							EcoreUtil.delete(containment);
							containmentsToOmit.add(containment);
						}
					}
				}
			}
		}
	}

	/**
	 * Fetches the right instance of the copier.
	 */
	@Override
	public void mergeDiffStart(MergeEvent event) {
		copier = MergeService.getCopier(event.getDifferences().get(0));
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void mergeOperationStart(MergeEvent event) {
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void mergeOperationEnd(MergeEvent event) {
	}

	/**
	 * Re-wires all referenced of the (the copy of the) specified
	 * <code>leftElement</code> and cleans up its contained objects which have
	 * an opposite in the match model.
	 * 
	 * @param object
	 *            object which was copied and added that needs to get re-wired.
	 */
	private void rewireAddedObject(EObject object) {
		// get copy
		EObject addedObject = copier.get(object);

		if (addedObject == null
				|| (addContainerOnly && containmentsToOmit
						.contains(addedObject))) {
			// is not a copy
			return;
		}

		// set XMI ID if possible
		if (object.eResource() != null
				&& object.eResource() instanceof XMIResource
				&& addedObject.eResource() != null
				&& addedObject.eResource() instanceof XMIResource) {
			UUIDUtil.copyUUID(object, addedObject);
		}

		// re-wire references of addedObject
		for (EStructuralFeature feature : addedObject.eClass()
				.getEAllStructuralFeatures()) {
			try {

				Object value = addedObject.eGet(feature);
				// copied objects might have null values although the original's
				// value is not
				if (value == null && object.eGet(feature) != null) {
					value = object.eGet(feature);
				} else if (value instanceof EList
						&& ((EList<?>) value).size() < ((EList<?>) object
								.eGet(feature)).size()) {
					value = object.eGet(feature);
				}
				// check if value might be replaced with copied or bound object
				if (value instanceof EObject) {
					EObject counterpartObject = null;
					if (copier.get(addedObject.eGet(feature)) != null) {
						EFactory.eSet(addedObject, feature.getName(),
								copier.get(value));
					} else if ((counterpartObject = mappingProvider
							.getCounterpartObject(
									getMatchingObject((EObject) value), null,
									addedObject)) != null) {
						EFactory.eSet(addedObject, feature.getName(),
								counterpartObject);
					} else {
						if (value instanceof EObject) {
							UUIDUtil.setUUID((EObject) value, UUIDUtil
									.getUUID((EObject) object.eGet(feature)));
						}
					}
				} else if (value instanceof EList) {
					EObject counterpartObject = null;
					EList<?> list = (EList<?>) value;
					EList<EObject> newList = new BasicEList<EObject>();
					for (Object valueInList : list) {
						if (copier.get(valueInList) != null
						// avoid setting containments to omit if set to do so
								&& !(addContainerOnly && containmentsToOmit
										.contains(copier.get(valueInList)))) {
							newList.add(copier.get(valueInList));
						} else if ((counterpartObject = mappingProvider
								.getCounterpartObject(
										getMatchingObject((EObject) valueInList),
										null, addedObject)) != null
								// avoid setting containments to omit if set to
								// do so
								&& !(addContainerOnly
										&& feature instanceof EReference
										&& ((EReference) feature)
												.isContainment()
										&& !((EReference) feature).isDerived() || valueInList instanceof EGenericType)) {
							newList.add(counterpartObject);
						} else if (!addContainerOnly) {
							int i = list.indexOf(valueInList);
							EList<?> originalList = ((EList<?>) object
									.eGet(feature));
							if (originalList.size() > i) {
								Object originalValueInList = originalList
										.get(i);
								if (originalValueInList instanceof EObject
										&& valueInList instanceof EObject) {
									UUIDUtil.setUUID(
											(EObject) valueInList,
											UUIDUtil.getUUID((EObject) originalValueInList));
								}
							}
						}
					}
					if (addContainerOnly) { // for iterative operation detection
						if (!feature.isDerived()
								&& feature.isChangeable()
								&& !(value instanceof EList<?>
										// avoid clearing caused by unmatched
										// generic super types:
										&& ((EList<?>) value).size() > 0
										&& newList.size() == 0 && ((EList<?>) value)
										.get(0) instanceof EGenericType)) {
							EFactory.eSet(addedObject, feature.getName(),
									newList);
						}
					} else {
						// old version
						// we might switch to the above again some time
						if (newList.size() > 0) {
							EFactory.eSet(addedObject, feature.getName(),
									newList);
						}
					}

				}
			} catch (FactoryException e) {
			}
		}

		// re-wire and clean-up contained objects
		TreeIterator<EObject> allContents = object.eAllContents();
		Set<EObject> objectsToRemove = new HashSet<EObject>();
		while (allContents.hasNext()) {
			EObject contentObject = allContents.next();
			if (hasMatchingObject(contentObject)) {
				// add copy of contentObject to the list of objects to remove
				objectsToRemove.add(copier.get(contentObject));
			} else {
				rewireAddedObject(contentObject);
			}
		}

		// remove objects to remove
		for (EObject eObject : objectsToRemove) {
			if (eObject != null) {
				EcoreUtil.remove(eObject);
			}
		}
	}

	/**
	 * Returns <code>true</code> if the specified <code>eObject</code> is
	 * matched in the match model.
	 * 
	 * @param eObject
	 *            to check for matching object.
	 * @return <code>true</code> if the specified <code>eObject</code> is
	 *         matched in the match model. <code>false</code> otherwise.
	 */
	private boolean hasMatchingObject(EObject eObject) {
		return getMatchingObject(eObject) != null;
	}

	/**
	 * Returns the matching object in the match model of the specified
	 * <code>eObject</code>. If there is no matching object this method returns
	 * <code>null</code>.
	 * 
	 * @param eObject
	 *            to find match of.
	 * @return the matching object or <code>null</code> if there is none.
	 */
	private EObject getMatchingObject(EObject eObject) {
		return MatchUtil.getMatchingObject(eObject, matchModel);
	}

}