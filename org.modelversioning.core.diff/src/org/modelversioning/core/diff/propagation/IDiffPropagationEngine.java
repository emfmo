/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.propagation;

import java.util.Collection;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.merge.EMFCompareEObjectCopier;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;

/**
 * Interface providing the propagation of diff elements, i.e., their execution
 * on other models. The mapping between the original model and the model to
 * apply the diff elements to has to be provided by the IMappingProvider.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IDiffPropagationEngine {

	/**
	 * Propagates the {@link DiffElement}s contained by <code>diffModel</code>
	 * to the counterpart objects provided by <code>mappingProvider</code>.
	 * 
	 * @param diffModel
	 *            containing {@link DiffElement}s to propagate.
	 * @param matchModel
	 *            containing the {@link MatchModel} matching original objects to
	 *            revised objects.
	 * @param mappingProvider
	 *            providing the mapping from original objects to counterpart
	 *            objects.
	 * @param monitor
	 *            to report progress to.
	 */
	public void propagate(DiffModel diffModel, MatchModel matchModel,
			IPropagationMappingProvider mappingProvider,
			IProgressMonitor monitor);

	/**
	 * Propagates the {@link DiffElement}s contained by
	 * <code>diffElements</code> to the counterpart objects provided by
	 * <code>mappingProvider</code>.
	 * 
	 * @param diffElements
	 *            containing {@link DiffElement}s to propagate.
	 * @param matchModel
	 *            containing the {@link MatchModel} matching original objects to
	 *            revised objects.
	 * @param mappingProvider
	 *            providing the mapping from original objects to counterpart
	 *            objects.
	 * @param monitor
	 *            to report progress to.
	 */
	public void propagate(Collection<DiffElement> diffElements,
			MatchModel matchModel, IPropagationMappingProvider mappingProvider,
			IProgressMonitor monitor);

	/**
	 * Returns the copier for used executing additions.
	 * 
	 * @return the copier.
	 */
	public EMFCompareEObjectCopier getCopier();

	/**
	 * Returns a list of all copies of the specified <code>original</code>
	 * created during propagation.
	 * 
	 * @param original
	 *            the original object.
	 * @return the list of copies.
	 */
	public EList<EObject> getCopies(EObject original);

	/**
	 * Returns the original object for the specified copy. If the specified
	 * <code>copy</code> has not been created by copying an original object,
	 * this method returns <code>null</code>.
	 * 
	 * @param copy
	 *            to get original for.
	 * @return the found original or <code>null</code>.
	 */
	public EObject getOriginalEObjectFromCopy(EObject copy);

	/**
	 * Sets whether this engine should add the container only and omit its
	 * containments.
	 * 
	 * <p>
	 * This is only useful if you incrementally evolve the model and diff again
	 * after each step (then the omitted containments get reported as new add
	 * diff element)
	 * </p>
	 * 
	 * @param addContainerOnly
	 *            <code>true</code> to only add containers and omit its
	 *            containments. Otherwise, <code>false</code>.
	 */
	void setAddContainerOnly(boolean addContainerOnly);

}