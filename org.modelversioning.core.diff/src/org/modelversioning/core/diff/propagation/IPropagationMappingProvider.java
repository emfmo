/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.propagation;

import java.util.Collection;

import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.ecore.EObject;

/**
 * Interface providing a mapping from original {@link EObject}s to counterpart
 * objects to which to propagate to.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IPropagationMappingProvider {

	/**
	 * Returns the counterpart objects which are mapped to <code>eObject</code>
	 * contained by the original model.
	 * 
	 * @param eObject
	 *            object contained by the original model of which the
	 *            counterpart is requested.
	 * @return mapped counterpart objects.
	 */
	public Collection<EObject> getCounterpartObjects(EObject eObject);

	/**
	 * Returns the counterpart object of the specified <code>eObject</code>
	 * contained by the by the original model.
	 * 
	 * If there are more than one objects a counterpart to <code>eObject</code>,
	 * this method should use <code>diffElement</code> and <code>context</code>
	 * to decide which object to return.
	 * 
	 * @param eObject
	 *            object contained by the example origin model of which the
	 *            counterpart is requested.
	 * @param diffElement
	 *            context diff element.
	 * @param context
	 *            context object.
	 * @return the counterpart object.
	 */
	public EObject getCounterpartObject(EObject eObject,
			DiffElement diffElement, EObject context);

	/**
	 * Specifies whether <code>object</code> is contained by the original model.
	 * 
	 * @param object
	 *            to check.
	 * @return <code>true</code> if in original model, <code>false</code>
	 *         otherwise.
	 */
	public boolean isInOriginModel(EObject object);

}
