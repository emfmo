/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.util;

import java.io.PrintStream;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.AttributeChange;
import org.eclipse.emf.compare.diff.metamodel.AttributeChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.AttributeChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.ConflictingDiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffFactory;
import org.eclipse.emf.compare.diff.metamodel.DiffGroup;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.ModelElementChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.MoveModelElement;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChange;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChangeLeftTarget;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChangeRightTarget;
import org.eclipse.emf.compare.diff.metamodel.UpdateAttribute;
import org.eclipse.emf.compare.diff.metamodel.UpdateReference;
import org.eclipse.emf.compare.util.ClassUtils;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.modelversioning.core.diff.copydiff.CopyDiffFactory;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;

/**
 * Utility class for {@link DiffModel} related tasks.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffUtil {

	/**
	 * Extracts the effective diff elements, i.e., diff elements which are not
	 * only grouping other diff elements, from the specified
	 * <code>diffModel</code>.
	 * 
	 * @param diffModel
	 *            to get effective {@link DiffElement}s from.
	 * @return a {@link EList} containing the effective {@link DiffElement}s.
	 */
	public static EList<DiffElement> getEffectiveDiffElements(
			DiffModel diffModel) {
		EList<DiffElement> effectiveDiffElements = new BasicEList<DiffElement>();
		for (DiffElement diffElement : diffModel.getOwnedElements()) {
			effectiveDiffElements.addAll(getEffectiveDiffElements(diffElement));
		}
		return effectiveDiffElements;
	}

	/**
	 * Extracts the effective diff elements, i.e., diff elements which are not
	 * only grouping other diff elements, from the specified
	 * <code>diffElement</code>. The resulting list might include the specified
	 * <code>diffElement</code> itself it is not only a group.
	 * 
	 * @param diffElement
	 *            to get effective {@link DiffElement}s from.
	 * @return a {@link EList} containing the effective {@link DiffElement}s.
	 */
	public static EList<DiffElement> getEffectiveDiffElements(
			DiffElement diffElement) {
		EList<DiffElement> effectiveDiffElements = new BasicEList<DiffElement>();
		// check if diff element is a group
		if (!(diffElement instanceof DiffGroup)) {
			effectiveDiffElements.add(diffElement);
		}
		// handle children
		for (DiffElement subDiffElement : diffElement.getSubDiffElements()) {
			effectiveDiffElements
					.addAll(getEffectiveDiffElements(subDiffElement));
		}
		return effectiveDiffElements;
	}

	/**
	 * Extracts the effective diff elements, i.e., diff elements which are not
	 * only grouping other diff elements, from the specified
	 * <code>diffElement</code>. in which the specified <code>eObject</code>
	 * plays the &quot;subject&quot;.
	 * 
	 * <p>
	 * An {@link EObject} is the subject if it is the changed, removed, moved or
	 * deleted object.
	 * </p>
	 * 
	 * @param eObject
	 *            subject
	 * @param diffModel
	 *            to get effective {@link DiffElement}s from.
	 * @return a {@link EList} containing the effective {@link DiffElement}s.
	 */
	public static EList<DiffElement> getEffectiveDiffElements(EObject eObject,
			DiffModel diffModel) {
		EList<DiffElement> effectiveDiffElements = new BasicEList<DiffElement>();
		for (DiffElement diffElement : getEffectiveDiffElements(diffModel)) {
			EObject leftElement = getLeftElement(diffElement);
			EObject rightElement = getRightElement(diffElement);
			if ((leftElement != null && leftElement.equals(eObject))
					|| (rightElement != null && rightElement.equals(eObject))) {
				effectiveDiffElements.add(diffElement);
			}
		}
		return effectiveDiffElements;
	}

	/**
	 * Returns the left element of the given {@link EObject}. Will try to invoke
	 * the method called "getLeftElement". Returns <code>null</code> if neither
	 * of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The left element of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getLeftElement(DiffElement object) {
		EObject leftElement = null;

		leftElement = (EObject) ClassUtils.invokeMethod(object,
				"getLeftElement"); //$NON-NLS-1$

		return leftElement;
	}

	/**
	 * Returns the right element of the given {@link EObject}. Will try to
	 * invoke the method called "getRightElement". Returns <code>null</code> if
	 * neither of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The right element of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getRightElement(DiffElement object) {
		EObject rightElement = null;

		rightElement = (EObject) ClassUtils.invokeMethod(object,
				"getRightElement"); //$NON-NLS-1$

		return rightElement;
	}

	/**
	 * Returns the right parent of the given {@link EObject}. Will try to invoke
	 * the method called "getRightParent". Returns <code>null</code> if neither
	 * of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The right parent of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getRightParent(DiffElement object) {
		EObject rightParent = null;

		rightParent = (EObject) ClassUtils.invokeMethod(object,
				"getRightParent"); //$NON-NLS-1$

		return rightParent;
	}

	/**
	 * Returns the right target of the given {@link EObject}. Will try to invoke
	 * the method called "getRightTarget". Returns <code>null</code> if neither
	 * of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The right parent of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getRightTarget(DiffElement object) {
		EObject rightTarget = null;

		rightTarget = (EObject) ClassUtils.invokeMethod(object,
				"getRightTarget"); //$NON-NLS-1$
		if (rightTarget == null) {
			EObject rightElement = getRightElement(object);
			if (rightElement != null) {
				return rightElement.eContainer();
			}
		}

		return rightTarget;
	}

	/**
	 * Returns the left parent of the given {@link EObject}. Will try to invoke
	 * the method called "getLeftParent". Returns <code>null</code> if neither
	 * of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The left parent of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getLeftParent(DiffElement object) {
		EObject leftParent = null;

		leftParent = (EObject) ClassUtils.invokeMethod(object, "getLeftParent"); //$NON-NLS-1$

		return leftParent;
	}

	/**
	 * Returns the left target of the given {@link EObject}. Will try to invoke
	 * the method called "getLeftTarget". Returns <code>null</code> if neither
	 * of these methods can be found.<br/>
	 * This method is intended to be called with a {@link DiffElement} or
	 * MatchElement as argument.
	 * 
	 * This method is a copy of
	 * org.eclipse.emf.compare.ui.util.EMFCompareEObjectUtils.
	 * 
	 * @param object
	 *            The {@link EObject}.
	 * @return The right parent of the given {@link EObject}.
	 * @author <a href="mailto:laurent.goubet@obeo.fr">Laurent Goubet</a>
	 */
	public static EObject getLeftTarget(DiffElement object) {
		EObject leftTarget = null;

		leftTarget = (EObject) ClassUtils.invokeMethod(object, "getLeftTarget"); //$NON-NLS-1$
		if (leftTarget == null) {
			EObject leftElement = getLeftElement(object);
			if (leftElement != null) {
				return leftElement.eContainer();
			}
		}

		return leftTarget;
	}

	/**
	 * Creates a shallow copy of the specified <code>diffElements</code>.
	 * 
	 * @param diffElements
	 *            to copy.
	 * @return copied diff element list.
	 */
	public static EList<DiffElement> shallowCopy(
			Collection<DiffElement> diffElements) {
		// TODO implement shallow copy more generically:
		// EObject eObject = DiffFactory.eINSTANCE.create(diffElement.eClass());
		// and setting the features generically
		EList<DiffElement> copiedDiffElements = new BasicEList<DiffElement>();
		for (DiffElement diffElement : diffElements) {
			if (diffElement instanceof DiffGroup) {
				DiffGroup newDiffGroup = DiffFactory.eINSTANCE
						.createDiffGroup();
				newDiffGroup.getSubDiffElements().addAll(
						shallowCopy(diffElement.getSubDiffElements()));
				copiedDiffElements.add(newDiffGroup);
			} else if (diffElement instanceof AttributeChangeLeftTarget) {
				AttributeChangeLeftTarget newDiffElement = DiffFactory.eINSTANCE
						.createAttributeChangeLeftTarget();
				newDiffElement
						.setAttribute(((AttributeChangeLeftTarget) diffElement)
								.getAttribute());
				newDiffElement
						.setLeftElement(((AttributeChangeLeftTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setLeftTarget(((AttributeChangeLeftTarget) diffElement)
								.getLeftTarget());
				newDiffElement
						.setRightElement(((AttributeChangeLeftTarget) diffElement)
								.getRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof AttributeChangeRightTarget) {
				AttributeChangeRightTarget newDiffElement = DiffFactory.eINSTANCE
						.createAttributeChangeRightTarget();
				newDiffElement
						.setAttribute(((AttributeChangeRightTarget) diffElement)
								.getAttribute());
				newDiffElement
						.setLeftElement(((AttributeChangeRightTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setRightTarget(((AttributeChangeRightTarget) diffElement)
								.getRightTarget());
				newDiffElement
						.setRightElement(((AttributeChangeRightTarget) diffElement)
								.getRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof UpdateAttribute) {
				UpdateAttribute newDiffElement = DiffFactory.eINSTANCE
						.createUpdateAttribute();
				newDiffElement.setAttribute(((UpdateAttribute) diffElement)
						.getAttribute());
				newDiffElement.setLeftElement(((UpdateAttribute) diffElement)
						.getLeftElement());
				newDiffElement.setRightElement(((UpdateAttribute) diffElement)
						.getRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof ConflictingDiffElement) {
				// we don't copy conflicting diff elements
			} else if (diffElement instanceof CopyElementLeftTarget) {
				CopyElementLeftTarget newDiffElement = CopyDiffFactory.eINSTANCE
						.createCopyElementLeftTarget();
				newDiffElement
						.setLeftElement(((CopyElementLeftTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setRightParent(((CopyElementLeftTarget) diffElement)
								.getRightParent());
				newDiffElement
						.setCopiedRightElement(((CopyElementLeftTarget) diffElement)
								.getCopiedRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof ModelElementChangeLeftTarget) {
				ModelElementChangeLeftTarget newDiffElement = DiffFactory.eINSTANCE
						.createModelElementChangeLeftTarget();
				newDiffElement
						.setLeftElement(((ModelElementChangeLeftTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setRightParent(((ModelElementChangeLeftTarget) diffElement)
								.getRightParent());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof ModelElementChangeRightTarget) {
				ModelElementChangeRightTarget newDiffElement = DiffFactory.eINSTANCE
						.createModelElementChangeRightTarget();
				newDiffElement
						.setLeftParent(((ModelElementChangeRightTarget) diffElement)
								.getLeftParent());
				newDiffElement
						.setRightElement(((ModelElementChangeRightTarget) diffElement)
								.getRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof MoveModelElement) {
				MoveModelElement newDiffElement = DiffFactory.eINSTANCE
						.createMoveModelElement();
				newDiffElement.setLeftTarget(((MoveModelElement) diffElement)
						.getLeftTarget());
				newDiffElement.setLeftElement(((MoveModelElement) diffElement)
						.getLeftElement());
				newDiffElement.setRightTarget(((MoveModelElement) diffElement)
						.getRightTarget());
				newDiffElement.setRightElement(((MoveModelElement) diffElement)
						.getRightElement());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof ReferenceChangeLeftTarget) {
				ReferenceChangeLeftTarget newDiffElement = DiffFactory.eINSTANCE
						.createReferenceChangeLeftTarget();
				newDiffElement
						.setLeftTarget(((ReferenceChangeLeftTarget) diffElement)
								.getLeftTarget());
				newDiffElement
						.setLeftElement(((ReferenceChangeLeftTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setRightTarget(((ReferenceChangeLeftTarget) diffElement)
								.getRightTarget());
				newDiffElement
						.setRightElement(((ReferenceChangeLeftTarget) diffElement)
								.getRightElement());
				newDiffElement
						.setReference(((ReferenceChangeLeftTarget) diffElement)
								.getReference());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof ReferenceChangeRightTarget) {
				ReferenceChangeRightTarget newDiffElement = DiffFactory.eINSTANCE
						.createReferenceChangeRightTarget();
				newDiffElement
						.setLeftTarget(((ReferenceChangeRightTarget) diffElement)
								.getLeftTarget());
				newDiffElement
						.setLeftElement(((ReferenceChangeRightTarget) diffElement)
								.getLeftElement());
				newDiffElement
						.setRightTarget(((ReferenceChangeRightTarget) diffElement)
								.getRightTarget());
				newDiffElement
						.setRightElement(((ReferenceChangeRightTarget) diffElement)
								.getRightElement());
				newDiffElement
						.setReference(((ReferenceChangeRightTarget) diffElement)
								.getReference());
				copiedDiffElements.add(newDiffElement);
			} else if (diffElement instanceof UpdateReference) {
				UpdateReference newDiffElement = DiffFactory.eINSTANCE
						.createUpdateReference();
				newDiffElement.setLeftTarget(((UpdateReference) diffElement)
						.getLeftTarget());
				newDiffElement.setLeftElement(((UpdateReference) diffElement)
						.getLeftElement());
				newDiffElement.setRightTarget(((UpdateReference) diffElement)
						.getRightTarget());
				newDiffElement.setRightElement(((UpdateReference) diffElement)
						.getRightElement());
				newDiffElement.setReference(((UpdateReference) diffElement)
						.getReference());
				copiedDiffElements.add(newDiffElement);
			}
		}
		return copiedDiffElements;
	}

	/**
	 * Returns the updated {@link EStructuralFeature} of the specified
	 * <code>diffElement</code>. If it is a {@link ReferenceChange} or an
	 * {@link AttributeChange} it returns the reference or attribute. If it is
	 * an addition or deletion, it will return the implicitly updated
	 * containment feature. Otherwise, this method returns <code>null</code> .
	 * 
	 * @param diffElement
	 *            to get updated feature.
	 * @return the udpated {@link EStructuralFeature} or <code>null</code>.
	 */
	public static EStructuralFeature getUpdatedFeature(DiffElement diffElement) {
		if (diffElement instanceof ReferenceChange) {
			return ((ReferenceChange) diffElement).getReference();
		} else if (diffElement instanceof AttributeChange) {
			return ((AttributeChange) diffElement).getAttribute();
		} else if (diffElement instanceof ModelElementChangeRightTarget) {
			return ((ModelElementChangeRightTarget) diffElement)
					.getRightElement().eContainingFeature();
		} else if (diffElement instanceof ModelElementChangeLeftTarget) {
			return ((ModelElementChangeLeftTarget) diffElement)
					.getLeftElement().eContainmentFeature();
		} else {
			return null;
		}
	}

	/**
	 * Debug prints the specified <code>diff</code> to <code>out</code>.
	 * 
	 * @param diff
	 *            to debug print.
	 * @param out
	 *            to print to.
	 */
	public static void debugPrint(DiffModel diff, PrintStream out) {
		out.println("====== Diff Model ======");
		EList<DiffElement> ownedElements = diff.getOwnedElements();
		for (DiffElement diffElement : ownedElements) {
			debugPring(diffElement, out);
		}
	}

	/**
	 * Debug prints the specified <code>diffElement</code> to <code>out</code>.
	 * 
	 * @param diffElement
	 *            to debug print.
	 * @param out
	 *            to print to.
	 */
	public static void debugPring(DiffElement diffElement, PrintStream out) {
		debugPring(diffElement, out, ""); //$NON-NLS-1$
	}

	/**
	 * Debug prints the specified <code>diffElement</code> to <code>out</code>.
	 * 
	 * @param diffElement
	 *            to debug print.
	 * @param out
	 *            to print to.
	 * @param offset
	 *            offset to move the output to the right.
	 */
	private static void debugPring(DiffElement diffElement, PrintStream out,
			String offset) {
		out.println(diffElement);
		if (diffElement.getSubDiffElements().size() > 0) {
			for (DiffElement subDiff : diffElement.getSubDiffElements()) {
				offset += "  "; //$NON-NLS-1$
				out.print(offset);
				debugPring(subDiff, out, offset);
			}
		}
	}
}
