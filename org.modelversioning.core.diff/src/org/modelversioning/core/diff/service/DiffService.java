/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.service;

import java.util.Date;

import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.diff.metamodel.DiffFactory;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.resource.Resource;
import org.modelversioning.core.diff.engine.IDiffEngine;
import org.modelversioning.core.diff.engine.IDiffEngineSelector;
import org.modelversioning.core.diff.engine.impl.DefaultDiffEngineSelector;

/**
 * This class offers services to create a {@link DiffModel} out of two or three
 * {@link Resource}s using a specified {@link MatchModel}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffService {

	/**
	 * An instance of the {@link IDiffEngineSelector} to use.
	 */
	private IDiffEngineSelector diffEngineSelector;

	/**
	 * Constructor setting the {@link IDiffEngineSelector} to use.
	 * 
	 * @param diffEngineSelector
	 *            the {@link IDiffEngineSelector} to use.
	 */
	public DiffService(IDiffEngineSelector diffEngineSelector) {
		super();
		this.diffEngineSelector = diffEngineSelector;
	}

	/**
	 * Empty default constructor.
	 */
	public DiffService() {
		super();
		this.diffEngineSelector = new DefaultDiffEngineSelector();
	}

	/**
	 * Executes the diff calculation and generates a {@link DiffModel} as result
	 * of the differentiation process.
	 * 
	 * <p>
	 * For an appropriate differentiation a suitable {@link IDiffEngine} will be
	 * selected using the set {@link IDiffEngineSelector} or the
	 * {@link DefaultDiffEngineSelector} if no specific
	 * {@link IDiffEngineSelector} is specified.
	 * </p>
	 * 
	 * @param matchModel
	 *            the basis for the differentiation process.
	 * @return the generated {@link DiffModel}.
	 */
	public DiffModel generateDiffModel(MatchModel matchModel) {
		return this.diffEngineSelector.selectEngine(matchModel)
				.generateDiffModel(matchModel);
	}

	/**
	 * Executes the diff calculation and generates a
	 * {@link ComparisonResourceSnapshot} as result of the differentiation
	 * process.
	 * 
	 * <p>
	 * For an appropriate differentiation a suitable {@link IDiffEngine} will be
	 * selected using the set {@link IDiffEngineSelector} or the
	 * {@link DefaultDiffEngineSelector} if no specific
	 * {@link IDiffEngineSelector} is specified.
	 * </p>
	 * 
	 * @param matchModel
	 *            the basis for the differentiation process.
	 * @return the generated {@link ComparisonResourceSnapshot}
	 */
	public ComparisonResourceSnapshot generateComparisonResourceSnapshot(
			MatchModel matchModel) {
		DiffModel diffModel = this.diffEngineSelector.selectEngine(matchModel)
				.generateDiffModel(matchModel);
		ComparisonResourceSnapshot comparisonResourceSnapshot = DiffFactory.eINSTANCE
				.createComparisonResourceSnapshot();
		comparisonResourceSnapshot.setDate(new Date());
		comparisonResourceSnapshot.setDiff(diffModel);
		comparisonResourceSnapshot.setMatch(matchModel);
		return comparisonResourceSnapshot;
	}

	/**
	 * Returns the {@link IDiffEngineSelector} in use.
	 * 
	 * @return the diffEngineSelector in use..
	 */
	public IDiffEngineSelector getDiffEngineSelector() {
		return diffEngineSelector;
	}

	/**
	 * Sets the {@link IDiffEngineSelector} to use.
	 * 
	 * @param diffEngineSelector
	 *            the diffEngineSelector to use.
	 */
	public void setDiffEngineSelector(IDiffEngineSelector diffEngineSelector) {
		this.diffEngineSelector = diffEngineSelector;
	}

}
