/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffGroup;
import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.diff.metamodel.ReferenceOrderChange;
import org.eclipse.emf.ecore.EClass;

/**
 * A diff signature is a sort of hash representing the changes contained by a
 * {@link DiffModel}. It is used as abstraction to enable a faster comparison of
 * {@link DiffElement} patterns.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffSignature {

	/**
	 * Comparator to get a total ordering of {@link EClass}es.
	 */
	private DiffElementTypeComparator diffElementTypeComparator = DiffElementTypeComparator
			.getInstance();

	/**
	 * The signature element map.
	 */
	private List<DiffElement> signatureElements = new ArrayList<DiffElement>();

	/**
	 * Creates a new {@link DiffSignature} for the specified
	 * <code>diffModel</code>.
	 * 
	 * @param diffModel
	 *            to create operation signature for.
	 */
	public DiffSignature(DiffModel diffModel) {
		add(diffModel);
	}

	/**
	 * Returns the totally ordered signature elements.
	 * 
	 * @return the totally ordered signature elements.
	 */
	public List<DiffElement> getSignatureElements() {
		return this.signatureElements;
	}

	/**
	 * Adds the leave diff elements of the specified <code>diffModel</code> to
	 * this signature.
	 * 
	 * @param diffModel
	 *            to add.
	 */
	public void add(DiffModel diffModel) {
		for (DiffElement diffElement : diffModel.getOwnedElements()) {
			add(diffElement);
		}
		Collections.sort(signatureElements, this.diffElementTypeComparator);
	}

	/**
	 * Adds the specified <code>diffElement</code> if it is a leave element or
	 * its children to this signature.
	 * 
	 * @param diffElement
	 *            to add.
	 */
	public void add(DiffElement diffElement) {
		if (diffElement.getSubDiffElements().size() > 0) {
			// add children recursively
			for (DiffElement subDiffElement : diffElement.getSubDiffElements()) {
				add(subDiffElement);
			}
		} else {
			// add leave diff element to signature
			// but filter reference order changes (for some reason we get these
			// changes but have to ignore them)
			if (!(diffElement instanceof DiffGroup)
					&& !(diffElement instanceof ReferenceOrderChange)) {
				signatureElements.add(diffElement);
			}
		}
	}

	/**
	 * Removes the specified <code>diffElement</code>.
	 * 
	 * @param diffElement
	 *            to remove.
	 */
	public void remove(DiffElement diffElement) {
		signatureElements.remove(diffElement);
	}

	/**
	 * Debug prints this signature to the specified <code>out</code> put stream.
	 * 
	 * @param out
	 *            output stream to print signature to.
	 */
	public void debugPring(PrintStream out) {
		for (DiffElement diffElement : this.signatureElements) {
			out.println(diffElement.toString());
		}
	}

}
