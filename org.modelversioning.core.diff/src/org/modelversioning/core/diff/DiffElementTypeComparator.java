/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff;

import java.util.Comparator;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.diff.metamodel.AttributeChange;
import org.eclipse.emf.compare.diff.metamodel.DiffElement;
import org.eclipse.emf.compare.diff.metamodel.DiffFactory;
import org.eclipse.emf.compare.diff.metamodel.DiffPackage;
import org.eclipse.emf.compare.diff.metamodel.ReferenceChange;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * {@link Comparator} for {@link DiffElement} types. This comparator compares
 * {@link DiffElement}s from an abstract point of view. The name of the
 * meta-class and the meta-classes of the elements they affect (cf. parameter)
 * are used to achieve a total, deterministic order and comparison of diff
 * elements.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiffElementTypeComparator implements Comparator<DiffElement> {

	/**
	 * Singleton instance.
	 */
	private static DiffElementTypeComparator INSTANCE = new DiffElementTypeComparator();

	private static final String SEPARATOR = ","; //$NON-NLS-1$

	/**
	 * The diff package to access meta features.
	 */
	private final DiffPackage diffPackage = DiffFactory.eINSTANCE
			.getDiffPackage();

	/**
	 * All structural features to check for parameter in the fixed order.
	 */
	private EList<EStructuralFeature> structuralFeatures = new BasicEList<EStructuralFeature>();

	/**
	 * Default constructor. Initializes this comparator.
	 */
	private DiffElementTypeComparator() {
		initStructuralFeatureList();
	}

	/**
	 * Returns the single instance.
	 * 
	 * @return the single instance of this comparator.
	 */
	public static DiffElementTypeComparator getInstance() {
		return INSTANCE;
	}

	/**
	 * Fills the list of structural features used for parameter comparison
	 * string generation.
	 */
	private void initStructuralFeatureList() {
		// changed feature names
		structuralFeatures.add(diffPackage.getAttributeChange_Attribute());
		structuralFeatures.add(diffPackage.getReferenceChange_Reference());
		// left elements
		structuralFeatures.add(diffPackage.getAttributeChange_LeftElement());
		structuralFeatures.add(diffPackage
				.getModelElementChangeLeftTarget_LeftElement());
		structuralFeatures.add(diffPackage.getReferenceChange_LeftElement());
		structuralFeatures.add(diffPackage.getUpdateModelElement_LeftElement());
		// left targets
		structuralFeatures.add(diffPackage
				.getAttributeChangeLeftTarget_LeftTarget());
		structuralFeatures.add(diffPackage.getMoveModelElement_LeftTarget());
		structuralFeatures.add(diffPackage
				.getReferenceChangeLeftTarget_LeftTarget());
		structuralFeatures.add(diffPackage
				.getReferenceChangeRightTarget_LeftTarget());
		structuralFeatures
				.add(diffPackage.getReferenceOrderChange_LeftTarget());
		structuralFeatures.add(diffPackage.getUpdateReference_LeftTarget());
		// right elements
		structuralFeatures.add(diffPackage.getAttributeChange_RightElement());
		structuralFeatures.add(diffPackage
				.getModelElementChangeRightTarget_RightElement());
		structuralFeatures.add(diffPackage.getReferenceChange_RightElement());
		structuralFeatures
				.add(diffPackage.getUpdateModelElement_RightElement());
		// right targets
		structuralFeatures.add(diffPackage
				.getAttributeChangeRightTarget_RightTarget());
		structuralFeatures.add(diffPackage.getMoveModelElement_RightTarget());
		structuralFeatures.add(diffPackage
				.getReferenceChangeLeftTarget_RightTarget());
		structuralFeatures.add(diffPackage
				.getReferenceChangeRightTarget_RightTarget());
		structuralFeatures.add(diffPackage
				.getReferenceOrderChange_RightTarget());
		structuralFeatures.add(diffPackage.getUpdateReference_RightTarget());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compare(DiffElement o1, DiffElement o2) {
		return getComparisonString(o1).compareTo(getComparisonString(o2));
	}

	/**
	 * Creates a total comparison string for a {@link DiffElement}.
	 * 
	 * @param diffElement
	 *            to get comparison string for.
	 * @return string that can be used to deterministically compare diff element
	 *         types.
	 */
	public String getComparisonString(DiffElement diffElement) {
		return diffElement.eClass().getName()
				+ getParameterComparisonString(diffElement);
	}

	/**
	 * Creates a comparison string for the parameters a diff element affects.
	 * 
	 * @param diffElement
	 *            to get parameter comparison string for.
	 * @return
	 */
	private String getParameterComparisonString(DiffElement diffElement) {
		String parameterString = ""; //$NON-NLS-1$
		// add parameter string for each feature if available
		for (EStructuralFeature feature : structuralFeatures) {
			try {
				EObject references = (EObject) diffElement.eGet(feature, true);
				if (references != null) {
					// in case this feature specifies the changed feature (only
					// for AttributeChange and ReferenceChange), get the feature
					// name
					if ((diffPackage.getAttributeChange_Attribute().equals(
							feature) || diffPackage
							.getReferenceChange_Reference().equals(feature))
							&& references instanceof EStructuralFeature
							&& (diffElement instanceof AttributeChange || diffElement instanceof ReferenceChange)) {
						parameterString += SEPARATOR
								+ ((EStructuralFeature) references).getName();
					} else {
						// else get the class name of the parameter
						parameterString += SEPARATOR
								+ references.eClass().getName();
					}
				}
			} catch (IllegalArgumentException e) {
			} catch (ClassCastException e) {
			} catch (NullPointerException npe) {
			}
		}
		return parameterString;
	}
}
