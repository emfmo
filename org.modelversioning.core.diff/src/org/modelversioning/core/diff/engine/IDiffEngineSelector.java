/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.engine;

import org.eclipse.emf.compare.match.metamodel.MatchModel;


/**
 * Provides an interface to select an appropriate implementation of a
 * {@link IDiffEngine}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public interface IDiffEngineSelector {
	
	/**
	 * Returns an instance of a {@link IDiffEngine} suitable for the specified
	 * {@link MatchModel} <code>matchModel</code>.
	 * 
	 * @param matchModel {@link MatchModel} to find an appropriate {@link IDiffEngine} for.
	 * @return the suitable instance of a {@link IDiffEngine}.
	 */
	public IDiffEngine selectEngine(MatchModel matchModel);

}
