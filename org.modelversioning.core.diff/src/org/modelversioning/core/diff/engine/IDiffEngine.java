/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.diff.engine;

import org.eclipse.emf.compare.diff.metamodel.DiffModel;
import org.eclipse.emf.compare.match.metamodel.MatchModel;

/**
 * A {@link IDiffEngine} has the responsibility to calculate and generate a
 * {@link DiffModel} out of a specified <i>two way</i> or <i>three way</i>
 * {@link MatchModel}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface IDiffEngine {

	/**
	 * Executes the diff calculation and generates a {@link DiffModel} as result
	 * of the differentiation process.
	 * 
	 * @param matchModel
	 *            the basis for the differentiation process.
	 * @return the generated {@link DiffModel}.
	 */
	public DiffModel generateDiffModel(MatchModel matchModel);

}
