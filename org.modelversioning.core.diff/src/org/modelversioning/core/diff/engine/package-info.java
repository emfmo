/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 *
 * This package contains the interfaces to select an appropriate {@link IDiffEngine}s and generating a {@link org.eclipse.emf.compare.diff.metamodel.DiffModel}.
 */
package org.modelversioning.core.diff.engine;