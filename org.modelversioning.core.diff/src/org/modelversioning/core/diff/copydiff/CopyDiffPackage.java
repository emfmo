/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff;

import org.eclipse.emf.compare.diff.metamodel.DiffPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.modelversioning.core.diff.copydiff.CopyDiffFactory
 * @model kind="package"
 * @generated
 */
public interface CopyDiffPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "copydiff";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.modelversioning.org/copydiff/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "copydiff";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CopyDiffPackage eINSTANCE = org.modelversioning.core.diff.copydiff.impl.CopyDiffPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.modelversioning.core.diff.copydiff.impl.CopyElementRightTargetImpl <em>Copy Element Right Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.diff.copydiff.impl.CopyElementRightTargetImpl
	 * @see org.modelversioning.core.diff.copydiff.impl.CopyDiffPackageImpl#getCopyElementRightTarget()
	 * @generated
	 */
	int COPY_ELEMENT_RIGHT_TARGET = 0;

	/**
	 * The feature id for the '<em><b>Sub Diff Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__SUB_DIFF_ELEMENTS = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__SUB_DIFF_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Is Hidden By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__IS_HIDDEN_BY = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__IS_HIDDEN_BY;

	/**
	 * The feature id for the '<em><b>Conflicting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__CONFLICTING = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__CONFLICTING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__KIND = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__KIND;

	/**
	 * The feature id for the '<em><b>Remote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__REMOTE = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__REMOTE;

	/**
	 * The feature id for the '<em><b>Left Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__LEFT_PARENT = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__LEFT_PARENT;

	/**
	 * The feature id for the '<em><b>Right Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__RIGHT_ELEMENT = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET__RIGHT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Copied Left Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Copy Element Right Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_RIGHT_TARGET_FEATURE_COUNT = DiffPackage.MODEL_ELEMENT_CHANGE_RIGHT_TARGET_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.modelversioning.core.diff.copydiff.impl.CopyElementLeftTargetImpl <em>Copy Element Left Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.modelversioning.core.diff.copydiff.impl.CopyElementLeftTargetImpl
	 * @see org.modelversioning.core.diff.copydiff.impl.CopyDiffPackageImpl#getCopyElementLeftTarget()
	 * @generated
	 */
	int COPY_ELEMENT_LEFT_TARGET = 1;

	/**
	 * The feature id for the '<em><b>Sub Diff Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__SUB_DIFF_ELEMENTS = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__SUB_DIFF_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Is Hidden By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__IS_HIDDEN_BY = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__IS_HIDDEN_BY;

	/**
	 * The feature id for the '<em><b>Conflicting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__CONFLICTING = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__CONFLICTING;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__KIND = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__KIND;

	/**
	 * The feature id for the '<em><b>Remote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__REMOTE = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__REMOTE;

	/**
	 * The feature id for the '<em><b>Right Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__RIGHT_PARENT = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__RIGHT_PARENT;

	/**
	 * The feature id for the '<em><b>Left Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__LEFT_ELEMENT = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET__LEFT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Copied Right Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Copy Element Left Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COPY_ELEMENT_LEFT_TARGET_FEATURE_COUNT = DiffPackage.MODEL_ELEMENT_CHANGE_LEFT_TARGET_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.diff.copydiff.CopyElementRightTarget <em>Copy Element Right Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Element Right Target</em>'.
	 * @see org.modelversioning.core.diff.copydiff.CopyElementRightTarget
	 * @generated
	 */
	EClass getCopyElementRightTarget();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.diff.copydiff.CopyElementRightTarget#getCopiedLeftElement <em>Copied Left Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Copied Left Element</em>'.
	 * @see org.modelversioning.core.diff.copydiff.CopyElementRightTarget#getCopiedLeftElement()
	 * @see #getCopyElementRightTarget()
	 * @generated
	 */
	EReference getCopyElementRightTarget_CopiedLeftElement();

	/**
	 * Returns the meta object for class '{@link org.modelversioning.core.diff.copydiff.CopyElementLeftTarget <em>Copy Element Left Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Copy Element Left Target</em>'.
	 * @see org.modelversioning.core.diff.copydiff.CopyElementLeftTarget
	 * @generated
	 */
	EClass getCopyElementLeftTarget();

	/**
	 * Returns the meta object for the reference '{@link org.modelversioning.core.diff.copydiff.CopyElementLeftTarget#getCopiedRightElement <em>Copied Right Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Copied Right Element</em>'.
	 * @see org.modelversioning.core.diff.copydiff.CopyElementLeftTarget#getCopiedRightElement()
	 * @see #getCopyElementLeftTarget()
	 * @generated
	 */
	EReference getCopyElementLeftTarget_CopiedRightElement();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CopyDiffFactory getCopyDiffFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.modelversioning.core.diff.copydiff.impl.CopyElementRightTargetImpl <em>Copy Element Right Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.diff.copydiff.impl.CopyElementRightTargetImpl
		 * @see org.modelversioning.core.diff.copydiff.impl.CopyDiffPackageImpl#getCopyElementRightTarget()
		 * @generated
		 */
		EClass COPY_ELEMENT_RIGHT_TARGET = eINSTANCE.getCopyElementRightTarget();

		/**
		 * The meta object literal for the '<em><b>Copied Left Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT = eINSTANCE.getCopyElementRightTarget_CopiedLeftElement();

		/**
		 * The meta object literal for the '{@link org.modelversioning.core.diff.copydiff.impl.CopyElementLeftTargetImpl <em>Copy Element Left Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.modelversioning.core.diff.copydiff.impl.CopyElementLeftTargetImpl
		 * @see org.modelversioning.core.diff.copydiff.impl.CopyDiffPackageImpl#getCopyElementLeftTarget()
		 * @generated
		 */
		EClass COPY_ELEMENT_LEFT_TARGET = eINSTANCE.getCopyElementLeftTarget();

		/**
		 * The meta object literal for the '<em><b>Copied Right Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT = eINSTANCE.getCopyElementLeftTarget_CopiedRightElement();

	}

} //CopyDiffPackage
