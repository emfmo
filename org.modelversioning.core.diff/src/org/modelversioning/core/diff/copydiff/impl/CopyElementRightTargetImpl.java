/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.compare.diff.metamodel.impl.ModelElementChangeRightTargetImpl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.modelversioning.core.diff.copydiff.CopyDiffPackage;
import org.modelversioning.core.diff.copydiff.CopyElementRightTarget;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copy Element Right Target</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.modelversioning.core.diff.copydiff.impl.CopyElementRightTargetImpl#getCopiedLeftElement <em>Copied Left Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CopyElementRightTargetImpl extends ModelElementChangeRightTargetImpl implements CopyElementRightTarget {
	/**
	 * The cached value of the '{@link #getCopiedLeftElement() <em>Copied Left Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopiedLeftElement()
	 * @generated
	 * @ordered
	 */
	protected EObject copiedLeftElement;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyElementRightTargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CopyDiffPackage.Literals.COPY_ELEMENT_RIGHT_TARGET;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getCopiedLeftElement() {
		if (copiedLeftElement != null && copiedLeftElement.eIsProxy()) {
			InternalEObject oldCopiedLeftElement = (InternalEObject)copiedLeftElement;
			copiedLeftElement = eResolveProxy(oldCopiedLeftElement);
			if (copiedLeftElement != oldCopiedLeftElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT, oldCopiedLeftElement, copiedLeftElement));
			}
		}
		return copiedLeftElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetCopiedLeftElement() {
		return copiedLeftElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopiedLeftElement(EObject newCopiedLeftElement) {
		EObject oldCopiedLeftElement = copiedLeftElement;
		copiedLeftElement = newCopiedLeftElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT, oldCopiedLeftElement, copiedLeftElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT:
				if (resolve) return getCopiedLeftElement();
				return basicGetCopiedLeftElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT:
				setCopiedLeftElement((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT:
				setCopiedLeftElement((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT:
				return copiedLeftElement != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @generated NOT
	 * @see org.eclipse.emf.compare.diff.metamodel.impl.DiffElementImpl#toString()
	 */
	@Override
	public String toString() {
		if (isRemote())
			return "A copy of " + rightElement + " has been remotely added";
		return rightElement + " has been removed";
	}

} //CopyElementRightTargetImpl
