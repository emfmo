/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.modelversioning.core.diff.copydiff.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyDiffFactoryImpl extends EFactoryImpl implements CopyDiffFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CopyDiffFactory init() {
		try {
			CopyDiffFactory theCopyDiffFactory = (CopyDiffFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.modelversioning.org/copydiff/1.0"); 
			if (theCopyDiffFactory != null) {
				return theCopyDiffFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CopyDiffFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyDiffFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CopyDiffPackage.COPY_ELEMENT_RIGHT_TARGET: return createCopyElementRightTarget();
			case CopyDiffPackage.COPY_ELEMENT_LEFT_TARGET: return createCopyElementLeftTarget();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyElementRightTarget createCopyElementRightTarget() {
		CopyElementRightTargetImpl copyElementRightTarget = new CopyElementRightTargetImpl();
		return copyElementRightTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyElementLeftTarget createCopyElementLeftTarget() {
		CopyElementLeftTargetImpl copyElementLeftTarget = new CopyElementLeftTargetImpl();
		return copyElementLeftTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyDiffPackage getCopyDiffPackage() {
		return (CopyDiffPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CopyDiffPackage getPackage() {
		return CopyDiffPackage.eINSTANCE;
	}

} //CopyDiffFactoryImpl
