/**
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.modelversioning.core.diff.copydiff.impl;

import org.eclipse.emf.compare.diff.metamodel.DiffPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.modelversioning.core.diff.copydiff.CopyDiffFactory;
import org.modelversioning.core.diff.copydiff.CopyDiffPackage;
import org.modelversioning.core.diff.copydiff.CopyElementLeftTarget;
import org.modelversioning.core.diff.copydiff.CopyElementRightTarget;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CopyDiffPackageImpl extends EPackageImpl implements CopyDiffPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyElementRightTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass copyElementLeftTargetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.modelversioning.core.diff.copydiff.CopyDiffPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CopyDiffPackageImpl() {
		super(eNS_URI, CopyDiffFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CopyDiffPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CopyDiffPackage init() {
		if (isInited) return (CopyDiffPackage)EPackage.Registry.INSTANCE.getEPackage(CopyDiffPackage.eNS_URI);

		// Obtain or create and register package
		CopyDiffPackageImpl theCopyDiffPackage = (CopyDiffPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CopyDiffPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CopyDiffPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DiffPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theCopyDiffPackage.createPackageContents();

		// Initialize created meta-data
		theCopyDiffPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCopyDiffPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CopyDiffPackage.eNS_URI, theCopyDiffPackage);
		return theCopyDiffPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyElementRightTarget() {
		return copyElementRightTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCopyElementRightTarget_CopiedLeftElement() {
		return (EReference)copyElementRightTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCopyElementLeftTarget() {
		return copyElementLeftTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCopyElementLeftTarget_CopiedRightElement() {
		return (EReference)copyElementLeftTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyDiffFactory getCopyDiffFactory() {
		return (CopyDiffFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		copyElementRightTargetEClass = createEClass(COPY_ELEMENT_RIGHT_TARGET);
		createEReference(copyElementRightTargetEClass, COPY_ELEMENT_RIGHT_TARGET__COPIED_LEFT_ELEMENT);

		copyElementLeftTargetEClass = createEClass(COPY_ELEMENT_LEFT_TARGET);
		createEReference(copyElementLeftTargetEClass, COPY_ELEMENT_LEFT_TARGET__COPIED_RIGHT_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DiffPackage theDiffPackage = (DiffPackage)EPackage.Registry.INSTANCE.getEPackage(DiffPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		copyElementRightTargetEClass.getESuperTypes().add(theDiffPackage.getModelElementChangeRightTarget());
		copyElementLeftTargetEClass.getESuperTypes().add(theDiffPackage.getModelElementChangeLeftTarget());

		// Initialize classes and features; add operations and parameters
		initEClass(copyElementRightTargetEClass, CopyElementRightTarget.class, "CopyElementRightTarget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCopyElementRightTarget_CopiedLeftElement(), ecorePackage.getEObject(), null, "copiedLeftElement", null, 0, 1, CopyElementRightTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(copyElementLeftTargetEClass, CopyElementLeftTarget.class, "CopyElementLeftTarget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCopyElementLeftTarget_CopiedRightElement(), ecorePackage.getEObject(), null, "copiedRightElement", null, 0, 1, CopyElementLeftTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //CopyDiffPackageImpl
