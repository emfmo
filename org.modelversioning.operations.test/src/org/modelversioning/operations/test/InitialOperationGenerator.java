/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.test;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.engines.IOperationGenerationEngine;
import org.modelversioning.operations.engines.impl.OperationGenerationEngineImpl;

/**
 * Launch-able class which creates an initial {@link Operation} for two
 * resources.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class InitialOperationGenerator {

	private Resource origin;
	private Resource working;
	private OperationSpecification operationModel;

	private IOperationGenerationEngine engine;

	/**
	 * Instantiates this class.
	 */
	public InitialOperationGenerator() {
		super();
	}

	/**
	 * Instantiates this class specifying the origin and the working
	 * {@link Resource}.
	 * 
	 * @param origin
	 *            {@link Resource} containing the origin model.
	 * @param working
	 *            {@link Resource} containing the working model.
	 */
	public InitialOperationGenerator(Resource origin, Resource working) {
		super();
		this.origin = origin;
		this.working = working;
	}

	/**
	 * Starts the generation.
	 */
	public void doGeneration() {
		// set engine if its not set
		if (engine == null)
			engine = new OperationGenerationEngineImpl();

		// call operation model generation
		generateOperationModel();
	}

	/**
	 * Generates the {@link Operation} model for the set {@link #origin} and
	 * {@link #working} resources and saves it to {@link #operationModel}.
	 */
	private void generateOperationModel() {
		// guard null resources
		if (origin == null || working == null)
			throw new RuntimeException("unset working or origin model");

		// generate operation model and save it to instance field.
		try {
			setOperationModel(engine.generateOperationSpecification(origin, working));
		} catch (MatchException e) {
			throw new RuntimeException("Match failed", e);
		}
	}

	/**
	 * @return the origin
	 */
	public Resource getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(Resource origin) {
		this.origin = origin;
	}

	/**
	 * @return the working
	 */
	public Resource getWorking() {
		return working;
	}

	/**
	 * @param working
	 *            the working to set
	 */
	public void setWorking(Resource working) {
		this.working = working;
	}

	/**
	 * @return the operationModel
	 */
	public OperationSpecification getOperationModel() {
		return operationModel;
	}

	/**
	 * @param operationModel
	 *            the operationModel to set
	 */
	public void setOperationModel(OperationSpecification operationModel) {
		this.operationModel = operationModel;
	}

	/**
	 * @return the engine
	 */
	public IOperationGenerationEngine getEngine() {
		return engine;
	}

	/**
	 * @param engine
	 *            the engine to set
	 */
	public void setEngine(IOperationGenerationEngine engine) {
		this.engine = engine;
	}

	/**
	 * Generates an initial {@link Operation} model for the specified resources.
	 * 
	 * @param args
	 *            First argument represents the file uri to the origin, the
	 *            second the working model and the third the path to save the
	 *            resulting operation model to.
	 * @throws IOException
	 *             if read or write of model files fails.
	 */
	public static void main(String[] args) throws IOException {
		// guard wrong number of arguments
		if (args.length != 3)
			throw new IllegalArgumentException("Wrong number of arguments: "
					+ args.length + " instead of 3.");

		// load resources
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());
		
		System.out.println("Loading origin model from " + args[0]);
		URI originFileURI = URI.createURI(args[0], true);
		Resource origin = resourceSet.getResource(originFileURI, true);
		
		System.out.println("Loading working model from " + args[1]);
		URI workingFileURI = URI.createURI(args[1], true);
		Resource working = resourceSet.getResource(workingFileURI, true);

		// instantiate this class
		System.out.println("Generation of operations model...");
		InitialOperationGenerator generator = new InitialOperationGenerator(
				origin, working);
		generator.doGeneration();
		System.out.println("Generation of operations model... DONE");

		// save operation model
		System.out.println("Saving operation model to " + args[2]);
		URI operationModelUri = URI.createURI(args[2], true);
		Resource operationModelResource = resourceSet
				.createResource(operationModelUri);
		operationModelResource.getContents().add(generator.getOperationModel());
		operationModelResource.save(null);
	}

}
