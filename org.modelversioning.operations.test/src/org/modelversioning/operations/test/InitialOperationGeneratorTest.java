package org.modelversioning.operations.test;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.util.UUIDUtil;
import org.modelversioning.operations.Iteration;
import org.modelversioning.operations.IterationType;
import org.modelversioning.operations.OperationsFactory;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.util.OperationModelToTextGenerator;

public class InitialOperationGeneratorTest extends TestCase {

	private String originURI = "/org.modelversioning.operations.test/models/ecore/extract_super_class/initial.ecore";
	private String workingURI = "/org.modelversioning.operations.test/models/ecore/extract_super_class/revised.ecore";
	private String operationModelURI = "models/ecore/extract_super_class/extract_super_class.operation";
	private String operationName = "Extract Super Class";

	private ResourceSet resourceSet;
	private Resource origin;
	private Resource working;

	protected void setUp() throws Exception {

		// load resources
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());

		System.out.println("Loading origin model from " + originURI);
		URI originFileURI = URI.createPlatformPluginURI(originURI, true);
		origin = resourceSet.getResource(originFileURI, true);

		System.out.println("Loading working model from " + workingURI);
		URI workingFileURI = URI.createPlatformPluginURI(workingURI, true);
		working = resourceSet.getResource(workingFileURI, true);

	}

	protected void tearDown() throws Exception {
		origin.unload();
		working.unload();
	}

	public void testDoGeneration() throws IOException {
		// instantiate this class
		System.out.println("Generation of operations model...");
		InitialOperationGenerator generator = new InitialOperationGenerator(
				origin, working);
		generator.doGeneration();
		generator.getOperationModel().setName(operationName);
		System.out.println("Generation of operations model... DONE");

		// add user inputs
		UserInput userInput = OperationsFactory.eINSTANCE.createUserInput();
		userInput.setName("Name of Super Class");
		Template classTemplate = (Template) generator.getOperationModel()
				.getPostconditions().getRootTemplate().getSubTemplates().get(1);
		userInput.setTemplate(classTemplate);
		for (EStructuralFeature feature : classTemplate.getRepresentative()
				.eClass().getEAllStructuralFeatures()) {
			if ("name".equals(feature.getName())) {
				userInput.setFeature(feature);
			}
		}
		generator.getOperationModel().getUserInputs().add(userInput);

		// add iterations
		Iteration iteration = OperationsFactory.eINSTANCE.createIteration();
		iteration.setIterationType(IterationType.FOR_SOME);
		Template operationTemplate = (Template) generator.getOperationModel()
				.getPreconditions().getRootTemplate().getSubTemplates().get(0)
				.getSubTemplates().get(0);
		iteration.setTemplate(operationTemplate);
		generator.getOperationModel().getIterations().add(iteration);

		// save operation model
		System.out.println("Saving operation model to " + operationModelURI);
		URI operationModelUri = URI.createURI(operationModelURI, true);
		Resource operationModelResource = resourceSet
				.createResource(operationModelUri);
		operationModelResource.getContents().add(generator.getOperationModel());
		
		// remove ids before saving:
		UUIDUtil.removeUUIDs(generator.getOperationModel());
		
		operationModelResource.save(null);

		// output to sys out
		new OperationModelToTextGenerator().generateText(generator
				.getOperationModel(), System.out);
	}

}
