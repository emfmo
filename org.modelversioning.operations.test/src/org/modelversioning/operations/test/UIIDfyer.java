package org.modelversioning.operations.test;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;

import junit.framework.TestCase;

public class UIIDfyer extends TestCase {
	
	private String originURI = "models/ecore/extract_super_class/initial.ecore";
	
	private ResourceSet resourceSet;
	private Resource origin;

	protected void setUp() throws Exception {
		super.setUp();
		// load resources
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());
		
		System.out.println("Loading model from " + originURI);
		URI originFileURI = URI.createURI(originURI, true);
		origin = resourceSet.getResource(originFileURI, true);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		origin.unload();
	}
	
	public void testUUIDfyer() throws IOException {
		origin.save(null);
	}

}
