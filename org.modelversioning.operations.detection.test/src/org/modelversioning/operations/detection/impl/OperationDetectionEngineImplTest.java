/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import java.util.Set;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.diff.service.DiffService;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.repository.impl.OperationRepositoryImpl;

/**
 * Tests basic function of {@link OperationDetectionEngineImpl} using predefined
 * operations and samples.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class OperationDetectionEngineImplTest extends TestCase {

	final static String opSpecPath = "models/operationSpecifications/";
	final static String samplesEcorePath = "models/samples/ecore/";

	ResourceSet resourceSet = new ResourceSetImpl();

	final DiffService diffService = new DiffService();
	final MatchService matchService = new MatchService();

	/**
	 * The repository.
	 */
	ModelOperationRepository repository = new OperationRepositoryImpl();

	/**
	 * Operation: Ecore Inline Class
	 */
	OperationSpecification opSpecEcoreInlineClass;

	/**
	 * Operation: Ecore Pull Up Field
	 */
	OperationSpecification opSpecEcorePullUpField;

	/**
	 * Operation: Ecore Extract Class
	 */
	OperationSpecification opSpecEcoreExtractClass;

	/**
	 * Operation: Ecore Super Extract Class
	 */
	OperationSpecification opSpecEcoreExtractSuperClass;

	/**
	 * Sample: Ecore Pull Up Field 1
	 */
	Resource sampleEcoreNoOperation1_Initial;
	Resource sampleEcoreNoOperation1_Revisied;

	/**
	 * Sample: Ecore Inline Class 1
	 */
	Resource sampleEcoreInlineClass1_Initial;
	Resource sampleEcoreInlineClass1_Revisied;

	/**
	 * Sample: Ecore Pull Up Field 1
	 */
	Resource sampleEcorePullUpField1_Initial;
	Resource sampleEcorePullUpField1_Revisied;

	/**
	 * Sample: Ecore Multiple Occurrence 1
	 */
	Resource sampleEcoreMultipleOccurrence1_Initial;
	Resource sampleEcoreMultipleOccurrence1_Revisied;

	/**
	 * {@inheritDoc}
	 * 
	 * Set up the operation repository and load sample models.
	 */
	protected void setUp() throws Exception {
		super.setUp();
		configureResourceSet();

		// load and register operation specifications
		loadOperationSpecificationEcoreInlineClass();
		loadOperationSpecificationEcorePullUpMethod();
		loadOperationSpecificationEcoreExtractClass();
		loadOperationSpecificationEcoreExtractSuperClass();
		registerToRepository(opSpecEcoreInlineClass);
		registerToRepository(opSpecEcorePullUpField);
		registerToRepository(opSpecEcoreExtractClass);
		registerToRepository(opSpecEcoreExtractSuperClass);

		// load ecore no operation sample 1
		sampleEcoreNoOperation1_Initial = loadResource(samplesEcorePath
				+ "noCompositeOperation/sample1_origin.ecore");
		Assert
				.assertTrue(sampleEcoreNoOperation1_Initial.getContents()
						.size() == 1);
		sampleEcoreNoOperation1_Revisied = loadResource(samplesEcorePath
				+ "noCompositeOperation/sample1_revised.ecore");
		Assert
				.assertTrue(sampleEcoreNoOperation1_Revisied.getContents()
						.size() == 1);

		// load ecore inline class sample 1
		sampleEcoreInlineClass1_Initial = loadResource(samplesEcorePath
				+ "inlineClass/sample1_origin.ecore");
		Assert
				.assertTrue(sampleEcoreInlineClass1_Initial.getContents()
						.size() == 1);
		sampleEcoreInlineClass1_Revisied = loadResource(samplesEcorePath
				+ "inlineClass/sample1_revised.ecore");
		Assert
				.assertTrue(sampleEcoreInlineClass1_Revisied.getContents()
						.size() == 1);
		// load ecore pull up field sample 1
		sampleEcorePullUpField1_Initial = loadResource(samplesEcorePath
				+ "pullUpField/sample1_origin.ecore");
		Assert
				.assertTrue(sampleEcorePullUpField1_Initial.getContents()
						.size() == 1);
		sampleEcorePullUpField1_Revisied = loadResource(samplesEcorePath
				+ "pullUpField/sample1_revised.ecore");
		Assert
				.assertTrue(sampleEcorePullUpField1_Revisied.getContents()
						.size() == 1);
		// load ecore multiple occurrence sample 1
		sampleEcoreMultipleOccurrence1_Initial = loadResource(samplesEcorePath
				+ "multipleOccurrence/sample1_origin.ecore");
		Assert.assertTrue(sampleEcoreMultipleOccurrence1_Initial.getContents()
				.size() == 1);
		sampleEcoreMultipleOccurrence1_Revisied = loadResource(samplesEcorePath
				+ "multipleOccurrence/sample1_revised.ecore");
		Assert.assertTrue(sampleEcoreMultipleOccurrence1_Revisied.getContents()
				.size() == 1);
	}

	private void registerToRepository(
			OperationSpecification operationSpecification) {
		this.repository.register(operationSpecification);
	}

	private void loadOperationSpecificationEcorePullUpMethod() {
		Resource opSpecEcorePullUpFieldResource = loadResource(opSpecPath
				+ "ecore_PullUpField.operation");
		Assert
				.assertTrue(opSpecEcorePullUpFieldResource.getContents().size() > 0);
		Assert
				.assertTrue(opSpecEcorePullUpFieldResource.getContents().get(0) instanceof OperationSpecification);
		opSpecEcorePullUpField = (OperationSpecification) opSpecEcorePullUpFieldResource
				.getContents().get(0);
	}

	private void loadOperationSpecificationEcoreInlineClass() {
		Resource opSpecEcoreInlineClassResource = loadResource(opSpecPath
				+ "ecore_InlineClass.operation");
		Assert
				.assertTrue(opSpecEcoreInlineClassResource.getContents().size() > 0);
		Assert
				.assertTrue(opSpecEcoreInlineClassResource.getContents().get(0) instanceof OperationSpecification);
		opSpecEcoreInlineClass = (OperationSpecification) opSpecEcoreInlineClassResource
				.getContents().get(0);
	}

	private void loadOperationSpecificationEcoreExtractClass() {
		Resource opSpecEcoreExtractClassResource = loadResource(opSpecPath
				+ "ecore_ExtractClass.operation");
		Assert
				.assertTrue(opSpecEcoreExtractClassResource.getContents()
						.size() > 0);
		Assert
				.assertTrue(opSpecEcoreExtractClassResource.getContents()
						.get(0) instanceof OperationSpecification);
		opSpecEcoreExtractClass = (OperationSpecification) opSpecEcoreExtractClassResource
				.getContents().get(0);
	}

	private void loadOperationSpecificationEcoreExtractSuperClass() {
		Resource opSpecEcoreExtractSuperClassResource = loadResource(opSpecPath
				+ "ecore_ExtractSuperClass.operation");
		Assert.assertTrue(opSpecEcoreExtractSuperClassResource.getContents()
				.size() > 0);
		Assert.assertTrue(opSpecEcoreExtractSuperClassResource.getContents()
				.get(0) instanceof OperationSpecification);
		opSpecEcoreExtractSuperClass = (OperationSpecification) opSpecEcoreExtractSuperClassResource
				.getContents().get(0);
	}

	private void configureResourceSet() {
		// init resource set
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new EcoreResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("xmi", new EcoreResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("operation", new EcoreResourceFactoryImpl());
	}

	/**
	 * Loads the resource with the specified <code>fileURI</code>.
	 * 
	 * @param fileURI
	 *            to load.
	 * @return loaded resource.
	 */
	private Resource loadResource(String fileURIString) {
		URI fileURI = URI.createURI(fileURIString, true);
		return resourceSet.getResource(fileURI, true);
	}

	/**
	 * {@inheritDoc}
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for
	 * {@link org.modelversioning.operations.detection.impl.OperationDetectionEngineImpl#findOccurrences(org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot)}
	 * .
	 * 
	 * @throws MatchException
	 *             if matching fails.
	 * @throws UnsupportedConditionLanguage
	 */
	public void testFindOccurrences() throws MatchException,
			UnsupportedConditionLanguage {
		// initialize detection engine
		OperationDetectionEngineImpl detectionEngine = new OperationDetectionEngineImpl();
		detectionEngine.setOperationsRepository(repository);

		// test ecore no operation sample 1
		doTestEcoreNoOperationSample1(detectionEngine);

		// test ecore pull up field sample 1
		doTestEcorePullUpFieldSample1(detectionEngine);

		// test ecore inline class sample 1
		doTestEcoreInlineClassSample1(detectionEngine);

		// test ecore multiple occurrences sample 1
		doTestEcoreMultipleOccurrenceSample1(detectionEngine);
	}

	private void doTestEcoreNoOperationSample1(
			OperationDetectionEngineImpl detectionEngine)
			throws MatchException, UnsupportedConditionLanguage {
		// start detection
		System.out.println("No Composite Operation Sample 1");
		System.out.println("===============================");

		// do the diff
		long beforeCompare = System.currentTimeMillis();
		ComparisonResourceSnapshot comparisonSnapshot = compare(
				this.sampleEcoreNoOperation1_Initial,
				this.sampleEcoreNoOperation1_Revisied);
		long beforeDetection = System.currentTimeMillis();
		System.out.println("Took " + (beforeDetection - beforeCompare)
				+ " ms to diff models");

		// conduct detection
		Set<OperationOccurrence> occurrences = detectionEngine
				.findOccurrences(comparisonSnapshot);
		long afterDetection = System.currentTimeMillis();

		// test occurrences
		assertEquals(0, occurrences.size());

		System.out.println("Took " + (afterDetection - beforeDetection)
				+ " ms to correctly find no operation in sample 1");
		System.out.println();

	}

	private void doTestEcorePullUpFieldSample1(
			OperationDetectionEngineImpl detectionEngine)
			throws MatchException, UnsupportedConditionLanguage {
		// start detection
		System.out.println("Pull Up Field Sample 1");
		System.out.println("======================");

		// do the diff
		long beforeCompare = System.currentTimeMillis();
		ComparisonResourceSnapshot comparisonSnapshot = compare(
				this.sampleEcorePullUpField1_Initial,
				this.sampleEcorePullUpField1_Revisied);
		long beforeDetection = System.currentTimeMillis();
		System.out.println("Took " + (beforeDetection - beforeCompare)
				+ " ms to diff models");

		// conduct detection
		Set<OperationOccurrence> occurrences = detectionEngine
				.findOccurrences(comparisonSnapshot);
		long afterDetection = System.currentTimeMillis();

		// test occurrences
		assertEquals(1, occurrences.size());
		OperationOccurrence pullUpFieldOccurrence = occurrences.iterator()
				.next();
		assertEquals(opSpecEcorePullUpField.getName(), pullUpFieldOccurrence
				.getAppliedOperationName());
//		TemplateBindingCollection preConditionBinding = pullUpFieldOccurrence
//				.getPreConditionBinding();
//		TemplateBindingCollection postConditionBinding = pullUpFieldOccurrence
//				.getPostConditionBinding();

		// TODO create tests checking pre and post condition binding

		System.out.println("Took " + (afterDetection - beforeDetection)
				+ " ms to correctly find "
				+ pullUpFieldOccurrence.getAppliedOperationName()
				+ " in sample 1");
		System.out.println(pullUpFieldOccurrence.getTitle());
		System.out.println();
	}

	private void doTestEcoreInlineClassSample1(
			OperationDetectionEngineImpl detectionEngine)
			throws MatchException, UnsupportedConditionLanguage {
		// start detection
		System.out.println("Inline Class Sample 1");
		System.out.println("======================");

		// do the diff
		long beforeCompare = System.currentTimeMillis();
		ComparisonResourceSnapshot comparisonSnapshot = compare(
				this.sampleEcoreInlineClass1_Initial,
				this.sampleEcoreInlineClass1_Revisied);
		long beforeDetection = System.currentTimeMillis();
		System.out.println("Took " + (beforeDetection - beforeCompare)
				+ " ms to diff models");

		// conduct detection
		Set<OperationOccurrence> occurrences = detectionEngine
				.findOccurrences(comparisonSnapshot);
		long afterDetection = System.currentTimeMillis();

		// test occurrences
		assertEquals(1, occurrences.size());
		OperationOccurrence inlineClassOccurrence = occurrences.iterator()
				.next();
		assertEquals(opSpecEcoreInlineClass.getName(), inlineClassOccurrence
				.getAppliedOperationName());
//		TemplateBindingCollection preConditionBinding = inlineClassOccurrence
//				.getPreConditionBinding();
//		TemplateBindingCollection postConditionBinding = inlineClassOccurrence
//				.getPostConditionBinding();

		// TODO create tests checking pre and post condition binding

		System.out.println("Took " + (afterDetection - beforeDetection)
				+ " ms to correctly find "
				+ inlineClassOccurrence.getAppliedOperationName()
				+ " in sample 1");
		System.out.println();
	}

	private void doTestEcoreMultipleOccurrenceSample1(
			OperationDetectionEngineImpl detectionEngine)
			throws MatchException, UnsupportedConditionLanguage {
		// start detection
		System.out.println("Multiple Occurrence Sample 1");
		System.out.println("============================");

		// do the diff
		long beforeCompare = System.currentTimeMillis();
		ComparisonResourceSnapshot comparisonSnapshot = compare(
				this.sampleEcoreMultipleOccurrence1_Initial,
				this.sampleEcoreMultipleOccurrence1_Revisied);
		long beforeDetection = System.currentTimeMillis();
		System.out.println("Took " + (beforeDetection - beforeCompare)
				+ " ms to diff models");

		// conduct detection
		Set<OperationOccurrence> occurrences = detectionEngine
				.findOccurrences(comparisonSnapshot);
		long afterDetection = System.currentTimeMillis();

		System.out.println("Detected Occurrences:");
		for (OperationOccurrence oc : occurrences) {
			System.out.println(oc.getAppliedOperationName() + ": "
					+ oc.getTitle());
		}

		// test occurrences
		assertEquals(5, occurrences.size());

		// TODO create tests checking pre and post condition binding

		System.out.println("Took " + (afterDetection - beforeDetection)
				+ " ms to correctly find " + occurrences.size()
				+ " operation occurrences in sample 1");
		System.out.println();
	}

	/**
	 * Compares the specified resources.
	 * 
	 * @param resource1
	 *            origin resource to compare.
	 * @param resource2
	 *            revised resource to compare.
	 * @return comparison result.
	 * @throws MatchException
	 *             if matching fails.
	 */
	private ComparisonResourceSnapshot compare(Resource resource1,
			Resource resource2) throws MatchException {
		MatchModel matchModel = matchService.generateMatchModel(resource1,
				resource2);
		ComparisonResourceSnapshot comparisonResourceSnapshot = diffService
				.generateComparisonResourceSnapshot(matchModel);
		return comparisonResourceSnapshot;
	}

}
