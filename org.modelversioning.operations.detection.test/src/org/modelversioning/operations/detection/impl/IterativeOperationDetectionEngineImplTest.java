/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.detection.impl;

import java.util.Set;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.diff.metamodel.ComparisonResourceSnapshot;
import org.eclipse.emf.compare.match.metamodel.MatchModel;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.templatebindings.TemplateBinding;
import org.modelversioning.core.diff.service.DiffService;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;
import org.modelversioning.core.match.MatchException;
import org.modelversioning.core.match.service.MatchService;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.detection.operationoccurrence.OperationOccurrence;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.repository.impl.OperationRepositoryImpl;

/**
 * Tests basic function of {@link IterativeOperationDetectionEngineImpl} using
 * predefined operations and samples.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class IterativeOperationDetectionEngineImplTest extends TestCase {

	final static String opSpecPath = "models/operationSpecifications/";
	final static String samplesEcorePath = "models/samples/ecore/";

	ResourceSet resourceSet = new ResourceSetImpl();

	final DiffService diffService = new DiffService();
	final MatchService matchService = new MatchService();

	/**
	 * The repository.
	 */
	ModelOperationRepository repository = new OperationRepositoryImpl();

	/**
	 * Operation: Add Reference
	 */
	OperationSpecification opSpecEcoreAddReference;

	/**
	 * Operation: Move Attribute Over Reference
	 */
	OperationSpecification opSpecEcoreMoveAttributeOverReference;

	/**
	 * Sample 1: First add reference and then move attribute over created
	 * reference
	 */
	Resource sampleIterativeEcore1_Initial;
	Resource sampleIterativeEcore1_Revisied;

	/**
	 * {@inheritDoc}
	 * 
	 * Set up the operation repository and load sample models.
	 */
	protected void setUp() throws Exception {
		super.setUp();
		configureResourceSet();

		// load and register operation specifications
		loadOperationSpecificationEcoreAddReference();
		loadOperationSpecificationEcoreMoveAttributeOverReference();
		registerToRepository(opSpecEcoreAddReference);
		registerToRepository(opSpecEcoreMoveAttributeOverReference);

		// load ecore no operation sample 1
		sampleIterativeEcore1_Initial = loadResource(samplesEcorePath
				+ "iterative-test/sample1_origin.ecore");
		Assert.assertTrue(sampleIterativeEcore1_Initial.getContents().size() == 1);
		sampleIterativeEcore1_Revisied = loadResource(samplesEcorePath
				+ "iterative-test/sample1_revised.ecore");
		Assert.assertTrue(sampleIterativeEcore1_Revisied.getContents().size() == 1);
	}

	private void registerToRepository(
			OperationSpecification operationSpecification) {
		this.repository.register(operationSpecification);
	}

	private void loadOperationSpecificationEcoreMoveAttributeOverReference() {
		Resource opSpecEcorePullUpFieldResource = loadResource(opSpecPath
				+ "ecore_MoveAttributeOverReference.operation");
		Assert.assertTrue(opSpecEcorePullUpFieldResource.getContents().size() > 0);
		Assert.assertTrue(opSpecEcorePullUpFieldResource.getContents().get(0) instanceof OperationSpecification);
		opSpecEcoreMoveAttributeOverReference = (OperationSpecification) opSpecEcorePullUpFieldResource
				.getContents().get(0);
	}

	private void loadOperationSpecificationEcoreAddReference() {
		Resource opSpecEcoreInlineClassResource = loadResource(opSpecPath
				+ "ecore_AddReference.operation");
		Assert.assertTrue(opSpecEcoreInlineClassResource.getContents().size() > 0);
		Assert.assertTrue(opSpecEcoreInlineClassResource.getContents().get(0) instanceof OperationSpecification);
		opSpecEcoreAddReference = (OperationSpecification) opSpecEcoreInlineClassResource
				.getContents().get(0);
	}

	private void configureResourceSet() {
		// init resource set
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("*", new UUIDResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("operation", new EcoreResourceFactoryImpl());
	}

	/**
	 * Loads the resource with the specified <code>fileURI</code>.
	 * 
	 * @param fileURI
	 *            to load.
	 * @return loaded resource.
	 */
	private Resource loadResource(String fileURIString) {
		URI fileURI = URI.createURI(fileURIString, true);
		return resourceSet.getResource(fileURI, true);
	}

	/**
	 * {@inheritDoc}
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testFindOccurrences() throws MatchException,
			UnsupportedConditionLanguage {
		// initialize detection engine
		IterativeOperationDetectionEngineImpl detectionEngine = new IterativeOperationDetectionEngineImpl();
		detectionEngine.setOperationsRepository(repository);
		detectionEngine.setDiffService(diffService);
		detectionEngine.setMatchService(matchService);
		detectionEngine.setResourceSet(resourceSet);

		// test ecore no operation sample 1
		doTestEcoreIterativeSample1(detectionEngine);
	}

	private void doTestEcoreIterativeSample1(
			IterativeOperationDetectionEngineImpl detectionEngine)
			throws MatchException, UnsupportedConditionLanguage {
		// start detection
		System.out.println("Iterative Detection Sample 1");
		System.out.println("===============================");

		// do the diff
		long beforeCompare = System.currentTimeMillis();
		ComparisonResourceSnapshot comparisonSnapshot = compare(
				this.sampleIterativeEcore1_Initial,
				this.sampleIterativeEcore1_Revisied);
		long beforeDetection = System.currentTimeMillis();
		System.out.println("Took " + (beforeDetection - beforeCompare)
				+ " ms to diff models");

		// conduct detection
		Set<OperationOccurrence> occurrences = detectionEngine
				.findOccurrences(comparisonSnapshot);
		long afterDetection = System.currentTimeMillis();

		// test occurrences
		for (OperationOccurrence occurrence : occurrences) {
			System.out.println(occurrence.getAppliedOperationName()
					+ " (Order Hint: " + occurrence.getOrderHint() + ")");
			System.out.println("==");
			for (TemplateBinding binding : occurrence.getPreConditionBinding()
					.getBindings()) {
				System.out.println(binding.getTemplateName() + ": ");
				for (EObject eObject : binding.getEObjects()) {
					System.out.println("\t " + eObject);
				}
			}
			System.out.println();
		}

		assertEquals(2, occurrences.size());

		System.out.println("Took " + (afterDetection - beforeDetection)
				+ " ms to correctly find 2 operation in sample 1");
		System.out.println();

	}

	/**
	 * Compares the specified resources.
	 * 
	 * @param resource1
	 *            origin resource to compare.
	 * @param resource2
	 *            revised resource to compare.
	 * @return comparison result.
	 * @throws MatchException
	 *             if matching fails.
	 */
	private ComparisonResourceSnapshot compare(Resource resource1,
			Resource resource2) throws MatchException {
		MatchModel matchModel = matchService.generateMatchModel(resource1,
				resource2);
		ComparisonResourceSnapshot comparisonResourceSnapshot = diffService
				.generateComparisonResourceSnapshot(matchModel);
		return comparisonResourceSnapshot;
	}

}
