/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.engines.impl.ConditionsGenerationEngineImpl;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 *
 */
public class ConditionsGenerationEngineImplTest extends TestCase {
	
	private Resource origin = null;
	private Resource uml_model = null;

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		// load resources to test generation
		ResourceSet resourceSet_id = new ResourceSetImpl();
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("uml", new UUIDResourceFactoryImpl());
		URI fileURI_id = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.conditions.test/models/origin_ids.ecore",
						true);
		origin = resourceSet_id.getResource(fileURI_id, true);
		URI fileURI_uml = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.conditions.test/models/My.uml",
						true);
		uml_model = resourceSet_id.getResource(fileURI_uml, true);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		
		//System.out.println(this.uml_model.getContents().get(0).eClass().getEPackage().getNsURI());
		
		super.tearDown();
		origin.unload();
		uml_model.unload();
	}

	/**
	 * Test method for {@link org.modelversioning.core.conditions.engines.impl.ConditionsGenerationEngineImpl#generateConditionsModel(org.eclipse.emf.ecore.EObject)}.
	 */
	public void testGenerateConditionsModel() {
		ConditionsGenerationEngineImpl engine = new ConditionsGenerationEngineImpl();
		
		// test conditions generation ecore models
		ConditionsModel conditionsModel = engine.generateConditionsModel(this.origin.getContents().get(0));
		assertEquals("changed : EPackage", conditionsModel.getRootTemplate().getTitle());
		assertEquals(2, conditionsModel.getRootTemplate().getSubTemplates().size());
		assertEquals("Class1 : EClass", conditionsModel.getRootTemplate().getSubTemplates().get(0).getTitle());
		assertEquals(4, conditionsModel.getRootTemplate().getSubTemplates().get(0).getSubTemplates().size());

//		TreeIterator<EObject> symbolIter = conditionsModel.getRootTemplate().eAllContents();
//		while (symbolIter.hasNext()) {
//			EObject object = symbolIter.next();
//			if (object instanceof Template) {
//				Template symbol = (Template)object;
//				System.out.println(symbol);
//				for (Condition featureCondition : symbol.getSpecifications()) {
//					System.out.println(featureCondition);
//				}
//			}
//		}
		
		// test conditions generation ecore models
		conditionsModel = engine.generateConditionsModel(this.uml_model.getContents().get(0));
		assertEquals("my.package : Package", conditionsModel.getRootTemplate().getTitle());
		assertEquals(2, conditionsModel.getRootTemplate().getSubTemplates().size());
		assertEquals("A : Class", conditionsModel.getRootTemplate().getSubTemplates().get(0).getTitle());
		assertEquals(3, conditionsModel.getRootTemplate().getSubTemplates().get(0).getSubTemplates().size());
		
//		symbolIter = conditionsModel.getRootTemplate().eAllContents();
//		while (symbolIter.hasNext()) {
//			EObject object = symbolIter.next();
//			if (object instanceof Template) {
//				Template symbol = (Template)object;
//				System.out.println(symbol);
//				for (Condition featureCondition : symbol.getSpecifications()) {
//					System.out.println(featureCondition);
//				}
//			}
//		}
	}

}
