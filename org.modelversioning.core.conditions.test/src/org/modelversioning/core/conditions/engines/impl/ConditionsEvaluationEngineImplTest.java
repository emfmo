/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.engines.impl;

import java.io.IOException;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ocl.ParserException;
import org.modelversioning.core.conditions.Condition;
import org.modelversioning.core.conditions.ConditionsModel;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.FeatureCondition;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.BindingException;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.ITemplateBindings;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;

/**
 * Tests class {@link ConditionsEvaluationEngineImpl}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsEvaluationEngineImplTest extends TestCase {

	private ResourceSet resourceSet = new ResourceSetImpl();

	private Resource umlClassDiagramResource = null;
	private Resource umlStatemachineDiagramResource = null;

	private Resource convertToSingletonInitialResource = null;
	private Resource convertToSingletonConditionsResource = null;
	private Resource convertToSingletonValid1Resource = null;
	private Resource convertToSingletonInvalid1Resource = null;

	private Resource encapsulateFieldInitialResource = null;
	private Resource encapsulateFieldConditionsResource = null;
	private Resource encapsulateFieldValid1Resource = null;
	private Resource encapsulateFieldValid2Resource = null;
	private Resource encapsulateFieldInvalid1Resource = null;

	private final URI convertToSingletonInitialURI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/convert_to_singleton/initial.ecore",
					true);
	private final URI convertToSingletonConditionsURI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/convert_to_singleton/initial.conditions",
					true);
	private final URI convertToSingletonValid1URI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/convert_to_singleton/valid1.ecore",
					true);
	private final URI convertToSingletonInvalid1URI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/convert_to_singleton/invalid1.ecore",
					true);

	private final URI encapsulateFieldInitialURI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/encapsulate_field/initial.ecore",
					true);
	private final URI encapsulateFieldConditionsURI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/encapsulate_field/initial.conditions",
					true);
	private final URI encapsulateFieldValid1URI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/encapsulate_field/valid1.ecore",
					true);
	private final URI encapsulateFieldValid2URI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/encapsulate_field/valid2.ecore",
					true);
	private final URI encapsulateFieldInvalid1URI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/binding/encapsulate_field/invalid1.ecore",
					true);

	private final URI umlClassDiagramURI = URI.createPlatformPluginURI(
			"/org.modelversioning.core.conditions.test/models/My.uml", true);
	private final URI umlStateDiagramURI = URI
			.createPlatformPluginURI(
					"/org.modelversioning.core.conditions.test/models/statemachine.uml",
					true);

	private ConditionsEvaluationEngineImpl evaluationEngine = new ConditionsEvaluationEngineImpl();

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		// load resources to test generation
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("uml", new UUIDResourceFactoryImpl());

		convertToSingletonInitialResource = resourceSet.getResource(
				convertToSingletonInitialURI, true);
		convertToSingletonConditionsResource = resourceSet.getResource(
				convertToSingletonConditionsURI, true);
		convertToSingletonValid1Resource = resourceSet.getResource(
				convertToSingletonValid1URI, true);
		convertToSingletonInvalid1Resource = resourceSet.getResource(
				convertToSingletonInvalid1URI, true);

		encapsulateFieldInitialResource = resourceSet.getResource(
				encapsulateFieldInitialURI, true);
		encapsulateFieldConditionsResource = resourceSet.getResource(
				encapsulateFieldConditionsURI, true);
		encapsulateFieldValid1Resource = resourceSet.getResource(
				encapsulateFieldValid1URI, true);
		encapsulateFieldValid2Resource = resourceSet.getResource(
				encapsulateFieldValid2URI, true);
		encapsulateFieldInvalid1Resource = resourceSet.getResource(
				encapsulateFieldInvalid1URI, true);

		umlClassDiagramResource = resourceSet.getResource(umlClassDiagramURI,
				true);
		umlStatemachineDiagramResource = resourceSet.getResource(
				umlStateDiagramURI, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		this.convertToSingletonInitialResource.unload();
		this.convertToSingletonConditionsResource.unload();
		this.convertToSingletonInvalid1Resource.unload();
		this.convertToSingletonValid1Resource.unload();
		this.encapsulateFieldInitialResource.unload();
		this.encapsulateFieldConditionsResource.unload();
		this.encapsulateFieldInvalid1Resource.unload();
		this.encapsulateFieldValid2Resource.unload();
		this.encapsulateFieldValid1Resource.unload();
		this.umlClassDiagramResource.unload();
		this.umlStatemachineDiagramResource.unload();
	}

	/**
	 * Sets each condition of the specified <code>template</code> to active in
	 * order to test their evaluation independently of the default
	 * deactiviation.
	 * 
	 * @param template
	 *            template to set active.
	 */
	protected void setActive(Template template) {
		// for test reasons set all conditions of template to active
		for (Condition condition : template.getSpecifications()) {
			condition.setActive(true);
		}
	}

	/**
	 * Returns a map suitable as prebinding for the specified
	 * <code>template</code> and the specified <code>eObject</code>.
	 * 
	 * @param template
	 *            template to create pre binding for.
	 * @param eObject
	 *            object to create pre binding for.
	 * @return created prebinding.
	 */
	protected ITemplateBinding createPreBinding(Template template,
			EObject eObject) {
		ITemplateBinding preBinding = new TemplateBindingImpl();
		preBinding.add(template, eObject);
		return preBinding;
	}

	/**
	 * Prints the specified {@link ITemplateBindings} for debugging.
	 * 
	 * @param tb
	 *            {@link ITemplateBindings} to print.
	 */
	protected void printTemplateBinding(ITemplateBindings tb) {
		Template rootTemplate = tb.getRootTemplate();

		System.out.println("Root: " + tb.getBoundObjects(rootTemplate));
		TreeIterator<EObject> templateIter = rootTemplate.eAllContents();
		while (templateIter.hasNext()) {
			EObject templatesChild = templateIter.next();
			if (templatesChild instanceof Template) {
				Template template = (Template) templatesChild;
				System.out.println(template.getName() + " = "
						+ tb.getBoundObjects(template));
			}
		}
	}

	protected void printAllPossibleBindings(ITemplateBindings tb) {
		System.out.println("*******************************************");
		for (ITemplateBinding binding : tb.getAllPossibleBindings()) {
			System.out.println("***********");
			Template rootTemplate = tb.getRootTemplate();
			System.out.println(rootTemplate.getName() + ": ");
			for (EObject boundObject : binding.getBoundObjects(rootTemplate)) {
				System.out.println("   " + boundObject);
			}
			TreeIterator<EObject> eAllContents = rootTemplate.eAllContents();
			while (eAllContents.hasNext()) {
				EObject nextChild = eAllContents.next();
				if (nextChild instanceof Template) {
					Template template = (Template) nextChild;
					System.out.println(template.getName() + ": ");
					if (binding.getBoundObjects(template) != null) {
						for (EObject boundObject : binding
								.getBoundObjects(template)) {
							System.out.println("   " + boundObject);
						}
					} else {
						System.out.println("   null");
					}
				}
			}
		}
		System.out.println("*******************************************");
	}

	/**
	 * This method can be used to create and save condition models. .
	 * 
	 * @throws UnsupportedConditionLanguage
	 * @throws IOException
	 */
	public void testCreateConditionModels()
			throws UnsupportedConditionLanguage, IOException {
		// generationEngine = new ConditionsGenerationEngineImpl();
		// evaluationEngine = new ConditionsEvaluationEngineImpl();
		//
		// // test conditions generation ecore models
		// ConditionsModel conditionsModel = generationEngine
		// .generateConditionsModel(this.encapsulateFieldInitialResource
		// .getContents().get(0));
		// evaluationEngine.setConditionsModel(conditionsModel);
		//				
		// URI operationModelUri = URI.createURI(
		// "./test.conditions", true);
		// Resource operationModelResource = resourceSet
		// .createResource(operationModelUri);
		// operationModelResource.getContents().add(conditionsModel);
		// operationModelResource.save(null);

		// // test conditions generation ecore models
		// conditionsModel = generationEngine
		// .generateConditionsModel(this.umlStatemachineDiagramResource
		// .getContents().get(0));
		// evaluationEngine.setConditionsModel(conditionsModel);
		//
		// operationModelUri = URI.createURI("models/statemachine.conditions",
		// true);
		// operationModelResource =
		// resourceSet.createResource(operationModelUri);
		// operationModelResource.getContents().add(conditionsModel);
		// operationModelResource.save(null);

	}

/**
	 * Test method for {@link ConditionsEvaluationEngineImpl#evaluate(Template, EObject, boolean).
	 * 
	 * @throws UnsupportedConditionLanguage
	 */
	public void testEvaluate_Template_EObject_Boolean()
			throws UnsupportedConditionLanguage {
		ConditionsModel singletonConditionsModel = (ConditionsModel) this.convertToSingletonConditionsResource
				.getContents().get(0);
		evaluationEngine.setConditionsModel(singletonConditionsModel);
		TreeIterator<EObject> treeIterator = singletonConditionsModel
				.getRootTemplate().eAllContents();
		while (treeIterator.hasNext()) {
			EObject next = treeIterator.next();
			if (next instanceof Template) {
				Template template = (Template) next;
				assertTrue(evaluationEngine.evaluate(template,
						template.getRepresentative(), true).isOK());
			}
		}
	}

	/**
	 * Test method for
	 * {@link ConditionsEvaluationEngineImpl#validate(Condition)}.
	 * 
	 * @throws UnsupportedConditionLanguage
	 */
	public void testValidate_Condition() throws UnsupportedConditionLanguage {
		ConditionsModel singletonConditionsModel = (ConditionsModel) this.convertToSingletonConditionsResource
				.getContents().get(0);
		evaluationEngine.setConditionsModel(singletonConditionsModel);

		FeatureCondition featureCondition = (FeatureCondition) singletonConditionsModel
				.getRootTemplate().getSpecifications().get(1);
		featureCondition.setExpression("= SOMETHINGWRONG");
		assertFalse(evaluationEngine.validate(featureCondition).isOK());

		featureCondition.setExpression("= #{EPackage_0}.name");
		assertTrue(evaluationEngine.validate(featureCondition).isOK());

		for (Condition condition : singletonConditionsModel.getRootTemplate()
				.getSubTemplates().get(0).getSpecifications()) {
			EvaluationResult result = evaluationEngine.validate(condition);
			if (!result.isOK()) {
				System.err.println(result.getException());
			}
			assertTrue(result.isOK());
		}
	}

	// /**
	// * Test method for
	// * {@link
	// ConditionsEvaluationEngineImpl#createRelativeOCLExpression(EObject,
	// EObject)}
	// */
	// public void testCreateRelativeOCLExpression_EObject_EObject() {
	// EObject baseObject = encapsulateFieldValid1Resource.getContents()
	// .get(0).eContents().get(0);
	// EObject targetObject = encapsulateFieldValid1Resource.getContents()
	// .get(0).eContents().get(1).eContents().get(1);
	//
	// // test two objects and vice versa
	// assertEquals(
	// "self.eContainer().eContents()"
	// + "->at(2).oclAsType(EPackage).eContents()->at(2).oclAsType(EPackage)",
	// evaluationEngine.createRelativeOCLExpression(baseObject,
	// targetObject));
	// assertEquals(
	// "self.eContainer().eContainer()"
	// + ".eContents()->at(1).oclAsType(EClass)",
	// evaluationEngine.createRelativeOCLExpression(targetObject,
	// baseObject));
	//
	// // test if one is root object
	// baseObject = encapsulateFieldValid1Resource.getContents().get(0);
	// assertEquals(
	// "self.eContents()->at(2).oclAsType(EPackage).eContents()"
	// + "->at(2).oclAsType(EPackage)", evaluationEngine
	// .createRelativeOCLExpression(baseObject, targetObject));
	// assertEquals(
	// "self.eContainer().eContainer()"
	// + ".oclAsType(EPackage)", evaluationEngine
	// .createRelativeOCLExpression(targetObject, baseObject));
	//
	// // test if both are equal
	// assertEquals("self", evaluationEngine.createRelativeOCLExpression(
	// targetObject, targetObject));
	//
	// // test if both are root objects
	// targetObject = encapsulateFieldValid1Resource.getContents().get(0);
	// assertEquals("self", evaluationEngine.createRelativeOCLExpression(
	// baseObject, targetObject));
	//
	// // test a direct child of the other
	// baseObject = encapsulateFieldValid1Resource.getContents().get(0)
	// .eContents().get(1);
	// targetObject = encapsulateFieldValid1Resource.getContents().get(0)
	// .eContents().get(1).eContents().get(0);
	// assertEquals(
	// "self.eContents()->at(1).oclAsType(EClass)",
	// evaluationEngine.createRelativeOCLExpression(baseObject,
	// targetObject));
	//
	// }

	/**
	 * Test method for
	 * {@link ConditionsEvaluationEngineImpl#findTemplateBinding(ITemplateBinding)}
	 * .
	 * 
	 * @throws UnsupportedConditionLanguage
	 * @throws BindingException
	 * @throws ParserException
	 *             if a constraint is invalid.
	 */
	public void testFindTemplateBinding_Binding()
			throws UnsupportedConditionLanguage, BindingException,
			ParserException {

		// ***** Convert to singleton
		ConditionsModel singletonConditionsModel = (ConditionsModel) this.convertToSingletonConditionsResource
				.getContents().get(0);
		evaluationEngine.setConditionsModel(singletonConditionsModel);
		evaluationEngine.initialize();

		// test with selected class which has to be converted to singleton
		Template class0Template = singletonConditionsModel.getRootTemplate()
				.getSubTemplates().get(0);
		EObject valid1Class0 = convertToSingletonValid1Resource.getContents()
				.get(0).eContents().get(1);

		evaluationEngine.initialize();
		ITemplateBindings bindings = evaluationEngine
				.findTemplateBinding(createPreBinding(class0Template,
						valid1Class0));

		// check created template binding
		assertEquals(valid1Class0, bindings.getBoundObjects(
				singletonConditionsModel.getRootTemplate().getSubTemplates()
						.get(0)).iterator().next());
		assertEquals(valid1Class0.eContents().get(1), bindings.getBoundObjects(
				singletonConditionsModel.getRootTemplate().getSubTemplates()
						.get(0).getSubTemplates().get(0)).iterator().next());
		assertFalse(bindings.isRemovable(valid1Class0, singletonConditionsModel
				.getRootTemplate().getSubTemplates().get(0)));

		// test with invalid class which has to be converted to singleton
		EObject invalid1Class0 = convertToSingletonInvalid1Resource
				.getContents().get(0).eContents().get(0);

		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				class0Template, invalid1Class0));

		assertEquals(0, bindings.getAllPossibleBindings().size());
		assertFalse(bindings.validate().isOK());

		// test with selected root which contains the class to be converted
		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				class0Template.getParentTemplate(), valid1Class0.eContainer()));

		printAllPossibleBindings(bindings);
		// TODO check created template binding

		// ***** Encapsulate Field
		ConditionsModel encapsulateConditionsModel = (ConditionsModel) this.encapsulateFieldConditionsResource
				.getContents().get(0);
		evaluationEngine.setConditionsModel(encapsulateConditionsModel);

		// Test with selected class in a sample where there are additional
		// subpackages for the inactive template
		Template classTemplate = encapsulateConditionsModel.getRootTemplate()
				.getSubTemplates().get(0).getSubTemplates().get(0);
		EObject validClass0_1 = encapsulateFieldValid1Resource.getContents()
				.get(0).eContents().get(1).eContents().get(1).eContents()
				.get(0);

		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				classTemplate, validClass0_1));

		printAllPossibleBindings(bindings);
		// TODO check created template binding

		// Test with selected class in a sample where there are no candidates
		// for inactive templates
		EObject validClass0_2 = encapsulateFieldValid2Resource.getContents()
				.get(0).eContents().get(0);

		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				classTemplate, validClass0_2));

		printAllPossibleBindings(bindings);
		// TODO check created template binding

		// test with root template and root object
		Template rootTemplate = encapsulateConditionsModel.getRootTemplate();
		EObject validPackage = encapsulateFieldValid1Resource.getContents()
				.get(0);

		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				rootTemplate, validPackage));

		printAllPossibleBindings(bindings);
		// TODO check created template binding

		// test with root template and invalid root object
		EObject invalidPackage = encapsulateFieldInvalid1Resource.getContents()
				.get(0);

		evaluationEngine.initialize();
		bindings = evaluationEngine.findTemplateBinding(createPreBinding(
				rootTemplate, invalidPackage));

		assertEquals(0, bindings.getAllPossibleBindings().size());
		assertFalse(bindings.validate().isOK());

	}
}
