/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.conditions.util;

import junit.framework.TestCase;

import org.modelversioning.core.conditions.ConditionsFactory;
import org.modelversioning.core.conditions.CustomCondition;
import org.modelversioning.core.conditions.TemplateMaskLiterals;
import org.modelversioning.core.conditions.engines.impl.OCLLiterals;

/**
 * Test class for {@link ConditionsUtil}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ConditionsUtilTest extends TestCase {

	/**
	 * Test method for
	 * {@link org.modelversioning.core.conditions.util.ConditionsUtil#getUnmaskedConditionExpression(org.modelversioning.core.conditions.Condition)}
	 * .
	 */
	public void testGetUnmaskedConditionExpression() {
		CustomCondition condition = ConditionsFactory.eINSTANCE
				.createCustomCondition();
		condition
				.setExpression(OCLLiterals.EQUAL
						+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
						+ "template1"
						+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
						+ ".name" + OCLLiterals.AND
						+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_START
						+ "template2"
						+ TemplateMaskLiterals.SINGLE_TEMPLATE_MASK_END
						+ ".upperBound");
		String s = ConditionsUtil.getUnmaskedConditionExpression(condition);
		assertEquals(" = template1.name and template2.upperBound", s);
	}
}
