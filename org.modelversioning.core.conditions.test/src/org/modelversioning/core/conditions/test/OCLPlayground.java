package org.modelversioning.core.conditions.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.query.conditions.eobjects.EObjectCondition;
import org.eclipse.emf.query.ocl.conditions.BooleanOCLCondition;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.ConstraintKind;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.ocl.options.ParsingOptions;
import org.modelversioning.core.impl.UUIDResourceFactoryImpl;

public class OCLPlayground extends TestCase {

	private static org.eclipse.ocl.ecore.OCL oclInstance = org.eclipse.ocl.ecore.OCL
			.newInstance();

	Resource origin = null;
	Resource uml_model = null;
	Resource statechart_resource = null;

	protected void setUp() throws Exception {
		super.setUp();
		// load id resources
		ResourceSet resourceSet_id = new ResourceSetImpl();
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new UUIDResourceFactoryImpl());
		resourceSet_id.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("uml", new UUIDResourceFactoryImpl());
		URI fileURI_id = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.conditions.test/models/origin_ids.ecore",
						true);
		origin = resourceSet_id.getResource(fileURI_id, true);
		URI fileURI_uml = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.conditions.test/models/My.uml",
						true);
		uml_model = resourceSet_id.getResource(fileURI_uml, true);

		URI fileURI_statechart = URI
				.createPlatformPluginURI(
						"/org.modelversioning.core.conditions.test/models/default.state",
						true);
		statechart_resource = resourceSet_id.getResource(fileURI_statechart,
				true);
	}

	public void testOCLConditionExecution() throws ParserException {

		System.out.println("================================ ecore:");

		EObject object = origin.getContents().get(0).eContents().get(0);
		for (EStructuralFeature feature : object.eClass()
				.getEAllStructuralFeatures()) {
			String oclExpression = "self." + feature.getName() + " = "
					+ object.eGet(feature);
			EObjectCondition condition = new BooleanOCLCondition<EClassifier, EClass, EObject>(
					OCLPlayground.oclInstance.getEnvironment(), oclExpression,
					null);
			System.out.println(oclExpression + " => "
					+ condition.isSatisfied(object));
		}

		System.out.println("================================ uml:");
		object = uml_model.getContents().get(0).eContents().get(0);

		// create reference list without containments
		EList<EReference> refList = new BasicEList<EReference>();
		refList.addAll(object.eClass().getEAllReferences());
		// refList.removeAll(object.eClass().getEAllContainments());

		// create and check all contraints
		for (EReference feature : refList) {
			// test value
			Object value = object.eGet(feature);
			String oclExpression = "self." + feature.getName();
			if (value instanceof EList) {
				// if list
				EList<?> list = (EList<?>) value;
				if (list.size() == 0) {
					oclExpression += "->isEmpty()";
				} else {
					oclExpression += "->notEmpty()";
					System.err.println("handle not empty references:" + list);
				}
			} else {
				// if single reference
				if (value == null) {
					oclExpression += " = null";
				} else {
					EModelElement modelElement = (EModelElement) value;
					oclExpression += " = " + modelElement;
				}
			}

			EObjectCondition condition = new BooleanOCLCondition<EClassifier, EClass, EObject>(
					OCLPlayground.oclInstance.getEnvironment(), oclExpression,
					null);
			System.out.println(oclExpression + " => "
					+ condition.isSatisfied(object));
		}

	}

	public void testGettingContainments() {
		System.out
				.println("================================ ecore containments:");
		EObject object = uml_model.getContents().get(0);
		for (EReference reference : object.eClass().getEAllContainments()) {
			System.out.println((EList<?>) object.eGet(reference));
		}

		System.out
				.println("================================ uml containments:");
		object = uml_model.getContents().get(0);
		for (EReference reference : object.eClass().getEAllContainments()) {
			System.out.println((EList<?>) object.eGet(reference));
		}
	}

	public void testOCLHelper() throws ParserException {
		EObject object = origin.getContents().get(0).eContents().get(0);

		OCLHelper<?, ?, ?, ?> helper = OCLPlayground.oclInstance.createOCLHelper();
		helper.setInstanceContext(object);

		System.out.println("================================ custom:");
		// helper.defineAttribute("TestAttributes : Sequence(EAttribute) = self.eAllAttributes->asSequence().oclAsType(EAttribute)");
		helper
				.defineAttribute("EClass_0 : Sequence(EClass) = self->asSequence().oclAsType(EClass)");
		helper
				.defineAttribute("EAttribute_0 : Sequence(EAttribute) = self.eAttributes->asSequence()->select(oclIsTypeOf(EAttribute)).oclAsType(EAttribute)");
		helper
				.defineAttribute("EAttribute_1 : Sequence(EAttribute) = self.eStructuralFeatures.oclAsType(EAttribute)->asSequence()");

		// FIXME eine alternative zu includes finden - sowas wie
		// includesAnyOf...
		// eventuell self.eType->asSequence()->excludesAll(#{}) == false // aber
		// auch nicht sehr schön - ganzes konzept überdenken?
		String oclExpression = "self.eAttributes->includes(EAttribute_0->select(name = 'attribute1')->first())";
		// oclExpression =
		// "self.eAttributes->includes(self.eStructuralFeatures->select(oclIsTypeOf(EAttribute)).oclAsType(EAttribute)->first())";

		Constraint constraint = (Constraint) helper.createConstraint(
				ConstraintKind.INVARIANT, oclExpression);
		OCLPlayground.oclInstance.check(object, constraint);

		EObjectCondition condition = new BooleanOCLCondition<EClassifier, EClass, EObject>(
				OCLPlayground.oclInstance.getEnvironment(), oclExpression, null);
		System.out.println(oclExpression + " => "
				+ condition.isSatisfied(object));

	}

	public void testRegex() {
		System.out
				.println("================================ RegEx Template Tests:");
		String hay = "eAllAttributes->includes(#{EAttribute_0}) and eAllAttributes->includes(#{EAttribute_1})";
		String regex = "(#\\{([a-zA-Z_0-9]*)\\})";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(hay);
		System.out.println(hay);
		while (matcher.find()) {
			String match = matcher.group();
			System.out.println("Found the template: "
					+ match.substring(2, match.length() - 1));
		}
	}
	
	public void testPrefixRegex() {
		System.out
				.println("================================ RegEx Prefix Template Tests:");
		String hay = "eAllAttributes->includes(#{initial:EAttribute_0}) and eAllAttributes->includes(#{test:EAttribute_1})";
		String regex = "(#\\{([a-zA-Z_0-9]*:[a-zA-Z_0-9]*)\\})";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(hay);
		System.out.println(hay);
		while (matcher.find()) {
			String match = matcher.group();
			String prefix = match.substring(2, match.indexOf(":"));
			String templateName = match.substring(match.indexOf(":") + 1, match.length() - 1);
			System.out.println("Found the template: "
					+ templateName + " in prefix " + prefix);
		}
	}
	
	public void testPrefixFeatureRegex() {
		System.out
				.println("================================ RegEx Prefix Feature Tests:");
		String hay = "self.name = #{initial:EAttribute_0}.name + #{initial:EAttribute_0}.size + #{test:EAttribute_1}.name";
		String regex = "(#\\{(initial:EAttribute_0)\\}.[a-zA-Z]*)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(hay);
		System.out.println(hay);
		while (matcher.find()) {
			String match = matcher.group();
			System.out.println("Found the feature: "
					+ match.substring(match.indexOf(".") + 1));
		}
	}
	
	public void testOCLEnvironment() throws ParserException {
		OCL<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> ocl = OCL
				.newInstance(EcoreEnvironmentFactory.INSTANCE);
		// set implicit root object option
		ParsingOptions.setOption(ocl.getEnvironment(), ParsingOptions
				.implicitRootClass(ocl.getEnvironment()),
				EcorePackage.Literals.EOBJECT);

		// test some queries and check implicit super class
		EObject transition = statechart_resource.getContents().get(0)
				.eContents().get(0).eContents().get(0);
		OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> oclHelper = ocl
				.createOCLHelper();
		oclHelper.setInstanceContext(transition);
		
		// TODO always returns false if ->at() is used???!?
		Constraint constraint = oclHelper
				.createInvariant("self.eContainer().eContents()->at(1).oclAsType(Transition).name = 'start'");
		OCLExpression<EClassifier> query = oclHelper.createQuery("self.eContainer().eContents()->at(1).oclAsType(Transition).name");
		assertEquals("start", ocl.evaluate(transition, query));
		assertTrue(ocl.check(transition, constraint));
	}

}
