/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.NotationPackage;

/**
 * Utilities for handling diagrams and models.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ModelDiagramUtil {

	/**
	 * Specifies whether the specified <code>resource</code> contains a GMF
	 * diagram.
	 * 
	 * @param resource
	 *            to check if it contains a GMF diagram.
	 * @return <code>true</code> if it contains a GMF diagram,
	 *         <code>false</code> otherwise.
	 */
	public static boolean containsDiagram(Resource resource) {
		// implicitly import notation package
		NotationPackage.eINSTANCE.getNsURI();
		// check for content size of resource
		if (resource.getContents().size() < 1) {
			return false;
		}
		try {
			// try to cast the contents
			Diagram diagram = (Diagram) resource.getContents().get(0);
			diagram.getElement();
			// if it worked it is a diagram
			return true;
		} catch (ClassCastException c) {
			return false;
		}
	}

	/**
	 * Returns a list of {@link IFile}s containing the models visualized by the
	 * specified <code>resourceContainingDiagram</code>. The URI of the diagram
	 * resource <code>resourceContainingDiagram</code> has to have an absolute
	 * path to successfully resolve the model file.
	 * 
	 * @param resourceContainingDiagram
	 *            a resource containing a diagram.
	 * @return a list of {@link IFile} containing the models.
	 */
	public static Collection<IFile> getModelFilesFromDiagram(
			Resource resourceContainingDiagram) {
		// implicitly import notation package
		NotationPackage.eINSTANCE.getNsURI();

		// check if it contains a diagram at all and is absolute
		Assert.isTrue(containsDiagram(resourceContainingDiagram),
				"Specified resource is no diagram"); //$NON-NLS-1$
		Assert.isTrue(resourceContainingDiagram.getURI().hasAbsolutePath(),
				"Diagram resource path is not absolute"); //$NON-NLS-1$

		// check size
		if (resourceContainingDiagram.getContents().size() < 1) {
			return Collections.emptyList();
		}

		List<IFile> modelFileList = new ArrayList<IFile>();
		for (EObject diagramEObject : resourceContainingDiagram.getContents()) {
			if (diagramEObject instanceof Diagram) {

				try {

					// get diagram object
					Diagram diagram = (Diagram) diagramEObject;

					// resolve model uri
					URI modelURI = org.eclipse.emf.ecore.util.EcoreUtil
							.getURI(diagram.getElement());
					URI resolvedModelURI = modelURI
							.resolve(resourceContainingDiagram.getURI());

					// get file string
					String fileString = resolvedModelURI.toFileString();

					// create IFile from file string
					IFile iFile = ResourcesPlugin.getWorkspace().getRoot()
							.getFileForLocation(new Path(fileString));

					modelFileList.add(iFile);

				} catch (ClassCastException c) {
					// ignore object
				}
			}
		}

		return Collections.unmodifiableCollection(modelFileList);
	}

	/**
	 * Returns the platform resource URI for the specified {@link IFile}.
	 * 
	 * @param file
	 *            to get URI for.
	 * @return the platform resource URI.
	 */
	public static String getPlatformResourceURI(IFile file) {
		if (file.getProject() != null) {
			return file.getProject().getName() + "/" //$NON-NLS-1$
					+ file.getProjectRelativePath();
		} else {
			return file.getFullPath().toOSString();
		}
	}

}
