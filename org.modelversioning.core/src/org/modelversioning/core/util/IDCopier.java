/**
 * <copyright>
 *
 * Copyright (c) 2010 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.util;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

/**
 * An ID aware implementation of the {@link Copier}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class IDCopier extends Copier {

	/**
	 * serial id.
	 */
	private static final long serialVersionUID = 2291072486079411360L;

	/**
	 * Sets the id of <code>eObject</code> to <code>copy</code>.
	 * 
	 * @param eObject
	 *            to get id from.
	 * @param copy
	 *            to set id to.
	 */
	private void maintainID(EObject eObject, EObject copy) {
		// copy id of root object
		UUIDUtil.copyUUID(eObject, copy);
		// copy id of children
//		TreeIterator<EObject> eAllContents = eObject.eAllContents();
//		while (eAllContents.hasNext()) {
//			EObject subOriginal = eAllContents.next();
//			EObject subCopy = this.get(subOriginal);
//			UUIDUtil.copyUUID(subOriginal, subCopy);
//		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EObject copy(EObject eObject) {
		EObject copy = super.copy(eObject);
		maintainID(eObject, copy);
		return copy;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> Collection<T> copyAll(Collection<? extends T> eObjects) {
		Collection<T> copyAll = super.copyAll(eObjects);
		for (T eObject : eObjects) {
			maintainID((EObject) eObject, this.get(eObject));
		}
		return copyAll;
	}

}
