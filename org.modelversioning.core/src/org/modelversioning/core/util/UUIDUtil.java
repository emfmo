/**
 * <copyright>
 *
 * Copyright (c) ${year} modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.util;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;

/**
 * Utility class for UUID related stuff.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UUIDUtil {

	private static final String NOT_CONTAINED_BY_XML_RESOURCE = "The specified argument"
			+ " is not contained by a XMLResource. ";

	/**
	 * Returns the <i>UUID</i> from the specified {@link EObject}
	 * <code>eObject</code> or <code>null</code> if there is none.
	 * 
	 * @param eObject
	 *            to get <i>UUID</i> from.
	 * @return the <i>UUID</i> of <code>eObject</code> or <code>null</code>.
	 */
	public static String getUUID(EObject eObject) {
		String objectID = null;
		if (eObject != null && eObject.eResource() instanceof XMIResource) {
			objectID = ((XMIResource) eObject.eResource()).getID(eObject);
		}
		return objectID;
	}

	/**
	 * Sets the XMI ID of the given {@link EObject} if it belongs in an
	 * {@link XMIResource}.
	 * 
	 * @param object
	 *            Object we want to set the XMI ID of.
	 * @param id
	 *            XMI ID to give to <code>object</code>.
	 */
	public static void setUUID(EObject object, String id) {
		if (object != null && object.eResource() instanceof XMIResource) {
			((XMIResource) object.eResource()).setID(object, id);
		}
	}

	/**
	 * Copies the UUID of <code>source</code> to the <code>target</code>.
	 * 
	 * @param source
	 *            to get id from.
	 * @param target
	 *            to copy id to.
	 */
	public static void copyUUID(EObject source, EObject target) {
		if (getUUID(source) != null) {
			setUUID(target, getUUID(source));
		}
	}

	/**
	 * Copies the UUID of <code>source</code> to its copy created with the
	 * specified <code>copier</code>.
	 * 
	 * @param source
	 *            the source of this copying process.
	 * @param copier
	 *            the copier to resolve copy.
	 */
	public static void copyUUID(EObject source, Copier copier) {
		copyUUID(source, copier.get(source));
	}

	/**
	 * Copies the UUIDs of <code>source</code> and all its children to their
	 * copies created with the specified <code>copier</code>.
	 * 
	 * @param source
	 *            the source of this copying process.
	 * @param copier
	 *            the copier to resolve copy.
	 */
	public static void copyUUIDs(EObject source, Copier copier) {
		copyUUID(source, copier.get(source));
		TreeIterator<EObject> treeIterator = source.eAllContents();
		while (treeIterator.hasNext()) {
			EObject next = treeIterator.next();
			copyUUID(next, copier.get(next));
		}
	}

	/**
	 * Returns the {@link EObject} with the specified <code>uuid</code> within
	 * the <code>resource</code>.
	 * 
	 * @param resource
	 *            {@link Resource} containing the {@link EObject} with the
	 *            specified <code>uuid</code>.
	 * @param uuid
	 *            <i>UUID</i> of the {@link EObject}.
	 * @return the {@link EObject} with the <code>uuid</code> or
	 *         <code>null</code>.
	 */
	public static EObject getObject(Resource resource, String uuid) {
		if (uuid == null)
			return null;
		return resource.getEObject(uuid);
	}

	/**
	 * Sets the id of the specified <code>eObject</code> and all its children to
	 * <code>null</code>.
	 * 
	 * <p>
	 * The specified <code>eObject</code> has to be contained by a
	 * {@link XMLResource}.
	 * </p>
	 * 
	 * <p>
	 * Throws an {@link IllegalArgumentException} if <code>eObject</code> is not
	 * contained by a {@link XMLResource}.
	 * </p>
	 * 
	 * @param eObject
	 *            object to remove id from.
	 */
	public static void removeUUIDs(EObject eObject) {
		throwExceptionIfNotXMLResource(eObject);
		setUUID(eObject, null);
		TreeIterator<EObject> treeIterator = eObject.eAllContents();
		while (treeIterator.hasNext()) {
			EObject contentObject = treeIterator.next();
			setUUID(contentObject, null);
		}
	}

	/**
	 * This method adds new UUIDs to the specified <code>eObject</code> and all
	 * its children.
	 * 
	 * <p>
	 * If <code>eObject</code> or one of its children already got a UUID, this
	 * method will not change it.
	 * </p>
	 * 
	 * <p>
	 * Throws an {@link IllegalArgumentException} if <code>eObject</code> is not
	 * contained by a {@link XMLResource}.
	 * </p>
	 * 
	 * @param eObject
	 *            to add UUID to.
	 */
	public static void addUUIDs(EObject eObject) {
		throwExceptionIfNotXMLResource(eObject);
		addUUID(eObject);
		TreeIterator<EObject> treeIterator = eObject.eAllContents();
		while (treeIterator.hasNext()) {
			addUUID(treeIterator.next());
		}
	}

	/**
	 * This method adds a new UUID to the specified <code>eObject</code>.
	 * 
	 * <p>
	 * If <code>eObject</code> already got a UUID, this method will not change
	 * it.
	 * </p>
	 * 
	 * <p>
	 * Throws an {@link IllegalArgumentException} if <code>eObject</code> is not
	 * contained by a {@link XMLResource}.
	 * </p>
	 * 
	 * @param eObject
	 *            to add UUID to.
	 */
	public static void addUUID(EObject eObject) {
		throwExceptionIfNotXMLResource(eObject);
		if (getUUID(eObject) == null) {
			setUUID(eObject, EcoreUtil.generateUUID());
		}
	}

	/**
	 * Throws an {@link IllegalArgumentException} if <code>eObject</code> is not
	 * contained by a {@link XMLResource}.
	 * 
	 * @param eObject
	 *            to check.
	 */
	private static void throwExceptionIfNotXMLResource(EObject eObject) {
		if (eObject.eResource() == null
				|| !(eObject.eResource() instanceof XMLResource)) {
			throw new IllegalArgumentException(NOT_CONTAINED_BY_XML_RESOURCE);
		}
	}

}
