/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.core.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Utilities for {@link EObject}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class EcoreUtil {

	/**
	 * Creates a list of parents of the specified <code>eObject</code> from
	 * <code>eObject</code>'s parent to root. If <code>eObject</code> is root
	 * itself, the returned list is empty.
	 * 
	 * @param eObject
	 *            object to start going upwards the containment hierarchy.
	 * @return a list of parent objects from <code>eObject</code>'s parent to
	 *         root.
	 */
	public static List<EObject> createParentList(EObject eObject) {
		List<EObject> parentsList = new ArrayList<EObject>();
		if (eObject == null) {
			return parentsList;
		}
		EObject parentObject = eObject;
		while ((parentObject = parentObject.eContainer()) != null) {
			parentsList.add(parentObject);
		}
		return parentsList;
	}

	/**
	 * Finds and returns the least common parent of both objects
	 * <code>eObject1</code> and <code>eObject2</code>. If no common parent is
	 * found this method returns <code>null</code>.
	 * 
	 * @param eObject1
	 *            first base object.
	 * @param eObject2
	 *            second base object.
	 * @return least common parent or <code>null</code> if there is none.
	 */
	public static EObject findLeastCommonParentObject(EObject eObject1,
			EObject eObject2) {

		List<EObject> parents1 = createParentList(eObject1);
		List<EObject> parents2 = createParentList(eObject2);

		// check if one of the objects is root
		if (eObject1.eContainer() == null && eObject2.eContainer() == null
				&& eObject1.equals(eObject2)) {
			return eObject1;
		} else if (eObject1.eContainer() == null || parents2.contains(eObject1)) {
			return eObject1;
		} else if (eObject2.eContainer() == null || parents1.contains(eObject2)) {
			return eObject2;
		} else {
			return findLeastCommonParentObject(parents1, parents2);
		}
	}

	/**
	 * Finds and returns the least common parent of both parent lists
	 * <code>parents1</code> and <code>parents2</code>. If no common parent is
	 * found this method returns <code>null</code>.
	 * 
	 * To create a list like <code>parents1</code> or <code>parents2</code> see
	 * {@link #createParentList(EObject)} or use
	 * {@link #findLeastCommonParentObject(EObject, EObject)} directly.
	 * 
	 * @param parents1
	 *            first list of parents.
	 * @param parents2
	 *            first list of parents.
	 * @return least common parent or <code>null</code> if there is none.
	 */
	public static EObject findLeastCommonParentObject(List<EObject> parents1,
			List<EObject> parents2) {
		for (EObject currentParent : parents1) {
			if (parents2.contains(currentParent)) {
				return currentParent;
			}
		}
		return null;
	}

	/**
	 * Returns the first root {@link EObject} contained by the specified
	 * <code>resource</code>.
	 * 
	 * @param resource
	 *            to get first root {@link EObject} of.
	 * @return root {@link EObject} or null, if there is none.
	 */
	public static EObject getFirstRootObject(Resource resource) {
		if (resource != null && !resource.getContents().isEmpty()) {
			return resource.getContents().get(0);
		} else {
			return null;
		}

	}

}
