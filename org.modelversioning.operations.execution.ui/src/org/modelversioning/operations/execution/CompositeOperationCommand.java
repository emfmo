/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.change.ChangeDescription;
import org.eclipse.emf.ecore.change.util.ChangeRecorder;
import org.eclipse.emf.edit.command.AbstractOverrideableCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.modelversioning.operations.execution.engines.IOperationExecutionEngine;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIPlugin;

/**
 * Command executing composite operaitons using a
 * {@link IOperationExecutionEngine}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class CompositeOperationCommand extends AbstractOverrideableCommand
		implements Command {

	private IOperationExecutionEngine executionEngine;
	private IOperationBinding operationBinding;
	private boolean canUndo = false;
	private boolean canDoExectue = true;
	private ChangeDescription changeDescription = null;

	private boolean triedAgain = false;

	/**
	 * Creates a new {@link Command} executing composite operations.
	 * 
	 * @param domain
	 *            to execute commands in.
	 * @param label
	 *            label of this command.
	 * @param description
	 *            description of this command.
	 * @param executionEngine
	 *            engine to use to execute the command.
	 * @param operationBinding
	 *            binding to execute.
	 */
	public CompositeOperationCommand(EditingDomain domain, String label,
			String description, IOperationExecutionEngine executionEngine,
			IOperationBinding operationBinding) {
		super(domain, label, description);
		this.executionEngine = executionEngine;
		this.operationBinding = operationBinding;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doExecute() {
		ChangeRecorder changeRecorder = new ChangeRecorder(
				this.domain.getResourceSet());
		try {
			this.executionEngine.execute(operationBinding);
			changeDescription = changeRecorder.endRecording();
			canUndo = true;
		} catch (Exception e) {
			changeRecorder.endRecording().apply();
			if (!triedAgain) {
				doExecute();
				triedAgain = true;
			} else {
				canUndo = false;
				OperationsExecutionUIPlugin.log(e);
				OperationsExecutionUIPlugin.showError(e);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doRedo() {
		doExecute();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doUndo() {
		if (changeDescription != null) {
			changeDescription.apply();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doCanExecute() {
		return canDoExectue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean doCanUndo() {
		return canUndo;
	}

}
