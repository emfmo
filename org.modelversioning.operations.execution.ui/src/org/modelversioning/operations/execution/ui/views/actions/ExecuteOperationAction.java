/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.util.ConditionsUtil;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIPlugin;
import org.modelversioning.operations.execution.ui.controller.ExecutionController;

/**
 * Executes the selected {@link OperationSpecification}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ExecuteOperationAction extends Action {

	/**
	 * The viewer.
	 */
	private TreeViewer viewer;

	/**
	 * Constructor setting the viewer.
	 * 
	 * @param viewer
	 *            viewer to set.
	 */
	public ExecuteOperationAction(TreeViewer viewer) {
		this.viewer = viewer;
		this.setText(OperationsExecutionUIMessages
				.getString("MOExecution.executeModelOperation")); //$NON-NLS-1$
		this.setImageDescriptor(OperationsExecutionUIPlugin
				.getImageDescriptor(OperationsExecutionUIPlugin.IMG_EXECUTE));
		this.setToolTipText(OperationsExecutionUIMessages
				.getString("MOExecution.executeModelOperation_tooltip")); //$NON-NLS-1$
	}

	/**
	 * Removes the currently selected operation specification from model
	 * operation repository.
	 */
	@Override
	public void run() {
		ISelection selection = viewer.getSelection();

		// guard no selection
		if (((IStructuredSelection) selection).size() < 1) {
			showMessage(OperationsExecutionUIMessages
					.getString("MOExecution.noSelectedOperation")); // $NON-NLS-1$
			super.run();
			return;
		}

		// guard multi-selection
		if (((IStructuredSelection) selection).size() > 1) {
			showMessage(OperationsExecutionUIMessages
					.getString("MOExecution.tooManySelectedOperations")); // $NON-NLS-1$
			super.run();
			return;
		}

		// get selected specification and template
		OperationSpecification selectedSpecification = null;
		Template selectedTemplate = null;
		Object o = ((IStructuredSelection) selection).getFirstElement();
		if (o instanceof OperationSpecification) {
			selectedSpecification = (OperationSpecification) o;
		} else if (o instanceof Template) {
			selectedTemplate = (Template) o;
			// try to get operation specification of currently selected template
			selectedSpecification = (OperationSpecification) ConditionsUtil
					.getContainingConditionsModel(selectedTemplate)
					.eContainer();
		} else {
			return;
		}

		// get active editor
		IEditorPart activeEditor = null;
		if (OperationsExecutionUIPlugin.getActiveWorkbenchWindow() != null
				&& OperationsExecutionUIPlugin.getActiveWorkbenchWindow()
						.getActivePage() != null) {
			IWorkbenchWindow workbenchWindow = OperationsExecutionUIPlugin
					.getActiveWorkbenchWindow();
			activeEditor = workbenchWindow.getActivePage().getActiveEditor();
		}

		// create and run ExecutionController with a copied version of the
		// operation specification and the selected template
		if (activeEditor != null) {
			ExecutionController execController = new ExecutionController(
					selectedSpecification, selectedTemplate, activeEditor);
			execController.showWizard();
		}
		super.run();
	}

	/**
	 * Shows a message with the specified message.
	 * 
	 * @param message
	 *            to show.
	 */
	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				OperationsExecutionUIMessages
						.getString("MOExecution.messageTitle"), // $NON-NLS-1$
				message);
	}

}
