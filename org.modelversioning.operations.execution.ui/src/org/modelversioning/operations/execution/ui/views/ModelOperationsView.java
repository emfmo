/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */
package org.modelversioning.operations.execution.ui.views;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.provider.ModelOperationRepositoryContentProvider;
import org.modelversioning.operations.execution.ui.provider.ModelOperationRepositoryLabelProvider;
import org.modelversioning.operations.execution.ui.views.actions.ExecuteOperationAction;
import org.modelversioning.operations.execution.ui.views.actions.UnregisterOperationAction;
import org.modelversioning.operations.repository.IModelOperationRepositoryListener;
import org.modelversioning.operations.repository.ModelOperationRepository;
import org.modelversioning.operations.repository.ModelOperationRepositoryPlugin;
import org.modelversioning.operations.ui.commons.provider.OperationSpecificationViewerSorter;

/**
 * View showing available model operations from the
 * {@link ModelOperationRepository}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 */
public class ModelOperationsView extends ViewPart implements
		IModelOperationRepositoryListener {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.modelversioning.operations.execution.ui.views.ModelOperationsView"; //$NON-NLS-1$

	/**
	 * Tree viewer of this view part.
	 */
	private TreeViewer viewer;
	/**
	 * The drill down adapter.
	 */
	private DrillDownAdapter drillDownAdapter;
	/**
	 * The action to unregister model operations.
	 */
	private Action unregisterAction;
	/**
	 * The action to execute model operations.
	 */
	private Action executeAction;

	/**
	 * The constructor.
	 */
	public ModelOperationsView() {
		ModelOperationRepositoryPlugin.getDefault().getOperationRepository()
				.addModelOperationListener(this);
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ModelOperationRepositoryContentProvider());
		viewer.setLabelProvider(new ModelOperationRepositoryLabelProvider());
		viewer.setSorter(new OperationSpecificationViewerSorter());
		viewer.setInput(ModelOperationRepositoryPlugin.getDefault()
				.getOperationRepository());

		// Create the help context id for the viewer's control
		PlatformUI
				.getWorkbench()
				.getHelpSystem()
				.setHelp(viewer.getControl(),
						"org.modelversioning.operations.execution.ui.viewer"); //$NON-NLS-1$
		makeActions();
		hookContextMenu();
		contributeToActionBars();
	}

	/**
	 * Hooks the context menus.
	 */
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				ModelOperationsView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Contributes to action bars.
	 */
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	/**
	 * Adds actions to the specified manager.
	 * 
	 * @param manager
	 *            to add actions to.
	 */
	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(unregisterAction);
		manager.add(new Separator());
		manager.add(executeAction);
	}

	/**
	 * Fills the context menu with actions.
	 * 
	 * @param manager
	 *            to add actions to.
	 */
	private void fillContextMenu(IMenuManager manager) {
		manager.add(unregisterAction);
		manager.add(executeAction);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	/**
	 * Fills the tool bar with actions.
	 * 
	 * @param manager
	 *            to add actions to.
	 */
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(unregisterAction);
		manager.add(executeAction);
		manager.add(new Separator());
		drillDownAdapter.addNavigationActions(manager);
	}

	/**
	 * Instantiates the actions.
	 */
	private void makeActions() {
		unregisterAction = new UnregisterOperationAction(viewer);
		executeAction = new ExecuteOperationAction(viewer);
		// execute on double click
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				Object firstElement = ((IStructuredSelection) event
						.getSelection()).getFirstElement();
				if (firstElement instanceof Template) {
					executeAction.run();
				} else {
					if (viewer.getExpandedState(firstElement)) {
						viewer.collapseToLevel(firstElement,
								AbstractTreeViewer.ALL_LEVELS);
					} else {
						viewer.expandToLevel(firstElement, 1);
					}
				}
			}
		});
	}

	/**
	 * Shows a message with the specified message.
	 * 
	 * @param message
	 *            to show.
	 */
	public void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(),
				OperationsExecutionUIMessages.getString("messageTitle"),
				message); // $NON-NLS-1$
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	/**
	 * Removes itself from the listeners of the model operations repository.
	 */
	@Override
	public void dispose() {
		ModelOperationRepositoryPlugin.getDefault().getOperationRepository()
				.removeModelOperationListener(this);
		super.dispose();
	}

	/**
	 * Refreshes the viewer.
	 */
	@Override
	public void registerAction(OperationSpecification operationSpecification) {
		viewer.setInput(ModelOperationRepositoryPlugin.getDefault()
				.getOperationRepository());
		viewer.refresh();
	}

	/**
	 * Refreshes the viewer.
	 */
	@Override
	public void unregisterAction(OperationSpecification operationSpecification) {
		viewer.setInput(ModelOperationRepositoryPlugin.getDefault()
				.getOperationRepository());
		viewer.refresh();
	}
}