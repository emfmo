package org.modelversioning.operations.execution.ui.popup.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.repository.ModelOperationRepositoryPlugin;

public class RegisterOperationAction implements IObjectActionDelegate {

	private Shell shell;

	/**
	 * The currently selected {@link IFile}s.
	 */
	private List<IFile> selectedFiles = new ArrayList<IFile>();

	/**
	 * {@link ResourceSet} to use.
	 */
	private ResourceSet resourceSet = new ResourceSetImpl();

	/**
	 * Constructor for Action1.
	 */
	public RegisterOperationAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if (selectedFiles.size() > 0) {
			OperationSpecification spec = loadOperationSpecification(selectedFiles
					.get(0));
			if (spec != null) {
				ModelOperationRepositoryPlugin.getDefault()
						.getOperationRepository().register(spec);
			}
		} else {
			MessageDialog
					.openInformation(shell, "Model Operation Registry",
							"Could not load operation specification from selected file.");
		}

	}

	/**
	 * Loads the {@link OperationSpecification} contained by the specified
	 * <code>iFile</code>.
	 * 
	 * @param iFile
	 *            {@link IFile} to load {@link OperationSpecification} from.
	 * @return {@link OperationSpecification} or <code>null</code>.
	 */
	private OperationSpecification loadOperationSpecification(IFile iFile) {
		Resource resource = resourceSet
				.getResource(getUriFromFile(iFile), true);
		if (resource.getContents().size() > 0) {
			EObject eObject = resource.getContents().get(0);
			if (eObject instanceof OperationSpecification) {
				return (OperationSpecification) eObject;
			}
		}
		return null;
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		// get selected resource
		selectedFiles = getSelectedFiles(selection);
	}

	/**
	 * Returns the selected files from specified <code>selection</code>.
	 * 
	 * @param selection
	 *            the {@link ISelection}.
	 * @return the selected files.
	 */
	private List<IFile> getSelectedFiles(ISelection selection) {
		List<IFile> fileList = new ArrayList<IFile>();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			for (Object o : structuredSelection.toList()) {
				fileList.add((IFile) o);
			}

		}
		return fileList;
	}

	/**
	 * Returns the {@link URI} from the specified <code>iFile</code>.
	 * 
	 * @param iFile
	 *            to get {@link URI} from.
	 * @return the {@link URI} from <code>iFile</code>.
	 */
	private URI getUriFromFile(IFile iFile) {
		return URI.createURI(iFile.getFullPath().toPortableString());
	}

}
