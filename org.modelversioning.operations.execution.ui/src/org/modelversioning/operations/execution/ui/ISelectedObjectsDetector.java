/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ui.IEditorPart;

/**
 * Provides facilities to detect {@link EObject}s currently selected in an
 * {@link IEditorPart}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public interface ISelectedObjectsDetector {

	/**
	 * Returns the currently selected {@link EObject}s in the
	 * <code>editor</code>.
	 * 
	 * @return the currently selected {@link EObject}s or an empty
	 *         {@link Set} if the detection is not possible.
	 */
	public Set<EObject> getSelectedObjects(IEditorPart editor);

}
