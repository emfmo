/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.wizards;

import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.Wizard;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.controller.ExecutionController;

/**
 * The dialog hosting the execution of an {@link OperationSpecification}.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ExecutionWizard extends Wizard {

	/**
	 * Page name of pre-binding.
	 */
	public static final String PREBINDING_PAGE_NAME = "PreBindingPage"; //$NON-NLS-1$
	/**
	 * Page name of binding.
	 */
	public static final String BINDING_PAGE_NAME = "BindingPage"; //$NON-NLS-1$
	/**
	 * Page name of user input.
	 */
	public static final String USER_INPUT_PAGE_NAME = "UserInputPage"; //$NON-NLS-1$
	/**
	 * The execution controller.
	 */
	private ExecutionController controller;
	/**
	 * Pre-binding wizard page.
	 */
	private PreBindingWizardPage preBindingPage;
	/**
	 * The binding wizard page.
	 */
	private BindingWizardPage bindingPage;
	/**
	 * The user input page.
	 */
	private UserInputWizardPage userInputPage;

	/**
	 * Creates a new {@link ExecutionWizard} using the data of the specified
	 * <code>controller</code>.
	 * 
	 * @param controller
	 *            the controller holding and managing the data.
	 */
	public ExecutionWizard(ExecutionController controller) {
		super();
		this.controller = controller;
		setWizardProperties();
	}

	/**
	 * Sets the wizard properties.
	 */
	private void setWizardProperties() {
		super.setWindowTitle(OperationsExecutionUIMessages.getString(
				"MOExecution.wizardWindowTitle", controller //$NON-NLS-1$
						.getOperationSpecification().getName()));
		setHelpAvailable(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Adds the pages necessary to collect all information to execute the
	 * operation.
	 */
	@Override
	public void addPages() {
		// add pre-binding page
		preBindingPage = new PreBindingWizardPage(PREBINDING_PAGE_NAME,
				controller);
		addPage(preBindingPage);
		// add binding page
		bindingPage = new BindingWizardPage(BINDING_PAGE_NAME, controller);
		addPage(bindingPage);
		// add user input page if needed
		if (controller.shouldShowUserInputPage()) {
			userInputPage = new UserInputWizardPage(USER_INPUT_PAGE_NAME,
					controller);
			addPage(userInputPage);
		}
		super.addPages();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean canFinish() {
		IStatus status = controller.validateCurrentBinding();
		if (status.isOK()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean performFinish() {
		// get user inputs
		if (userInputPage != null && userInputPage.getUserInputValues() != null) {
			Map<UserInput, String> userInputValues = userInputPage
					.getUserInputValues();
			for (UserInput userInput : userInputValues.keySet()) {
				if (userInputValues.get(userInput) != null
						&& userInputValues.get(userInput).length() > 0) {
					this.controller.setUserInputValue(userInput,
							userInputValues.get(userInput));
				}
			}
		}
		// execute
		return controller.execute();
	}

}
