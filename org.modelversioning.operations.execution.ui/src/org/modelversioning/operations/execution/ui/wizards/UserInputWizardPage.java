/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.wizards;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.controller.ExecutionController;

/**
 * The page collecting {@link UserInput} values.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class UserInputWizardPage extends WizardPage {

	/**
	 * The controller.
	 */
	private ExecutionController controller;
	/**
	 * Map saving user inputs and values.
	 */
	private Map<UserInput, Text> userInputValues = new HashMap<UserInput, Text>();

	/**
	 * Constructor providing the <code>pageName</code> and the
	 * <code>controller</code>.
	 * 
	 * @param pageName
	 *            name of page.
	 * @param controller
	 *            controller controlling this page.
	 */
	protected UserInputWizardPage(String pageName,
			ExecutionController controller) {
		super(pageName);
		this.controller = controller;
		// set some values
		setTitle(OperationsExecutionUIMessages
				.getString("MOExecution.userInputPageTitle")); //$NON-NLS-1$
		setDescription(OperationsExecutionUIMessages
				.getString("MOExecution.userInputPageDescription")); //$NON-NLS-1$
		setPageComplete(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Creates the controls of this page.
	 */
	@Override
	public void createControl(final Composite parent) {
		// create overall container
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(createOverallLayout());
		setControl(container);
		// create inputs for each user input
		for (UserInput userInput : controller.getOperationSpecification()
				.getUserInputs()) {
			// create label
			Label label = new Label(container, SWT.NULL);
			label.setText(userInput.getName());
			// create input
			Text text = new Text(container, SWT.BORDER);
			text.setText(controller.getUserInputValue(userInput));
			text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			this.userInputValues.put(userInput, text);
		}
	}

	/**
	 * Creates the overall layout of this wizard page.
	 * 
	 * @return the overall layout.
	 */
	private Layout createOverallLayout() {
		GridLayout layout = new GridLayout(2, false);
		return layout;
	}
	
	/**
	 * Returns a {@link Map} of {@link UserInput}s to specified {@link String}s.
	 * 
	 * @return {@link Map} of {@link UserInput}s to specified {@link String}s.
	 */
	public Map<UserInput, String> getUserInputValues() {
		Map<UserInput, String> userInputValues = new HashMap<UserInput, String>();
		for (UserInput userInput : controller.getOperationSpecification()
				.getUserInputs()) {
			userInputValues.put(userInput, this.userInputValues.get(userInput)
					.getText());
		}
		return userInputValues;
	}
}
