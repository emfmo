/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.wizards;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.controller.ExecutionController;
import org.modelversioning.operations.ui.commons.views.TemplateTreeView;
import org.modelversioning.ui.commons.parts.EcoreTreeView;
import org.modelversioning.ui.commons.parts.LineDrawingCenterPart;

/**
 * The page showing the actual binding.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class BindingWizardPage extends WizardPage implements TreeListener,
		Listener, SelectionListener {

	final static int centerPartWidth = 34;

	/**
	 * The controller managing and providing the data.
	 */
	private ExecutionController controller;

	/**
	 * Tree view of ecore objects.
	 */
	private EcoreTreeView ecoreTreeView;

	/**
	 * Tree view of templates.
	 */
	private TemplateTreeView templateTreeView;

	/**
	 * The line drawing center part.
	 */
	private LineDrawingCenterPart centerPart;

	/**
	 * The unbind button.
	 */
	private Button unbindButton;

	/**
	 * Constructor providing the <code>pageName</code> and the
	 * <code>controller</code>.
	 * 
	 * @param pageName
	 *            name of page.
	 * @param controller
	 *            controller controlling this page.
	 */
	protected BindingWizardPage(String pageName, ExecutionController controller) {
		super(pageName);
		this.controller = controller;
		// set some values
		setTitle(OperationsExecutionUIMessages
				.getString("MOExecution.bindingPageTitle"));
		setDescription(OperationsExecutionUIMessages
				.getString("MOExecution.bindingPageDescription"));
		setPageComplete(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Creates the controls of this page.
	 */
	@Override
	public void createControl(final Composite parent) {
		// create overall container
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(createOverallLayout());
		setControl(container);

		// create tree container
		Composite treeContainer = new Composite(container, SWT.NULL);
		// configure tree container
		treeContainer.setLayoutData(createTreeContainerGridData());
		treeContainer.setLayout(createTreeViewsLayout());

		// show ecore tree
		Composite ecoreTreeViewContainer = new Composite(treeContainer,
				SWT.NULL);
		ecoreTreeViewContainer.setLayout(new FillLayout());
		ecoreTreeViewContainer.setLayoutData(createTreeViewGridData());
		ecoreTreeView = new EcoreTreeView(controller.getRootObject());
		ecoreTreeView.createPartControl(ecoreTreeViewContainer);
		ecoreTreeView.getTree().addTreeListener(this);
		ecoreTreeView.getTree().addSelectionListener(this);
		ecoreTreeView.getTree().addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				centerPart.redraw();
			}
		});

		// add center part container
		Composite centerPartContainer = new Composite(treeContainer, SWT.NULL);
		centerPartContainer.setLayout(new FillLayout());
		centerPartContainer
				.setLayoutData(createCenterPartContainerGridData(centerPartWidth));

		// show template tree
		Composite templateTreeViewContainer = new Composite(treeContainer,
				SWT.NULL);
		templateTreeViewContainer.setLayout(new FillLayout());
		templateTreeViewContainer.setLayoutData(createTreeViewGridData());
		templateTreeView = new TemplateTreeView(controller.getRootTemplate());
		templateTreeView.createPartControl(templateTreeViewContainer);
		templateTreeView.getTree().addTreeListener(this);
		templateTreeView.getTree().addSelectionListener(this);
		templateTreeView.getTree().addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				centerPart.redraw();
			}
		});

		// add center part to center part container
		// we have to add container before line drawing center part since it
		// accesses the both trees which will be initialized when painting.
		centerPart = new LineDrawingCenterPart(centerPartContainer,
				ecoreTreeView, templateTreeView, centerPartWidth, controller,
				controller);

		// create button container
		Composite buttonContainer = new Composite(container, SWT.NULL);
		// configure button container
		buttonContainer.setLayout(createButtonLayout());
		buttonContainer.setLayoutData(createButtonContainerGridData());
		// create buttons
		createUnbindButton(buttonContainer);
	}

	/**
	 * Creates the unbind button ({@link #unbindButton}.
	 * 
	 * @param parent
	 *            parent to add button to.
	 */
	private void createUnbindButton(Composite parent) {
		unbindButton = new Button(parent, SWT.PUSH);
		unbindButton.setText(OperationsExecutionUIMessages
				.getString("MOExecution.unbindButtonText")); //$NON-NLS-1$
		unbindButton.addListener(SWT.Selection, this);
		unbindButton.setEnabled(false);
	}

	/**
	 * Creates the {@link GridData} for the tree container.
	 * 
	 * @return the {@link GridData} for the tree container.
	 */
	private GridData createTreeContainerGridData() {
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the {@link GridData} for the button container.
	 * 
	 * @return the {@link GridData} for the button container.
	 */
	private GridData createButtonContainerGridData() {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the overall layout of this wizard page.
	 * 
	 * @return the overall layout.
	 */
	private Layout createOverallLayout() {
		GridLayout layout = new GridLayout(1, false);
		return layout;
	}

	/**
	 * Creates the {@link Layout} for the container containing the trees.
	 * 
	 * @return the {@link Layout} for the container containing the trees.
	 */
	private Layout createTreeViewsLayout() {
		GridLayout layout = new GridLayout(3, false);
		return layout;
	}

	/**
	 * Creates the {@link GridData} for the tree views.
	 * 
	 * @return the {@link GridData} for the tree views.
	 */
	private GridData createTreeViewGridData() {
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the {@link GridData} for the center part.
	 * 
	 * @return the {@link GridData} for the tree view container.
	 */
	private GridData createCenterPartContainerGridData(int centerPartWidth) {
		GridData gridData = new GridData();
		gridData.widthHint = centerPartWidth;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		return gridData;
	}

	/**
	 * Creates the button layout.
	 * 
	 * @return the button layout.
	 */
	private Layout createButtonLayout() {
		RowLayout layout = new RowLayout(SWT.HORIZONTAL);
		layout.justify = false;
		layout.center = true;
		layout.fill = true;
		layout.pack = false;
		layout.spacing = 4;
		return layout;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Calls {@link #refreshTrees()}.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void treeCollapsed(TreeEvent e) {
		refreshTrees();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Calls {@link #refreshTrees()}.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void treeExpanded(TreeEvent e) {
		refreshTrees();
	}

	/**
	 * Refreshes the {@link #centerPart} if the {@link #ecoreTreeView} or the
	 * {@link #templateTreeView} get changed.
	 */
	private void refreshTrees() {
		this.templateTreeView.layout();
		this.ecoreTreeView.layout();
		this.centerPart.redraw();
		validate();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Refreshes the {@link #centerPart} if the {@link #ecoreTreeView} or the
	 * {@link #templateTreeView} get changed.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void handleEvent(Event event) {
		if (event.widget.equals(unbindButton)) {
			unbindButtonPushed();
		}

	}

	/**
	 * Un-binds the currently selected {@link EObject} from {@link Template}s.
	 */
	private void unbindButtonPushed() {
		Template[] templates = templateTreeView.getSelectedTemplates();
		if (templates.length == 1) {
			for (EObject eObject : ecoreTreeView.getSelectedEObjects()) {
				Template template = templates[0];
				controller.removeFromBinding(eObject, template);
			}
			refreshTrees();
			validate();
		}
		unbindButton.setEnabled(false);
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		/* nothing todo */
	}

	/**
	 * Activates or de-activates the buttons.
	 * 
	 * @param e
	 *            event.
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		updateButtons();
	}

	/**
	 * Updates the state of the buttons.
	 */
	private void updateButtons() {
		EObject[] selectedEObjects = ecoreTreeView.getSelectedEObjects();
		Template[] templates = templateTreeView.getSelectedTemplates();
		if (templates.length != 1 || selectedEObjects.length < 1) {
			unbindButton.setEnabled(false);
		} else {
			Template template = templates[0];
			boolean isRemovable = true;
			for (EObject eObject : selectedEObjects) {
				if (!controller.isRemovableFromBinding(eObject, template)) {
					isRemovable = false;
				}
			}
			if (selectedEObjects.length > 0) {
				unbindButton.setEnabled(isRemovable);
			}
		}
	}

	/**
	 * Validates the displayed binding and visualizes the result.
	 */
	public void validate() {
		updateButtons();
		IStatus status = controller.validateCurrentBinding();
		if (status.isOK()) {
			this.setMessage(null);
			this.setPageComplete(true);
		} else {
			String details = ""; //$NON-NLS-1$
			if (status.getMessage() != null && status.getMessage().length() > 0) {
				details = status.getMessage(); //$NON-NLS-1$
			}
			this.setMessage(
					OperationsExecutionUIMessages
							.getString("MOExecution.bindingPageErrorMsg_" //$NON-NLS-1$
									+ status.getCode()) + "\n" + details, ERROR); //$NON-NLS-1$
			this.setPageComplete(false);
		}
	}
}
