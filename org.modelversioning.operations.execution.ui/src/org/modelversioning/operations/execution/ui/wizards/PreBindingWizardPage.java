/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.wizards;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.controller.ExecutionController;
import org.modelversioning.operations.ui.commons.views.TemplateTreeView;
import org.modelversioning.ui.commons.parts.EcoreTreeView;
import org.modelversioning.ui.commons.parts.LineDrawingCenterPart;

/**
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class PreBindingWizardPage extends WizardPage implements TreeListener,
		Listener, SelectionListener {

	final static int centerPartWidth = 34;

	/**
	 * The controller managing and providing the data.
	 */
	private ExecutionController controller;

	/**
	 * Tree view of ecore objects.
	 */
	private EcoreTreeView ecoreTreeView;

	/**
	 * Tree view of templates.
	 */
	private TemplateTreeView templateTreeView;

	/**
	 * The line drawing center part.
	 */
	private LineDrawingCenterPart centerPart;

	/**
	 * The bind button.
	 */
	private Button bindButton;

	/**
	 * The unbind button.
	 */
	private Button unbindButton;

	/**
	 * Constructor providing the <code>pageName</code>.
	 * 
	 * @param pageName
	 *            name of page.
	 * @param rootObject
	 *            root object of model to transform.
	 * @param preBinding
	 *            pre-binding to visualize.
	 */
	protected PreBindingWizardPage(String pageName,
			ExecutionController controller) {
		super(pageName);
		this.controller = controller;
		// set some values
		setTitle(OperationsExecutionUIMessages
				.getString("MOExecution.preBindingPageTitle"));
		setDescription(OperationsExecutionUIMessages
				.getString("MOExecution.preBindingPageDescription"));
		setPageComplete(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Creates the controls of this page.
	 */
	@Override
	public void createControl(Composite parent) {
		// create overall container
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(createOverallLayout());
		setControl(container);

		// create tree container
		Composite treeContainer = new Composite(container, SWT.NULL);
		// configure tree container
		treeContainer.setLayoutData(createTreeContainerGridData());
		treeContainer.setLayout(createTreeViewsLayout());

		// show ecore tree
		Composite ecoreTreeViewContainer = new Composite(treeContainer,
				SWT.NULL);
		ecoreTreeViewContainer.setLayout(new FillLayout());
		ecoreTreeViewContainer.setLayoutData(createTreeViewGridData());
		ecoreTreeView = new EcoreTreeView(controller.getRootObject());
		ecoreTreeView.createPartControl(ecoreTreeViewContainer);
		ecoreTreeView.getTree().addTreeListener(this);
		ecoreTreeView.getTree().addSelectionListener(this);
		ecoreTreeView.getTree().addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				centerPart.redraw();
			}
		});

		// add center part container
		Composite centerPartContainer = new Composite(treeContainer, SWT.NULL);
		centerPartContainer.setLayout(new FillLayout());
		centerPartContainer
				.setLayoutData(createCenterPartContainerGridData(centerPartWidth));

		// show template tree
		Composite templateTreeViewContainer = new Composite(treeContainer,
				SWT.NULL);
		templateTreeViewContainer.setLayout(new FillLayout());
		templateTreeViewContainer.setLayoutData(createTreeViewGridData());
		templateTreeView = new TemplateTreeView(controller.getRootTemplate());
		templateTreeView.createPartControl(templateTreeViewContainer);
		templateTreeView.getTree().addTreeListener(this);
		templateTreeView.getTree().addSelectionListener(this);
		templateTreeView.getTree().addPaintListener(new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				centerPart.redraw();
			}
		});

		// add center part to center part container
		// we have to add container before line drawing center part since it
		// accesses the both trees which will be initialized when painting.
		centerPart = new LineDrawingCenterPart(centerPartContainer,
				ecoreTreeView, templateTreeView, centerPartWidth, controller,
				controller);

		// create button container
		Composite buttonContainer = new Composite(container, SWT.NULL);
		// configure button container
		buttonContainer.setLayout(createButtonLayout());
		buttonContainer.setLayoutData(createButtonContainerGridData());
		// create buttons
		createBindButton(buttonContainer);
		createUnbindButton(buttonContainer);

		// initialize page complete status
		refreshPageCompleteStatus();
	}

	/**
	 * Creates the bind button ({@link #bindButton}.
	 * 
	 * @param parent
	 *            parent to add button to.
	 */
	private void createBindButton(Composite parent) {
		bindButton = new Button(parent, SWT.PUSH);
		bindButton.setText(OperationsExecutionUIMessages
				.getString("MOExecution.bindButtonText")); //$NON-NLS-1$
		bindButton.addListener(SWT.Selection, this);
		bindButton.setEnabled(false);
	}

	/**
	 * Creates the unbind button ({@link #unbindButton}.
	 * 
	 * @param parent
	 *            parent to add button to.
	 */
	private void createUnbindButton(Composite parent) {
		unbindButton = new Button(parent, SWT.PUSH);
		unbindButton.setText(OperationsExecutionUIMessages
				.getString("MOExecution.unbindButtonText")); //$NON-NLS-1$
		unbindButton.addListener(SWT.Selection, this);
		unbindButton.setEnabled(false);
	}

	/**
	 * Creates the {@link GridData} for the tree container.
	 * 
	 * @return the {@link GridData} for the tree container.
	 */
	private GridData createTreeContainerGridData() {
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the {@link GridData} for the button container.
	 * 
	 * @return the {@link GridData} for the button container.
	 */
	private GridData createButtonContainerGridData() {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the overall layout of this wizard page.
	 * 
	 * @return the overall layout.
	 */
	private Layout createOverallLayout() {
		GridLayout layout = new GridLayout(1, false);
		return layout;
	}

	/**
	 * Creates the {@link Layout} for the container containing the trees.
	 * 
	 * @return the {@link Layout} for the container containing the trees.
	 */
	private Layout createTreeViewsLayout() {
		GridLayout layout = new GridLayout(3, false);
		return layout;
	}

	/**
	 * Creates the {@link GridData} for the tree views.
	 * 
	 * @return the {@link GridData} for the tree views.
	 */
	private GridData createTreeViewGridData() {
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		return gridData;
	}

	/**
	 * Creates the {@link GridData} for the center part.
	 * 
	 * @return the {@link GridData} for the tree view container.
	 */
	private GridData createCenterPartContainerGridData(int centerPartWidth) {
		GridData gridData = new GridData();
		gridData.widthHint = centerPartWidth;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		return gridData;
	}

	/**
	 * Creates the button layout.
	 * 
	 * @return the button layout.
	 */
	private Layout createButtonLayout() {
		RowLayout layout = new RowLayout(SWT.HORIZONTAL);
		layout.justify = false;
		layout.center = true;
		layout.fill = true;
		layout.pack = false;
		layout.spacing = 4;
		return layout;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Calls {@link #refreshTrees()}.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void treeCollapsed(TreeEvent e) {
		refreshTrees();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Calls {@link #refreshTrees()}.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void treeExpanded(TreeEvent e) {
		refreshTrees();
	}

	/**
	 * Refreshes the {@link #centerPart} if the {@link #ecoreTreeView} or the
	 * {@link #templateTreeView} get changed.
	 */
	private void refreshTrees() {
		this.templateTreeView.layout();
		this.ecoreTreeView.layout();
		this.centerPart.redraw();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Refreshes the {@link #centerPart} if the {@link #ecoreTreeView} or the
	 * {@link #templateTreeView} get changed.
	 * 
	 * @param event
	 *            to process.
	 */
	@Override
	public void handleEvent(Event event) {
		if (event.widget.equals(bindButton)) {
			bindButtonPushed();
		} else if (event.widget.equals(unbindButton)) {
			unbindButtonPushed();
		}

	}

	/**
	 * Binds the currently selected {@link EObject}s to {@link Template}.
	 */
	private void bindButtonPushed() {
		Template[] templates = templateTreeView.getSelectedTemplates();
		if (templates.length > 0) {
			Template template = templates[0];
			controller.addToPreBinding(template,
					ecoreTreeView.getSelectedEObjects());
		}
		refreshTrees();
		refreshPageCompleteStatus();
	}

	/**
	 * Un-binds the currently selected {@link EObject} from {@link Template}s.
	 */
	private void unbindButtonPushed() {
		controller.removeFromPreBinding(ecoreTreeView.getSelectedEObjects());
		refreshTrees();
		refreshPageCompleteStatus();
	}

	/**
	 * Does nothing.
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent e) {
		/* nothing todo */
	}

	/**
	 * Activates or de-activates the buttons.
	 * 
	 * @param e
	 *            event.
	 */
	@Override
	public void widgetSelected(SelectionEvent e) {
		Template[] templates = templateTreeView.getSelectedTemplates();
		if (templates.length > 0) {
			Template template = templates[0];
			EObject[] selectedEObjects = ecoreTreeView.getSelectedEObjects();
			// check if it is bound
			if (controller.isPreBound(template, selectedEObjects)) {
				unbindButton.setEnabled(true);
				return;
			}
			// activate bind button if binding is possible.

			if (controller.mayPreBind(template, selectedEObjects)) {
				bindButton.setEnabled(true);
				return;
			}
		}
		bindButton.setEnabled(false);
		unbindButton.setEnabled(false);
	}

	/**
	 * Refreshes the status of the page complete status.
	 */
	private void refreshPageCompleteStatus() {
		this.setPageComplete(controller.mayProceedToBinding());
	}
}
