/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.modelversioning.core.conditions.EvaluationResult;
import org.modelversioning.core.conditions.Template;
import org.modelversioning.core.conditions.engines.IConditionEvaluationEngine;
import org.modelversioning.core.conditions.engines.ITemplateBinding;
import org.modelversioning.core.conditions.engines.UnsupportedConditionLanguage;
import org.modelversioning.core.conditions.engines.impl.ConditionsEvaluationEngineImpl;
import org.modelversioning.core.conditions.engines.impl.TemplateBindingImpl;
import org.modelversioning.operations.OperationSpecification;
import org.modelversioning.operations.UserInput;
import org.modelversioning.operations.execution.CompositeOperationCommand;
import org.modelversioning.operations.execution.IOperationBinding;
import org.modelversioning.operations.execution.engines.IOperationBindingGenerator;
import org.modelversioning.operations.execution.engines.IOperationExecutionEngine;
import org.modelversioning.operations.execution.engines.impl.OperationBindingGeneratorImpl;
import org.modelversioning.operations.execution.engines.impl.OperationExecutionEngineImpl;
import org.modelversioning.operations.execution.ui.IEditorRefreshAdapter;
import org.modelversioning.operations.execution.ui.ISelectedObjectsDetector;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIMessages;
import org.modelversioning.operations.execution.ui.OperationsExecutionUIPlugin;
import org.modelversioning.operations.execution.ui.impl.DiagramEditorSelectedObjectsDetector;
import org.modelversioning.operations.execution.ui.impl.EcoreEditorRefreshAdapter;
import org.modelversioning.operations.execution.ui.impl.SelectionProviderSelectedObjectsDetector;
import org.modelversioning.operations.execution.ui.wizards.BindingWizardPage;
import org.modelversioning.operations.execution.ui.wizards.ExecutionWizard;
import org.modelversioning.ui.commons.ILineDecorationProvider;
import org.modelversioning.ui.commons.ITreeConnectionProvider;

/**
 * Controller which controls the configuration and execution of
 * {@link OperationSpecification}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class ExecutionController implements ITreeConnectionProvider,
		ILineDecorationProvider, IPageChangedListener {

	/**
	 * Enumeration specifying the status of the execution.
	 * 
	 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
	 * 
	 */
	private enum Status {
		RESOURCE_SELECTION, PREBINDING, BINDING, USER_INPUT, EXECUTION
	}

	/**
	 * Operation binding null message.
	 */
	private static final String OPERATION_BINDING_NULL = "Operation Binding is null";

	/**
	 * The operation specification.
	 */
	private OperationSpecification operationSpecification;
	/**
	 * The editing domain of the current editor.
	 */
	private EditingDomain editingDomain;
	/**
	 * The specified pre-binding.
	 */
	private ITemplateBinding preBinding;
	/**
	 * The root object of the model to transform.
	 */
	private EObject rootObject = null;
	/**
	 * The editor currently active.
	 */
	private IEditorPart activeEditor = null;
	/**
	 * The {@link IOperationBindingGenerator} to use.
	 */
	private IOperationBindingGenerator operationBindingGenerator;
	/**
	 * The evaluation engine to use.
	 */
	private IConditionEvaluationEngine evaluationEngine = null;
	/**
	 * The current {@link IOperationBinding}.
	 */
	private IOperationBinding currentOperationBinding = null;
	/**
	 * The current status.
	 */
	private ExecutionController.Status currentStatus;
	/**
	 * The execution engine.
	 */
	private IOperationExecutionEngine executionEngine = null;

	/**
	 * Creates a new controller for executing the specified
	 * <code>operationSpecification</code> using the specified
	 * <code>editingDomain</code> on the basis of the specified
	 * <code>preBinding</code>.
	 * 
	 * @param operationSpecification
	 *            to execute.
	 * @param editingDomain
	 *            to use (might be <code>null</code>).
	 * @param preBinding
	 *            to start with (might be empty).
	 */
	public ExecutionController(OperationSpecification operationSpecification,
			EditingDomain editingDomain, ITemplateBinding preBinding) {
		super();
		this.operationSpecification = operationSpecification;
		this.editingDomain = editingDomain;
		this.preBinding = preBinding;
		determineRootObject();
		checkAndCleanPreBinding();
	}

	/**
	 * Creates a new controller for executing the specified
	 * <code>operationSpecification</code> and providing the
	 * <code>activeEditor</code> and the <code>selectedTemplate</code> to derive
	 * pre-binding and editingDomain.
	 * 
	 * @param operationSpecification
	 *            to execute.
	 * @param selectedTemplate
	 *            to determine pre-binding.
	 * @param activeEditor
	 *            to determine pre-binding and {@link ResourceSet}.
	 */
	public ExecutionController(OperationSpecification operationSpecification,
			Template selectedTemplate, IEditorPart activeEditor) {
		this.activeEditor = activeEditor;
		this.operationSpecification = operationSpecification;
		tryFindPreBinding(activeEditor, selectedTemplate);
		tryFindEditingDomain(activeEditor);
		determineRootObject();
		checkAndCleanPreBinding();
	}

	private void checkAndCleanPreBinding() {
		// find out bindings to remove
		ITemplateBinding toRemove = new TemplateBindingImpl();
		for (Template template : this.preBinding.getTemplates()) {
			for (EObject eObject : this.preBinding.getBoundObjects(template)) {
				EvaluationResult result = getEvaluationEngine().evaluate(
						template, eObject, true);
				if (!result.isOK()) {
					toRemove.add(template, eObject);
				}
			}
		}

		// remove bindings to remove
		boolean hasRemoved = false;
		for (Template template : toRemove.getTemplates()) {
			this.preBinding
					.remove(template, toRemove.getBoundObjects(template));
			hasRemoved = true;
		}
		// show message if we removed something
		if (hasRemoved) {
			MessageDialog.openWarning(OperationsExecutionUIPlugin.getShell(),
					OperationsExecutionUIMessages
							.getString("MOExecution.removedBindingTitle"), //$NON-NLS-1$
					OperationsExecutionUIMessages
							.getString("MOExecution.removedBindingMessage")); //$NON-NLS-1$
		}
	}

	/**
	 * @return the evaluationEngine
	 */
	public IConditionEvaluationEngine getEvaluationEngine() {
		if (evaluationEngine == null) {
			this.evaluationEngine = new ConditionsEvaluationEngineImpl();
			try {
				this.evaluationEngine.setConditionsModel(operationSpecification
						.getPreconditions());
			} catch (UnsupportedConditionLanguage e) {
				OperationsExecutionUIPlugin.log(e);
			}
		}
		return evaluationEngine;
	}

	/**
	 * @param evaluationEngine
	 *            the evaluationEngine to set
	 */
	public void setEvaluationEngine(IConditionEvaluationEngine evaluationEngine) {
		this.evaluationEngine = evaluationEngine;
	}

	/**
	 * Returns the {@link IOperationBindingGenerator} to use.
	 * 
	 * @return the used {@link IOperationBindingGenerator}.
	 */
	public IOperationBindingGenerator getOperationBindingGenerator() {
		if (operationBindingGenerator == null) {
			operationBindingGenerator = new OperationBindingGeneratorImpl();
		}
		return operationBindingGenerator;
	}

	/**
	 * @param operationBindingGenerator
	 *            the operationBindingGenerator to set
	 */
	public void setOperationBindingGenerator(
			IOperationBindingGenerator operationBindingGenerator) {
		this.operationBindingGenerator = operationBindingGenerator;
	}

	/**
	 * Tries to detach the used {@link EditingDomain} from the
	 * <code>activeEditor</code>.
	 * 
	 * @param activeEditor
	 *            to try to detach {@link EditingDomain} from.
	 */
	private void tryFindEditingDomain(IEditorPart activeEditor) {
		if (activeEditor != null) {
			if (activeEditor instanceof DiagramEditor) {
				editingDomain = ((DiagramEditor) activeEditor)
						.getEditingDomain();
			} else

			if (activeEditor instanceof IEditingDomainProvider) {
				editingDomain = ((IEditingDomainProvider) activeEditor)
						.getEditingDomain();
			}
		}
	}

	/**
	 * Tries to derive a pre-binding from selected elements in
	 * <code>activeEditor</code> and the <code>selectedTemplate</code>.
	 * 
	 * @param activeEditor
	 *            the active editor.
	 * @param selectedTemplate
	 *            the selected template.
	 */
	private void tryFindPreBinding(IEditorPart activeEditor,
			Template selectedTemplate) {
		Set<EObject> selectedObjects = getSelectedObjectDetector(activeEditor)
				.getSelectedObjects(activeEditor);
		// create pre-binding if available
		preBinding = new TemplateBindingImpl();
		if (selectedTemplate != null && selectedObjects.size() > 0) {
			preBinding.add(selectedTemplate, selectedObjects);
		}
	}

	/**
	 * Determines the root object by trying to iterate the preBinding or getting
	 * the model of the activeEditor.
	 */
	private void determineRootObject() {
		if (this.preBinding.getTemplates().size() > 1) {
			for (EObject eObject : this.preBinding.getBoundObjects()) {
				EObject root = EcoreUtil.getRootContainer(eObject);
				if (root != null) {
					this.rootObject = root;
					return;
				}
			}
		} else if (activeEditor != null) {
			Collection<EObject> selectedObjects = getSelectedObjectDetector(
					activeEditor).getSelectedObjects(activeEditor);
			for (EObject eObject : selectedObjects) {
				EObject root = EcoreUtil.getRootContainer(eObject);
				if (root != null) {
					this.rootObject = root;
					return;
				}
			}
		}
	}

	/**
	 * Specifies whether the root object is known.
	 * 
	 * @return <code>true</code> if it is known already, <code>false</code>
	 *         otherwise.
	 */
	public boolean isKnownRootObject() {
		return !(getRootObject() == null);
	}

	/**
	 * @return the operationSpecification
	 */
	public OperationSpecification getOperationSpecification() {
		return operationSpecification;
	}

	/**
	 * @return the editingDomain
	 */
	public EditingDomain getEditingDomain() {
		return editingDomain;
	}

	/**
	 * @return the preBinding
	 */
	public ITemplateBinding getPreBinding() {
		return preBinding;
	}

	/**
	 * @return the rootObject
	 */
	public EObject getRootObject() {
		return rootObject;
	}

	/**
	 * @param rootObject
	 *            the rootObject to set
	 */
	public void setRootObject(EObject rootObject) {
		this.rootObject = rootObject;
	}

	/**
	 * @return the activeEditor
	 */
	public IEditorPart getActiveEditor() {
		return activeEditor;
	}

	/**
	 * @param activeEditor
	 *            the activeEditor to set
	 */
	public void setActiveEditor(IEditorPart activeEditor) {
		this.activeEditor = activeEditor;
	}

	/**
	 * Shows the {@link ExecutionWizard} to collect the necessary information
	 * and execute the {@link OperationSpecification}.
	 */
	public void showWizard() {
		// ask for model if necessary
		if (shouldShowResourceSelectionPage()) {
			askForResourceToExecuteOn();
			if (this.rootObject != null) {
				// open editor for the selected file
				IEditorPart editor = openDefaultEditor(this.rootObject);
				this.activeEditor = editor;
			} else {
				// do not start execution wizard without rootObject
				return;
			}
		}

		Runnable runnable = new Runnable() {
			public void run() {
				WizardDialog wizardDialog = new WizardDialog(
						OperationsExecutionUIPlugin.getShell(),
						new ExecutionWizard(ExecutionController.this));
				wizardDialog.addPageChangedListener(ExecutionController.this);
				wizardDialog.open();
			}
		};
		syncExec(runnable);
	}

	/**
	 * Synchronizes the specified <code>runnable</code>.
	 * 
	 * @param runnable
	 *            to synchronize.
	 */
	private void syncExec(Runnable runnable) {
		if (Display.getCurrent() == null) {
			Display.getDefault().syncExec(runnable);
		} else {
			runnable.run();
		}
	}

	/**
	 * Shows a dialog which asks the user to choose a resource to execute the
	 * operation on.
	 */
	private void askForResourceToExecuteOn() {
		// ask for file to open
		FileDialog fileDialog = new FileDialog(OperationsExecutionUIPlugin
				.getShell());
		fileDialog.setText(OperationsExecutionUIMessages
				.getString("MOExecution.fileSelectionText")); //$NON-NLS-1$
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		fileDialog.setFilterPath(root.getLocation().toOSString());
		String selectedFile = fileDialog.open();
		// check if file is an ecore resource
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(URI
				.createFileURI(selectedFile), true);

		if (resource != null && resource.getContents().size() > 0) {
			// file is ecore resource and not empty
			// so detach root object
			this.rootObject = resource.getContents().get(0);
		} else {
			// resource can not be used, so show message
			ErrorDialog errDiag = new ErrorDialog(
					OperationsExecutionUIPlugin.getShell(),
					OperationsExecutionUIMessages
							.getString("MOExecution.fileSelectionErrorDialogTitle"), //$NON-NLS-1$
					OperationsExecutionUIMessages
							.getString("MOExecution.fileSelectionErrorDialogMessage"), //$NON-NLS-1$
					new org.eclipse.core.runtime.Status(
							IStatus.ERROR,
							OperationsExecutionUIPlugin.PLUGIN_ID,
							OperationsExecutionUIMessages
									.getString("MOExecution.fileSelectionErrorDialogMessage")), //$NON-NLS-1$
					0);
			errDiag.open();
		}
	}

	/**
	 * Opens the default editor for the specified <code>rootObject</code>.
	 * 
	 * @param rootObject
	 *            to open editor for.
	 * @return the opened editor or <code>null</code>.
	 */
	private IEditorPart openDefaultEditor(EObject rootObject2) {
		// URI uri = EcoreUtil.getURI(rootObject2);
		// IWorkbenchPage page = OperationsExecutionUIPlugin
		// .getActiveWorkbenchWindow().getActivePage();
		// IEditorRegistry editorRegistry = OperationsExecutionUIPlugin
		// .getActiveWorkbench().getEditorRegistry();
		// IEditorDescriptor defaultEditor = editorRegistry.getDefaultEditor(uri
		// .toFileString());
		// try {
		// if (defaultEditor == null) {
		// return page.openEditor(new URIEditorInput(uri),
		// "org.eclipse.emf.genericEditor");
		// } else {
		// return page.openEditor(new URIEditorInput(uri),
		// defaultEditor.getId());
		// }
		// } catch (PartInitException e) {
		return null;
		// }
	}

	/**
	 * Returns a {@link ISelectedObjectsDetector} service to determine currently
	 * selected {@link EObject}s in <code>editor</code>.
	 * 
	 * @param editor
	 *            for which the detection service is needed.
	 * @return suitable detection service.
	 */
	public static ISelectedObjectsDetector getSelectedObjectDetector(
			IEditorPart editor) {
		return getBestObjectDetector(editor);
	}

	/**
	 * Returns an {@link ISelectedObjectsDetector} which is best for
	 * <code>editor</code>.
	 * 
	 * @param editor
	 *            to find best {@link ISelectedObjectsDetector} for.
	 * @return best available {@link ISelectedObjectsDetector}.
	 */
	private static ISelectedObjectsDetector getBestObjectDetector(
			IEditorPart editor) {
		// TODO implement using extension points?
		if (editor instanceof ISelectionProvider) {
			return new SelectionProviderSelectedObjectsDetector();
		} else if (editor instanceof org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor) {
			return new DiagramEditorSelectedObjectsDetector();
		} else {
			return new SelectionProviderSelectedObjectsDetector();
		}
		// FIXME create NoneAvailableSelectedObjectDetection which always
		// returns empty list
		// FIXME only maintain one instance of the detections.

	}

	/**
	 * Returns a {@link IEditorRefreshAdapter} service to refresh the
	 * <code>editor</code>.
	 * 
	 * @param editor
	 *            for which the refresh adapter is needed.
	 * @return suitable refresh adapter.
	 */
	public static IEditorRefreshAdapter getEditorAdapter(IEditorPart editor) {
		return getBestEditorRefreshAdapter(editor);
	}

	/**
	 * Returns an {@link IEditorRefreshAdapter} which is best for the specified
	 * <code>editor</code>.
	 * 
	 * @param editor
	 *            to find best {@link IEditorRefreshAdapter} for.
	 * @return best available {@link IEditorRefreshAdapter}.
	 */
	private static IEditorRefreshAdapter getBestEditorRefreshAdapter(
			IEditorPart editor) {
		// TODO implement using extension points?
		// currently we only support fixed set of detection services.
		// if (editor instanceof ISelectionProvider) //...
		// FIXME create NoneAvailableSelectedObjectDetection which always
		// returns empty list
		// FIXME only maintain one instance of the detections.
		return new EcoreEditorRefreshAdapter();
	}

	/**
	 * Returns the root {@link Template} of the preconditions contained by the
	 * currently set {@link OperationSpecification}.
	 * 
	 * @return the precondition's root {@link Template}.
	 */
	public Template getRootTemplate() {
		return this.operationSpecification.getPreconditions().getRootTemplate();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Provides the connection of the current preBinding.
	 */
	@Override
	public TreeItem[] getConnectedItems(TreeItem leftItem, Tree rightTree) {
		List<TreeItem> treeItems = new ArrayList<TreeItem>();
		if (leftItem.getData() instanceof EObject) {
			EObject leftObject = (EObject) leftItem.getData();
			if (getCurrentStatus().equals(Status.PREBINDING)) {
				// if current status is prebinding take the prebindings
				for (Template template : getPreBinding().getTemplates()) {
					if (getPreBinding().getBoundObjects(template).contains(
							leftObject)) {
						treeItems.addAll(findTreeItems(rightTree.getItems(),
								template));
					}
				}
			} else if (getCurrentStatus().equals(Status.BINDING)) {
				// if current status is binding take the bindings
				Set<Template> boundTemplates = currentOperationBinding
						.getTemplateBinding().getBoundTemplates(leftObject);
				for (Template boundTemplate : boundTemplates) {
					treeItems.addAll(findTreeItems(rightTree.getItems(),
							boundTemplate));
				}

			}
		}
		return treeItems.toArray(new TreeItem[0]);
	}

	/**
	 * Finds the {@link TreeItem} in <code>treeItems</code> or their children
	 * which carries <code>object</code> in its data field.
	 * 
	 * @param treeItems
	 *            to start searching with.
	 * @param object
	 *            object to find.
	 * @return found item or <code>null</code>.
	 */
	private List<TreeItem> findTreeItems(TreeItem[] treeItems, Object object) {
		List<TreeItem> treeItemList = new ArrayList<TreeItem>();
		for (TreeItem treeItem : treeItems) {
			if (object.equals(treeItem.getData())) {
				treeItemList.add(treeItem);
			}
			treeItemList.addAll(findTreeItems(treeItem.getItems(), object));
		}
		return treeItemList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ILineDecoration getLineDecoration(TreeItem leftItem,
			TreeItem rightItem, boolean isOneHidden) {
		int lineStyle = SWT.LINE_SOLID;
		RGB color = new RGB(50, 50, 220);
		int width = 2;
		// make hidden thinner and lighter
		if (isOneHidden) {
			lineStyle = SWT.LINE_DOT;
			width = 1;
			color = new RGB(100, 100, 220);
		}
		if (Status.BINDING.equals(getCurrentStatus())) {
			// highlight pre-binded
			if (rightItem.getData() instanceof Template) {
				Template template = (Template) rightItem.getData();
				if (getPreBinding().getBoundObjects(template) != null
						&& getPreBinding().getBoundObjects(template).contains(
								leftItem.getData())) {
					color = new RGB(220, 50, 50);
				} else
				// highlight removable
				if (leftItem.getData() instanceof EObject) {
					EObject eObject = (EObject) leftItem.getData();
					if (isRemovableFromBinding(eObject, template)) {
						color = new RGB(50, 220, 50);
					}
				}
			}

		}

		return new LineDecoration(color, lineStyle, width);
	}

	/**
	 * Returns whether it is possible to bind the <code>eObjects</code> to the
	 * <code>template</code>.
	 * 
	 * @param template
	 *            template to check.
	 * @param eObjects
	 *            eObjects to check.
	 * @return <code>true</code> if possible, <code>false</code> otherwise.
	 */
	public boolean mayPreBind(Template template, EObject[] eObjects) {
		boolean mayBind = true;
		for (EObject eObject : eObjects) {
			if (!isPreBound(eObject)) {
				EvaluationResult result = getEvaluationEngine().evaluate(
						template, eObject, true);
				if (!result.isOK()) {
					mayBind = false;
				}
			} else {
				return false;
			}
		}
		return mayBind;
	}

	/**
	 * Adds the <code>eObjects</code> to the pre-binding with
	 * <code>template</code>.
	 * 
	 * @param template
	 *            template to bind <code>eObjects</code> to.
	 * @param eObjects
	 *            <code>eObjects</code> to bind.
	 */
	public void addToPreBinding(Template template, EObject[] eObjects) {
		if (!mayPreBind(template, eObjects)) {
			return;
		}
		for (EObject object : eObjects) {
			getPreBinding().add(template, object);
		}
	}

	/**
	 * Removes the <code>eObjects</code> from pre-binding.
	 * 
	 * @param eObjects
	 *            to remove.
	 */
	public void removeFromPreBinding(EObject[] eObjects) {
		for (EObject eObject : eObjects) {
			getPreBinding().remove(getPreBinding().getBoundTemplate(eObject),
					eObject);
		}
	}

	/**
	 * Returns whether it <code>eObjects</code> are bound to the
	 * <code>template</code>.
	 * 
	 * @param template
	 *            template to check.
	 * @param eObjects
	 *            eObjects to check.
	 * @return <code>true</code> if bound, <code>false</code> otherwise.
	 */
	public boolean isPreBound(Template template, EObject[] eObjects) {
		if (getPreBinding().getBoundObjects(template) != null) {
			if (getPreBinding().getBoundObjects(template).containsAll(
					Arrays.asList(eObjects))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if <code>eObject</code> is bound to any
	 * {@link Template}.
	 * 
	 * @param objects
	 *            to check.
	 * @return <code>true</code> if bound. <code>false</code> otherwise.
	 */
	public boolean isPreBound(EObject eObject) {
		if (getPreBinding().getBoundObjects().contains(eObject)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Specifies whether we may go on with current pre-binding to the binding
	 * page.
	 * 
	 * @return <code>true</code> if we may proceed, <code>false</code>
	 *         otherwise.
	 */
	public boolean mayProceedToBinding() {
		if (getPreBinding().getBoundObjects().size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gets notified when next is pressed in the wizard to prepare data for next
	 * page.
	 * 
	 * @param event
	 *            event to process.
	 */
	@Override
	public void pageChanged(PageChangedEvent event) {
		IWizardPage page = (IWizardPage) event.getSelectedPage();
		if (ExecutionWizard.PREBINDING_PAGE_NAME.equals(page.getName())) {
			this.setCurrentStatus(ExecutionController.Status.PREBINDING);
		} else if (ExecutionWizard.BINDING_PAGE_NAME.equals(page.getName())) {
			initializeOperationBinding();
			if (page instanceof BindingWizardPage) {
				((BindingWizardPage) page).validate();
			}
			this.setCurrentStatus(ExecutionController.Status.BINDING);
		}
	}

	/**
	 * Initializes the currently set {@link IOperationBinding} in
	 * {@link #currentOperationBinding} using the currently set
	 * {@link #preBinding}.
	 */
	private void initializeOperationBinding() {
		IOperationBindingGenerator operationBindingGenerator = getOperationBindingGenerator();
		try {
			currentOperationBinding = operationBindingGenerator
					.generateOperationBinding(operationSpecification,
							preBinding);
		} catch (UnsupportedConditionLanguage e) {
			OperationsExecutionUIPlugin.log(e);
		}
	}

	/**
	 * Sets the current status.
	 * 
	 * @param status
	 *            status to set.
	 */
	private void setCurrentStatus(ExecutionController.Status status) {
		this.currentStatus = status;
	}

	/**
	 * Returns the current status.
	 * 
	 * @return the current status.
	 */
	public ExecutionController.Status getCurrentStatus() {
		return currentStatus;
	}

	/**
	 * Validates the current binding and returns the result in terms of a
	 * {@link IStatus}.
	 * 
	 * @return the result of the validation.
	 */
	public IStatus validateCurrentBinding() {
		if (currentOperationBinding == null) {
			return new org.eclipse.core.runtime.Status(IStatus.ERROR,
					OperationsExecutionUIPlugin.PLUGIN_ID,
					OPERATION_BINDING_NULL);
		} else {
			return currentOperationBinding.validate();
		}
	}

	/**
	 * Returns <code>true</code> if current <code>eObject</code>/
	 * <code>template</code> binding is removable in current binding.
	 * 
	 * @param eObject
	 *            to check if it is removable.
	 * @param template
	 *            to check for.
	 * @return <code>true</code> if removable. <code>false</code> if not.
	 */
	public boolean isRemovableFromBinding(EObject eObject, Template template) {
		// guard unset operation binding and template binding
		if (currentOperationBinding == null
				|| currentOperationBinding.getTemplateBinding() == null) {
			return false;
		}
		// return false if they are not bound together
		if (!currentOperationBinding.getTemplateBinding().getBoundObjects(
				template).contains(eObject)) {
			return false;
		}
		// ask template binding if it is removable.
		return currentOperationBinding.getTemplateBinding().isRemovable(
				eObject, template);
	}

	/**
	 * Removes the specified <code>eObject</code>/<code>template</code> binding
	 * from current binding.
	 * 
	 * @param eObject
	 *            to remove.
	 * @param template
	 *            to remove from.
	 */
	public void removeFromBinding(EObject eObject, Template template) {
		currentOperationBinding.getTemplateBinding().remove(eObject, template);
	}

	/**
	 * Executes the currently configured {@link OperationSpecification}.
	 * 
	 * @return <code>true</code> if successfully executed. <code>false</code> if
	 *         not.
	 */
	public boolean execute() {
		boolean didExecute = false;
		if (validateCurrentBinding().isOK()) {
			// if we have an editing domain use the command stack to execute the
			// operation
			if (editingDomain != null) {
				Command compositeOperationCommand = new CompositeOperationCommand(
						editingDomain, operationSpecification.getName(),
						operationSpecification.getDescription(),
						getExecutionEngine(), currentOperationBinding);
				editingDomain.getCommandStack().execute(
						compositeOperationCommand);
			} else {
				getExecutionEngine().execute(currentOperationBinding);
			}
			// if activeEditor is set refresh it
			if (activeEditor != null) {
				getBestEditorRefreshAdapter(activeEditor).refresh(activeEditor,
						currentOperationBinding);
				getBestEditorRefreshAdapter(activeEditor)
						.setDirty(activeEditor);
			} else {
				// save modified model if there is no editor to show changes
				try {
					this.rootObject.eResource().save(null);
					MessageDialog
							.openInformation(
									OperationsExecutionUIPlugin.getShell(),
									OperationsExecutionUIMessages
											.getString("MOExecution.executedAndSavedTitle"), //$NON-NLS-1$
									OperationsExecutionUIMessages
											.getString("MOExecution.executedAndSavedMessage")); //$NON-NLS-1$
				} catch (IOException e) {
					MessageDialog
							.openError(
									OperationsExecutionUIPlugin.getShell(),
									OperationsExecutionUIMessages
											.getString("MOExecution.cannotSaveTitle"), //$NON-NLS-1$
									OperationsExecutionUIMessages
											.getString("MOExecution.cannotSaveMessage")); //$NON-NLS-1$
				}
			}
			didExecute = true;
		}
		return didExecute;
	}

	/**
	 * Returns the currently used {@link IOperationExecutionEngine}.
	 * 
	 * @return
	 */
	public IOperationExecutionEngine getExecutionEngine() {
		if (executionEngine == null) {
			executionEngine = new OperationExecutionEngineImpl();
		}
		return executionEngine;
	}

	/**
	 * @param executionEngine
	 *            the execution engine to set
	 */
	public void setExecutionEngine(IOperationExecutionEngine executionEngine) {
		this.executionEngine = executionEngine;
	}

	/**
	 * Specifies whether the wizard should show the user input page.
	 * 
	 * @return <code>true</code> if it should show the user input page,
	 *         <code>false</code> if not.
	 */
	public boolean shouldShowUserInputPage() {
		return operationSpecification.getUserInputs().size() > 0;
	}

	/**
	 * Returns the value of the specified <code>userInput</code>.
	 * 
	 * @param userInput
	 *            {@link UserInput} to get value of.
	 * @return value of the specified <code>userInput</code>.
	 */
	public String getUserInputValue(UserInput userInput) {
		if (this.currentOperationBinding != null
				&& this.currentOperationBinding.getUserInputValues() != null) {
			Object object = this.currentOperationBinding.getUserInputValues()
					.get(userInput);
			if (object != null) {
				return object.toString();
			}
		}
		return "";
	}

	/**
	 * Sets the specified <code>value</code> for the specified
	 * <code>userInput</code>.
	 * 
	 * @param userInput
	 *            {@link UserInput} to set value for.
	 * @param value
	 *            value to set.
	 */
	public void setUserInputValue(UserInput userInput, String value) {
		this.currentOperationBinding.getUserInputValues().put(userInput, value);
	}

	/**
	 * Specifies whether a resource selection page should be shown.
	 * 
	 * @return if <code>true</code> show the page, if <code>false</code> don't.
	 */
	public boolean shouldShowResourceSelectionPage() {
		if (this.rootObject == null) {
			return true;
		} else {
			return false;
		}
	}
}
