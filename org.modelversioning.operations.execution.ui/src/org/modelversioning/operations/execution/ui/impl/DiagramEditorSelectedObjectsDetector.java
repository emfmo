/**
 * <copyright>
 *
 * Copyright (c) 2009 modelversioning.org
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * </copyright>
 */

package org.modelversioning.operations.execution.ui.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.modelversioning.operations.execution.ui.ISelectedObjectsDetector;

/**
 * Implements {@link ISelectedObjectsDetector} for {@link DiagramEditor}s.
 * 
 * @author <a href="mailto:langer@big.tuwien.ac.at">Philip Langer</a>
 * 
 */
public class DiagramEditorSelectedObjectsDetector implements
		ISelectedObjectsDetector {

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Set<EObject> getSelectedObjects(IEditorPart editor) {
		// guard wrong editors
		if (!(editor instanceof DiagramEditor)) {
			return Collections.emptySet();
		}

		// try to find selected objects
		ISelection selection = ((DiagramEditor) editor).getSite()
				.getSelectionProvider().getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			Iterator<Object> iterator = structuredSelection.iterator();
			Set<EObject> set = new HashSet<EObject>();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof IGraphicalEditPart) {
					IGraphicalEditPart iGraphicalEditPart = (IGraphicalEditPart) next;
					if (iGraphicalEditPart.resolveSemanticElement() != null) {
						set.add(iGraphicalEditPart.resolveSemanticElement());
					}
				}
			}
			return set;
		}
		return Collections.emptySet();
	}

}
